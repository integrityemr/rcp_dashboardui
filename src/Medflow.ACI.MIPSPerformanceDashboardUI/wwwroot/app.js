﻿'use strict';

// we will use this script file to inject the dependencies at module level

var aci = angular.module('aci.mipsPerformanceDashboard', [
        'ui.router',
        'ui.grid',
        'ui.grid.selection',
        'ui.grid.resizeColumns',
        'ngAnimate',
        'ngTouch',
        'ui.bootstrap',
        'configuration.constants',
        'aci.constants',
        'amc.constants',
        'amc.ACIMeasureconstants',
        'dashBoard.constants',
        'anr.ACIMeasureconstants',
        'practiceLevelDashBoard.constants',
        'groupLevelDashBoard.constants',
        'clinicianLevelDashBoard.constants',
        'configuration.datepicker',
        'aci.datepicker',
        'utilities.directives',
        'angularjs-dropdown-multiselect',
        'aci.utilities',
        'ngPrint',
        'ngDragDrop',
        'aci.modalService',
        'griuserconfiguration.constants',
        'anr.ACIMeasureconstants',
        'ngMaterial',
        'ngRealTimeExportconstants',
        'ngScheduledExportconstants',
       'amc.QualityDashboardConstants',
       'amc.QualityConfigurationConstants'
])

.directive('aciLoader', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (value) {
                if (value) {
                    element.removeClass('ng-hide');
                } else {
                    element.addClass('ng-hide');
                }
            });
        }
    };
}])
.config(['$httpProvider', function ($httpProvider) {
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.common = {};
    }
    $httpProvider.defaults.headers.common["Cache-Control"] = "no-cache";
    $httpProvider.defaults.headers.common.Pragma = "no-cache";
    $httpProvider.defaults.headers.common["If-Modified-Since"] = "0";
}])
.run(['$http',function($http) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('TokenValue');
    $http.defaults.headers.common['ProductKey'] = localStorage.getItem('ProductKey');
    $http.defaults.headers.common['PracticeProductKey'] = localStorage.getItem('ProductKey');
}]);
