(function (angular) {
    'use strict';

    var mod = angular.module('ngPrint', []);

    function printDirective() {
        var printSection = document.getElementById('printSection');

        function link(scope, element, attrs) {
            element.on('click', function () {
                var elemToPrint = document.getElementById(attrs.printElementId);
                if (elemToPrint) {
                    printElement(elemToPrint);
                    window.print();
                }
            });

            if (window.matchMedia) {
                var mediaQueryList = window.matchMedia('print');
                mediaQueryList.addListener(function (mql) {
                    if (!mql.matches) {
                        afterPrint();
                        scope.printingDone();
                    } else {
                        // before print (currently does nothing)
                    }
                });
            }

            window.onafterprint = afterPrint;
        }

        function afterPrint() {
            // clean the print section before adding new content
            printSection.innerHTML = '';
        }

        function printElement(elem) {
            // clones the element you want to print
            var domClone = elem.cloneNode(true);

            printSection = document.createElement("div");
            printSection.innerHTML = "";
            printSection.id = "printSection";
            document.body.appendChild(printSection);
        }

        return {
            link: link,
            restrict: 'A',
            scope: {
                printingDone: '&'
            }
        };
    }

    mod.directive('ngPrint', [printDirective]);
}(window.angular));
