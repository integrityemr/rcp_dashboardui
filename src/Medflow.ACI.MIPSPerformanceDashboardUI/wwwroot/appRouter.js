﻿angular.module('aci.mipsPerformanceDashboard');
aci.config(function ($stateProvider, $urlRouterProvider) {

    //$urlRouterProvider.otherwise('CPIAConfig');
    $urlRouterProvider.otherwise('DashBoard/Home');
    $stateProvider.state('Home', {
        url: '/Home',
        parent: 'DashBoard',
        title: 'Home',
        templateUrl: 'app/MIPSPerformanceDashboard/component/Home/home.tpl.html',
        controller: 'homeController as homeController'
    }),
    $stateProvider.state('anr', {
        url: '/ANR',
        parent: 'DashBoard',
        title: "Patient List",
        templateUrl: 'app/MIPSPerformanceDashboard/component/ANR/anr.tpl.html',
        controller: 'anrController as anrMeasure'
    }),
    $stateProvider.state('CPIAConfig', {
        url: '/CPIAConfig',
        parent: 'DashBoard',
        title: "Improvement Activities (IA) Configurations",
        templateUrl: 'app/MIPSPerformanceDashboard/component/CPIAConfig/CPIAConfiguration.tpl.html',
        controller: 'CPIAConfigController as cpiaConfig'
    }),
    $stateProvider.state('CPIAPrint', {
        title: "IA Print",
        parent: 'DashBoard',
        templateUrl: 'app/MIPSPerformanceDashboard/component/CPIAConfigPrint/cpiaPrintActivities.tpl.html',
        controller: 'cpiaPrintActivitiesController as cpiaPrintCtrl'
    }),
    $stateProvider.state('practiceInformation', {
        url: '/practiceInformation',
        parent: 'DashBoard',
        title: "Practice Information",
        templateUrl: 'app/MIPSPerformanceDashboard/component/practiceInformation/practiceInformation.tpl.html',
        controller: 'practiceInformationController as practiceInfo'
    }),
    $stateProvider.state('ACIConfig', {
        url: '/ACIConfig',
        parent: 'DashBoard',
        title: "Promoting Interoperability(PI) Configuration",
        templateUrl: 'app/MIPSPerformanceDashboard/component/ACIConfig/ACIConfiguration.tpl.html',
        controller: 'ACIConfigController as aciConfig'
    }),
        $stateProvider.state('GriUser', {
            url: '/GriUser',
            parent: 'DashBoard',
            title: "GRI List",
            templateUrl: 'app/MIPSPerformanceDashboard/component/GRIUser/griUser.tpl.html',
            controller: 'griUserController as griUser'
        }),
    $stateProvider.state('mipsDashBoard', {
        url: '/mipsDashBoard',
        parent: 'DashBoard',
        title: "MIPS PI-Measure Report",
        templateUrl: 'app/MIPSPerformanceDashboard/component/MipsDashBoard/mipsDashBoard.tpl.html',
        controller: 'mipsDashBoardController as dashBoard'
    }),

    //Practice Level DashBoard
    $stateProvider.state('practiceLevelDashBoard', {
        url: '/practiceLevelDashBoard',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Practice Level",
        templateUrl: 'app/MIPSPerformanceDashboard/component/mipsPerformanceDashBoard/practiceLevelDashBoard/practiceLevelDashBoard.tpl.html',
        controller: 'practiceLevelController as practiceLevel'
    }),

    //Group Level DashBoard
    $stateProvider.state('groupLevelDashBoard', {
        url: '/groupLevelDashBoard',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Group Level",
        templateUrl: 'app/MIPSPerformanceDashboard/component/mipsPerformanceDashBoard/groupLevelDashBoard/groupLevelDashBoard.tpl.html',
        controller: 'groupLevelController as groupLevel'
    }),
    //Clinician Level DashBoard
    $stateProvider.state('clinicianLevelDashBoard', {
        url: '/clinicianLevelDashBoard',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Individual Level",
        templateUrl: 'app/MIPSPerformanceDashboard/component/mipsPerformanceDashBoard/clinicianLevelDashBoard/clinicianLevelDashBoard.tpl.html',
        controller: 'clinicianLevelController as clinicianLevel'
    }),

    //Group Level CPIA Activities
    $stateProvider.state('groupCPIAActivities', {
        url: '/groupCPIAActivities',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Improvement Activities",
        templateUrl: 'app/MIPSPerformanceDashboard/component/improvementActivities/groupCPIAActivities.tpl.html',
        controller: 'groupCPIAController as groupCPIA'
    }),

    //Clinician Level CPIA Activities
    $stateProvider.state('individualCPIAActivities', {
        url: '/individualCPIAActivities',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Improvement Activities",
        templateUrl: 'app/MIPSPerformanceDashboard/component/improvementActivities/individualCPIAActivities.tpl.html',
        controller: 'individualCPIAController as individualCPIA'
    }),

    //Clinician Group Level CPIA Activities
    $stateProvider.state('groupIndividualCPIAActivities', {
        url: '/groupIndividualCPIAActivities',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Improvement Activities",
        templateUrl: 'app/MIPSPerformanceDashboard/component/improvementActivities/groupIndividualCPIAActivities.tpl.html',
        controller: 'groupIndividualCPIAController as groupIndividualCPIA'
    }),

    //Independent Level CPIA Activities
    $stateProvider.state('independentCPIAActivities', {
        url: '/independentCPIAActivities',
        parent: 'DashBoard',
        title: "MIPS Performance Dashboard - Improvement Activities",
        templateUrl: 'app/MIPSPerformanceDashboard/component/improvementActivities/independentCPIAActivities.tpl.html',
        controller: 'independentCPIAController as independentCPIA'
    }),

    //ACI Measure Report - Access this widget from DashBoard 
    $stateProvider.state('amcMeasureReport', {
        url: '/amcMeasureReport',
        parent: 'DashBoard',
        title: "MIPS PI-Measure Report",
        templateUrl: 'app/MIPSPerformanceDashboard/component/AMC/amcMeasureReport.tpl.html',
        controller: 'amcMeasureReportController as amcMeasure'
    }),

    //Meaningful Use Measure Report
    $stateProvider.state('muMeasureReport', {
        url: '/muMeasureReport',
        parent: 'DashBoard',
        title: "Meaningful Use-Measure Report",
        templateUrl: 'app/MIPSPerformanceDashboard/component/MUMeasure/muMeasureReport.tpl.html',
        controller: 'muMeasureReportController as muMeasure'
    }),
     $stateProvider.state('aciMeasurePrint', {
         title: "PI Measure Print",
         parent: 'DashBoard',
         templateUrl: 'app/MIPSPerformanceDashboard/component/AMCPrint/aciMeasurePrint.tpl.html',
         controller: 'aciMeasurePrintController as amcMeasurePrint'
     }),
    $stateProvider.state('muMeasurePrint', {
        title: "MU Measure Print",
        parent: 'DashBoard',
        templateUrl: 'app/MIPSPerformanceDashboard/component/mumeasureprint/mumeasureprint.tpl.html',
        controller: 'muMeasurePrintController as muMeasurePrint'
    }),
     $stateProvider.state('practiceLevelDashBoardPrint', {
         title: "Practice Level DashBoard Print",
         parent: 'DashBoard',
         templateUrl: 'app/MIPSPerformanceDashboard/component/mipsperformancedashboard/practiceleveldashboard/practiceleveldashboardprint.tpl.html',
         controller: 'practiceLevelPrintController as practiceLevelPrint'
     }),
     $stateProvider.state('clinicianLevelDashBoardPrint', {
         title: "Individual Level DashBoard Print",
         parent: 'DashBoard',
         templateUrl: 'app/MIPSPerformanceDashboard/component/mipsperformancedashboard/clinicianLeveldashboard/clinicianLevelDashBoardprint.tpl.html',
         controller: 'clinicianLevelPrintController as clinicianLevelPrint'
     }),
    $stateProvider.state('individualLevelCPIAPrint', {
        title: "Individual Level IA DashBoard Print",
        parent: 'DashBoard',
        templateUrl: 'app/mipsperformancedashboard/component/improvementactivities/individualcpiaactivitiesprint.tpl.html',
        controller: 'individualCPIAPrintController as individualLevelCPIAPrint'
    }),
     $stateProvider.state('groupIndividualCPIAPrint', {
         title: "Group Individual IA DashBoard Print",
         parent: 'DashBoard',
         templateUrl: 'app/mipsperformancedashboard/component/improvementactivities/groupindividualcpiaactivitiesprint.tpl.html',
         controller: 'groupIndividualCPIAPrintController as groupIndividualCPIAPrint'
     }),
    $stateProvider.state('groupLevelDashBoardPrint', {
        title: "Group Level DashBoard Print",
        parent: 'DashBoard',
        templateUrl: 'app/MIPSPerformanceDashboard/component/mipsperformancedashboard/groupLeveldashboard/groupLevelDashBoardprint.tpl.html',
        controller: 'groupLevelPrintController as groupLevelPrint'

    }),
    $stateProvider.state('groupLevelCPIAPrint', {
        title: "Group Level IA DashBoard Print",
        parent: 'DashBoard',
        templateUrl: 'app/MIPSPerformanceDashboard/component/improvementActivities/groupCPIAActivitiesPrint.tpl.html',
        controller: 'groupCPIAPrintController as groupLevelCPIAPrint'

    }),
    $stateProvider.state('logout', {
        url: '/logout',
        parent: 'DashBoard',
        title: "logout",
        templateUrl: 'app/MIPSPerformanceDashboard/component/logout/logout.tpl.html',
        controller: 'logoutController as logout'
    }),
     $stateProvider.state('DashBoard', {
         url: '/DashBoard',
         title: "DashBoard",
         templateUrl: 'app/mipsperformancedashboard/component/dashboardhome/dashboardhome.tpl.html',
         controller: 'dashboardHomeController as dhPage'
     })
    ,
    $stateProvider.state('RealTimeExport', {
        url: '/RealTimeExport',
        parent: 'DashBoard',
        title: "RealTime Data Export",
        templateUrl: 'app/mipsperformancedashboard/component/RealTimeDataExport/RealTimeExport.tpl.html',
        controller: 'RealTimeExportController as dataExportCtrl'

    })
    ,
    $stateProvider.state('qppExport', {
        url: '/qppExport',
        parent: 'DashBoard',
        title: "QPP Export",
        templateUrl: 'app/mipsperformancedashboard/component/QPP/qppExport.html',
        controller: 'qppExportController as qppExportCtrl'

    })
    ,
    $stateProvider.state('ScheduledExport', {
        url: '/ScheduledExport',
        parent: 'DashBoard',
        title: "Scheduled Data Export",
        templateUrl: 'app/MIPSPerformanceDashboard/component/ScheduledDataExport/ScheduledDataExport.tpl.html',
        controller: 'ScheduledDataExportController as schedulerConfigCtrl'
    }),
     $stateProvider.state('qualitymeasureDashBoardNav', {
         url: '/qualitymeasureDashBoardNav',
         parent: 'DashBoard',
         title: "Quality Measure DashBoard",
         templateUrl: 'app/mipsperformancedashboard/component/QualityMeasureDashboardNav/QualityMeasureDashboardNav.html',
         controller: 'QualityMeasureDashboardNavController as QualityMeasureDashboardNavCtrl'
     }),
    $stateProvider.state('qualityMeasuredashboard', {
        url: '/qualityMeasuredashboard',
        parent: 'DashBoard',
        title: "Quality Measure Dashboard",
        templateUrl: 'app/mipsperformancedashboard/component/qualitymeasuredashboard/qualitymeasuredashboard.html',
        controller: 'qualityMeasureDashboardController as qualitydashboardCtrl'

    })
     ,
     $stateProvider.state('qualityMeasureConfig', {
         url: '/qualityMeasureConfig',
         parent: 'DashBoard',
         title: "Quality Measure Configuration",
         templateUrl: 'app/mipsperformancedashboard/component/QualityConfiguration/qualityConfiguration.html',
         controller: 'qualityConfigurationController as qualityConfig'

     })
}).run(['$state', '$rootScope', function ($state, $rootScope) {
    $rootScope.$on('$stateChangeStart', function (e, toState) {
        $rootScope.headerName = toState.title;
        if (toState.title == "Home") {
            $rootScope.newheaderName = "Regulatory Compliance Platform";
        }
        else if (toState.title == "MIPS Performance Dashboard - Improvement Activities") {
            $rootScope.newheaderName = " Improvement Activities(IA) Report";
        }
        else if (toState.title == "MIPS PI-Measure Report") {
            $rootScope.newheaderName = "Promoting Interoperability(PI) Measure Report";
        }
        else if (toState.title == "RealTime Data Export") {
            $rootScope.newheaderName = "Data Export";
        }
        else {
            $rootScope.newheaderName = toState.title;
        }


        if (localStorage.getItem("TokenValue") == null && (toState.title != "logout" && toState.title != "Home")) {
            $rootScope.headerName = 'logout';
            $state.go('logout');
            e.preventDefault();
        }
    });
}]);
