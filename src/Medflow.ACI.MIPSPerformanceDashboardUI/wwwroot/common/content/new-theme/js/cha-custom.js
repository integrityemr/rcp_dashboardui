/**
 *	Skua Main
 *
 *	Design by: www.design7.in
 **/

var public_vars = public_vars || {};

; (function ($, window, undefined) {

	"use strict";

	$(document).ready(function () {
		// Main Vars
		public_vars.$body = $("body");
		public_vars.$pageContainer = public_vars.$body.find(".page-container");
		public_vars.$chat = public_vars.$pageContainer.find("#chat");
		public_vars.$sidebarMenu = public_vars.$pageContainer.find('.sidebar-menu');
		public_vars.$mainMenu = public_vars.$sidebarMenu.find('.main-menu');

		public_vars.$horizontalNavbar = public_vars.$body.find('.navbar.horizontal-menu');
		public_vars.$horizontalMenu = public_vars.$horizontalNavbar.find('.navbar-nav');

		public_vars.$mainContent = public_vars.$pageContainer.find('.main-content');
		public_vars.$mainFooter = public_vars.$body.find('footer.main-footer');

		public_vars.$userInfoMenuHor = public_vars.$body.find('.navbar.horizontal-menu');
		public_vars.$userInfoMenu = public_vars.$body.find('nav.navbar.user-info-navbar');

		public_vars.$settingsPane = public_vars.$body.find('.settings-pane');
		public_vars.$settingsPaneIn = public_vars.$settingsPane.find('.settings-pane-inner');

		public_vars.wheelPropagation = true; // used in Main menu (sidebar)

		public_vars.$pageLoadingOverlay = public_vars.$body.find('.page-loading-overlay');

		public_vars.defaultColorsPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#4fcdfc', '#00b19d', '#ff6264', '#f7aa47'];



		// Page Loading Overlay
		if (public_vars.$pageLoadingOverlay.length) {
			$(window).load(function () {
				public_vars.$pageLoadingOverlay.addClass('loaded');
			});
		}

		window.onerror = function () {
			// failsafe remove loading overlay
			public_vars.$pageLoadingOverlay.addClass('loaded');
		}


		// Setup Sidebar Menu
		setup_sidebar_menu();


		// Setup Horizontal Menu
		setup_horizontal_menu();


		// Sticky Footer
		if (public_vars.$mainFooter.hasClass('sticky')) {
			stickFooterToBottom();
			$(window).on('cha.resized', stickFooterToBottom);
		}


		// Perfect Scrollbar
		if ($.isFunction($.fn.perfectScrollbar)) {
			if (public_vars.$sidebarMenu.hasClass('fixed'))
				ps_init();

			$(".ps-scrollbar").each(function (i, el) {
				var $el = $(el);

				$el.perfectScrollbar({
					wheelPropagation: false
				});
			});


			// Chat Scrollbar
			var $chat_inner = public_vars.$pageContainer.find('#chat .chat-inner');

			if ($chat_inner.parent().hasClass('fixed'))
				$chat_inner.css({ maxHeight: $(window).height() }).perfectScrollbar();


			// User info opening dropdown trigger PS update
			$(".user-info-navbar .dropdown:has(.ps-scrollbar)").each(function (i, el) {
				var $scrollbar = $(this).find('.ps-scrollbar');

				$(this).on('click', '[data-toggle="dropdown"]', function (ev) {
					ev.preventDefault();

					setTimeout(function () {
						$scrollbar.perfectScrollbar('update');
					}, 1);
				});
			});


			// Scrollable
			$("div.scrollable").each(function (i, el) {
				var $this = $(el),
					max_height = parseInt(attrDefault($this, 'max-height', 200), 10);

				max_height = max_height < 0 ? 200 : max_height;

				$this.css({ maxHeight: max_height }).perfectScrollbar({
					wheelPropagation: true
				});
			});
		}


		// User info search button
		var $uim_search_form = $(".user-info-menu .search-form, .nav.navbar-right .search-form");

		$uim_search_form.each(function (i, el) {
			var $uim_search_input = $(el).find('.form-control');

			$(el).on('click', '.btn', function (ev) {
				if ($uim_search_input.val().trim().length == 0) {
					jQuery(el).addClass('focused');
					setTimeout(function () { $uim_search_input.focus(); }, 100);
					return false;
				}
			});

			$uim_search_input.on('blur', function () {
				jQuery(el).removeClass('focused');
			});
		});











		// Auto hidden breadcrumbs
		$(".breadcrumb.auto-hidden").each(function (i, el) {
			var $bc = $(el),
				$as = $bc.find('li a'),
				collapsed_width = $as.width(),
				expanded_width = 0;

			$as.each(function (i, el) {
				var $a = $(el);

				expanded_width = $a.outerWidth(true);
				$a.addClass('collapsed').width(expanded_width);

				$a.hover(function () {
					$a.removeClass('collapsed');
				},
					function () {
						$a.addClass('collapsed');
					});
			});
		});



		// Close Modal on Escape Keydown
		$(window).on('keydown', function (ev) {
			// Escape
			if (ev.keyCode == 27) {
				// Close opened modal
				if (public_vars.$body.hasClass('modal-open'))
					$(".modal-open .modal:visible").modal('hide');
			}
		});


		// Minimal Addon focus interaction
		$(".input-group.input-group-minimal:has(.form-control)").each(function (i, el) {
			var $this = $(el),
				$fc = $this.find('.form-control');

			$fc.on('focus', function () {
				$this.addClass('focused');
			}).on('blur', function () {
				$this.removeClass('focused');
			});
		});



		// Spinner
		$(".input-group.spinner").each(function (i, el) {
			var $ig = $(el),
				$dec = $ig.find('[data-type="decrement"]'),
				$inc = $ig.find('[data-type="increment"]'),
				$inp = $ig.find('.form-control'),

				step = attrDefault($ig, 'step', 1),
				min = attrDefault($ig, 'min', 0),
				max = attrDefault($ig, 'max', 0),
				umm = min < max;


			$dec.on('click', function (ev) {
				ev.preventDefault();

				var num = new Number($inp.val()) - step;

				if (umm && num <= min) {
					num = min;
				}

				$inp.val(num);
			});

			$inc.on('click', function (ev) {
				ev.preventDefault();

				var num = new Number($inp.val()) + step;

				if (umm && num >= max) {
					num = max;
				}

				$inp.val(num);
			});
		});




		// Select2 Dropdown replacement
		if ($.isFunction($.fn.select2)) {
			$(".select2").each(function (i, el) {
				var $this = $(el),
					opts = {
						allowClear: attrDefault($this, 'allowClear', false)
					};

				$this.select2(opts);
				$this.addClass('visible');

				//$this.select2("open");
			});


			if ($.isFunction($.fn.niceScroll)) {
				$(".select2-results").niceScroll({
					cursorcolor: '#d4d4d4',
					cursorborder: '1px solid #ccc',
					railpadding: { right: 3 }
				});
			}
		}





	});


	// Enable/Disable Resizable Event
	var wid = 0;

	$(window).resize(function () {
		clearTimeout(wid);
		wid = setTimeout(trigger_resizable, 200);
	});



	// Dropdown Script
	var options = [];

	$('.dropdown-menu li').on('click', function (event) {
		//alert("hello");
		var $target = $(event.currentTarget),
			val = $target.attr('data-value'),
			$inp = $target.find('input'),
			idx;
		if ((idx = options.indexOf(val)) > -1) {
			options.splice(idx, 1);
			setTimeout(function () { $inp.prop('checked', false) }, 0);
		} else {
			options.push(val);
			setTimeout(function () { $inp.prop('checked', true) }, 0);
		}
		$(event.target).blur();
		console.log(options);
		return false;
	});




	$('.dropdown-menu li').on('click', function (event) {

		var seletedLiId = this.id;

		$('.dropdown-menu li').each(function (i) {

			var value = $(this).attr('id');
			if(value!=seletedLiId){
				$('input[name='+value+']').attr('checked', false);
			}
			
		});

		var checkedValues = [];
		var seletedLiId = this.id;

		$.each($("input[name='" + seletedLiId + "']:checked"), function () {
			checkedValues.push($(this).val());
		});
	//	alert("checked values are: " + checkedValues.join(", "));
	});




})(jQuery, window);



// Sideber Menu Setup function
var sm_duration = .01,
	sm_transition_delay = 150;

function setup_sidebar_menu() {
	if (public_vars.$sidebarMenu.length) {
		var $items_with_subs = public_vars.$sidebarMenu.find('li:has(> .submenu ul)'),
			toggle_others = public_vars.$sidebarMenu.hasClass('toggle-others');

		$items_with_subs.filter('.active').addClass('expanded');

		$items_with_subs.each(function (i, el) {
			var $li = jQuery(el),
				$a = $li.children('a'),
				$sub = $li.children('.submenu');

			$li.addClass('has-sub');

			$a.on('click', function (ev) {
				ev.preventDefault();

				if (toggle_others) {
					sidebar_menu_close_items_siblings($li);
				}

				if ($li.hasClass('expanded') || $li.hasClass('opened'))
					sidebar_menu_item_collapse($li, $sub);
				else
					sidebar_menu_item_expand($li, $sub);
			});
		});
	}
}

function sidebar_menu_item_expand($li, $sub) {
	if ($li.data('is-busy') || ($li.parent('.main-menu').length && public_vars.$sidebarMenu.hasClass('collapsed')))
		return;

	$li.addClass('expanded').data('is-busy', true);
	$sub.show();

	var $sub_items = $sub.children(),
		sub_height = $sub.outerHeight(),

		win_y = jQuery(window).height(),
		total_height = $li.outerHeight(),
		current_y = public_vars.$sidebarMenu.scrollTop(),
		item_max_y = $li.position().top + current_y,
		fit_to_viewpport = public_vars.$sidebarMenu.hasClass('fit-in-viewport');

	$sub_items.addClass('is-hidden');
	$sub.height(0);


	TweenMax.to($sub, sm_duration, {
		css: { height: sub_height }, onUpdate: ps_update, onComplete: function () {
			$sub.height('');
		}
	});

	var interval_1 = $li.data('sub_i_1'),
		interval_2 = $li.data('sub_i_2');

	window.clearTimeout(interval_1);

	interval_1 = setTimeout(function () {
		$sub_items.each(function (i, el) {
			var $sub_item = jQuery(el);

			$sub_item.addClass('is-shown');
		});

		var finish_on = sm_transition_delay * $sub_items.length,
			t_duration = parseFloat($sub_items.eq(0).css('transition-duration')),
			t_delay = parseFloat($sub_items.last().css('transition-delay'));

		if (t_duration && t_delay) {
			finish_on = (t_duration + t_delay) * 1000;
		}

		// In the end
		window.clearTimeout(interval_2);

		interval_2 = setTimeout(function () {
			$sub_items.removeClass('is-hidden is-shown');

		}, finish_on);


		$li.data('is-busy', false);

	}, 0);

	$li.data('sub_i_1', interval_1),
		$li.data('sub_i_2', interval_2);
}

function sidebar_menu_item_collapse($li, $sub) {
	if ($li.data('is-busy'))
		return;

	var $sub_items = $sub.children();

	$li.removeClass('expanded').data('is-busy', true);
	$sub_items.addClass('hidden-item');

	TweenMax.to($sub, sm_duration, {
		css: { height: 0 }, onUpdate: ps_update, onComplete: function () {
			$li.data('is-busy', false).removeClass('opened');

			$sub.attr('style', '').hide();
			$sub_items.removeClass('hidden-item');

			$li.find('li.expanded ul').attr('style', '').hide().parent().removeClass('expanded');

			ps_update(true);
		}
	});
}

function sidebar_menu_close_items_siblings($li) {
	$li.siblings().not($li).filter('.expanded, .opened').each(function (i, el) {
		var $_li = jQuery(el),
			$_sub = $_li.children('.submenu');

		sidebar_menu_item_collapse($_li, $_sub);
	});
}


// Horizontal Menu
function setup_horizontal_menu() {
	if (public_vars.$horizontalMenu.length) {
		var $items_with_subs = public_vars.$horizontalMenu.find('li:has(> .submenu ul)'),
			click_to_expand = public_vars.$horizontalMenu.hasClass('click-to-expand');

		if (click_to_expand) {
			public_vars.$mainContent.add(public_vars.$sidebarMenu).on('click', function (ev) {
				$items_with_subs.removeClass('hover');
			});
		}

		$items_with_subs.each(function (i, el) {
			var $li = jQuery(el),
				$a = $li.children('a'),
				$sub = $li.children('.submenu'),
				is_root_element = $li.parent().is('.navbar-nav');

			$li.addClass('has-sub');

			// Mobile Only
			$a.on('click', function (ev) {
			//	if (isxs()) {
					ev.preventDefault();

					// Automatically will toggle other menu items in mobile view
					if (true) {
						sidebar_menu_close_items_siblings($li);
					}

					if ($li.hasClass('expanded') || $li.hasClass('opened'))
						sidebar_menu_item_collapse($li, $sub);
					else
						sidebar_menu_item_expand($li, $sub);
			//	}
			});

			// Click To Expand
			if (click_to_expand) {
				$a.on('click', function (ev) {
					ev.preventDefault();

					if (isxs())
						return;

					// For parents only
					if (is_root_element) {
						$items_with_subs.filter(function (i, el) { return jQuery(el).parent().is('.navbar-nav'); }).not($li).removeClass('hover');
						$li.toggleClass('hover');
					}
					// Sub menus
					else {
						var sub_height;

						// To Expand
						if ($li.hasClass('expanded') == false) {
							$li.addClass('expanded');
							$sub.addClass('is-visible');

							sub_height = $sub.outerHeight();

							$sub.height(0);

							TweenLite.to($sub, .15, { css: { height: sub_height }, ease: Sine.easeInOut, onComplete: function () { $sub.attr('style', ''); } });

							// Hide Existing in the list
							$li.siblings().find('> ul.is-visible').not($sub).each(function (i, el) {
								var $el = jQuery(el);

								sub_height = $el.outerHeight();

								$el.removeClass('is-visible').height(sub_height);
								$el.parent().removeClass('expanded');

								TweenLite.to($el, .15, { css: { height: 0 }, onComplete: function () { $el.attr('style', ''); } });
							});
						}
						// To Collapse
						else {
							sub_height = $sub.outerHeight();

							$li.removeClass('expanded');
							$sub.removeClass('is-visible').height(sub_height);
							TweenLite.to($sub, .15, { css: { height: 0 }, onComplete: function () { $sub.attr('style', ''); } });
						}
					}
				});
			}
			// Hover To Expand
			else {
				// $li.hoverIntent({
				// 	over: function()
				// 	{
				// 		if(isxs())
				// 			return;

				// 		if(is_root_element)
				// 		{
				// 			$li.addClass('hover');
				// 		}
				// 		else
				// 		{
				// 			$sub.addClass('is-visible');
				// 			sub_height = $sub.outerHeight();

				// 			$sub.height(0);

				// 			TweenLite.to($sub, .25, {css: {height: sub_height}, ease: Sine.easeInOut, onComplete: function(){ $sub.attr('style', ''); }});
				// 		}
				// 	},
				// 	out: function()
				// 	{
				// 		if(isxs())
				// 			return;

				// 		if(is_root_element)
				// 		{
				// 			$li.removeClass('hover');
				// 		}
				// 		else
				// 		{
				// 			sub_height = $sub.outerHeight();

				// 			$li.removeClass('expanded');
				// 			$sub.removeClass('is-visible').height(sub_height);
				// 			TweenLite.to($sub, .25, {css: {height: 0}, onComplete: function(){ $sub.attr('style', ''); }});
				// 		}
				// 	},
				// 	timeout: 200,
				// 	interval: is_root_element ? 10 : 100
				// });
			}
		});
	}
}


function ps_update(destroy_init) {
	if (isxs())
		return;

	if (jQuery.isFunction(jQuery.fn.perfectScrollbar)) {
		if (public_vars.$sidebarMenu.hasClass('collapsed')) {
			return;
		}

		public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('update');

		if (destroy_init) {
			ps_destroy();
			ps_init();
		}

	}
}


function ps_init() {
	if (isxs())
		return;

	if (jQuery.isFunction(jQuery.fn.perfectScrollbar)) {
		if (public_vars.$sidebarMenu.hasClass('collapsed') || !public_vars.$sidebarMenu.hasClass('fixed')) {
			return;
		}

		public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar({
			wheelSpeed: 2,
			wheelPropagation: public_vars.wheelPropagation
		});
	}
}

function ps_destroy() {
	if (jQuery.isFunction(jQuery.fn.perfectScrollbar)) {
		public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('destroy');
	}
}



