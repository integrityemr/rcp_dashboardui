﻿angular.module('aci.datepicker', []).directive('aciDatepicker', function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            selectedDate: '=ngModel',
            isDisabled: "=ngDisabled",
            dateChanged: '&',
            enable: "=enable",
            minDate: '=minDate',
            maxDate: '=maxDate',
            isRequired: '=ngRequired',
            formName: '=formName',
            inputName: '@',
            fromSubmmited: "=fromSubmmited"
        },
        templateUrl: 'common/directives/aciDatePicker.tpl.html',
        transclude: true,
        link: function ($scope, elem, attr, ctrl) {
            $scope.$on('$destroy', function () {
                elem.remove();
            });
        },
        controller: function ($scope, $timeout, $filter) {
            $scope.lostFocus = false;
            $scope.today = function () {
                $scope.input = $scope.selectedDate === null ? null : new Date($scope.selectedDate);
            };
            $scope.today();
            $scope.clear = function () {
                $scope.selectedDate = null;
            };
            $scope.inlineOptions = {
                minDate: $scope.minDate,
                showWeeks: true
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                maxDate: $scope.maxDate,
                minDate: $scope.minDate,
                startingDay: 1
            };
            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                  mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.$watch('minDate', function () {
                $scope.dateOptions.minDate = $scope.minDate;
            });

            $scope.changed = function () {
                $scope.selectedDate = $scope.input;
                $timeout(function () {
                    $scope.dateChanged();
                });
            }

            $scope.open = function ($event) {
                //$event.preventDefault();
                //$event.stopPropagation();
                $scope.status.opened = true;
            };
            $scope.setDate = function (year, month, day) {
                $scope.selectedDate = new Date(year, month, day);
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
            $scope.format = $scope.formats[4];
            $scope.status = {
                opened: false
            };

            // For android, keycode are the same except for numbers, which are all equal to 229
            var isAndroid = navigator.userAgent.match(/(Android)/i);
            $scope.filterDateValue = function (event) {
                $scope.disablePopup(event);

                if ((event.which >= 48 && event.which <= 57) || (isAndroid && event.which === 229)) {
                    return true; // numbers
                } else if (event.which >= 96 && event.which <= 105) {
                    return true; // numpad number
                } else if ([8, 9, 46, 37, 38, 39, 40, 111, 191].indexOf(event.which) > -1) {
                    return true; // backspace, tab, del, arrows, forward slash
                } else {
                    event.preventDefault();
                    return false;
                }
            };

            $scope.disablePopup = function (event) {
                if (event.keyCode === 40) { //down arrow, previously activated dropdown
                    event.preventDefault();
                    event.stopPropagation();
                    event.bubbles = false;
                    return false;
                }
            };

            $scope.autoFormatDate = function ($event) {
                if ($event.keyCode === 8 || $event.keyCode === 46 || $event.keyCode === 9) { //backspace or delete key or tab
                    return;
                }

                if ($event.keyCode === 37) { //left arrow
                    var ctl = $event.target;
                    var currentValue = ctl.value;
                    var startPos = ctl.selectionStart;
                    if (startPos === 2 && currentValue.length >= 5 && currentValue[2] === "/" && currentValue[4] === "/") {
                        returnFomatedDate(currentValue.substr(0, 3) + "0" + currentValue.substr(3, 10));
                    }
                    return;
                }

                if ($event.keyCode === 39) { //right arrow
                    var ctl = $event.target;
                    var currentValue = ctl.value;
                    var startPos = ctl.selectionStart;
                    if (startPos === 2 && currentValue.length >= 2 && currentValue[1] === "/") {
                        returnFomatedDate("0" + currentValue.substr(0, 10));
                    } else if (startPos === 5 && currentValue.length >= 5 && currentValue[2] === "/" && currentValue[4] === "/") {
                        returnFomatedDate(currentValue.substr(0, 3) + "0" + currentValue.substr(3, 10));
                    }
                    return;
                }

                var isAndroid = navigator.userAgent.match(/(Android)/i);
                var currentValue = $event.target.value;
                var newValue;
                if (($event.keyCode >= 48 && $event.keyCode <= 57) || ($event.keyCode >= 96 && $event.keyCode <= 105) || (isAndroid && event.which === 229) || ($event.keyCode === 191)) { //only numeric numbers
                    var ctl = $event.target;
                    var startPos = ctl.selectionStart;
                    if (currentValue.length > startPos && currentValue[startPos] === "/") { //if next char is '/'
                        return;
                    }
                    newValue = autoFormatDate(currentValue); // "123" will be changed to "12/3"
                } else if ($event.keyCode === 111 || $event.keyCode === 191) {
                    newValue = paddifyZero(currentValue); // "4/" will be changed to "04/"
                } else { // type any other characters would cause return
                    return;
                }
                $event.target.value = newValue;
                returnFomatedDate(newValue);
            };

            var returnFomatedDate = function (value) {
                if ($scope.showCalender === 'true') {
                    if (value.length === 10) {
                        $scope.input = new Date(value);
                        if ($scope.input != null) {
                            $scope.selectedDate = $scope.input;
                        }
                    }
                } else {
                    var valid = $scope.isDateValid(value, '/');
                    if (!valid || value.length !== 10) return false;
                    //$timeout(function () {
                    $scope.input = new Date(value);
                    if ($scope.input != null) {
                        $scope.selectedDate = new Date(value);
                        $scope.dateChanged({ changedDate: $scope.selectedDate });
                    }
                    //});

                }
            };

            var autoFormatDate = function (currentValue) {
                if ((currentValue.length === 2 && currentValue[1] === "/") || (currentValue.length === 5 && currentValue[4] === "/")) {
                    return paddifyZero(currentValue);
                }

                if ((currentValue.length === 3 && currentValue[2] === "/") || (currentValue.length === 6 && currentValue[5] === "/")) {
                    return currentValue;
                }
                currentValue = currentValue.replace(/[^0-9]/g, "");
                var newValue = currentValue;
                if (currentValue.length >= 3) newValue = currentValue.substr(0, 2) + "/" + currentValue.substr(2, 2);
                if (currentValue.length >= 5) newValue += "/" + currentValue.substr(4, 4);
                return newValue;
            };

            var paddifyZero = function (currentValue) {
                if (currentValue.length === 2 || (currentValue.length === 3 && currentValue[1] === "/")) {
                    return "0" + currentValue;
                }
                if (currentValue.length === 5) {
                    return currentValue.substr(0, 3) + "0" + currentValue.substr(3, 2);
                }
                if (currentValue.length === 6 && currentValue[4] === "/") {
                    return currentValue.substr(0, 3) + "0" + currentValue.substr(3, 3);
                }
                return currentValue;
            };

            $scope.loseFocus = function (event) {
                $scope.lostFocus = true;
                if (event != undefined) {
                    //kept condition for excluding calling of 'datechanged()' event when calendar pops-up in Start/End Dates field (Blur event).
                    var baseURI = event.currentTarget.baseURI
                    if (!baseURI.includes("practiceLevelDashBoard") && !baseURI.includes("groupLevelDashBoard") && !baseURI.includes("clinicianLevelDashBoard"))
                        returnFomatedDate(event.target.value);
                }
            };

            $scope.getFocus = function () {
                $scope.lostFocus = false;
            };

            $scope.$watch('selectedDate', function (newValue, oldValue) {
                if ((new Date(newValue).toLocaleDateString() !== new Date(oldValue).toLocaleDateString())) {
                    $scope.input = $scope.selectedDate;
                }
            });

            $scope.isDateValid = function (date, seperator) {
                if (date === "") return true;
                var dateArray = date.split(seperator);
                var y = dateArray[2], m = dateArray[0], d = dateArray[1];
                var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if ((!(y % 4) && y % 100) || !(y % 400)) {
                    daysInMonth[1] = 29;
                }
                return d <= daysInMonth[--m];
            }
        }
    };
});
