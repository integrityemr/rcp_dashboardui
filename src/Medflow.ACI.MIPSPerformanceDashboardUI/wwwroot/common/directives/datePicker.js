﻿angular.module('configuration.datepicker', []).directive('myDatepicker', function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            selectedDate: '=ngModel',
            dateChanged: '&',
            enable: "=enable",
            minDate: '=minDate',
            maxDate: '=maxDate',
            isRequired: '=ngRequired'
        },
        templateUrl: 'common/directives/mydatepicker.html',
        transclude: true,
        link: function ($scope, elem, attr, ctrl) {
            $scope.$on('$destroy', function () {
                elem.remove();
            });
        },
        controller: function ($scope, $timeout, $filter) {
            $scope.today = function () {
                $scope.selectedDate = $scope.selectedDate === null ? null : new Date($scope.selectedDate);
            };
            $scope.today();
            $scope.clear = function () {
                $scope.selectedDate = null;
            };
            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: $scope.minDate,
                showWeeks: true
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                maxDate: $scope.maxDate,
                minDate: $scope.minDate,
                startingDay: 1
            };
            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                  mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.$watch('minDate', function () {
                $scope.dateOptions.minDate = $scope.minDate;
            });

            $scope.changed = function () {
                $timeout(function () {
                    $scope.dateChanged();                    
                });
            }

            $scope.open = function ($event) {
                //$event.preventDefault();
                //$event.stopPropagation();
                $scope.status.opened = true;
            };
            $scope.setDate = function (year, month, day) {
                $scope.selectedDate = new Date(year, month, day);
            };
            
            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'MM/dd/yyyy'];
            $scope.format = $scope.formats[4];
            $scope.status = {
                opened: false
            };
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
              {
                  date: tomorrow,
                  status: 'full'
              },
              {
                  date: afterTomorrow,
                  status: 'partially'
              }
            ];

            function getDayClass(data) {
                var date = data.date,
                  mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            }
        }
    };
});
