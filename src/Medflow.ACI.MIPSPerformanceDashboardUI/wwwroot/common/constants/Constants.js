﻿'use strict'
angular.module('aci.constants', [
])
    // service urls required for each service.
    .constant("constants", {

        cpiaServiceUrl: "http://localhost:53340/api/v1/CPIA/",
        practiceInfoServiceUrl: "http://localhost:53340/api/v1/PracticeInformation/",
        aciServiceUrl: "http://localhost:53340/api/v1/ACIConfig/",
        getGriServiceUrl: "http://localhost:53341/api/v1/registryList/",
        getPublicHealthServiceUrl: "http://localhost:62936/api/v1/publicHealth/",
        uiRoot: 'http://localhost:5000/',
        auditServiceUrl: "http://localhost:62936/api/v1/AuditReport/",
        amcServiceUrl: "http://localhost:53349/api/v1/AMC/",
        muServiceUrl: "http://localhost:53349/api/v1/AMC/",
        anrServiceUrl: "http://localhost:53349/api/v1/AMC/",
        dashBoardServiceUrl: "http://localhost:53343/api/v1/MIPSPerformanceDashboard/",
        dataExportServiceUrl: "http://localhost:53341/api/v1/DataExport/",
        schedulerConfiguration: "http://localhost:53341/api/v1/SchedulerConfig/",
        qppServiceUrl: "http://localhost:53349/api/v1/QPPExport/",
        //AuditClientTimevalue name is fixed do not update the value
        AuditClientTimevalue: "AuditClientTime"
    })
    .constant("regex", {
        onlyNumbers: /^\d+$/
    });
function getServiceUrl() {
    //if you are changing practiceInfoServiceUrl above please chage below also 
    return "http://localhost:53340/api/v1/GetUsername/";
}

