﻿angular.module('aci.utilities', [
])
    .service('util', [
        function () {
            var utilities = this;
            
            utilities.truncateString = function (string, cutoff) {
                if (string) {
                    if (string.length > cutoff) {
                        string = string.substr(0, cutoff) + '... ';
                    }

                    return string;
                }
                return '';
            };

            utilities.browserTime = function () {
                utilities.browserClientTime = "";
                var currentDate = new Date,
                    currentDateFormat = [currentDate.getMonth() + 1,
                               currentDate.getDate(),
                               currentDate.getFullYear()].join('/') + ' ' +
                               [currentDate.getHours(),
                               currentDate.getMinutes(),
                               currentDate.getSeconds()].join(':');
                return currentDateFormat;
            };

            utilities.clean = function (e) {
                if (e) {
                    e.stopPropagation();
                    e.preventDefault();
                }
                return false;
            };

            utilities.isDateValid = function (date, seperator) {
                if (date === "") return true;
                var dateArray = date.split(seperator);
                var y = dateArray[2], m = dateArray[0], d = dateArray[1];
                var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if ((!(y % 4) && y % 100) || !(y % 400)) {
                    daysInMonth[1] = 29;
                }
                return d <= daysInMonth[--m];
            }
            
            utilities.dateDiff = function (starDate, endDate) {
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(starDate);
                var secondDate = new Date(endDate);

                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                return diffDays;
            }

            utilities.sum = function (items, prop) {
                return items.reduce(function (a, b) {
                    return a + b[prop];
                }, 0);
            };

            utilities.numberOnly = function (key) {
                return (key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
            }

            utilities.getParameterByName = function(name, url) {
                if (!url) {
                    url = window.location.href;
                }
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }
            return utilities;
        }]);