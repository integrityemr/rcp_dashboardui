﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('logoutController', logoutController);
logoutController.$inject = ['$state', '$scope'];

function logoutController($state, $scope) {
    var logout = this;
    logout.init = function () {
        logout.title = "Logout from ACI";
    }
    logout.init();
}
