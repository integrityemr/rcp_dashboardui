﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('groupLevelService', groupLevelService);

groupLevelService.$inject = ['httpFactory', 'constants', 'groupLevelConstants'];

function groupLevelService(httpFactory, constants, groupLevelConstants) {
    var groupLevelService = this;
    var serviceUrl = constants.dashBoardServiceUrl;

    //to get the Clinicians for the selected Group
    groupLevelService.getClinicianForGroup = function (aciStage, tin, fromDate, toDate,selectedYear) {
        var url = serviceUrl + groupLevelConstants.getCliniciansForGroupUrl + aciStage + '/' + tin + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var clinicianList = httpFactory.get(url);
        return clinicianList;
    }

    //to get the list of activities of that selected Group
    groupLevelService.getCPIAActivitiesForGroup = function (groupId, fromDate, toDate, selectedYear) {
        var url = serviceUrl + groupLevelConstants.getCPIADetailsForGroupUrl + groupId + '/' + fromDate + '/' + toDate +'/'+selectedYear;
        var groupCPIAList = httpFactory.get(url);
        return groupCPIAList;
    }

    //to set the group object for print
    groupLevelService.setPracticeLevelPrintDashBoardObj = function (groupObj, clinicianObj,isShowGroup) {
        groupLevelService.groupsList = groupObj;
        groupLevelService.clinicianList = clinicianObj;
        groupLevelService.isShowGroup = isShowGroup;
    }

    //set Group CPIA for Print
    groupLevelService.setGroupCPIAPrintObj = function (cpiaActivitiesPrintObj, mipsCPIAScore, totalActivityPointsEarned, groupName, groupTIN, cpiaReportingPeriod) {
        groupLevelService.groupCPIAActivitiesList = cpiaActivitiesPrintObj;
        groupLevelService.mipsCPIAScore = mipsCPIAScore;
        groupLevelService.totalActivityPointsEarned = totalActivityPointsEarned;
        groupLevelService.groupName = groupName;
        groupLevelService.groupTIN = groupTIN;
        groupLevelService.cpiaReportingPeriod = cpiaReportingPeriod

    }
    //to set the screen name when clicking the back Button
    groupLevelService.setScreenName = function (screenName) {
        groupLevelService.screenName = screenName;
    }
}