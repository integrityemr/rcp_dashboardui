﻿angular.module('aci.mipsPerformanceDashboard').service('GRIUserService', GRIUserService);

GRIUserService.$inject = ['httpFactory', 'griuserconfigurationConstants', 'constants'];
function GRIUserService(httpFactory, griuserconfigurationConstants, constants) {

    var GRIUserService = this;
    var serviceUrl = constants.getGriServiceUrl;
    GRIUserService.activities = [];

    GRIUserService.UpdateRegistryDetails = function (requestObject) {
        var url = serviceUrl + griuserconfigurationConstants.UpdateRegistryDetails;
        var activities = httpFactory.post(url, requestObject);
        return activities;
    }
    GRIUserService.GetRegistryList = function (auditlog) {
        var url = serviceUrl + griuserconfigurationConstants.GetRegistryList + '/' + auditlog;
        var activities = httpFactory.get(url);
        return activities;
    }
    //to set the screen name when clicking the back Button
    GRIUserService.setScreenName = function (screenName) {
        GRIUserService.screenName = screenName;
    }
}