﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('practiceLevelService', practiceLevelService);

practiceLevelService.$inject = ['httpFactory', 'constants', 'practiceLevelConstants'];

function practiceLevelService(httpFactory, constants, practiceLevelConstants) {
    var practiceLevelService = this;
    var serviceUrl = constants.dashBoardServiceUrl;

    practiceLevelService.setReportingObj = function (reportingObj) {
        practiceLevelService.reportingObject = reportingObj;
    }
    //to set the print object for practiceLevel DashBoard
    practiceLevelService.setPracticeLevelPrintDashBoardObj = function (printObj) {
        practiceLevelService.practiceLevelPrintObject = printObj;
    }
    //to set the screen name when clicking the back Button
    practiceLevelService.setScreenName = function (screenName) {
        practiceLevelService.screenName = screenName;
    }
    //
    practiceLevelService.setClinicianDashBoardObj = function (clinicinaDashBoardObj) {
        practiceLevelService.clinicianDashBoardObject = clinicinaDashBoardObj;
    }

    //TO GET THE LOGGED IN  user details
    practiceLevelService.getLoggedInUserDetails = function () {
        var url = serviceUrl + practiceLevelConstants.getLoggedInUserDetailsUrl;
        var loggedInUserData = httpFactory.get(url);
        return loggedInUserData;
    }
    //To get the practice details
    practiceLevelService.getPracticeDetails = function () {
        var url = serviceUrl + practiceLevelConstants.getPracticeDetailsUrl;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }
    //to set the Practice Details

    practiceLevelService.setPracticeDetails = function (practiceData) {
        practiceLevelService.practiceDetailsPrintObj = practiceData;
    }
    //to set the logged in user
    practiceLevelService.setLoggedInUser = function (userObject) {
        practiceLevelService.userObject = userObject;
    }

    //to get the clinicians and Groups
    practiceLevelService.getDashBoardDataForPractice = function (selectedYear) {
        // alert(selectedYear);
       // var url = serviceUrl + practiceLevelConstants.getPracticeLevelDashBoardDataUrl;
         var url = serviceUrl + practiceLevelConstants.getPracticeLevelDashBoardDataUrl + selectedYear;
        var practiceDashBoardData = httpFactory.get(url);
        return practiceDashBoardData;
    }
    //to get the ACI And CPIA scores for Specific  clinician
    practiceLevelService.getIndividualACICPIAScores = function (stageId, npi, tin, fromDate, toDate) {
        var url = serviceUrl + practiceLevelConstants.getIndividualAciAndCpiaScoreUrl + stageId + '/' + npi + '/' + tin
            + '/' + fromDate + '/' + toDate;
        var individualACIandCPIAScores = httpFactory.get(url);
        return individualACIandCPIAScores;
    }

    //to get the ACI And CPIA scores for Specific  group 
    practiceLevelService.getGroupACICPIAScores = function (stageId, tin, fromDate, toDate,selectedYear) {
        var url = serviceUrl + practiceLevelConstants.getGroupAciAndCpiaScoreUrl + stageId + '/' + tin
            + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var groupACIandCPIAScores = httpFactory.get(url);
        return groupACIandCPIAScores;
    }

    //to get the ACI And CPIA scores for groupIndividual by GroupTIN and Individual NPI
    practiceLevelService.getGroupIndividualACICPIAScores = function (stageId, npi, tin, fromDate, toDate, selectedYear) {
        var url = serviceUrl + practiceLevelConstants.getGroupIndividualACICPIAScoresUrl + stageId + '/' + npi + '/' + tin
            + '/' + fromDate + '/' + toDate+'/'+selectedYear;
        var groupACIandCPIAScores = httpFactory.get(url);
        return groupACIandCPIAScores;
    }
    practiceLevelService.getActivityStartDatesOfGroup = function (tin) {
        var url = serviceUrl + practiceLevelConstants.getActivityStartDatesOfGroupUrl + tin;
        var datesListForGroup = httpFactory.get(url);
        return datesListForGroup;
    }

    practiceLevelService.getActivityStartDatesOfIndividual = function (npi, tin) {
        var url = serviceUrl + practiceLevelConstants.getActivityStartDatesOfIndividualUrl + npi + '/' + tin;
        var datesListForIndividual = httpFactory.get(url);
        return datesListForIndividual;
    }

    practiceLevelService.getCPIARegistryStartDates = function () {
        var url = serviceUrl + practiceLevelConstants.getCPIARegisgtsryStartDatesUrl;
        var registryStartDates = httpFactory.get(url);
        return registryStartDates;
    }

    practiceLevelService.updateACIReportingPeriod = function (aciReportingPeriodObj) {
        var url = serviceUrl + practiceLevelConstants.updateACIReportingPeriodUrl;
        var isSuccess = httpFactory.post(url, aciReportingPeriodObj);
        return isSuccess;
    }

    practiceLevelService.getActivityStartDatesOfIndependent = function (npi, tin) {
        var url = serviceUrl + practiceLevelConstants.getActivityStartDatesOfIndependentUrl + npi + '/' + tin;
        var datesListForIndividual = httpFactory.get(url);
        return datesListForIndividual;
    }
    practiceLevelService.LogUserAudit = function (userAuditData) {
        var url = constants.auditServiceUrl + practiceLevelConstants.CreateActionLog;
        var isSuccess = httpFactory.post(url, userAuditData);
        return isSuccess;
    }
}