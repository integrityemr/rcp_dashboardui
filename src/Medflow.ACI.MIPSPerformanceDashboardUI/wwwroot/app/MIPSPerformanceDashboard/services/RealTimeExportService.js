﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('RealTimeExportService', dataExportService);

dataExportService.$inject = ['httpFactory'];
function dataExportService(dataExportFactory) {
    var service = {
        getData: getData,
        postData: postData,
        putData: putData,
    };

    return service;

    function getData(url, params) {
        return dataExportFactory.get(url, params);
    }
    function postData(url, params) {
        return dataExportFactory.post(url, params);
    }
    function putData(url, params) {
        return dataExportFactory.put(url, params);
    }
}