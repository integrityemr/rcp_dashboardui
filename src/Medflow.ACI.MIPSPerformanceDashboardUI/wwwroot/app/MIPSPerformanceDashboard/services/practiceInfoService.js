﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('practiceInfoService', practiceInfoService);

practiceInfoService.$inject = ['httpFactory', 'configurationConstants', 'constants'];

function practiceInfoService(httpFactory, configurationConstants, constants) {

    var practiceInfoService = this;
    var serviceUrl = constants.practiceInfoServiceUrl;

    practiceInfoService.getAttributes = function () {
        var url = serviceUrl + configurationConstants.getAttributes;
        var attributes = httpFactory.get(url);
        return attributes;
    }
    practiceInfoService.getClinicians = function () {
        var url = serviceUrl + configurationConstants.getClinicians;
        var clinicians = httpFactory.get(url);
        return clinicians;
    }
    practiceInfoService.getGroupBillingList = function () {
        var url = serviceUrl + configurationConstants.getGroupBillingList;
        var groupBillingList = httpFactory.get(url);
        return groupBillingList;
    }
    practiceInfoService.getIndividualBillingList = function () {
        var url = serviceUrl + configurationConstants.getIndividualBillingList;
        var individualBillingList = httpFactory.get(url);
        return individualBillingList;
    }
    practiceInfoService.saveGroupReporting = function (data) {
        var url = serviceUrl + configurationConstants.saveGroupReporting;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }
    practiceInfoService.saveIndividualReporting = function (data) {
        var url = serviceUrl + configurationConstants.saveIndividualReporting;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }
    practiceInfoService.createAuditLog = function (data) {
        var url = serviceUrl + configurationConstants.createAuditLog;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }
    practiceInfoService.GetUserDetails = function () {
        var url = serviceUrl + configurationConstants.getUsername;
        var userDetails = httpFactory.getUser(url);
        return userDetails;
    }
    practiceInfoService.GetUserRole = function () {
        var url = serviceUrl + configurationConstants.getUserRole;
        var userRoleDetails = httpFactory.getUser(url);
        return userRoleDetails;
    }
    //to set the screen name when clicking the back Button
    practiceInfoService.setScreenName = function (screenName) {
        practiceInfoService.screenName = screenName;
    }
    practiceInfoService.GetPracticeLogoPath = function () {
        var url = serviceUrl + configurationConstants.getPracticeLogoPath;
        var userDetails = httpFactory.getUser(url);
        return userDetails;
    }

    //getting the latest build version details
    practiceInfoService.GetBuildVersionDetails = function () {
        var url = serviceUrl + configurationConstants.getBuildDetails;
        var buildDetails = httpFactory.getUser(url);
        return buildDetails;
    }
    return practiceInfoService;
}