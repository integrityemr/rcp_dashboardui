﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('amcService', amcService);

amcService.$inject = ['httpFactory', 'constants', 'amcMeasureConstants', 'muMeasureConstants'];

function amcService(httpFactory, constants, amcMeasureConstants, muMeasureConstants) {

    var amcService = this;
    var serviceUrl = constants.amcServiceUrl;

    function setDashBoardObject(dashBoardObj) {
        amcService.dashBoardObject = dashBoardObj;
    }

    //to get the MU stages 
    amcService.getMUStages = function () {
        var url = serviceUrl + muMeasureConstants.getMUStageUrl;
        var muStages = httpFactory.get(url);
        return muStages;
    }

    //to get the clinicians and Groups
    amcService.getGroupsAndClinicians = function () {
        var url = serviceUrl + muMeasureConstants.getGroupsCliniciansUrl;
        var groupsClinicians = httpFactory.get(url);
        return groupsClinicians;
    }

    amcService.getMeasureReportForGroup = function (muStageId, groupReportingId, fromDate, toDate, tin) {
        var url = serviceUrl + amcMeasureConstants.getAMCMeasureReportForGroupUrl + muStageId + '/' + groupReportingId
            + '/' + fromDate + '/' + toDate + '/' + tin ;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    amcService.getMeasureReportForClinician = function (muStageId, clinicianId, fromDate, toDate, tin, npi) {
        var url = serviceUrl + amcMeasureConstants.getAMCMeasureReportForIndividualClinicianUrl + muStageId + '/' + clinicianId
            + '/' + fromDate + '/' + toDate + '/' + tin + '/' + npi;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    amcService.getMeasureReportForGroupClinician = function (muStageId, fromDate, toDate, tin, npi) {
        var url = serviceUrl + amcMeasureConstants.getAMCMeasureReportForGroupIndividualClinicianUrl + muStageId + '/' + fromDate + '/' + toDate + '/' + tin + '/' + npi;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    //set print Object
    amcService.setACIMeasureReportObj = function (measureReportBaseScore, measureReportPerformancePoints, measureReportBonusPoints,
        selectedStage, selectedClinicianName, selectedClinicianNPI, selectedClinicianTIN, totalMeasurePoints, aciScore, selectedGroupName, selectedGroupTIN, type, fromDate, toDate, stageName) {
        amcService.measureReportBaseScore = measureReportBaseScore;
        amcService.measureReportPerformancePoints = measureReportPerformancePoints;
        amcService.measureReportBonusPoints = measureReportBonusPoints;
        amcService.selectedStage = selectedStage;
        amcService.selectedClinicianName = selectedClinicianName;
        amcService.selectedClinicianNPI = selectedClinicianNPI;
        amcService.selectedClinicianTIN = selectedClinicianTIN;
        amcService.totalMeasurePoints = totalMeasurePoints;
        amcService.aciScore = aciScore;
        amcService.selectedGroupName = selectedGroupName;
        amcService.selectedGroupTIN = selectedGroupTIN;
        amcService.type = type;
        amcService.fromDate = fromDate;
        amcService.toDate = toDate;
        amcService.stageName = stageName;
    }

    //to set the screen name when clicking the back Button
    amcService.setScreenName = function (screenName) {
        amcService.screenName = screenName;
    }
}



