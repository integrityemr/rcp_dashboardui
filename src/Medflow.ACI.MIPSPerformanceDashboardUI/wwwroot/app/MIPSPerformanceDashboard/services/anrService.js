﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('anrService', anrService);

anrService.$inject = ['httpFactory', 'constants', 'anrMeasureConstants', 'muMeasureConstants'];

function anrService(httpFactory, constants, anrMeasureConstants, muMeasureConstants) {

    var anrService = this;
    var serviceUrl = constants.anrServiceUrl;

    function setDashBoardObject(dashBoardObj) {
        anrService.dashBoardObject = dashBoardObj;
    }

    anrService.getANRMeasurePatientsData = function (stageId, muStageId, fromDate, toDate, tin, npi, type) {
        var url = serviceUrl + anrMeasureConstants.getANRMeasurePatientsDataUrl + stageId + "/" + muStageId + '/' + fromDate + '/' + toDate + '/' + tin + '/' + npi + '/' + type;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }

    anrService.GetVendorIsMRNValue = function () {
        var url = serviceUrl + anrMeasureConstants.getVendorIsMRNValue;
        var mrnValue = httpFactory.get(url);
        return mrnValue;
    }
}



