﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('muMeasureService', muMeasureService);

muMeasureService.$inject = ['httpFactory', 'muMeasureConstants', 'constants'];

function muMeasureService(httpFactory, muMeasureConstants, constants) {

    var muMeasureService = this;
    var amcServiceUrl = constants.muServiceUrl;

    //to get the MU Measure report for clinician
    muMeasureService.getMUMeasureReport = function (muStageId, tin, npi, fromDate, toDate) {
        var url = amcServiceUrl + muMeasureConstants.getMUMeasureReportUrl + muStageId + '/' + tin + '/' + npi
            + '/' + fromDate + '/' + toDate;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    //to get the MU stages 
    muMeasureService.getMUStages = function () {
        var url = amcServiceUrl + muMeasureConstants.getMUStageUrl;
        var muStages = httpFactory.get(url);
        return muStages;
    }

    //to get the clinicians and Groups
    muMeasureService.getGroupsAndClinicians = function () {
        var url = amcServiceUrl + muMeasureConstants.getCliniciansUrl;
        var groupsClinicians = httpFactory.get(url);
        return groupsClinicians;
    }

    ////to update and insert Excludes in MU measure report
    muMeasureService.postMUMeasureExclude = function (data) {
        var url = amcServiceUrl + muMeasureConstants.PostMUMeasureExclude;
        var MUMeasureExclude = httpFactory.post(url, data);
        return MUMeasureExclude;
    }

    muMeasureService.setMUPrintObj = function (data, physicianName, physicianNPI, physicianTIN,fromDate,toDate,muMeasureTitle) {
        muMeasureService.muPrintObj = data;
        muMeasureService.physicianName = physicianName;
        muMeasureService.physicianNPI = physicianNPI;
        muMeasureService.physicianTIN = physicianTIN;
        muMeasureService.fromDate = fromDate;
        muMeasureService.toDate = toDate;
        muMeasureService.muMeasureTitle = muMeasureTitle;
    }

    //To Audit 
    muMeasureService.auditForAMC = function (auditInfodata) {
        var url = amcServiceUrl + muMeasureConstants.saveAuditLogAMC;
        var created = httpFactory.post(url, auditInfodata);
        return created;
    }

    //to set the screen name when clicking the back Button
    muMeasureService.setScreenName = function (screenName) {
        muMeasureService.screenName = screenName;
    }
}
