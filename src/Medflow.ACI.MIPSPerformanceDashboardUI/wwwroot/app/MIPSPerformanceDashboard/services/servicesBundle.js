'use strict'
angular.module('aci.mipsPerformanceDashboard').service('aciService', aciService);

aciService.$inject = ['httpFactory', 'configurationConstants', 'constants'];

function aciService(httpFactory, configurationConstants, constants) {

    var aciService = this;
    var serviceUrl = constants.aciServiceUrl;

    aciService.getMeasuresList = function (year) {
        var url = serviceUrl + configurationConstants.getMeasuresList + year;
        var measures = httpFactory.get(url);
        return measures;
    }

    aciService.getSRAHistoryList = function () {
        var url = serviceUrl + configurationConstants.getSRAHistoryList;
        var history = httpFactory.get(url);
        return history;
    }
    aciService.createAuditLogACIConfiguration = function (data) {
        var url = serviceUrl + configurationConstants.createAuditLogACIConfiguration;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }

    aciService.getACIConfigurationCountDetails = function () {
        var url = serviceUrl + configurationConstants.getACIConfigurationCountDetails;
        var countDetails = httpFactory.get(url);
        return countDetails;
    }

    aciService.getSRADate = function () {
        var url = serviceUrl + configurationConstants.getSRADate;
        var sraDate = httpFactory.get(url);
        return sraDate;
    }

    aciService.ACIMeasureSetList = function (muStageId, configTypeId, individualId, groupId, selectedYear) {
        var url = serviceUrl + configurationConstants.ACIMeasureSetList + muStageId + '/' + configTypeId + '/' + individualId + '/' + groupId + '/' + selectedYear;
        var measureSetList = httpFactory.get(url);
        return measureSetList;
    }

    aciService.getACIMeasureSetListByTINAndNPI = function (npi, tin, stageId, cpiaType) {
        var url = serviceUrl + configurationConstants.ACIMeasureSetListByTINAndNPI + npi + '/' + tin + '/' + stageId + '/' + cpiaType;
        var measureSetList = httpFactory.get(url);
        return measureSetList;
    }

    aciService.createACIMeasureAndSRADate = function (requestObject) {
        var url = serviceUrl + configurationConstants.createACIMeasureAndSRADate;
        var created = httpFactory.post(url, requestObject);
        return created;
    }

    aciService.updateACIMeasures = function (requestObject) {
        var url = serviceUrl + configurationConstants.updateACIMeasures;
        var updated = httpFactory.put(url, requestObject);
        return updated;
    }

    aciService.getACICliniciansList = function () {
        var url = serviceUrl + configurationConstants.getACICliniciansList;
        var clinicians = httpFactory.get(url);
        return clinicians;
    }

    aciService.getGroupList = function (requestObject) {
        var url = serviceUrl + configurationConstants.getGroupList;
        var groups = httpFactory.get(url);
        return groups;
    }

    aciService.GetUserDetails = function () {
        var url = serviceUrl + configurationConstants.getUserDetails;
        var userDetails = httpFactory.get(url);
        return userDetails;
    }

    aciService.GetPracticeDetails = function () {
        var url = serviceUrl + configurationConstants.getPracticeDetails;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }

    aciService.enabledPublicHealthPoints = function (code) {
        var url = constants.getGriServiceUrl + configurationConstants.enabledPublicHealthPoints + code;
        var groups = httpFactory.get(url);
        return groups;
    }

    aciService.enableQualityRegistryPoints = function () {
        var url = constants.getGriServiceUrl + configurationConstants.enableQualityRegistryPoints;
        var userDetails = httpFactory.get(url);
        return userDetails;
    }


    aciService.eligibleCPIAPoints = function (CPIATypeId) {
        var url = constants.cpiaServiceUrl + configurationConstants.eligibleCPIAPoints + CPIATypeId;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }

    aciService.eligibleCPIAPointsforGroupsandIndividual = function (CPIATypeId, GroupId, IndividualId, NPI, TIN, selectedYear) {
        var url = constants.cpiaServiceUrl + configurationConstants.EligibleCPIAPointsforGroupsandIndividuals + CPIATypeId + '/' + GroupId + '/' + IndividualId + '/' + NPI + '/' + TIN + '/' + selectedYear;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }

    aciService.CreateAudit = function (AuditInfo) {
        var url = constants.auditServiceUrl + configurationConstants.CreateActionLogFromAuditReportProject;
        var created = httpFactory.post(url, AuditInfo);
        return true;
    }
    //to set the screen name when clicking the back Button
    aciService.setScreenName = function (screenName) {
        aciService.screenName = screenName;
    }
    aciService.getMeasureSetByCPIAType = function (cpiaType, selectedType, selectedClinicianId, selectedGroupId, tin, npi, selectedYear) {
        var url = constants.aciServiceUrl + configurationConstants.getMeasureSetIdByCPIATypeUrl + cpiaType + '/' + selectedType + '/' + selectedClinicianId + '/' + selectedGroupId + '/' + tin + '/' + npi + '/' + selectedYear;
        var stageId = httpFactory.get(url);
        return stageId;
    }
    aciService.auditForACI = function (AuditInfo) {
        var url = serviceUrl + configurationConstants.auditCPIAUrl;
        var created = httpFactory.post(url, AuditInfo);
        return created;
    }
    aciService.getCEHRT = function () {
        var url = serviceUrl + configurationConstants.getCEHRT;
        var cehrt = httpFactory.get(url);
        return cehrt;
    }
    aciService.deleteSRAHistory = function (id) {
        var url = serviceUrl + configurationConstants.deleteSRAHistory;
        var created = httpFactory.post(url, id);
        return created;
    }
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').service('amcService', amcService);

amcService.$inject = ['httpFactory', 'constants', 'amcMeasureConstants', 'muMeasureConstants'];

function amcService(httpFactory, constants, amcMeasureConstants, muMeasureConstants) {

    var amcService = this;
    var serviceUrl = constants.amcServiceUrl;

    function setDashBoardObject(dashBoardObj) {
        amcService.dashBoardObject = dashBoardObj;
    }

    //to get the MU stages 
    amcService.getMUStages = function () {
        var url = serviceUrl + muMeasureConstants.getMUStageUrl;
        var muStages = httpFactory.get(url);
        return muStages;
    }

    //to get the clinicians and Groups
    amcService.getGroupsAndClinicians = function () {
        var url = serviceUrl + muMeasureConstants.getGroupsCliniciansUrl;
        var groupsClinicians = httpFactory.get(url);
        return groupsClinicians;
    }

    amcService.getMeasureReportForGroup = function (muStageId, groupReportingId, fromDate, toDate, tin) {
        var url = serviceUrl + amcMeasureConstants.getAMCMeasureReportForGroupUrl + muStageId + '/' + groupReportingId
            + '/' + fromDate + '/' + toDate + '/' + tin ;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    amcService.getMeasureReportForClinician = function (muStageId, clinicianId, fromDate, toDate, tin, npi) {
        var url = serviceUrl + amcMeasureConstants.getAMCMeasureReportForIndividualClinicianUrl + muStageId + '/' + clinicianId
            + '/' + fromDate + '/' + toDate + '/' + tin + '/' + npi;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    amcService.getMeasureReportForGroupClinician = function (muStageId, fromDate, toDate, tin, npi) {
        var url = serviceUrl + amcMeasureConstants.getAMCMeasureReportForGroupIndividualClinicianUrl + muStageId + '/' + fromDate + '/' + toDate + '/' + tin + '/' + npi;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    //set print Object
    amcService.setACIMeasureReportObj = function (measureReportBaseScore, measureReportPerformancePoints, measureReportBonusPoints,
        selectedStage, selectedClinicianName, selectedClinicianNPI, selectedClinicianTIN, totalMeasurePoints, aciScore, selectedGroupName, selectedGroupTIN, type, fromDate, toDate, stageName) {
        amcService.measureReportBaseScore = measureReportBaseScore;
        amcService.measureReportPerformancePoints = measureReportPerformancePoints;
        amcService.measureReportBonusPoints = measureReportBonusPoints;
        amcService.selectedStage = selectedStage;
        amcService.selectedClinicianName = selectedClinicianName;
        amcService.selectedClinicianNPI = selectedClinicianNPI;
        amcService.selectedClinicianTIN = selectedClinicianTIN;
        amcService.totalMeasurePoints = totalMeasurePoints;
        amcService.aciScore = aciScore;
        amcService.selectedGroupName = selectedGroupName;
        amcService.selectedGroupTIN = selectedGroupTIN;
        amcService.type = type;
        amcService.fromDate = fromDate;
        amcService.toDate = toDate;
        amcService.stageName = stageName;
    }

    //to set the screen name when clicking the back Button
    amcService.setScreenName = function (screenName) {
        amcService.screenName = screenName;
    }
}




'use strict'
angular.module('aci.mipsPerformanceDashboard').service('clinicianLevelService', clinicianLevelService);

clinicianLevelService.$inject = ['httpFactory', 'constants', 'clinicianLevelConstants'];

function clinicianLevelService(httpFactory, constants, clinicianLevelConstants) {
    var clinicianLevelService = this;
    var serviceUrl = constants.dashBoardServiceUrl;

    //to get the Clinicians for the selected Group
    clinicianLevelService.getGroupsForClinician = function (measureCode, tin, npi, fromDate, toDate) {
        var url = serviceUrl + clinicianLevelConstants.getGroupsForClinicianUrl + measureCode + '/' + tin
            + '/' + npi + '/' + fromDate + '/' + toDate;
        var groupList = httpFactory.get(url);
        return groupList;
    }
    //to get the Individual DashBoard data for the selected user
    clinicianLevelService.getClinicianMeasuresForIndividualDashBoard = function (userId) {
        var url = serviceUrl + clinicianLevelConstants.getClinicianMeasuresForIndividualDashBoardUrl + userId;
        var groupList = httpFactory.get(url);
        return groupList;
    }
    //to get the list of activities of that selected Clinician
    clinicianLevelService.getCPIAActivitiesForClinician = function (npi, tin, fromDate, toDate, selectedYear) {
        var url = serviceUrl + clinicianLevelConstants.getCPIADetailsForIndividualUrl + npi + '/' + tin + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var clinicianCPIAList = httpFactory.get(url);
        return clinicianCPIAList;
    }
    //to get the list of activities of that selected Group Clinician
    clinicianLevelService.getCPIAActivitiesForGroupClinician = function (npi, tin, fromDate, toDate, selectedYear) {
        var url = serviceUrl + clinicianLevelConstants.getCPIADetailsForGroupIndividualUrl + npi + '/' + tin + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var clinicianCPIAList = httpFactory.get(url);
        return clinicianCPIAList;
    }
    //set dashboard data for print
    clinicianLevelService.setClinicianLevelPrintDashBoardObj = function (clinicianObj, groupObj) {
        clinicianLevelService.clinicianPrintObj = clinicianObj;
        clinicianLevelService.groupPrintObj = groupObj;
    }
    //set CPIA dashBoard Data for print

    clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj = function (cpiaActivities, clinicianName, clinicianNPI, clinicianTIN, mipsCPIAScore, totalActivityPointsEarned, cpiaReportingPeriod) {
        clinicianLevelService.clinicianLevelCPIAObj = cpiaActivities;
        clinicianLevelService.clinicianName = clinicianName;
        clinicianLevelService.clinicinaNPI = clinicianNPI;
        clinicianLevelService.clinicianTIN = clinicianTIN;
        clinicianLevelService.mipsCPIAScore = mipsCPIAScore;
        clinicianLevelService.totalActivityPointsEarned = totalActivityPointsEarned;
        clinicianLevelService.cpiaReportingPeriod = cpiaReportingPeriod
    }

    //to set the screen name when clicking the back Button
    clinicianLevelService.setScreenName = function (screenName) {
        clinicianLevelService.screenName = screenName;
    }
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').service('cpiaService', cpiaService);

cpiaService.$inject = ['httpFactory', 'configurationConstants', 'constants'];

function cpiaService(httpFactory, configurationConstants, constants) {

    var cpiaService = this;
    var serviceUrl = constants.cpiaServiceUrl;
    var qppUrl = constants.qppServiceUrl;
    cpiaService.activities = [];

   
    //To get the practice details
    cpiaService.getPracticeDetails = function (productKey) {
        var url = serviceUrl + configurationConstants.getPracticeDetailsUrl + productKey;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }
    //to set the Practice Details

    cpiaService.setPracticeDetails = function (practiceData) {
        cpiaService.practiceDetailsPrintObj = practiceData;
    }

    //to set the screen name when clicking the back Button
    cpiaService.setScreenName = function (screenName) {
        cpiaService.screenName = screenName;
    }
    cpiaService.setPrintObject = function (printObject) {
        cpiaService.printObject = printObject;
    }
    cpiaService.setActivityObject = function (activityObj) {
        cpiaService.activityObject = activityObj;
    }
    cpiaService.getActivities = function (cpiaTypeId, groupReportingId, individualReportingId, selectedYear) {
    //    alert("cpiaTypeId" + cpiaTypeId + "groupReportingId" + groupReportingId + "individualReportingId" + individualReportingId + "selec tedyear" + selectedYear);
        var url = serviceUrl + configurationConstants.getActivitiesByType + cpiaTypeId + '/' + individualReportingId
            + '/' + groupReportingId + '/' + selectedYear;
        var activities = httpFactory.get(url);
        return activities;
    }
    cpiaService.getActivitiesByNPIandTIN = function (npi, tin, cpiaType,selectYear) {
        var url = serviceUrl + configurationConstants.getActivitiesByNPIandTIN + npi + '/' + tin + '/' + cpiaType + '/' + selectedYear;
        var activities = httpFactory.get(url);
        return activities;
    }

    cpiaService.getGroupList = function () {
        var url = serviceUrl + configurationConstants.getGroupList;
        var groups = httpFactory.get(url);
        return groups;
    }

    cpiaService.getCliniciansList = function () {
        var url = serviceUrl + configurationConstants.getCliniciansList;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }
    cpiaService.GetEntities = function () {
        debugger;
        var url = qppUrl + configurationConstants.getentitiesList;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }
    cpiaService.GetCategoriesList = function () {
        var url = qppUrl + configurationConstants.GetCategoriesList;
        var categories = httpFactory.get(url);
        return categories;
    }
    //get Qpp Audit Log
    cpiaService.getAuditLogData = function () {
        var url = qppUrl + configurationConstants.GetQppAuditLogData;
        var AuditLogData = httpFactory.get(url);
        return AuditLogData;
    }
    // post Qpp Audit Data
    cpiaService.saveQppAuditLogData = function (requestObject) {
        var url = qppUrl + configurationConstants.SaveQppAuditLogData;
        var AuditSavedData = httpFactory.post(url, requestObject);
        return AuditSavedData;
    }
    cpiaService.getCPIATypes = function () {
        var url = serviceUrl + configurationConstants.getCPIATypes;
        var cpiaTypes = httpFactory.get(url);
        return cpiaTypes;
    }

    cpiaService.getGroupDetailsForPrint = function (groupReportingId) {
        var url = serviceUrl + configurationConstants.getGroupDetailsForPrint + groupReportingId;
        var groupDetails = httpFactory.get(url);
        return groupDetails;
    }

    cpiaService.getReportingCount = function () {
        var url = serviceUrl + configurationConstants.getReportingCount;
        var reportingCount = httpFactory.get(url);
        return reportingCount;
    }

    cpiaService.getCliniciansForPrint = function (individualReportingId) {
        var url = serviceUrl + configurationConstants.getCliniciansForPrint + individualReportingId;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }

    cpiaService.updateCPIAActivities = function (requestObject) {
        var url = serviceUrl + configurationConstants.updateCPIAActivities;
        var reportingCount = httpFactory.put(url, requestObject);
        return reportingCount;
    }

    cpiaService.saveCPIAActivities = function (requestObject) {
        var url = serviceUrl + configurationConstants.saveCPIAActivities;
        var activities = httpFactory.post(url, requestObject);
        return activities;
    }

    cpiaService.copyCPIAActivities = function (requestObject) {
        var url = serviceUrl + configurationConstants.copyCPIAActivities;
        var activities = httpFactory.post(url, requestObject);
        return activities;
    }

    cpiaService.GetUserDetails = function () {
        var url = serviceUrl + configurationConstants.getUserDetails;
        var userDetails = httpFactory.get(url);
        return userDetails;
    }
    cpiaService.auditForCPIA = function (AuditInfo) {
        var url = serviceUrl + configurationConstants.auditCPIAUrl;
        var created = httpFactory.post(url, AuditInfo);
        return created;
    }

    //QPP Export related methods

    cpiaService.GetExportresults = function () {
        var url = qppUrl + configurationConstants.getqppExportData;
        var results = httpFactory.get(url);
        return results;
    }
    cpiaService.GetConfigStatus = function (data) {
        var url = qppUrl + configurationConstants.getConfigReportingStatusForQPPExport;
        var results = httpFactory.post(url, data);
        return results;
    }

    cpiaService.GetEntities = function () {
        var url = qppUrl + configurationConstants.getentitiesList;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }
    cpiaService.GetCategoriesList = function () {
        var url = qppUrl + configurationConstants.GetCategoriesList;
        var categories = httpFactory.get(url);
        return categories;
    }

    cpiaService.QppExport = function (data) {
        var url = qppUrl + configurationConstants.qppExport;
        var exportjsons = httpFactory.post(url, data);
        return exportjsons;
    }

    cpiaService.QppExportStatusSave = function (data) {
        var url = qppUrl + configurationConstants.qppexportStatusSave;
        var result = httpFactory.post(url, data);
        return result;
    }
    cpiaService.GetGroupsQppExport = function (data) {
        var url = qppUrl + configurationConstants.getGroupList;
        var groups = httpFactory.get(url);
        return groups;
    }
    cpiaService.GetCliniciansQppExport = function (data) {
        var url = qppUrl + configurationConstants.getCliniciansList;
        var Clinicians = httpFactory.get(url);
        return Clinicians;
    }

}
angular.module('aci.mipsPerformanceDashboard').service('GRIUserService', GRIUserService);

GRIUserService.$inject = ['httpFactory', 'griuserconfigurationConstants', 'constants'];
function GRIUserService(httpFactory, griuserconfigurationConstants, constants) {

    var GRIUserService = this;
    var serviceUrl = constants.getGriServiceUrl;
    GRIUserService.activities = [];

    GRIUserService.UpdateRegistryDetails = function (requestObject) {
        var url = serviceUrl + griuserconfigurationConstants.UpdateRegistryDetails;
        var activities = httpFactory.post(url, requestObject);
        return activities;
    }
    GRIUserService.GetRegistryList = function (auditlog) {
        var url = serviceUrl + griuserconfigurationConstants.GetRegistryList + '/' + auditlog;
        var activities = httpFactory.get(url);
        return activities;
    }
    //to set the screen name when clicking the back Button
    GRIUserService.setScreenName = function (screenName) {
        GRIUserService.screenName = screenName;
    }
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').service('groupLevelService', groupLevelService);

groupLevelService.$inject = ['httpFactory', 'constants', 'groupLevelConstants'];

function groupLevelService(httpFactory, constants, groupLevelConstants) {
    var groupLevelService = this;
    var serviceUrl = constants.dashBoardServiceUrl;

    //to get the Clinicians for the selected Group
    groupLevelService.getClinicianForGroup = function (aciStage, tin, fromDate, toDate) {
        var url = serviceUrl + groupLevelConstants.getCliniciansForGroupUrl + aciStage  + '/' + tin + '/' + fromDate + '/' + toDate;
        var clinicianList = httpFactory.get(url);
        return clinicianList;
    }

    //to get the list of activities of that selected Group
    groupLevelService.getCPIAActivitiesForGroup = function (groupId, fromDate, toDate, selectedYear) {
        var url = serviceUrl + groupLevelConstants.getCPIADetailsForGroupUrl + groupId + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var groupCPIAList = httpFactory.get(url);
        return groupCPIAList;
    }

    //to set the group object for print
    groupLevelService.setPracticeLevelPrintDashBoardObj = function (groupObj, clinicianObj,isShowGroup) {
        groupLevelService.groupsList = groupObj;
        groupLevelService.clinicianList = clinicianObj;
        groupLevelService.isShowGroup = isShowGroup;
    }

    //set Group CPIA for Print
    groupLevelService.setGroupCPIAPrintObj = function (cpiaActivitiesPrintObj, mipsCPIAScore, totalActivityPointsEarned, groupName, groupTIN, cpiaReportingPeriod) {
        groupLevelService.groupCPIAActivitiesList = cpiaActivitiesPrintObj;
        groupLevelService.mipsCPIAScore = mipsCPIAScore;
        groupLevelService.totalActivityPointsEarned = totalActivityPointsEarned;
        groupLevelService.groupName = groupName;
        groupLevelService.groupTIN = groupTIN;
        groupLevelService.cpiaReportingPeriod = cpiaReportingPeriod

    }
    //to set the screen name when clicking the back Button
    groupLevelService.setScreenName = function (screenName) {
        groupLevelService.screenName = screenName;
    }
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').service('muMeasureService', muMeasureService);

muMeasureService.$inject = ['httpFactory', 'muMeasureConstants', 'constants'];

function muMeasureService(httpFactory, muMeasureConstants, constants) {

    var muMeasureService = this;
    var amcServiceUrl = constants.muServiceUrl;

    //to get the MU Measure report for clinician
    muMeasureService.getMUMeasureReport = function (muStageId, tin, npi, fromDate, toDate) {
        var url = amcServiceUrl + muMeasureConstants.getMUMeasureReportUrl + muStageId + '/' + tin + '/' + npi
            + '/' + fromDate + '/' + toDate;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }
    //to get the MU stages 
    muMeasureService.getMUStages = function () {
        var url = amcServiceUrl + muMeasureConstants.getMUStageUrl;
        var muStages = httpFactory.get(url);
        return muStages;
    }

    //to get the clinicians and Groups
    muMeasureService.getGroupsAndClinicians = function () {
        var url = amcServiceUrl + muMeasureConstants.getCliniciansUrl;
        var groupsClinicians = httpFactory.get(url);
        return groupsClinicians;
    }

    ////to update and insert Excludes in MU measure report
    muMeasureService.postMUMeasureExclude = function (data) {
        var url = amcServiceUrl + muMeasureConstants.PostMUMeasureExclude;
        var MUMeasureExclude = httpFactory.post(url, data);
        return MUMeasureExclude;
    }

    muMeasureService.setMUPrintObj = function (data, physicianName, physicianNPI, physicianTIN,fromDate,toDate,muMeasureTitle) {
        muMeasureService.muPrintObj = data;
        muMeasureService.physicianName = physicianName;
        muMeasureService.physicianNPI = physicianNPI;
        muMeasureService.physicianTIN = physicianTIN;
        muMeasureService.fromDate = fromDate;
        muMeasureService.toDate = toDate;
        muMeasureService.muMeasureTitle = muMeasureTitle;
    }

    //To Audit 
    muMeasureService.auditForAMC = function (auditInfodata) {
        var url = amcServiceUrl + muMeasureConstants.saveAuditLogAMC;
        var created = httpFactory.post(url, auditInfodata);
        return created;
    }

    //to set the screen name when clicking the back Button
    muMeasureService.setScreenName = function (screenName) {
        muMeasureService.screenName = screenName;
    }
}

'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('practiceInfoService', practiceInfoService);

practiceInfoService.$inject = ['httpFactory', 'configurationConstants', 'constants'];

function practiceInfoService(httpFactory, configurationConstants, constants) {

    var practiceInfoService = this;
    var serviceUrl = constants.practiceInfoServiceUrl;

    practiceInfoService.getAttributes = function () {
        var url = serviceUrl + configurationConstants.getAttributes;
        var attributes = httpFactory.get(url);
        return attributes;
    }
    practiceInfoService.getClinicians = function () {
        var url = serviceUrl + configurationConstants.getClinicians;
        var clinicians = httpFactory.get(url);
        return clinicians;
    }
    practiceInfoService.getGroupBillingList = function () {
        var url = serviceUrl + configurationConstants.getGroupBillingList;
        var groupBillingList = httpFactory.get(url);
        return groupBillingList;
    }
    practiceInfoService.getIndividualBillingList = function () {
        var url = serviceUrl + configurationConstants.getIndividualBillingList;
        var individualBillingList = httpFactory.get(url);
        return individualBillingList;
    }
    practiceInfoService.saveGroupReporting = function (data) {
        var url = serviceUrl + configurationConstants.saveGroupReporting;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }
    practiceInfoService.saveIndividualReporting = function (data) {
        var url = serviceUrl + configurationConstants.saveIndividualReporting;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }
    practiceInfoService.createAuditLog = function (data) {
        var url = serviceUrl + configurationConstants.createAuditLog;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }
    practiceInfoService.GetUserDetails = function () {
        var url = serviceUrl + configurationConstants.getUsername;
        var userDetails = httpFactory.getUser(url);
        return userDetails;
    }
    practiceInfoService.GetUserRole = function () {
        var url = serviceUrl + configurationConstants.getUserRole;
        var userRoleDetails = httpFactory.getUser(url);
        return userRoleDetails;
    }
    //to set the screen name when clicking the back Button
    practiceInfoService.setScreenName = function (screenName) {
        practiceInfoService.screenName = screenName;
    }
    practiceInfoService.GetPracticeLogoPath = function () {
        var url = serviceUrl + configurationConstants.getPracticeLogoPath;
        var userDetails = httpFactory.getUser(url);
        return userDetails;
    }

    //getting the latest build version details
    practiceInfoService.GetBuildVersionDetails = function () {
        var url = serviceUrl + configurationConstants.getBuildDetails;
        var buildDetails = httpFactory.getUser(url);
        return buildDetails;
    }
    return practiceInfoService;
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').service('practiceLevelService', practiceLevelService);

practiceLevelService.$inject = ['httpFactory', 'constants', 'practiceLevelConstants'];

function practiceLevelService(httpFactory, constants, practiceLevelConstants) {
    var practiceLevelService = this;
    var serviceUrl = constants.dashBoardServiceUrl;

    practiceLevelService.setReportingObj = function (reportingObj) {
        practiceLevelService.reportingObject = reportingObj;
    }
    //to set the print object for practiceLevel DashBoard
    practiceLevelService.setPracticeLevelPrintDashBoardObj = function (printObj) {
        practiceLevelService.practiceLevelPrintObject = printObj;
    }
    //to set the screen name when clicking the back Button
    practiceLevelService.setScreenName = function (screenName) {
        practiceLevelService.screenName = screenName;
    }
    //
    practiceLevelService.setClinicianDashBoardObj = function (clinicinaDashBoardObj) {
        practiceLevelService.clinicianDashBoardObject = clinicinaDashBoardObj;
    }

    //TO GET THE LOGGED IN  user details
    practiceLevelService.getLoggedInUserDetails = function () {
        var url = serviceUrl + practiceLevelConstants.getLoggedInUserDetailsUrl;
        var loggedInUserData = httpFactory.get(url);
        return loggedInUserData;
    }
    //To get the practice details
    practiceLevelService.getPracticeDetails = function () {
        var url = serviceUrl + practiceLevelConstants.getPracticeDetailsUrl;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }
    //to set the Practice Details

    practiceLevelService.setPracticeDetails = function (practiceData) {
        practiceLevelService.practiceDetailsPrintObj = practiceData;
    }
    //to set the logged in user
    practiceLevelService.setLoggedInUser = function (userObject) {
        practiceLevelService.userObject = userObject;
    }

    //to get the clinicians and Groups
    practiceLevelService.getDashBoardDataForPractice = function (selectedYear) {
        // alert(selectedYear);
        //var url = serviceUrl + practiceLevelConstants.getPracticeLevelDashBoardDataUrl;
        var url = serviceUrl + practiceLevelConstants.getPracticeLevelDashBoardDataUrl + '/' + selectedYear;
        var practiceDashBoardData = httpFactory.get(url);
        return practiceDashBoardData;
    }
    //to get the ACI And CPIA scores for Specific  clinician
    practiceLevelService.getIndividualACICPIAScores = function (stageId, npi, tin, fromDate, toDate) {
        var url = serviceUrl + practiceLevelConstants.getIndividualAciAndCpiaScoreUrl + stageId + '/' + npi + '/' + tin
            + '/' + fromDate + '/' + toDate;
        var individualACIandCPIAScores = httpFactory.get(url);
        return individualACIandCPIAScores;
    }

    //to get the ACI And CPIA scores for Specific  group 
    practiceLevelService.getGroupACICPIAScores = function (stageId, tin, fromDate, toDate,selectedYear) {
        var url = serviceUrl + practiceLevelConstants.getGroupAciAndCpiaScoreUrl + stageId + '/' + tin
            + '/' + fromDate + '/' + toDate + '/'+ selectedYear;
        var groupACIandCPIAScores = httpFactory.get(url);
        return groupACIandCPIAScores;
    }

    //to get the ACI And CPIA scores for groupIndividual by GroupTIN and Individual NPI
    practiceLevelService.getGroupIndividualACICPIAScores = function (stageId, npi, tin, fromDate, toDate,selectedYear) {
        var url = serviceUrl + practiceLevelConstants.getGroupIndividualACICPIAScoresUrl + stageId + '/' + npi + '/' + tin
            + '/' + fromDate + '/' + toDate+'/'+selectedYear;
        var groupACIandCPIAScores = httpFactory.get(url);
        return groupACIandCPIAScores;
    }
    practiceLevelService.getActivityStartDatesOfGroup = function (tin) {
        var url = serviceUrl + practiceLevelConstants.getActivityStartDatesOfGroupUrl + tin;
        var datesListForGroup = httpFactory.get(url);
        return datesListForGroup;
    }

    practiceLevelService.getActivityStartDatesOfIndividual = function (npi, tin) {
        var url = serviceUrl + practiceLevelConstants.getActivityStartDatesOfIndividualUrl + npi + '/' + tin;
        var datesListForIndividual = httpFactory.get(url);
        return datesListForIndividual;
    }

    practiceLevelService.getCPIARegistryStartDates = function () {
        var url = serviceUrl + practiceLevelConstants.getCPIARegisgtsryStartDatesUrl;
        var registryStartDates = httpFactory.get(url);
        return registryStartDates;
    }

    practiceLevelService.updateACIReportingPeriod = function (aciReportingPeriodObj) {
        var url = serviceUrl + practiceLevelConstants.updateACIReportingPeriodUrl;
        var isSuccess = httpFactory.post(url, aciReportingPeriodObj);
        return isSuccess;
    }

    practiceLevelService.getActivityStartDatesOfIndependent = function (npi, tin) {
        var url = serviceUrl + practiceLevelConstants.getActivityStartDatesOfIndependentUrl + npi + '/' + tin;
        var datesListForIndividual = httpFactory.get(url);
        return datesListForIndividual;
    }
    practiceLevelService.LogUserAudit = function (userAuditData) {
        var url = constants.auditServiceUrl + practiceLevelConstants.CreateActionLog;
        var isSuccess = httpFactory.post(url, userAuditData);
        return isSuccess;
    }
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').service('anrService', anrService);

anrService.$inject = ['httpFactory', 'constants', 'anrMeasureConstants', 'muMeasureConstants'];

function anrService(httpFactory, constants, anrMeasureConstants, muMeasureConstants) {

    var anrService = this;
    var serviceUrl = constants.anrServiceUrl;

    function setDashBoardObject(dashBoardObj) {
        anrService.dashBoardObject = dashBoardObj;
    }

    anrService.getANRMeasurePatientsData = function (stageId, muStageId, fromDate, toDate, tin, npi, type) {
        var url = serviceUrl + anrMeasureConstants.getANRMeasurePatientsDataUrl + stageId + "/" + muStageId + '/' + fromDate + '/' + toDate + '/' + tin + '/' + npi + '/' + type;
        var muMeasureReport = httpFactory.get(url);
        return muMeasureReport;
    }

    anrService.GetVendorIsMRNValue = function () {
        var url = serviceUrl + anrMeasureConstants.getVendorIsMRNValue;
        var mrnValue = httpFactory.get(url);
        return mrnValue;
    }
}




'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('RealTimeExportService', dataExportService);

dataExportService.$inject = ['httpFactory'];
function dataExportService(dataExportFactory) {
    var service = {
        getData: getData,
        postData: postData,
        putData: putData,
    };

    return service;

    function getData(url, params) {
        return dataExportFactory.get(url, params);
    }
    function postData(url, params) {
        return dataExportFactory.post(url, params);
    }
    function putData(url, params) {
        return dataExportFactory.put(url, params);
    }
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('ScheduledExportService', schedulerConfigurationService);
schedulerConfigurationService.$inject = ['httpFactory'];
function schedulerConfigurationService(schedulerConfigurationFactory) {
    var service = {
        getData: getData,
        postData: postData,
        putData: putData,
    };
    return service;
    function getData(url, params) {
        return schedulerConfigurationFactory.get(url, params);
    }
    function postData(url, params) {
        return schedulerConfigurationFactory.post(url, params);
    }
    function putData(url, params) {
        return schedulerConfigurationFactory.put(url, params);
    }
}