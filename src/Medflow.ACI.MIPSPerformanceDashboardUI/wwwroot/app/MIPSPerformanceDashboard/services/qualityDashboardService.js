﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('qualityDashboardService', qualityDashboardService);

qualityDashboardService.$inject = ['httpFactory', 'QualityDashboardConstants', 'constants'];

function qualityDashboardService(httpFactory, QualityDashboardConstants, constants) {
    var qualityDashboardService = this;

    qualityDashboardService.GetReportingMeasuresDataGroup = function (data) {
        debugger;
        var url = constants.QualityDashboardUrl + QualityDashboardConstants.GetReportingMeasuresData;
        var result = httpFactory.post(url, data);
        return result;
    }

    qualityDashboardService.GetReportingMeasuresDataIndividual = function (data) {
        var url = constants.QualityDashboardUrl + QualityDashboardConstants.GetReportingMeasuresData;
        var result = httpFactory.post(url, data);
        return result;
    }

    qualityDashboardService.GetMonitoringMeasuresDataGroup = function (data) {
        var url = constants.QualityDashboardUrl + QualityDashboardConstants.GetMonitoringMeasuresData;
        var result = httpFactory.post(url, data);
        return result;
    }

    qualityDashboardService.GetImprovementScore = function (data) {
        var url = constants.QualityDashboardUrl + QualityDashboardConstants.GetImprovementScoreData;
        var result = httpFactory.post(url, data);
        return result;
    }

    qualityDashboardService.GetMonitoringMeasuresDataIndividual = function (data) {
        var url = constants.QualityDashboardUrl + QualityDashboardConstants.GetMonitoringMeasuresData;
        var result = httpFactory.post(url, data);
        return result;
    }

    qualityDashboardService.setReportingDetails = function (reportingDetails) {
        qualityDashboardService.reportingDetails = reportingDetails;
    }
}