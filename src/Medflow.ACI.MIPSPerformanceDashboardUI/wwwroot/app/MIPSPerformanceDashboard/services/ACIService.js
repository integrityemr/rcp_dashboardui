﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('aciService', aciService);

aciService.$inject = ['httpFactory', 'configurationConstants', 'constants'];

function aciService(httpFactory, configurationConstants, constants) {

    var aciService = this;
    var serviceUrl = constants.aciServiceUrl;

    aciService.getMeasuresList = function (year) {
        var url = serviceUrl + configurationConstants.getMeasuresList + year;
        var measures = httpFactory.get(url);
        return measures;
    }

    aciService.getSRAHistoryList = function (selectedYear) {
        var url = serviceUrl + configurationConstants.getSRAHistoryList + '/' + selectedYear;
        var history = httpFactory.get(url);
        return history;
    }
    aciService.createAuditLogACIConfiguration = function (data) {
        var url = serviceUrl + configurationConstants.createAuditLogACIConfiguration;
        var isSuccess = httpFactory.post(url, data);
        return isSuccess;
    }

    aciService.getACIConfigurationCountDetails = function () {
        var url = serviceUrl + configurationConstants.getACIConfigurationCountDetails;
        var countDetails = httpFactory.get(url);
        return countDetails;
    }

    aciService.getSRADate = function (selectedYear) {
        var url = serviceUrl + configurationConstants.getSRADate + '/' + selectedYear;;
        var sraDate = httpFactory.get(url);
        return sraDate;
    }

    aciService.ACIMeasureSetList = function (muStageId, configTypeId, individualId, groupId, selectedYear) {
        var url = serviceUrl + configurationConstants.ACIMeasureSetList + muStageId + '/' + configTypeId + '/' + individualId + '/' + groupId + '/' + selectedYear;
        var measureSetList = httpFactory.get(url);
        return measureSetList;
    }

    aciService.getACIMeasureSetListByTINAndNPI = function (npi, tin, stageId, cpiaType, selectedYear) {
        var url = serviceUrl + configurationConstants.ACIMeasureSetListByTINAndNPI + npi + '/' + tin + '/' + stageId + '/' + cpiaType + '/' + selectedYear;
        var measureSetList = httpFactory.get(url);
        return measureSetList;
    }

    aciService.createACIMeasureAndSRADate = function (requestObject) {
        var url = serviceUrl + configurationConstants.createACIMeasureAndSRADate;
        var created = httpFactory.post(url, requestObject);
        return created;
    }

    aciService.updateACIMeasures = function (requestObject) {
        var url = serviceUrl + configurationConstants.updateACIMeasures;
        var updated = httpFactory.put(url, requestObject);
        return updated;
    }

    aciService.getACICliniciansList = function () {
        var url = serviceUrl + configurationConstants.getACICliniciansList;
        var clinicians = httpFactory.get(url);
        return clinicians;
    }

    aciService.getGroupList = function (requestObject) {
        var url = serviceUrl + configurationConstants.getGroupList;
        var groups = httpFactory.get(url);
        return groups;
    }

    aciService.GetUserDetails = function () {
        var url = serviceUrl + configurationConstants.getUserDetails;
        var userDetails = httpFactory.get(url);
        return userDetails;
    }

    aciService.GetPracticeDetails = function () {
        var url = serviceUrl + configurationConstants.getPracticeDetails;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }

    aciService.enabledPublicHealthPoints = function (code) {
        var url = constants.getGriServiceUrl + configurationConstants.enabledPublicHealthPoints + code;
        var groups = httpFactory.get(url);
        return groups;
    }

    aciService.enableQualityRegistryPoints = function () {
        var url = constants.getGriServiceUrl + configurationConstants.enableQualityRegistryPoints;
        var userDetails = httpFactory.get(url);
        return userDetails;
    }


    aciService.eligibleCPIAPoints = function (CPIATypeId) {
        var url = constants.cpiaServiceUrl + configurationConstants.eligibleCPIAPoints + CPIATypeId;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }

    aciService.eligibleCPIAPointsforGroupsandIndividual = function (CPIATypeId, GroupId, IndividualId, NPI, TIN, selectedYear) {
        var url = constants.cpiaServiceUrl + configurationConstants.EligibleCPIAPointsforGroupsandIndividuals + CPIATypeId + '/' + GroupId + '/' + IndividualId + '/' + NPI + '/' + TIN + '/' + selectedYear;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }

    aciService.CreateAudit = function (AuditInfo) {
        var url = constants.auditServiceUrl + configurationConstants.CreateActionLogFromAuditReportProject;
        var created = httpFactory.post(url, AuditInfo);
        return true;
    }
    //to set the screen name when clicking the back Button
    aciService.setScreenName = function (screenName) {
        aciService.screenName = screenName;
    }
    aciService.getMeasureSetByCPIAType = function (cpiaType, selectedType, selectedClinicianId, selectedGroupId, tin, npi, selectedYear) {
        var url = constants.aciServiceUrl + configurationConstants.getMeasureSetIdByCPIATypeUrl + cpiaType + '/' + selectedType + '/' + selectedClinicianId + '/' + selectedGroupId + '/' + tin + '/' + npi + '/' + selectedYear;
        var stageId = httpFactory.get(url);
        return stageId;
    }
    aciService.auditForACI = function (AuditInfo) {
        var url = serviceUrl + configurationConstants.auditCPIAUrl;
        var created = httpFactory.post(url, AuditInfo);
        return created;
    }
    aciService.getCEHRT = function () {
        var url = serviceUrl + configurationConstants.getCEHRT;
        var cehrt = httpFactory.get(url);
        return cehrt;
    }
    aciService.deleteSRAHistory = function (id) {
        var url = serviceUrl + configurationConstants.deleteSRAHistory;
        var created = httpFactory.post(url, id);
        return created;
    }
}