﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('cpiaService', cpiaService);

cpiaService.$inject = ['httpFactory', 'configurationConstants', 'constants'];

function cpiaService(httpFactory, configurationConstants, constants) {

    var cpiaService = this;
    var serviceUrl = constants.cpiaServiceUrl;
    var qppUrl = constants.qppServiceUrl;
    cpiaService.activities = [];

   
    //To get the practice details
    cpiaService.getPracticeDetails = function (productKey) {
        var url = serviceUrl + configurationConstants.getPracticeDetailsUrl + productKey;
        var practiceDetails = httpFactory.get(url);
        return practiceDetails;
    }
    //to set the Practice Details

    cpiaService.setPracticeDetails = function (practiceData) {
        cpiaService.practiceDetailsPrintObj = practiceData;
    }

    //to set the screen name when clicking the back Button
    cpiaService.setScreenName = function (screenName) {
        cpiaService.screenName = screenName;
    }
    cpiaService.setPrintObject = function (printObject) {
        cpiaService.printObject = printObject;
    }
    cpiaService.setActivityObject = function (activityObj) {
        cpiaService.activityObject = activityObj;
    }
    cpiaService.getActivities = function (cpiaTypeId, groupReportingId, individualReportingId, selectedYear) {
    //    alert("cpiaTypeId" + cpiaTypeId + "groupReportingId" + groupReportingId + "individualReportingId" + individualReportingId + "selec tedyear" + selectedYear);
        var url = serviceUrl + configurationConstants.getActivitiesByType + cpiaTypeId + '/' + individualReportingId
            + '/' + groupReportingId + '/' + selectedYear;
        var activities = httpFactory.get(url);
        return activities;
    }
    cpiaService.getActivitiesByNPIandTIN = function (npi, tin, cpiaType, selectedYear) {
        var url = serviceUrl + configurationConstants.getActivitiesByNPIandTIN + npi + '/' + tin + '/' + cpiaType+ '/' + selectedYear;
        var activities = httpFactory.get(url);
        return activities;
    }

    cpiaService.getGroupList = function () {
        var url = serviceUrl + configurationConstants.getGroupList;
        var groups = httpFactory.get(url);
        return groups;
    }

    cpiaService.getCliniciansList = function () {
        var url = serviceUrl + configurationConstants.getCliniciansList;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }
    cpiaService.GetEntities = function () {
        debugger;
        var url = qppUrl + configurationConstants.getentitiesList;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }
    cpiaService.GetCategoriesList = function () {
        var url = qppUrl + configurationConstants.GetCategoriesList;
        var categories = httpFactory.get(url);
        return categories;
    }
    //get Qpp Audit Log
    cpiaService.getAuditLogData = function () {
        var url = qppUrl + configurationConstants.GetQppAuditLogData;
        var AuditLogData = httpFactory.get(url);
        return AuditLogData;
    }
    // post Qpp Audit Data
    cpiaService.saveQppAuditLogData = function (requestObject) {
        var url = qppUrl + configurationConstants.SaveQppAuditLogData;
        var AuditSavedData = httpFactory.post(url, requestObject);
        return AuditSavedData;
    }
    cpiaService.getCPIATypes = function () {
        var url = serviceUrl + configurationConstants.getCPIATypes;
        var cpiaTypes = httpFactory.get(url);
        return cpiaTypes;
    }

    cpiaService.getGroupDetailsForPrint = function (groupReportingId) {
        var url = serviceUrl + configurationConstants.getGroupDetailsForPrint + groupReportingId;
        var groupDetails = httpFactory.get(url);
        return groupDetails;
    }

    cpiaService.getReportingCount = function () {
        var url = serviceUrl + configurationConstants.getReportingCount;
        var reportingCount = httpFactory.get(url);
        return reportingCount;
    }

    cpiaService.getCliniciansForPrint = function (individualReportingId) {
        var url = serviceUrl + configurationConstants.getCliniciansForPrint + individualReportingId;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }

    cpiaService.updateCPIAActivities = function (requestObject) {
        var url = serviceUrl + configurationConstants.updateCPIAActivities;
        var reportingCount = httpFactory.put(url, requestObject);
        return reportingCount;
    }

    cpiaService.saveCPIAActivities = function (requestObject) {
        var url = serviceUrl + configurationConstants.saveCPIAActivities;
        var activities = httpFactory.post(url, requestObject);
        return activities;
    }

    cpiaService.copyCPIAActivities = function (requestObject) {
        var url = serviceUrl + configurationConstants.copyCPIAActivities;
        var activities = httpFactory.post(url, requestObject);
        return activities;
    }

    cpiaService.GetUserDetails = function () {
        var url = serviceUrl + configurationConstants.getUserDetails;
        var userDetails = httpFactory.get(url);
        return userDetails;
    }
    cpiaService.auditForCPIA = function (AuditInfo) {
        var url = serviceUrl + configurationConstants.auditCPIAUrl;
        var created = httpFactory.post(url, AuditInfo);
        return created;
    }

    //QPP Export related methods

    cpiaService.GetExportresults = function () {
        var url = qppUrl + configurationConstants.getqppExportData;
        var results = httpFactory.get(url);
        return results;
    }
    cpiaService.GetConfigStatus = function (data) {
        var url = qppUrl + configurationConstants.getConfigReportingStatusForQPPExport;
        var results = httpFactory.post(url, data);
        return results;
    }

    cpiaService.GetEntities = function () {
        var url = qppUrl + configurationConstants.getentitiesList;
        var clinitians = httpFactory.get(url);
        return clinitians;
    }
    cpiaService.GetCategoriesList = function () {
        var url = qppUrl + configurationConstants.GetCategoriesList;
        var categories = httpFactory.get(url);
        return categories;
    }

    cpiaService.QppExport = function (data) {
        var url = qppUrl + configurationConstants.qppExport;
        var exportjsons = httpFactory.post(url, data);
        return exportjsons;
    }

    cpiaService.QppExportStatusSave = function (data) {
        var url = qppUrl + configurationConstants.qppexportStatusSave;
        var result = httpFactory.post(url, data);
        return result;
    }
    cpiaService.GetGroupsQppExport = function (data) {
        var url = qppUrl + configurationConstants.getGroupList;
        var groups = httpFactory.get(url);
        return groups;
    }
    cpiaService.GetCliniciansQppExport = function (data) {
        var url = qppUrl + configurationConstants.getCliniciansList;
        var Clinicians = httpFactory.get(url);
        return Clinicians;
    }

}