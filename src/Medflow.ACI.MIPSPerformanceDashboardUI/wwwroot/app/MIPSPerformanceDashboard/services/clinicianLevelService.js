﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('clinicianLevelService', clinicianLevelService);

clinicianLevelService.$inject = ['httpFactory', 'constants', 'clinicianLevelConstants'];

function clinicianLevelService(httpFactory, constants, clinicianLevelConstants) {
    var clinicianLevelService = this;
    var serviceUrl = constants.dashBoardServiceUrl;

    //to get the Clinicians for the selected Group
    clinicianLevelService.getGroupsForClinician = function (measureCode, tin, npi, fromDate, toDate ,selectedYear) {
        var url = serviceUrl + clinicianLevelConstants.getGroupsForClinicianUrl + measureCode + '/' + tin
            + '/' + npi + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var groupList = httpFactory.get(url);
        return groupList;
    }
    //to get the Individual DashBoard data for the selected user
    clinicianLevelService.getClinicianMeasuresForIndividualDashBoard = function (userId) {
        var url = serviceUrl + clinicianLevelConstants.getClinicianMeasuresForIndividualDashBoardUrl + userId;
        var groupList = httpFactory.get(url);
        return groupList;
    }
    //to get the list of activities of that selected Clinician
    clinicianLevelService.getCPIAActivitiesForClinician = function (npi, tin, fromDate, toDate, selectedYear) {
        var url = serviceUrl + clinicianLevelConstants.getCPIADetailsForIndividualUrl + npi + '/' + tin + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var clinicianCPIAList = httpFactory.get(url);
        return clinicianCPIAList;
    }
    //to get the list of activities of that selected Group Clinician
    clinicianLevelService.getCPIAActivitiesForGroupClinician = function (npi, tin, fromDate, toDate, selectedYear) {
        var url = serviceUrl + clinicianLevelConstants.getCPIADetailsForGroupIndividualUrl + npi + '/' + tin + '/' + fromDate + '/' + toDate + '/' + selectedYear;
        var clinicianCPIAList = httpFactory.get(url);
        return clinicianCPIAList;
    }
    //set dashboard data for print
    clinicianLevelService.setClinicianLevelPrintDashBoardObj = function (clinicianObj, groupObj) {
        clinicianLevelService.clinicianPrintObj = clinicianObj;
        clinicianLevelService.groupPrintObj = groupObj;
    }
    //set CPIA dashBoard Data for print

    clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj = function (cpiaActivities, clinicianName, clinicianNPI, clinicianTIN, mipsCPIAScore, totalActivityPointsEarned, cpiaReportingPeriod) {
        clinicianLevelService.clinicianLevelCPIAObj = cpiaActivities;
        clinicianLevelService.clinicianName = clinicianName;
        clinicianLevelService.clinicinaNPI = clinicianNPI;
        clinicianLevelService.clinicianTIN = clinicianTIN;
        clinicianLevelService.mipsCPIAScore = mipsCPIAScore;
        clinicianLevelService.totalActivityPointsEarned = totalActivityPointsEarned;
        clinicianLevelService.cpiaReportingPeriod = cpiaReportingPeriod
    }

    //to set the screen name when clicking the back Button
    clinicianLevelService.setScreenName = function (screenName) {
        clinicianLevelService.screenName = screenName;
    }
}