﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').service('qualityConfigurationService', qualityConfigurationService);

qualityConfigurationService.$inject = ['httpFactory', 'qualityConfigurationConstants', 'constants'];

function qualityConfigurationService(httpFactory, qualityConfigurationConstants, constants) {
    var qualityConfigurationService = this;
    qualityConfigurationService.getPerformancYear = function () {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.GetPerformanceYear;
        var performanceYear = httpFactory.get(url);
        return performanceYear;
    }
    qualityConfigurationService.getreportType = function () {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.GetReportType;
        var reportType = httpFactory.get(url);
        return reportType;
    }
    qualityConfigurationService.GetMonitoringType = function () {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.GetMonitoringType;
        var MonitoringType = httpFactory.get(url);
        return MonitoringType;
    }
    qualityConfigurationService.GetQualityMeasureMasterData = function () {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.GetQualityMeasures;
        var QualityMeasure = httpFactory.get(url);
        return QualityMeasure;
    }
    qualityConfigurationService.SaveQualityMeasureData = function (param) {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.SaveQualityMeasures;
        var SaveQualityMeasure = httpFactory.post(url, param);
        return SaveQualityMeasure;
    }
    qualityConfigurationService.UpdateQualityMeasureData = function (param) {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.UpdateQualityMeasures;
        var UpdateQualityMeasure = httpFactory.post(url, param);
        return UpdateQualityMeasure;
    }
    qualityConfigurationService.GetQualityMeasureData = function (param) {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.GetSaveQualityMeasuresData;
        var UpdateQualityMeasure = httpFactory.post(url, param);
        return UpdateQualityMeasure;
    }
    qualityConfigurationService.GetQualityMeasureDecline = function () {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.GetQualityMeasureDecline;
        var QualityMeasureDecline = httpFactory.get(url);
        return QualityMeasureDecline;
    }
    qualityConfigurationService.CopyQualityMeasureData = function (param) {
        var url = constants.QualityConfigServiceUrl + qualityConfigurationConstants.CopyQualityMeasures;
        var CopyQualityMeasure = httpFactory.post(url, param);
        return CopyQualityMeasure;
    }

    
}