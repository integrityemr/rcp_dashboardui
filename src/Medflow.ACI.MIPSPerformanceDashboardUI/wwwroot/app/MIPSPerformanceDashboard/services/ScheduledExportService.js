﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('ScheduledExportService', schedulerConfigurationService);
schedulerConfigurationService.$inject = ['httpFactory'];
function schedulerConfigurationService(schedulerConfigurationFactory) {
    var service = {
        getData: getData,
        postData: postData,
        putData: putData,
    };
    return service;
    function getData(url, params) {
        return schedulerConfigurationFactory.get(url, params);
    }
    function postData(url, params) {
        return schedulerConfigurationFactory.post(url, params);
    }
    function putData(url, params) {
        return schedulerConfigurationFactory.put(url, params);
    }
}