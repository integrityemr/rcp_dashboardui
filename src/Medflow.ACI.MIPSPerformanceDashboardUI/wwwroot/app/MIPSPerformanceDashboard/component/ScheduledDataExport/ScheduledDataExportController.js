﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('ScheduledDataExportController', SchedulerConfigController);
SchedulerConfigController.$inject = ['$state', '$scope', 'constants', 'ScheduledExportconstants', 'ScheduledExportService', '$window', '$filter'];

function SchedulerConfigController($state, $scope, constants, schedulerConfigurationConstants, schedulerConfigurationService, $window, $filter) {
    var schedulerConfigCtrl = this;
    schedulerConfigCtrl.init = function () {
        schedulerConfigCtrl.startDate = new Date();
        schedulerConfigCtrl.stopDate = new Date();
        schedulerConfigCtrl.SchedulerLst();
    }


    schedulerConfigCtrl.SchedulerLst = function () { // method to Get List of Configured Schedulers        
        var url = constants.schedulerConfiguration + schedulerConfigurationConstants.GetConfiguredScheduler;
        schedulerConfigurationService.getData(url).then(function (res) {
            if (res.data != null) {
                if (res.data.schedulerConfig != null) {
                    schedulerConfigCtrl.SchLst = res.data.schedulerConfig;
                    if (res.data.schedulerConfig.length > 0) {
                        // schedulerConfigCtrl.SchedulerGrdView.data = res.data.schedulerConfig;
                        schedulerConfigCtrl.id = res.data.schedulerConfig[0].id;
                        schedulerConfigCtrl.isUpdateDissable = true;
                    }
                    else {
                        schedulerConfigCtrl.isUpdateDissable = false;
                    }
                }
                else
                    toastr.error("Practice was not configured.");
                if (res.data.masterTimeType != null) {
                    schedulerConfigCtrl.masterTimeType = res.data.masterTimeType;
                }
                else {
                    toastr.error("There is no data available for Destination Type.");
                }
            }
            else
                toastr.error("There is no data available for the selected practice.");
        }, function (res) {
            toastr.error("unable to load Scheduler details");
        });
    }

    schedulerConfigCtrl.init();
    schedulerConfigCtrl.ClearAllFields = function () {
        schedulerConfigCtrl.startDate = new Date();
        schedulerConfigCtrl.stopDate = new Date();
        schedulerConfigCtrl.scheduledTime = null;
        schedulerConfigCtrl.destinationPath = null;
        schedulerConfigCtrl.masterTimeTypeId = null;
    }

    schedulerConfigCtrl.saveSchl = function () {
        try {            
            var utcHours = new Date().getUTCHours();
            var data = {
                id: schedulerConfigCtrl.id,
                StartDateTime: $filter('date')(schedulerConfigCtrl.startDate.setHours(utcHours), 'medium'),
                StopDateTime: $filter('date')(schedulerConfigCtrl.stopDate.setHours(utcHours), 'medium'),
                ScheduledTime: $filter('date')(schedulerConfigCtrl.scheduledTime, 'HH:mm:ss'),
                TimeType: schedulerConfigCtrl.masterTimeTypeId,
                DestinationPath: schedulerConfigCtrl.destinationPath
            };            
            var url = constants.schedulerConfiguration + schedulerConfigurationConstants.SaveSchedulerConfig;
            schedulerConfigurationService.postData(url, data).then(function (res) {
                if (res.data != null) {
                    schedulerConfigCtrl.init();
                    toastr.success("Scheduled Export Configurations saved successfully");
                    schedulerConfigCtrl.id = null;
                    schedulerConfigCtrl.ClearAllFields();
                }
            }, function (res) {
                toastr.error("unable to save Scheduled Export Configurations");
            });
        }
        catch (e) {
            toastr.error("unable to save Scheduled Export Configurations");
        }
    }

    schedulerConfigCtrl.DestinationChange = function () {
        if (schedulerConfigCtrl.destinationTypeId != null) {
            schedulerConfigCtrl.masterFileType = null;
            schedulerConfigCtrl.masterTimeType = null;
            var url = constants.schedulerConfiguration + schedulerConfigurationConstants.GetFileAndTimeTyp + schedulerConfigCtrl.destinationTypeId;
            schedulerConfigurationService.getData(url).then(function (res) {
                if (res.data != null) {
                    if (res.data.masterFileType.length > 0)
                        schedulerConfigCtrl.masterFileType = res.data.masterFileType;
                    else
                        toastr.error("No File Type exits for selected destination type.");
                    if (res.data.masterTimeType.length > 0) {
                        schedulerConfigCtrl.masterTimeType = res.data.masterTimeType;
                    }
                    else {
                        toastr.error("No Time Type exits for selected destination type.");
                    }
                }
                else
                    toastr.error("There is no data available for the selected practice.");
            }, function (res) {
                toastr.error("unable to update Destination details");
            });
        }
    }

    schedulerConfigCtrl.deleteSchl = function (id) {
        try {
            var url = constants.schedulerConfiguration + schedulerConfigurationConstants.DeleteSchedulerConfig + id;
            schedulerConfigurationService.postData(url).then(function (res) {
                if (res.data != null) {
                    schedulerConfigCtrl.id = null;
                    schedulerConfigCtrl.init();
                    toastr.success("Removed successfully");
                }
            }, function (res) {
                toastr.error("unable to delete scheduler");
            });
        }
        catch (e) {
            toastr.error("unable to delete scheduler");
        }
    }

    schedulerConfigCtrl.selectSchl = function (data) {
        try {
            if (data.sch != null) {
                schedulerConfigCtrl.id = data.sch.id;
                schedulerConfigCtrl.startDate = new Date(data.sch.startDate);
                schedulerConfigCtrl.stopDate = new Date(data.sch.stopDate);
                schedulerConfigCtrl.scheduledTime = new Date(data.sch.scheduledTimeZone);
                schedulerConfigCtrl.destinationPath = data.sch.destinationPath;
                schedulerConfigCtrl.masterTimeTypeId = data.sch.timeType;
                schedulerConfigCtrl.isUpdateDissable = false;
            }
        }
        catch (e) {
        }
    }


    schedulerConfigCtrl.chkFileExsit = function (data) {
        try {
            if (schedulerConfigCtrl.SchLst.length > 0 && schedulerConfigCtrl.isUpdateDissable == true) {
                toastr.error("Cannot add  more than one scheduled export configuration.");
            }
        }
        catch (e) {
        }
    }

    function schedulerGrid() {
        schedulerConfigCtrl.SchedulerGrdView = {
            enableColumnMenus: false,
            multiSelect: false,
            enableSorting: true,
            enableFullRowSelection: true,
            gridMenuShowHideColumns: false,
            enableSelectAll: false,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            modifierKeysToMultiSelect: false,

            onRegisterApi: function (gridApi) {
                schedulerConfigCtrl.SchedulerGrdView.gridApi = gridApi;
                schedulerConfigCtrl.SchedulerGrdView.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    //griAdmin.clearGriDetails();
                    //griAdmin.getSingleGRIGridDetails(row.entity.id, row.entity.scheduleId);
                });
            }
        };

        schedulerConfigCtrl.SchedulerGrdView.columnDefs =
            [
               { name: 'id', displayName: "ID", visible: false, enableCellEdit: false },
               { name: 'startDate', displayName: "Start date", visible: true, cellTooltip: true },
               { name: 'stopDate', displayName: "End date", visible: true, cellTooltip: true },
               { name: 'scheduledTimeZone', displayName: "File Type", visible: true, cellTooltip: true },
               {
                   name: 'delete', displayName: '', width: 40, enableColumnMenu: false, enableHiding: false,
                   cellTemplate:
                   '<button class="btn btn-danger btn-xs btn-close" ng-click="$event.stopPropagation();grid.appScope.griAdmin.disableGriConfigurationById(row.entity.id,row.entity.registry,row.entity.scheduleId)"><i class="fa fa-close"></i></button>'
               }
            ];
        schedulerConfigCtrl.SchedulerGrdView.data = [];
    }

    function griAdminValidateTimeFormat() {
        var timestamphours = $('#timestamphours').val();
        if (timestamphours > 12)
            $('#timestamphours').val(null);
        var timestampminutes = $('#timestampminutes').val();
        if (timestampminutes > 59)
            $('#timestampminutes').val(null);
    }

    function griAdminTimeKeyDown(e) {
        if ((e.keyCode == 190 || e.keyCode == 110)) {
            e.preventDefault();
        }
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.key >= 0)) {
            if (e.srcElement.id == 'timestampminutes') {
                if (e.target.selectionStart == 0) {
                    if (e.key > 5) {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            else {
                if (e.target.selectionStart == 0) {
                    if (e.key > 1) {
                        e.preventDefault();
                        return false;
                    }
                }
                else if (e.target.selectionStart == 1) {
                    if (e.target.value == 1 && e.key > 2) {
                        e.preventDefault();
                        return false;
                    }
                    else if (e.target.value > 1) {
                        e.preventDefault();
                        return false;
                    }
                }
                else if (e.target.selectionStart == 2) {
                    e.preventDefault();
                    return false;
                }
            }
        }
        else if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}