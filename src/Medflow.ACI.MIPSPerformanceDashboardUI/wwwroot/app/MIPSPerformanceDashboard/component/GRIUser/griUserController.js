﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('griUserController', griUserController);
griUserController.$inject = ['$state', '$scope', 'uiGridConstants', 'GRIUserService', '$q', 'enums', '$filter', '$timeout', 'util', 'constants', 'griuserconfigurationConstants'];
function griUserController($state, $scope, uiGridConstants, GRIUserService, $q, enums, $filter, $timeout, util, constants, griuserconfigurationConstants) {
    var griUser = this;
    griUser.init = function () {
        var date = new Date();
        griUser.currentDate = new Date();
        griUser.yearEndDate = new Date(date.getFullYear(), 11, 31);
        griUser.yearStartDate = new Date(date.getFullYear(), 0, 1);
        griUser.reverseSort = false;
        griUser.gridPreviousData = [];
        griUser.configuredRecord = [];
        griUser.isIrisEnabled = true;
    }
    griUser.sortByName = function () {
        if (griUser.reverseSort) {
            griUser.reverseSort = false;
            return griUser.reverseSort = false;
        }
        else {
            griUser.reverseSort = true;
            return griUser.reverseSort = true;
        }
    }

    var promises = [
         GRIUserService.GetRegistryList(true)
    ];
    $q.all(promises).then(function (data) {
        griUser.gridPreviousData = [];
        griUser.gridNewData = [];
        griUser.gridPreviousData = angular.copy(data[0].data);
        griUser.RegistryListGrid = data[0].data;
        griUser.RegistryListGrid.map(function (RegistryList) {            
            RegistryList.isEnabled = !RegistryList.isEnabled;
            if (RegistryList.isEnabled == false) {
                RegistryList.fromDate = null;
                RegistryList.toDate = null;
                RegistryList.isIrisEnabled = true;
            }
            else if (RegistryList.destination === griuserconfigurationConstants.IRISLegacy || RegistryList.destination === griuserconfigurationConstants.IRISGRITechnology) {
                RegistryList.fromDate = griUser.yearStartDate;
                RegistryList.toDate = griUser.yearEndDate;
                RegistryList.isIrisEnabled = false;
                RegistryList.isEnabled = true;
            }
            else {
                RegistryList.isIrisEnabled = true;
            }
            griUser.gridNewData.push(RegistryList);
        });
    },
    function (error) {
        toastr.error("unable to load registry list");
    });

    griUser.disable = function (id) {
        toastr.error(id);
    }

    griUser.onDateChange = function (griUserObj) {
        griUserObj.isEnableSave = true;
    }

    griUser.setStartEndDates = function (griUserObj) {
        griUser.configuredRecord = [];
        griUserObj.isEnableSave = true;
        $filter('date')(griUser.gridPreviousData, 'yyyy-MM-dd');
        griUser.configuredRecord = $filter('filter')(griUser.gridPreviousData, { registryTypeCode: griUserObj.registryTypeCode, id: griUserObj.id });

        if (griUserObj.isEnabled === true) {

            if (griUserObj.fromDate === null || typeof griUserObj.fromDate === 'undefined') {
                if (griUserObj.destination === griuserconfigurationConstants.IRISLegacy || griUserObj.destination === griuserconfigurationConstants.IRISGRITechnology) {
                    griUserObj.fromDate = angular.copy(griUser.yearStartDate);
                    griUserObj.toDate = angular.copy(griUser.yearEndDate);
                }
                else {
                    griUserObj.fromDate = angular.copy(griUser.currentDate);
                    griUserObj.toDate = angular.copy(griUser.yearEndDate);
                }
            }
        }
        if (griUserObj.isEnabled === false) {
            griUserObj.fromDate = null;
            griUserObj.toDate = null;            
            //griUserObj.toDate = angular.copy(griUser.currentDate);
        }

    }

    griUser.setReadOnlyIfIrisEnabled = function (griUserObj) {        
        if (griUserObj.isIrisEnabled === true && griUserObj.isEnabled === true) {
            return false;
        }
        else {
           return true;
        }
    }

    griUser.auditValidation = function (oldValue, Newvalue, Message) {
        var returnmessage = "";
        if (oldValue !== Newvalue) {
            returnmessage = Message;
            return returnmessage;
        }
        else {
            return returnmessage;
        }
    };
    griUser.saveDetails = function (griUserObj) {
        if ((typeof griUserObj.fromDate === 'undefined' || griUserObj.fromDate === null || griUserObj.toDate === null ||
            typeof griUserObj.toDate === 'undefined') && griUserObj.isEnabled == true) {
            toastr.error('Please enter valid date');
            return false;
        }
        else if ((griUser.currentDate.getFullYear() != new Date(griUserObj.fromDate).getFullYear()) && griUserObj.isEnabled == true) {
            toastr.error("Please enter valid date");
            return false;
        }
        else if ((griUser.currentDate.getFullYear() != new Date(griUserObj.toDate).getFullYear()) && griUserObj.isEnabled == true) {
            toastr.error("Please enter valid date");
            return false;
        }

        else if ((griUserObj.fromDate > griUserObj.toDate) && griUserObj.isEnabled == true) {
            toastr.error("Please enter valid date");
            return false;
        }
        else if (($filter('date')(griUser.currentDate, "MM/dd/yyyy") > $filter('date')(griUserObj.fromDate, "MM/dd/yyyy")) && (!griUserObj.destination == griuserconfigurationConstants.IRISLegacy && !griUserObj.destination == griuserconfigurationConstants.IRISGRITechnology)) {
            toastr.error("Please enter valid date");
            return false;
        }
        else if ($filter('date')(griUserObj.fromDate, "MM/dd/yyyy") != ($filter('date')(griUser.yearStartDate, "MM/dd/yyyy")) && (griUserObj.destination == griuserconfigurationConstants.IRISLegacy || griUserObj.destination == griuserconfigurationConstants.IRISGRITechnology)) {
            toastr.error("Please enter valid date");
            return false;
        }      
        else {
            var saveobject =
           {
               Id: griUserObj.id,
               IsEnabled: !griUserObj.isEnabled,
               registryTypeCode: griUserObj.registryTypeCode,
               FromDate: $filter('date')(griUserObj.fromDate, 'yyyy-MM-dd'),
               ToDate: $filter('date')(griUserObj.toDate, 'yyyy-MM-dd'),
               StartDate: $filter('date')(griUserObj.startDate, 'yyyy-MM-dd'),
               EndDate: $filter('date')(griUserObj.endDate, 'yyyy-MM-dd')
           };

            //Audit Log start
            var auditLogMessage = "";

            griUser.configuredRecord = $filter('filter')(griUser.gridPreviousData, { registryTypeCode: griUserObj.registryTypeCode, id: griUserObj.id });

            auditLogMessage += " Updated " + griUser.configuredRecord[0].name;


            var oldauditIsEnabled = griUser.configuredRecord[0].isEnabled;
            var newauditIsEnabled = saveobject.IsEnabled;

            if (oldauditIsEnabled === true && newauditIsEnabled === false) {
                var newDate = new Date();
                saveobject.StartDate = $filter('date')(newDate, 'yyyy-MM-dd')

                //Adding and year for the End Date
                var aYearFromNow = new Date();
                aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
                saveobject.EndDate = $filter('date')(aYearFromNow, 'yyyy-MM-dd')

                auditLogMessage += " changed Registry from Disabled.";
            }


            if (oldauditIsEnabled === false && newauditIsEnabled === true) {

                auditLogMessage += " changed Registry from Enabled.";
            }

            else if (oldauditIsEnabled === false && newauditIsEnabled === false) {
                var auditStartDate = $filter('date')(griUser.configuredRecord[0].fromDate, 'yyyy-MM-dd');
                var auditEndDate = $filter('date')(griUser.configuredRecord[0].toDate, 'yyyy-MM-dd');
                auditLogMessage += griUser.auditValidation(auditStartDate, saveobject.FromDate, " changed StartDate from " + auditStartDate + ".");
                auditLogMessage += griUser.auditValidation(auditEndDate, saveobject.ToDate, " changed EndDate from " + auditEndDate + ".");
            }

            var currentDate = new Date();
            var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
            var Auditdetails = {
                UserName: "Admin",
                ScreenName: "GRI Dashboard",
                ActionType: "Change",
                Action: auditLogMessage,
                CreatedDate: ClientTime
            };
            //Audit Log End

            saveobject.UserAuditViewModel = Auditdetails;
            GRIUserService.UpdateRegistryDetails(saveobject).then(function () {
                toastr.success('Registry configurations saved successfully');
                GRIUserService.GetRegistryList(false).then(function (response) {
                    griUser.RegistryListGrid = "";
                    griUser.gridPreviousData = "";
                    griUser.RegistryListGrid = angular.copy(response.data);
                    griUser.RegistryListGrid.map(function (RegistryList) {
                        RegistryList.isEnabled = !RegistryList.isEnabled;
                    });
                    griUser.gridPreviousData = angular.copy(response.data);
                });
                griUserObj.isEnableSave = false;
            }, function (error) {
                toastr.error("Unable to Save the Details");
            });
        }
    }
    griUser.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}