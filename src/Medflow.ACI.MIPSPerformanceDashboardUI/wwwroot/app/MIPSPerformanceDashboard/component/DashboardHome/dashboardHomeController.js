﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('dashboardHomeController', dashboardHomeController);
dashboardHomeController.$inject = ['$state', '$scope', 'practiceInfoService', '$uibModal', '$filter', '$timeout'];
function dashboardHomeController($state, $scope, practiceInfoService, $uibModal, $filter, $timeout) {
    var dhPage = {}
    init();
    function init() {
        dhPage.username = '';
        dhPage.emergencyAccess = false;
        dhPage.mipsDashboard = false;
        dhPage.muMeasure = false;
        dhPage.griDashBoard = false;
        dhPage.configSettings = false;
        dhPage.qppExport = false;
        UserName();
        PracticeLogoPath();
    }
    function UserName() {
        practiceInfoService.GetUserDetails().then(function (res) {
            if (res) {
                dhPage.username = res.data.username;
                dhPage.emergencyAccess = res.data.emergencyAccess;
                localStorage.setItem("emergencyAccess", res.data.emergencyAccess);
                if (localStorage.getItem('emergencyAccess') != "true") {
                    UserRole();
                }
                else {
                    dhPage.mipsDashboard = true;
                    dhPage.muMeasure = true;
                    dhPage.griDashBoard = true;
                    dhPage.configSettings = true;
                    dhPage.qppExport = true;
                    UserRole();
                }
            }

        });
    }
    function UserRole() {
        practiceInfoService.GetUserRole().then(function (res) {
            if (res) {
                if (res.data.role === undefined || res.data.role === null || res.data.role === "") {
                    toastr.error('You are not authorized.');
                    $state.go('logout');
                }
                else {
                    if (res.data.role === "ADMINISTRATOR") {
                        dhPage.mipsDashboard = true;
                        dhPage.muMeasure = true;
                        dhPage.griDashBoard = true;
                        dhPage.configSettings = true; 
                        
                    }
                    else if (res.data.role === "MIPS ASSURANCE MANAGER") {
                        dhPage.mipsDashboard = true;
                        dhPage.muMeasure = true;
                        dhPage.griDashBoard = true;
                        dhPage.configSettings = true;
                        dhPage.qppExport = true;
                    }
                    else if (res.data.role === "ELIGIBLE CLINICIAN") {
                        dhPage.mipsDashboard = true;
                        dhPage.muMeasure = true;
                    }
                    else {
                        dhPage.mipsDashboard = true;
                        dhPage.muMeasure = true;
                    }
                }
            }

        });
    }

    function PracticeLogoPath() {
        practiceInfoService.GetPracticeLogoPath().then(function (res) {
            if (res) {
                dhPage.practiceLogoPath = res.data;
            }

        });
    }


    //to load the clock timer after 1 second (time interval)
    dhPage.interval = 1000;
    var timer = function () {
        var currentDateTime = $filter('date')(new Date(Date.now()), 'MM/dd/yyyy hh:mm:ss a');
        dhPage.clockTimer = currentDateTime;
        $timeout(timer, dhPage.interval);
    }
    $timeout(timer, dhPage.interval);

    //Method to display the help modal popup
    dhPage.HelpModal = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: true,
            windowTopClass: 'modalTop',
            templateUrl: 'helpModal.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                result: practiceInfoService.GetBuildVersionDetails().then(function (res) {
                    if (res) {
                        return res;
                    }
                })
            }
        });
    }
    return dhPage;
}

//for modal pop - events 
angular.module('aci.mipsPerformanceDashboard').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, result, $filter) {
    //binding the result - build version data 
    $scope.buildVersion = result.data.buildVersion;
    $scope.buildNumber = result.data.buildNumber;
    $scope.buildDate = $filter('date')(new Date(result.data.buildDate), "MM/dd/yyyy hh:mm:ss a")
    $scope.ok = function () {
        $uibModalInstance.dismiss('cancel');
    };
});