'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('CPIAConfigController', CPIAConfigController);
CPIAConfigController.$inject = ['$state', '$scope', 'cpiaService', 'practiceInfoService', '$q', 'configEnums', '$filter', '$timeout', 'util', 'aciService', 'actionTypeEnum', 'practiceLevelService'];
function CPIAConfigController($state, $scope, cpiaService, practiceInfoService, $q, enums, $filter, $timeout, util, aciService, actionTypeEnum, practiceLevelService) {
    var cpiaConfig = this;
    cpiaConfig.enums = enums;
    cpiaConfig.groupReportingId = 0;
    cpiaConfig.individualReportingId = 0;
    cpiaConfig.copyToList = [];
    cpiaConfig.selectedCopyToList = [];
    cpiaConfig.search = '';
    cpiaConfig.fromDate = null;
    cpiaConfig.toDate = null;
    $scope.submitted = false;
    cpiaConfig.isClinicianDropdownSelected = false;
    cpiaConfig.settings = {
        showCheckAll: false,
        showUncheckAll: false,
        scrollable: false
    };
    cpiaConfig.AuditFormDate = "";
    cpiaConfig.AuditToDate = "";
    cpiaConfig.showCompleteListButton = "+ Complete CPIA List";
    cpiaConfig.disableCompleteButton = false;
    cpiaConfig.cacheActivityObject = cpiaService.activityObject;

    cpiaConfig.init = function () {
        if (localStorage.getItem('emergencyAccess') == "true") {
            cpiaConfig.loadInit();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role != "ADMINISTRATOR") {
                        toastr.error('You are not authorized.');
                        $state.go('Home');
                    }
                    else {
                        cpiaConfig.loadInit();
                    }
                }

            });
        }
    };

    cpiaConfig.loadInit = function () {
        cpiaConfig.printDisabled = false;
        var date = new Date();

        cpiaConfig.currentDate = new Date();
        cpiaConfig.yearEndDate = new Date(date.getFullYear(), 11, 31);
        cpiaConfig.yearStartDate = new Date(date.getFullYear(), 0, 1);
        //cpiaConfig.updateToDate(cpiaConfig.fromDate);

        cpiaConfig.typeOptions = [
            { name: 'Group', value: enums.cpiaType.group },
            { name: 'Individual', value: enums.cpiaType.individual }
        ];

        var promises = [
            cpiaService.getGroupList(),
            cpiaService.getCliniciansList()
            //cpiaService.getReportingCount()
        ];

        $q.all(promises).then(function (data) {

            cpiaConfig.groups = data[0].data;
            cpiaConfig.groups.unshift({
                groupReportingId: 0,
                groupReportingName: 'Select Group',
            });

            cpiaConfig.clinicians = data[1].data;
            cpiaConfig.clinicians.unshift({
                physicianName: 'Select Clinician',
                individualReportingId: 0
            });
            cpiaConfig.cpiaRadioType = enums.cpiaType.simple;
            cpiaConfig.cpiaType = enums.cpiaType.simple;

            cpiaConfig.groupReportingId = cpiaConfig.groups[0].groupReportingId;
            //cpiaConfig.individualReportingId = cpiaConfig.clinicians[0].individualReportingId;
            cpiaConfig.selectedClinician = cpiaConfig.clinicians[0];
            cpiaConfig.loadActivitiesByType(cpiaConfig.cpiaType);

        }, function (error) {
            toastr.error("unable to load save CPIA configurations");
        });
    };

    cpiaConfig.loadActivitiesByType = function (cpiaType) {
        //cpiaConfig.isCheckAll = false;        
        var groupId = null;
        var individualId = null;
        cpiaConfig.cpiaType = cpiaType;
        if (cpiaType === enums.cpiaType.group) {
            groupId = cpiaConfig.groupReportingId;
            cpiaConfig.loadCPIAActivities(cpiaType, groupId, individualId)
        }
        else if (cpiaType === enums.cpiaType.individual) {
            if (cpiaConfig.type === 'Participant') {
                cpiaService.getActivitiesByNPIandTIN(cpiaConfig.npi, cpiaConfig.tin, cpiaType).then(function (response) {
                    cpiaConfig.defaultActivitiesData = angular.copy(response.data);
                    var printRecordsCount = $filter('filter')(cpiaConfig.defaultActivitiesData, { isEnabled: true })
                    if (printRecordsCount.length == 0) {
                        cpiaConfig.printDisabled = true;
                    }
                    else {
                        cpiaConfig.printDisabled = false;
                    }
                    //end  to enable and disabling the Print button 
                    cpiaConfig.activities = angular.copy(response.data);
                    var configuredRecord = $filter('filter')(cpiaConfig.activities, { isCPIAConfigured: true })[0];
                    if (typeof configuredRecord !== 'undefined') {
                        cpiaConfig.fromDate = new Date(configuredRecord.fromDate);
                        cpiaConfig.toDate = new Date(configuredRecord.toDate);
                    }
                    else {
                        cpiaConfig.fromDate = null;
                        cpiaConfig.toDate = null;
                    }
                    cpiaConfig.updateActivityData(cpiaConfig.activities);
                    cpiaConfig.ShowAllActivities();
                    cpiaConfig.updateCopyList();
                    cpiaConfig.getPrintDetails(cpiaType);
                }, function (error) {
                    toastr.error("unable to load CPIA activities");
                });
            }
            else {
                individualId = cpiaConfig.individualReportingId;
                cpiaConfig.loadCPIAActivities(cpiaType, groupId, individualId);
            }
        }
        else {
            cpiaConfig.loadCPIAActivities(cpiaType, groupId, individualId);
        }

    };

    cpiaConfig.loadCPIAActivities = function (cpiaType, groupId, individualId) {
        cpiaService.getActivities(cpiaType, groupId, individualId).then(function (response) {
            cpiaConfig.defaultActivitiesData = angular.copy(response.data);
            // start to enable and disabling the Print button 
            var printRecordsCount = $filter('filter')(cpiaConfig.defaultActivitiesData, { isEnabled: true })
            if (printRecordsCount.length == 0) {
                cpiaConfig.printDisabled = true;
            }
            else {
                cpiaConfig.printDisabled = false;
            }
            //end  to enable and disabling the Print button 
            cpiaConfig.activities = angular.copy(response.data);
            var configuredRecord = $filter('filter')(cpiaConfig.activities, { isCPIAConfigured: true })[0];
            if (typeof configuredRecord !== 'undefined') {
                cpiaConfig.fromDate = new Date(configuredRecord.fromDate);
                cpiaConfig.toDate = new Date(configuredRecord.toDate);
                cpiaConfig.existingFromDate = cpiaConfig.fromDate;
                cpiaConfig.existingToDate = cpiaConfig.toDate;
            }
            else {
                cpiaConfig.fromDate = null;
                cpiaConfig.toDate = null;
            }

            cpiaConfig.updateActivityData(cpiaConfig.activities);
            cpiaConfig.ShowAllActivities();
            cpiaConfig.updateCopyList();
            cpiaConfig.getPrintDetails(cpiaType);
        }, function () {
            toastr.error("unable to load save CPIA Activities");
        })
    }

    cpiaConfig.updateActivityData = function (activities) {
        angular.forEach(cpiaConfig.activities, function (item, key) {
            cpiaConfig.activities[key].isUpdated = false;
            cpiaConfig.activities[key].isDisabled = !cpiaConfig.activities[key].isEnabled;
        });

    };

    cpiaConfig.ShowAllActivities = function (showAll) {
        var count = $filter('filter')(cpiaConfig.activities, { isShortList: false, isComplete: true }).length;
        var shortListCount = $filter('filter')(cpiaConfig.activities, { isShortList: true }).length;

        var i = 0;
        angular.forEach(cpiaConfig.activities, function (value, key) {
            if (key >= 15) {
                if (value.isEnabled === true) {
                    i = i + 1;
                }
            }
        });
        if (i > 0) {
            cpiaConfig.activitiesLimit = cpiaConfig.activities.length;
            cpiaConfig.showAll = true;
            cpiaConfig.disableCompleteButton = true;
        }
        else {
            cpiaConfig.disableCompleteButton = false;
            cpiaConfig.activitiesLimit = shortListCount;
            cpiaConfig.showAll = false;
        }

        //if (count > 0) {
        //    cpiaConfig.activitiesLimit = cpiaConfig.activities.length;
        //    cpiaConfig.showAll = true;
        //    cpiaConfig.disableCompleteButton = true;

        //} else {
        //    cpiaConfig.activitiesLimit = shortListCount;
        //    cpiaConfig.showAll = false;
        //}
    };

    cpiaConfig.updateCopyList = function () {
        cpiaConfig.copyToList = [];
        cpiaConfig.selectedCopyToList = [];
        if (cpiaConfig.cpiaType === enums.cpiaType.group) {
            var IntialGroupReportingId = cpiaConfig.groups != null ? cpiaConfig.groups[0].groupReportingId : 0;
            cpiaConfig.groups.map(function (group) {
                if (group.groupReportingId !== cpiaConfig.groupReportingId && group.groupReportingId !== IntialGroupReportingId) {
                    cpiaConfig.copyToList.push({
                        id: group.groupReportingId, label: group.groupReportingName
                    });
                }
            });
        }

        if (cpiaConfig.cpiaType === enums.cpiaType.individual) {
            var IntialIndivisualReportingId = cpiaConfig.clinicians != null ? cpiaConfig.clinicians[0].individualReportingId : 0;
            cpiaConfig.clinicians.map(function (clinician) {
                if (clinician.uniqueId !== cpiaConfig.uniqueId && clinician.individualReportingId !== IntialIndivisualReportingId) {
                    cpiaConfig.copyToList.push({
                        id: clinician.uniqueId, label: clinician.physicianName
                    });
                }
            });
        }
    };
    
    cpiaConfig.getPrintDetails = function (cpiaType) {
        var groupDetails = {};
        var clinicianDetails = {};
        if (cpiaType === enums.cpiaType.group) {
            groupDetails = $filter('filter')(cpiaConfig.groups, { groupReportingId: cpiaConfig.groupReportingId })
            cpiaConfig.name = groupDetails[0].groupReportingName;
            cpiaConfig.groupAuditName = groupDetails[0].auditGroupName
            cpiaConfig.id = groupDetails[0].tin;
        } else if (cpiaType === enums.cpiaType.individual) {
            clinicianDetails = $filter('filter')(cpiaConfig.clinicians, { individualReportingId: cpiaConfig.individualReportingId })
            cpiaConfig.name = clinicianDetails[0].clinicianName;
            cpiaConfig.id = clinicianDetails[0].physicianNPI;
            cpiaConfig.clinicianAuditName = clinicianDetails[0].clinicianName + "-" + clinicianDetails[0].individualReportingId
        }
    };

    cpiaConfig.showActivitiesByType = function (cpiaType) {
        cpiaConfig.activitiesLimit = $filter('filter')(cpiaConfig.activities, { isShortList: true }).length;
        cpiaConfig.showCompleteListButton = "+ Complete CPIA List";
        cpiaConfig.name = '';
        cpiaConfig.id = '';
        if (cpiaType === enums.cpiaType.group) {
            cpiaConfig.individualReportingId = cpiaConfig.clinicians[0].individualReportingId;
        }
        cpiaConfig.loadActivitiesByType(cpiaType);
    };


    cpiaConfig.onClinicianDropDownChange = function (cpiaId) {
        cpiaConfig.isClinicianDropdownSelected = true;
        var aa = cpiaConfig.selectedClinician;
        cpiaConfig.individualReportingId = cpiaConfig.selectedClinician.individualReportingId;
        cpiaConfig.uniqueId = cpiaConfig.selectedClinician.uniqueId;
        cpiaConfig.type = cpiaConfig.selectedClinician.type;
        cpiaConfig.tin = cpiaConfig.selectedClinician.physicianTIN;
        cpiaConfig.npi = cpiaConfig.selectedClinician.physicianNPI;
        cpiaConfig.onTypeChange(cpiaId);
    }

    cpiaConfig.onTypeChange = function (cpiaId) {
        if (cpiaId == true)
            cpiaConfig.groupReportingId = cpiaConfig.groups != null ? cpiaConfig.groups[0].groupReportingId : null;
        if (cpiaConfig.cpiaType === enums.cpiaType.group) {
            cpiaConfig.isClinicianDropdownSelected = false;
        }
        if (!cpiaConfig.isClinicianDropdownSelected) {
            cpiaConfig.selectedClinician = cpiaConfig.clinicians[0];
        }
        cpiaConfig.showActivitiesByType(cpiaConfig.cpiaType);
    };

    cpiaConfig.filterOnEnter = function ($event) {
        if ($event.type === 'keydown' && $event.which === 13) {
            cpiaConfig.filterActivities();
        }
    };

    cpiaConfig.filterActivities = function () {
        if (cpiaConfig.search.length > 0) {
            cpiaConfig.activitiesLimit = cpiaConfig.activities.length;
            cpiaConfig.showAll = true;
        } else {
            var shortListCount = $filter('filter')(cpiaConfig.activities, { isShortList: true }).length;
            cpiaConfig.activitiesLimit = shortListCount;
            cpiaConfig.showAll = false;
        }
        cpiaConfig.query = angular.copy(cpiaConfig.search);
    };

    cpiaConfig.onFromDateChange = function () {
        cpiaConfig.updateToDate(cpiaConfig.fromDate);
    };

    cpiaConfig.updateToDate = function (fromDate) {
        if (fromDate === undefined || fromDate === null) { return false; }
        cpiaConfig.toDate = angular.copy(fromDate);

        var endofyear = new Date(fromDate.getFullYear(), 11, 31);
        var one_day = 1000 * 60 * 60 * 24;

        //Calculate difference btw the two dates, and convert to days
        var daysLeftinYear = Math.ceil((endofyear.getTime() - fromDate.getTime()) / (one_day));
        if (daysLeftinYear < 89) {
            cpiaConfig.toDate.setDate(fromDate.getDate() + daysLeftinYear);
        } else {
            cpiaConfig.toDate.setDate(fromDate.getDate() + 89);
        }
    };

    cpiaConfig.onActivityDateChange = function (activity) {
        //activity.isUpdated = true;
    };

    cpiaConfig.onActivityChange = function (activity) {
        if (activity.isEnabled && activity.activityStartDate === null) {
            activity.activityStartDate = new Date();
        } else if (!activity.isEnabled && activity.activityDiscontinueDate === null) {
            activity.activityDiscontinueDate = new Date();
        }

        activity.isDisabled = !activity.isEnabled;
        //activity.isUpdated = true;
    };

    //cpiaConfig.onCheckAll = function () {
    //    var activities = angular.copy(cpiaConfig.activities);
    //    if (cpiaConfig.isCheckAll) {
    //        activities.map(function (activity) {
    //            if (!activity.isCPIAConfigured) {
    //                activity.startDate = new Date();
    //                activity.isCPIAConfigured = true;
    //                activity.isUpdated = true;
    //            }
    //        });
    //    } else {
    //        activities.map(function (activity) {
    //            activity.discontinueDate = new Date();
    //            activity.isCPIAConfigured = false;
    //            activity.isUpdated = true;
    //        });
    //    }
    //    cpiaConfig.activities = activities;
    //    cpiaConfig.showAllActivities();
    //};

    cpiaConfig.copyActivities = function () {
        cpiaConfig.activities.map(function (value) {
            if (value.cpiaConfigurationId === 0 && !value.isEnabled && value.activityStartDate === null && value.activityDiscontinueDate === null) {
                value.isUpdated = false;
            }

            if (value.cpiaConfigurationId > 0 && (value.activityStartDate !== null || value.activityDiscontinueDate !== null)) {
                value.isUpdated = true;
            }

            if (value.isEnabled || (value.cpiaConfigurationId === 0 && value.activityStartDate !== null)) {
                value.isUpdated = true;
            }
        });

        var updatedActivities = $filter('filter')(cpiaConfig.activities, { isUpdated: true });
        if (updatedActivities.length === 0) {
            toastr.error("Please select an Activity");
            return false;
        }


        if (cpiaConfig.fromDate == "" || cpiaConfig.fromDate==undefined) {
            toastr.error("Please select reporting period dates");
            return false;
        }
        if (cpiaConfig.toDate == "" || cpiaConfig.toDate == undefined) {
            toastr.error("Please select reporting period dates");
            return false;
        }

        if (cpiaConfig.cpiaType == 2)//group
        {
            if (cpiaConfig.groupReportingId == 0) {
                toastr.error("Please select Group")
                return false;
            }
        }
        if (cpiaConfig.cpiaType == 1)//Individual
        {
            if (cpiaConfig.individualReportingId == 0) {
                toastr.error("Please select Individual")
                return false;
            }
        }

        var requestObject = [];
        var CopyrequestObject = [];
        var copyActivityList = [];
        var valid = true;
        var defaultActivities = angular.copy(cpiaConfig.defaultActivitiesData);
        var configuredActivities = $filter('filter')(cpiaConfig.activities, { isCPIAConfigured: true });

        configuredActivities.map(function (activity) {
            var result = $filter('filter')(defaultActivities, { cpiaListId: activity.cpiaListId }, true)[0];
            if (result.activityStartDate === null) {
                valid = false;
            } else {
                copyActivityList.push(result);
            }
        });

        if (!valid) {
            toastr.error("Activity start date can not be empty");
            return false;
        }

        cpiaConfig.selectedCopiedClinicians = [];


        cpiaConfig.clinicians.map(function (item) {
            cpiaConfig.selectedCopyToList.map(function (clinician) {
                if (item.uniqueId === clinician.id) {
                    cpiaConfig.selectedCopiedClinicians.push(item);
                }
            });
        });


        cpiaConfig.selectedCopiedClinicians.map(function (copyValue) {

            if (cpiaConfig.cpiaType === 1)//if individual
            {
                var response = cpiaConfig.updateActivityObject(copyValue.individualReportingId, copyActivityList);
                requestObject = response;
                requestObject.map(function (activityItem) {
                    activityItem.tin = copyValue.physicianTIN;
                    activityItem.npi = copyValue.physicianNPI;
                    activityItem.type = copyValue.type;
                })
                CopyrequestObject = CopyrequestObject.concat(requestObject);
            }

        });


        cpiaConfig.selectedCopyToList.map(function (copyValue) {
            if (cpiaConfig.cpiaType === 2)// if group 
            {
                var response = cpiaConfig.updateActivityObject(copyValue.id, copyActivityList);
                requestObject = requestObject.concat(response);
                CopyrequestObject = CopyrequestObject.concat(requestObject);
            }

        });

        cpiaConfig.selectedCopiedGroups = [];
        cpiaConfig.groups.map(function (mainGroup) {
            cpiaConfig.selectedCopyToList.map(function (group) {
                if (mainGroup.groupReportingId === group.id) {
                    cpiaConfig.selectedCopiedGroups.push(mainGroup);
                }
            })
        })



        cpiaConfig.copy(CopyrequestObject);
    };

    cpiaConfig.saveActivities = function () {
        if (cpiaConfig.cpiaType == 2)//group
        {
            if (cpiaConfig.groupReportingId == 0) {
                toastr.error("Please select Group")
                return false;
            }
        }
        if (cpiaConfig.cpiaType == 1)//Individual
        {
            if (cpiaConfig.individualReportingId == 0) {
                toastr.error("Please select Individual")
                return false;
            }
        }
        $scope.submitted = true;
        cpiaConfig.activities.map(function (value) {
            if (value.cpiaConfigurationId === 0 && !value.isEnabled && value.activityStartDate === null && value.activityDiscontinueDate === null) {
                value.isUpdated = false;
            }

            if (value.cpiaConfigurationId > 0 && (value.activityStartDate !== null || value.activityDiscontinueDate !== null)) {
                value.isUpdated = true;
            }

            if (value.isEnabled || (value.cpiaConfigurationId === 0 && value.activityStartDate !== null)) {
                value.isUpdated = true;
            }
        });

        var updatedActivities = $filter('filter')(cpiaConfig.activities, { isUpdated: true });
        if (updatedActivities.length === 0) {
            toastr.error("Please select an Activity");
            return false;
        }
        var reportingId = 0;
        var cpiaTypeId = (cpiaConfig.cpiaRadioType === enums.cpiaType.simple) ? enums.cpiaType.simple : cpiaConfig.cpiaType;

        if (cpiaTypeId === enums.cpiaType.group) {
            reportingId = cpiaConfig.groupReportingId;
        } if (cpiaTypeId === enums.cpiaType.individual) {
            if (cpiaConfig.type === 'Independent') {
                reportingId = cpiaConfig.individualReportingId;
            }
            else { //participant
                updatedActivities.map(function (activity) {
                    activity.tin = cpiaConfig.tin;
                    activity.npi = cpiaConfig.npi;
                    activity.type = cpiaConfig.type;
                })
            }
        }

        var validationObject = cpiaConfig.updateActivityObject(reportingId, updatedActivities, true);

        if (!validationObject.validationResponse.valid) {
            toastr.error(validationObject.validationResponse.validationMessage);
            return false;
        }
        cpiaConfig.save(validationObject.updatedActivities);
    };

    cpiaConfig.updateActivityObject = function (reportingId, activities, validationRequired) {
        var resultObject = {};
        var updatedActivities = angular.copy(activities);
        var response = { valid: true, message: '' };
        var warning = false;
        updatedActivities.map(function (value) {
            if (response.valid) {
                if (validationRequired) {
                    response = cpiaConfig.validateData(value);
                    if (!warning && response.valid && new Date(value.activityStartDate).toLocaleDateString() > new Date(cpiaConfig.fromDate).toLocaleDateString()) {
                        toastr.warning('Activity start date must be on/before the start of reporting period for receiving credit.');
                        warning = true;
                    }
                }
                //value.fromDate = $filter('date')(cpiaConfig.fromDate, "MM/dd/yyyy");
                //value.toDate = $filter('date')(cpiaConfig.toDate, "MM/dd/yyyy");


                value.fromDate = (cpiaConfig.fromDate !== null) ? new Date(cpiaConfig.fromDate) : null;
                if (value.fromDate != null) {
                    value.fromDate = $filter('date')(value.fromDate, 'yyyy-MM-dd');
                }
                value.toDate = (cpiaConfig.toDate !== null) ? new Date(cpiaConfig.toDate) : null;
                if (value.toDate != null) {
                    value.toDate = $filter('date')(value.toDate, 'yyyy-MM-dd');
                }


                if (cpiaConfig.existingFromDate != null && cpiaConfig.existingFromDate != "") {
                    cpiaConfig.existingFromDate = $filter('date')(cpiaConfig.existingFromDate, 'yyyy-MM-dd');
                }

                if (cpiaConfig.existingToDate != null && cpiaConfig.existingToDate != "") {
                    cpiaConfig.existingToDate = $filter('date')(cpiaConfig.existingToDate, 'yyyy-MM-dd');
                }

                //if (value.fromDate !== cpiaConfig.existingFromDate) {
                //    cpiaConfig.AuditToDate = "Reporting period ToDate has been changed from " + cpiaConfig.existingToDate + ".";
                // }
                if (value.fromDate !== cpiaConfig.existingFromDate || value.toDate !== cpiaConfig.existingToDate) {
                    cpiaConfig.existingFromDate = $filter('date')(new Date(cpiaConfig.existingFromDate), 'MM/dd/yyyy');
                    cpiaConfig.existingToDate = $filter('date')(new Date(cpiaConfig.existingToDate), 'MM/dd/yyyy');
                    if (cpiaConfig.cpiaType === enums.cpiaType.group) {
                        cpiaConfig.AuditFormDate = "Reporting period changed for Group-" + cpiaConfig.selectedGroupReportingName + "-FromDate-" + cpiaConfig.existingFromDate + "-ToDate-" + cpiaConfig.existingToDate + ".";
                    }
                    else if (cpiaConfig.cpiaType === enums.cpiaType.individual) {
                        cpiaConfig.AuditFormDate = "Reporting period changed for Clinician-" + cpiaConfig.selectedClinician.clinicianName + "-FromDate-" + cpiaConfig.existingFromDate + "-ToDate-" + cpiaConfig.existingToDate + ".";
                    }

                    else {
                        if (cpiaConfig.existingFromDate === undefined) {
                            cpiaConfig.existingFromDate = "";
                        }
                        if (cpiaConfig.existingToDate === undefined) {
                            cpiaConfig.existingToDate = "";
                        }
                        cpiaConfig.AuditFormDate = "Reporting period changed for Simple FromDate-" + cpiaConfig.existingFromDate + "-ToDate-" + cpiaConfig.existingToDate + ".";
                    }
                }

                value.AuditFormDate = cpiaConfig.AuditFormDate;
                value.AuditToDate = cpiaConfig.AuditToDate;
                cpiaConfig.existingFromDate = value.fromDate;
                cpiaConfig.existingToDate = value.toDate;
                cpiaConfig.AuditFormDate = "";
                value.activityStartDate = (value.activityStartDate !== null) ? new Date(value.activityStartDate) : null;

                if (value.activityStartDate != null) {
                    value.activityStartDate = $filter('date')(value.activityStartDate, 'yyyy-MM-dd');
                }


                value.activityDiscontinueDate = (value.activityDiscontinueDate !== null) ? new Date(value.activityDiscontinueDate) : null;
                if (value.activityDiscontinueDate != null) {
                    value.activityDiscontinueDate = $filter('date')(value.activityDiscontinueDate, 'yyyy-MM-dd');
                }


                value.cpiaTypeId = (cpiaConfig.cpiaRadioType === enums.cpiaType.simple) ? enums.cpiaType.simple : cpiaConfig.cpiaType;
                if (value.cpiaTypeId === enums.cpiaType.group) {
                    value.individualReportingId = 0;
                    value.groupReportingId = reportingId;
                    value.groupName = cpiaConfig.selectedGroupReportingName;
                    angular.forEach(cpiaConfig.groups, function (values, index) {
                        if (values.groupReportingId === value.groupReportingId) {
                            value.groupName = values.auditGroupName;
                        }
                    });
                } else if (value.cpiaTypeId === enums.cpiaType.individual) {
                    value.individualReportingId = reportingId;
                    value.groupReportingId = 0;
                    value.clinicianName = cpiaConfig.selectedClinician.physicianName;
                    angular.forEach(cpiaConfig.clinicians, function (val, index) {
                        if (val.individualReportingId === cpiaConfig.selectedClinician.individualReportingId) {
                            value.clinicianName = val.clinicianName +"-"+ cpiaConfig.selectedClinician.individualReportingId;
                        }
                    });
                } else {
                    value.individualReportingId = 0;
                    value.groupReportingId = 0;
                }
            }
        });

        if (validationRequired) {
            resultObject.updatedActivities = updatedActivities;
            resultObject.validationResponse = response;
            return resultObject;
        }

        return updatedActivities;
    }

    cpiaConfig.validateData = function (activity) {
        var message = 'success';
        var isValid = true;
        var currentDate = new Date();
        var fromdateInput = document.getElementById('reportingFromDate').text;
        if (cpiaConfig.fromDate === null ||
                cpiaConfig.toDate === null) {
            isValid = false;
            message = 'Please select reporting period dates';
            return { valid: isValid, validationMessage: message };
        }
        else if (typeof activity.activityStartDate === 'undefined'
                    ) {
            isValid = false;
            message = 'Invalid activity start date';
            return { valid: isValid, validationMessage: message };
        }

        else if (typeof activity.activityDiscontinueDate === 'undefined') {
            isValid = false;
            message = 'Invalid  activity discontinue date';
            return { valid: isValid, validationMessage: message };
        }
        else if (typeof cpiaConfig.fromDate === 'undefined') {
            isValid = false;
            message = 'Invalid reporting date';
            return { valid: isValid, validationMessage: message };

        }
        else if (typeof cpiaConfig.toDate === 'undefined') {
            isValid = false;
            message = 'Invalid reporting date';
            return {
                valid: isValid, validationMessage: message
            };
        }
        var startDate = (activity.activityStartDate !== null) ? new Date(activity.activityStartDate).toDateString() : null;
        var endDate = (activity.activityDiscontinueDate !== null) ? new Date(activity.activityDiscontinueDate).toDateString() : null;

        if (cpiaConfig.fromDate === null) {
            isValid = false;
            message = 'Please select reporting period dates';
        } else if (cpiaConfig.toDate === null) {
            isValid = false;
            message = 'Please select reporting period dates';
        } else if ((cpiaConfig.fromDate.getFullYear() !== cpiaConfig.toDate.getFullYear())) {
            isValid = false;
            message = 'Reporting period is not within the calendar year';
        } else if (util.dateDiff(cpiaConfig.fromDate, cpiaConfig.toDate) < 89) {
            isValid = false;
            message = 'Reporting period must be greater than or equal to 90 days';
        } else if (startDate === null) {
            isValid = false;
            message = 'Please enter the Activity start date. ';
        } else if (endDate !== null && new Date(endDate) < new Date(startDate)) {
            message = 'Activity discontinued date must be on/after the activity start date.';
            isValid = false;
        }
        else if (endDate !== null && new Date(startDate) > new Date(endDate)) {
            message = 'Activity discontinued date must be on/after the activity start date.';
            isValid = false;
        }

        return {
            valid: isValid, validationMessage: message
        };
    };

    cpiaConfig.copy = function (requestObject) {
        cpiaService.copyCPIAActivities(requestObject).then(function (response) {
            if (!response.data) {
                toastr.error("Please Save an Activity");
                return false
            }
            var message = "CPIA configurations copied successfully";
            toastr.success(message);
            $scope.submitted = false;
            cpiaConfig.loadActivitiesByType(cpiaConfig.cpiaType);

            cpiaConfig.groupNamesForAuditLog = "";
            cpiaConfig.selectedCopiedGroups.map(function (selectedGroup) {
                if (cpiaConfig.cpiaType === enums.cpiaType.group) {
                    if (cpiaConfig.groupNamesForAuditLog === "") {
                        cpiaConfig.groupNamesForAuditLog = selectedGroup.auditGroupName;
                    }
                    else {
                        cpiaConfig.groupNamesForAuditLog = cpiaConfig.groupNamesForAuditLog + "," + selectedGroup.auditGroupName;
                    }
                }
            })

            cpiaConfig.clinicianNamesForAuditLog = "";
            cpiaConfig.selectedCopiedClinicians.map(function (selectedClinician) {
                if (cpiaConfig.cpiaType === enums.cpiaType.individual) {
                    if (cpiaConfig.clinicianNamesForAuditLog === "") {
                        cpiaConfig.clinicianNamesForAuditLog = cpiaConfig.clinicianAuditName;
                    }
                    else {
                        cpiaConfig.clinicianNamesForAuditLog = cpiaConfig.clinicianNamesForAuditLog + "," + selectedClinician.clinicianName + "-" + selectedClinician.individualReportingId;
                    }
                }
            })

            if (cpiaConfig.cpiaType === enums.cpiaType.group) {
                cpiaConfig.action = "Copied CPIA Configurations for Group from " + cpiaConfig.groupAuditName + " to " + cpiaConfig.groupNamesForAuditLog;
            }
            if (cpiaConfig.cpiaType === enums.cpiaType.individual) {
                cpiaConfig.action = "Copied CPIA Configurations for  Clinician from " + cpiaConfig.selectedClinician.physicianName + " to " + cpiaConfig.clinicianNamesForAuditLog;
            }

            if (cpiaConfig.clinicianNamesForAuditLog !== "") {
                var data = {
                    Action: cpiaConfig.action,
                    ActionType: 'Copy'
                };
                cpiaService.auditForCPIA(data).then(function (res) {
                    if (res) {

                    }
                })
            }
            if (cpiaConfig.groupNamesForAuditLog !== "") {
                var data = {
                    Action: cpiaConfig.action,
                    ActionType: 'Copy'
                };
                cpiaService.auditForCPIA(data).then(function (res) {
                    if (res) {
                    }
                })
            }

        }, function (error) {
            var message = "unable to copy CPIA activities";
            if (error.data) {
                message = error.data.error.Message;
            }
            toastr.error(message);
        });
    };

    cpiaConfig.save = function (requestObject) {
        var count = 0;
        requestObject.map(function (item) {
            if (item.isEnabled === true) {
                count = count + 1;
            }
        });
        if (count === 0) {
            toastr.error("Please select an activity");
            return false;
        }
        else {
            cpiaService.saveCPIAActivities(requestObject).then(function () {
                var message = "CPIA configurations saved successfully";
                toastr.success(message);
                $scope.submitted = false;
                cpiaConfig.loadActivitiesByType(cpiaConfig.cpiaType);

            }, function (error) {
                var message = "unable to save CPIA activities";
                if (error.data) {
                    message = error.data.error.Message;
                }
                toastr.error(message);
            });
        }
    };

    cpiaConfig.showAllActivities = function (event) {
        var count = $filter('filter')(cpiaConfig.activities, {
            isShortList: false, isComplete: true
        }).length;
        var shortListCount = $filter('filter')(cpiaConfig.activities, {
            isShortList: true
        }).length;

        var i = 0;
        angular.forEach(cpiaConfig.activities, function (value, key) {
            if (key >= 15) {
                if (value.isEnabled === true) {
                    i = i + 1;
                }
            }
        });
        if (i > 0) {
            cpiaConfig.disableCompleteButton = true;
        }
        else {
            cpiaConfig.disableCompleteButton = false;
        }
        //if (count > 0) {
        //    cpiaConfig.disableCompleteButton = true;
        //}
        if (cpiaConfig.showCompleteListButton === "+ Complete CPIA List") {
            cpiaConfig.activitiesLimit = cpiaConfig.activities.length;
            cpiaConfig.showCompleteListButton = "- Complete CPIA List";
            cpiaConfig.showAll = true;
            cpiaConfig.SaveUserAuditLog(actionTypeEnum.ActionType.SelectActionType, "Complete CPIA List");
        } else {
            cpiaConfig.activitiesLimit = shortListCount;
            cpiaConfig.showCompleteListButton = "+ Complete CPIA List";
            cpiaConfig.showAll = false;
            cpiaConfig.SaveUserAuditLog(actionTypeEnum.ActionType.SelectActionType, "Minimised CPIA List");
        }
    };

    cpiaConfig.setFullCalender = function () {
        cpiaConfig.fromDate = angular.copy(cpiaConfig.yearStartDate);
        cpiaConfig.toDate = angular.copy(cpiaConfig.yearEndDate);
    };

    cpiaConfig.cancelActivities = function () {
        //cpiaConfig.activities = cpiaConfig.defaultActivitiesData;
        cpiaConfig.loadActivitiesByType(cpiaConfig.cpiaType);
        cpiaConfig.search = "";
        cpiaConfig.query = "";
    };


    cpiaConfig.print = function () {

        cpiaConfig.productkey = localStorage.getItem('ProductKey');

        var activities = $filter('filter')(cpiaConfig.defaultActivitiesData, {
            isEnabled: true
        });
        var printObject = {};
        printObject.data = activities;
        printObject.fromDate = cpiaConfig.fromDate;
        printObject.toDate = cpiaConfig.toDate;
        printObject.name = typeof cpiaConfig.name === 'undefined' ? '' : cpiaConfig.name;
        printObject.id = cpiaConfig.id;
        printObject.cpiaType = cpiaConfig.cpiaType;

        if (cpiaConfig.cpiaType === enums.cpiaType.group) {
            if (cpiaConfig.groupReportingId !== null || typeof cpiaConfig.groupReportingId !== 'undefined') {
                cpiaService.getGroupDetailsForPrint(cpiaConfig.groupReportingId).then(function (response) {
                    printObject.name = response.data[0].groupName;
                    printObject.id = response.data[0].tin;
                    printObject.groupReportingId = cpiaConfig.groupReportingId;
                }, function (error) {
                    toastr.error("unable to load group data for print");
                })
            }
        }
        else if (cpiaConfig.cpiaType === enums.cpiaType.individual) {
            if (cpiaConfig.individualReportingId !== null || typeof cpiaConfig.individualReportingId !== 'undefined') {
                cpiaService.getCliniciansForPrint(cpiaConfig.individualReportingId).then(function (response) {
                    printObject.name = response.data[0].clinicianName;
                    printObject.id = response.data[0].clinicianNPI;
                    printObject.individualReportingId = cpiaConfig.individualReportingId;
                }, function (error) {
                    toastr.error("unable to load clinicians data for print");
                })
            }
        }
        cpiaConfig.getPrintDetails(cpiaConfig.cpiaType);
        var promises = [
            cpiaService.GetUserDetails()

        ];

        $q.all(promises).then(function (data) {
            printObject.userName = data[0].data.userName;
            //printObject.practiceDetails = data[1].data;
            cpiaService.getPracticeDetails(cpiaConfig.productkey).then(function (response) {
                printObject.practiceDetails = response.data;
                cpiaService.setPrintObject(printObject);
                toastr.remove();
                cpiaConfig.printObject = printObject;
                cpiaConfig.printObject.fromDate = $filter('date')(printObject.fromDate, 'MM/dd/yyyy');
                cpiaConfig.printObject.toDate = $filter('date')(printObject.toDate, 'MM/dd/yyyy');
                cpiaConfig.userName = printObject.userName;
                cpiaConfig.cpiaType = printObject.cpiaType;
                cpiaConfig.individualReportingId = printObject.individualReportingId;
                cpiaConfig.groupReportingId = printObject.groupReportingId;
                window.setTimeout(function () {
                    cpiaConfig.printContents = $("#printActivitiesGrid").html();
                    var popupWin = window.open('', '_blank', 'width=1320,height=650');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + cpiaConfig.printContents + '</body></html>');
                    popupWin.document.close();

                }, 1);
            }, function (error) {
                toastr.error("unable getting practice details for print");
            })


        }, function (error) {
            toastr.error("unable to load practice details for print");
        })
        // commented temporarily while fixing Audit issues will uncomment
        // cpiaConfig.SaveUserAuditLog("CPIA in Printing");

        if (cpiaConfig.cpiaType === enums.cpiaType.group) {
            cpiaConfig.action = "Printed CPIA Configurations for Group " + cpiaConfig.groupAuditName;
        }
        if (cpiaConfig.cpiaType === enums.cpiaType.individual) {
            cpiaConfig.action = "Printed CPIA Configurations for  Clinician " + cpiaConfig.clinicianAuditName;
        }
        if (cpiaConfig.cpiaType === enums.cpiaType.simple) {
            cpiaConfig.action = "Printed CPIA Configurations for  Simple";
        }
        var data = {
            Action: cpiaConfig.action,
            ActionType: 'Print'
        };
        cpiaService.auditForCPIA(data).then(function (res) {
            if (res.data) {

            }

        }
    , function (error) {
        console.log("unable to save Audit ");
    })
    };

    // insert user auditlog while print/excel,csv export operations
    //cpiaConfig.SaveUserAuditLog = function (Action) {
    //    var currentDate = new Date();
    //    var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
    //    var data = {
    //        Action: Action,
    //    };
    //    cpiaService.auditForCPIA(data).then(function (res) {
    //        console.log(res.data);
    //    }, function (error) {
    //        console.log("server error occured for Audit Save");
    //    });
    //}

    cpiaConfig.getGroupReportingName = function (groupReportingId) {
        if (cpiaConfig.groups) {
            cpiaConfig.groups.map(function (group) {
                if (group.groupReportingId === groupReportingId) {
                    cpiaConfig.selectedGroupReportingName = group.groupReportingName;
                }
            })
        }
    }

    cpiaConfig.onAcitivityDesc = function (data) {
        cpiaConfig.ActivityDescData = data;
        $('#AcitivityDescModal').modal('show');
    }


    $scope.$watch('cpiaConfig.groupReportingId', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            cpiaConfig.getGroupReportingName(newVal);
        }
    });

    cpiaConfig.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
};
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('cpiaPrintActivitiesController', cpiaPrintActivitiesController);
cpiaPrintActivitiesController.$inject = ['$state', '$scope', '$stateParams', '$timeout', 'cpiaService', '$rootScope', '$filter', 'practiceLevelService'];
function cpiaPrintActivitiesController($state, $scope, $stateParams, $timeout, cpiaService, $rootScope, $filter, practiceLevelService) {
    var cpiaPrintCtrl = this;
    cpiaPrintCtrl.currentDate = Date.now();
    cpiaPrintCtrl.init = function () {
        $rootScope.print = true;
        cpiaPrintCtrl.printObject = cpiaService.printObject;
        cpiaPrintCtrl.printObject.fromDate = $filter('date')(cpiaPrintCtrl.printObject.fromDate, 'MM/dd/yyyy');
        cpiaPrintCtrl.printObject.toDate = $filter('date')(cpiaPrintCtrl.printObject.toDate, 'MM/dd/yyyy');
        cpiaPrintCtrl.userName = cpiaPrintCtrl.printObject.userName;
        $stateParams.cpiaType = cpiaPrintCtrl.printObject.cpiaType;
        $stateParams.individualReportingId = cpiaPrintCtrl.printObject.individualReportingId;
        $stateParams.groupReportingId = cpiaPrintCtrl.printObject.groupReportingId;
        var stateParams = $stateParams;
        cpiaService.setActivityObject(stateParams);
        cpiaPrintCtrl.printingDone = function () {
            $rootScope.print = false;
            $state.go("CPIAConfig");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printActivitiesGridButton').click();
            }, 100);
        })
    }
    cpiaPrintCtrl.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
};