﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupIndividualCPIAController', groupIndividualCPIAController);
groupIndividualCPIAController.$inject = ['$state', '$scope', '$filter', '$q', 'practiceLevelService', 'clinicianLevelService', 'practiceLevelConstants'];
function groupIndividualCPIAController($state, $scope, $filter, $q, practiceLevelService, clinicianLevelService, practiceLevelConstants) {
    var groupIndividualCPIA = this;
    groupIndividualCPIA.screenTitle = "groupIndividualCPIADashBoard";
    groupIndividualCPIA.model = {};
    groupIndividualCPIA.fromScreenName = localStorage.getItem('setScreenName');
    groupIndividualCPIA.cpiaActivities = [];
    groupIndividualCPIA.getReportingObjTogetBackData = [];
    groupIndividualCPIA.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('CPIAScoreObjectdata'));

    // groupIndividualCPIA.dashBoarData = practiceLevelService.reportingObject;
    groupIndividualCPIA.dashBoarData = groupIndividualCPIA.getReportingObjTogetBackData[0];
    var dateYearIndividual = $filter('date')(new Date(groupIndividualCPIA.dashBoarData.cpiaFromDate), 'yyyy');
    // dateYearIndividual = '2018';
    groupIndividualCPIA.yearValue = dateYearIndividual;
    groupIndividualCPIA.cpiaReportingPeriod = [];
    groupIndividualCPIA.setReportingObjTogetBackData = [];


    groupIndividualCPIA.init = function () {
        groupIndividualCPIA.clinicianName = groupIndividualCPIA.dashBoarData.name;
        groupIndividualCPIA.clinicianTIN = groupIndividualCPIA.dashBoarData.tin;
        groupIndividualCPIA.clinicianNPI = groupIndividualCPIA.dashBoarData.npi;
        groupIndividualCPIA.mipsCPIAScore = 0;
        groupIndividualCPIA.totalActivityPointsEarned = 0;
        groupIndividualCPIA.getCPIAActivitiesForgroupIndividual();
        groupIndividualCPIA.productkey = localStorage.getItem('ProductKey');
        //Checking with Group Id to identify whether the name is of Group or Physician.
        if (groupIndividualCPIA.dashBoarData.groupId != null && groupIndividualCPIA.dashBoarData.groupId != "") {
            groupIndividualCPIA.GroupClinicianAuditName = " - for Group: " + groupIndividualCPIA.clinicianName;
        }
        else {
            groupIndividualCPIA.GroupClinicianAuditName = " - for Clinician: " + groupIndividualCPIA.clinicianName;
        }
        groupIndividualCPIA.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashImpActivities + groupIndividualCPIA.GroupClinicianAuditName);
    }
    groupIndividualCPIA.goToDashBoard = function () {
        //get the screen name from the service and redirect to that screen
        if (localStorage.getItem('setScreenName') != null && localStorage.getItem('setScreenName') != "" && localStorage.getItem('setScreenName') != "undefined") {
            if (localStorage.getItem('setScreenName') == "PracticeLevelDashBoard")
                $state.go('practiceLevelDashBoard');
            else if (localStorage.getItem('setScreenName') == "GroupLevelDashBoard") {
                if (groupIndividualCPIA.dashBoarData.type === "Individual") {
                    groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.getReportingObjTogetBackData[1]);
                }
                groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.dashBoarData); // [0]

                practiceLevelService.setReportingObj(groupIndividualCPIA.setReportingObjTogetBackData);
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') == "ClinicianLevelDashBoard") {
                if (groupIndividualCPIA.dashBoarData.type === "Group") {
                    groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.getReportingObjTogetBackData[1]);
                }
                groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.dashBoarData); // [0]

                practiceLevelService.setReportingObj(groupIndividualCPIA.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
        }
        else {
            $state.go('practiceLevelDashBoard');
        }
    }

    groupIndividualCPIA.getCPIAActivitiesForgroupIndividual = function () {
        var clinicianId;
        var groupId;
        var fromDate;
        var toDate;
        var tin;
        var npi;
        fromDate = groupIndividualCPIA.dashBoarData.fromDate;
        toDate = groupIndividualCPIA.dashBoarData.toDate;
        groupIndividualCPIA.fromDate = $filter('date')(fromDate, 'MM-dd-yyyy');
        groupIndividualCPIA.toDate = $filter('date')(toDate, 'MM-dd-yyyy');
        tin = groupIndividualCPIA.dashBoarData.tin;
        npi = groupIndividualCPIA.dashBoarData.npi;

        var firstDate = new Date(groupIndividualCPIA.dashBoarData.cpiaFromDate);
        var secondDate = new Date(groupIndividualCPIA.dashBoarData.cpiaToDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        groupIndividualCPIA.cpiaReportingPeriod = {
            fromDate: $filter('date')(new Date(groupIndividualCPIA.dashBoarData.cpiaFromDate), 'MM/dd/yyyy'),
            toDate: $filter('date')(new Date(groupIndividualCPIA.dashBoarData.cpiaToDate), 'MM/dd/yyyy'),
            noOfDays: diffDays + 1
        };

        clinicianLevelService.getCPIAActivitiesForGroupClinician(npi, tin, groupIndividualCPIA.fromDate, groupIndividualCPIA.toDate, dateYearIndividual).then(function (response) {
            groupIndividualCPIA.cpiaActivities = response.data;
            if (groupIndividualCPIA.cpiaActivities.length !== 0) {
                groupIndividualCPIA.cpiaActivities.map(function (item) {
                    groupIndividualCPIA.totalActivityPointsEarned = groupIndividualCPIA.totalActivityPointsEarned + item.points;
                    if (item.name == 'Participating with an APM Entity') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                    else if (item.name == 'Participating with a PCMH') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                });
                if (groupIndividualCPIA.totalActivityPointsEarned > 40) {
                    groupIndividualCPIA.totalActivityPointsEarned = 40;
                }
                groupIndividualCPIA.mipsCPIAScore = parseFloat(groupIndividualCPIA.totalActivityPointsEarned) * 15 / 40;
                groupIndividualCPIA.mipsCPIAScore = parseFloat(groupIndividualCPIA.mipsCPIAScore).toFixed(2);
            }
            else {
                groupIndividualCPIA.mipsCPIAScore = 0;
                groupIndividualCPIA.totalActivityPointsEarned = 0;
                groupIndividualCPIA.toDate = null;
                groupIndividualCPIA.fromDate = null;
                groupIndividualCPIA.diffDays = null;
            }
        }, function (error) {
            toastr.error("error ocuured while getting Clinicians Activities");
        });

        if (diffDays === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            groupIndividualCPIA.cpiaReportingPeriod.fromDate = null;
            groupIndividualCPIA.cpiaReportingPeriod.toDate = null;
            groupIndividualCPIA.cpiaReportingPeriod.noOfDays = null;
        }

        if (groupIndividualCPIA.fromDate !== null && groupIndividualCPIA.toDate !== null) {
            groupIndividualCPIA.calculateReportingDays();
        }

        if (groupIndividualCPIA.dashBoarData.type === 'Independent') {
            clinicianId = groupIndividualCPIA.dashBoarData.clinicianId;
        }
    }

    groupIndividualCPIA.calculateReportingDays = function () {
        var timestamp1 = new Date(groupIndividualCPIA.toDate).getTime();
        var timestamp2 = new Date(groupIndividualCPIA.fromDate).getTime();
        var diff = timestamp1 - timestamp2
        var newDate = new Date(diff);
        var one_day = 1000 * 60 * 60 * 24;
        groupIndividualCPIA.reportingDuration = diff / one_day;
        groupIndividualCPIA.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    groupIndividualCPIA.print = function () {
        //set print Object
        clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj(groupIndividualCPIA.cpiaActivities,
            groupIndividualCPIA.clinicianName, groupIndividualCPIA.clinicianNPI, groupIndividualCPIA.clinicianTIN,
            groupIndividualCPIA.mipsCPIAScore, groupIndividualCPIA.totalActivityPointsEarned, groupIndividualCPIA.cpiaReportingPeriod);
        var promises = [
             practiceLevelService.getLoggedInUserDetails(),
             practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            groupIndividualCPIA.loggedInUserObj = data[0].data.userName;
            groupIndividualCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(groupIndividualCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(groupIndividualCPIA.practiceDetails);
            groupIndividualCPIA.practiceDetailsPrintObj = {};
            groupIndividualCPIA.currentDate = Date.now();
            groupIndividualCPIA.clinicianCPIAActivities = clinicianLevelService.clinicianLevelCPIAObj;
            groupIndividualCPIA.mipsCPIAScore = clinicianLevelService.mipsCPIAScore;
            groupIndividualCPIA.totalActivityPointsEarned = clinicianLevelService.totalActivityPointsEarned;

            groupIndividualCPIA.clinicianName = clinicianLevelService.clinicianName;
            groupIndividualCPIA.clinicianNpi = clinicianLevelService.clinicinaNPI;
            groupIndividualCPIA.cpiaReportingPeriod = clinicianLevelService.cpiaReportingPeriod;

            if (typeof groupIndividualCPIA.clinicianCPIAActivities !== 'undefined') {
                groupIndividualCPIA.clinicianCPIAActivities.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');
                });
            }

            groupIndividualCPIA.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj != null) {
                groupIndividualCPIA.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }

            window.setTimeout(function () {
                var printContents = $("#printGroupIndividualCPIADashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            groupIndividualCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + groupIndividualCPIA.GroupClinicianAuditName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }
    //Saving user audit details
    groupIndividualCPIA.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }


    groupIndividualCPIA.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });

}


