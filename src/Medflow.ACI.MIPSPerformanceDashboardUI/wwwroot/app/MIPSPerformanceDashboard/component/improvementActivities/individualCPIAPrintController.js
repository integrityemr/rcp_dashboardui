﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('individualCPIAPrintController', individualCPIAPrintController);
individualCPIAPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'clinicianLevelService'];
function individualCPIAPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, clinicianLevelService) {

    var individualLevelCPIAPrint = this;

    individualLevelCPIAPrint.init = function () {
        individualLevelCPIAPrint.practiceDetailsPrintObj = {};
        individualLevelCPIAPrint.currentDate = Date.now();
        individualLevelCPIAPrint.clinicianCPIAActivities = clinicianLevelService.clinicianLevelCPIAObj;
        individualLevelCPIAPrint.mipsCPIAScore = clinicianLevelService.mipsCPIAScore;
        individualLevelCPIAPrint.totalActivityPointsEarned = clinicianLevelService.totalActivityPointsEarned;

        individualLevelCPIAPrint.clinicianName = clinicianLevelService.clinicianName;
        individualLevelCPIAPrint.clinicianNpi = clinicianLevelService.clinicinaNPI;
        individualLevelCPIAPrint.cpiaReportingPeriod = clinicianLevelService.cpiaReportingPeriod;
        var dateYearIndividual = $filter('date')(new Date(individualLevelCPIAPrint.cpiaReportingPeriod.fromDate), 'yyyy');
       // dateYearIndividual = '2018';
        individualLevelCPIAPrint.yearValue = dateYearIndividual;

        if (typeof individualLevelCPIAPrint.clinicianCPIAActivities !== 'undefined') {
            individualLevelCPIAPrint.clinicianCPIAActivities.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');
            });
        }

        individualLevelCPIAPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            individualLevelCPIAPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        individualLevelCPIAPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("individualCPIAActivities");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printIndividualLevelCPIAGridButton').click();
            }, 100);
        })
    }
    individualLevelCPIAPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}