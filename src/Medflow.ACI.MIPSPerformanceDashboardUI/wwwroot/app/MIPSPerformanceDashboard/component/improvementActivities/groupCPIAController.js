﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupCPIAController', groupCPIAController);
groupCPIAController.$inject = ['$state', '$scope', '$q', '$filter', 'practiceLevelService', 'groupLevelService', 'muStageEnums', 'practiceLevelConstants'];
function groupCPIAController($state, $scope, $q, $filter, practiceLevelService, groupLevelService, muStageEnums, practiceLevelConstants) {
    var groupCPIA = this;
    groupCPIA.screenTitle = "GroupLevelCPIADashBoard";
    groupCPIA.mipsCPIAScore = 0;
    groupCPIA.PIBonus = false;
    groupCPIA.totalActivityPointsEarned = 0;
    groupCPIA.model = {};
    groupCPIA.getReportingObjTogetBackData = [];
    groupCPIA.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('CPIAScoreObjectdata'));
   //groupCPIA.dashBoarData = practiceLevelService.reportingObject[0];
    groupCPIA.dashBoarData = groupCPIA.getReportingObjTogetBackData[0];
    var dateYear = $filter('date')(new Date(groupCPIA.dashBoarData.cpiaFromDate), 'yyyy');
     groupCPIA.dateYear = $filter('date')(new Date(groupCPIA.dashBoarData.cpiaFromDate), 'yyyy');
    if (dateYear < 2019)
    {
        groupCPIA.PIBonus = true;
    }
    groupCPIA.yearValue = dateYear;
    
    groupCPIA.cpiaReportingPeriod = [];
    groupCPIA.setReportingObjTogetBackData = [];

    groupCPIA.init = function () {
        groupCPIA.groupName = groupCPIA.dashBoarData.name;
        groupCPIA.tin = groupCPIA.dashBoarData.tin;
        groupCPIA.getCPIAActivitiesForGroup();
        groupCPIA.productkey = localStorage.getItem('ProductKey');
        groupCPIA.AuditGroupName = groupCPIA.dashBoarData.name;
        groupCPIA.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Group level: " + groupCPIA.AuditGroupName);
    }

    groupCPIA.getCPIAActivitiesForGroup = function () {
        var groupId;
        var fromDate;
        var toDate;
        var tin;
        fromDate = $filter('date')(new Date(groupCPIA.dashBoarData.fromDate), 'yyyy-MM-dd');
        toDate = $filter('date')(new Date(groupCPIA.dashBoarData.toDate), 'yyyy-MM-dd');
        tin = groupCPIA.dashBoarData.tin;
        groupLevelService.getCPIAActivitiesForGroup(tin, fromDate, toDate, dateYear).then(function (response) {
            groupCPIA.cpiaActivities = response.data;
            groupCPIA.cpiaActivities.map(function (item) {
                groupCPIA.totalActivityPointsEarned = groupCPIA.totalActivityPointsEarned + item.points;
                if (item.name == 'Participating with an APM Entity') {
                    item.value = "";
                    item.scalingFactor = "";
                }
                else if (item.name == 'Participating with a PCMH') {
                    item.value = "";
                    item.scalingFactor = "";
                }
            });
            if (groupCPIA.totalActivityPointsEarned > 40) {
                groupCPIA.totalActivityPointsEarned = 40;

            }
            groupCPIA.mipsCPIAScore = (parseFloat(groupCPIA.totalActivityPointsEarned) * 15 / 40).toFixed(2);

        }, function (error) {
            toastr.error("error ocuured while getting Group Activities");
        });

        var firstDate = new Date(groupCPIA.dashBoarData.cpiaFromDate);
        var secondDate = new Date(groupCPIA.dashBoarData.cpiaToDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        groupCPIA.cpiaReportingPeriod = {
            fromDate: $filter('date')(new Date(groupCPIA.dashBoarData.cpiaFromDate), 'MM/dd/yyyy'),
            toDate: $filter('date')(new Date(groupCPIA.dashBoarData.cpiaToDate), 'MM/dd/yyyy'),
            noOfDays: diffDays + 1
        };

        if (typeof groupCPIA.cpiaActivities !== 'undefined') {
            if (groupCPIA.cpiaActivities.length !== 0) {
                groupCPIA.cpiaActivities.map(function (item) {
                    groupCPIA.totalActivityPointsEarned = groupCPIA.totalActivityPointsEarned + item.points;
                });
                if (groupCPIA.totalActivityPointsEarned > 40) {
                    groupCPIA.totalActivityPointsEarned = 40;

                }
                groupCPIA.mipsCPIAScore = (parseFloat(groupCPIA.totalActivityPointsEarned) * 15 / 40).toFixed(2);
            }
        }

        if (diffDays === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            groupCPIA.cpiaReportingPeriod.fromDate = null;
            groupCPIA.cpiaReportingPeriod.toDate = null;
            groupCPIA.cpiaReportingPeriod.noOfDays = null;
        }
    }
    groupCPIA.calculateReportingDays = function () {
        var timestamp1 = new Date(groupCPIA.toDate).getTime();
        var timestamp2 = new Date(groupCPIA.fromDate).getTime();
        var diff = timestamp1 - timestamp2
        var newDate = new Date(diff);
        var one_day = 1000 * 60 * 60 * 24;
        groupCPIA.reportingDuration = diff / one_day;
        groupCPIA.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    groupCPIA.print = function () {
        groupLevelService.setGroupCPIAPrintObj(groupCPIA.cpiaActivities, groupCPIA.mipsCPIAScore, groupCPIA.totalActivityPointsEarned,
            groupCPIA.dashBoarData.name, groupCPIA.dashBoarData.tin, groupCPIA.cpiaReportingPeriod);
        var promises = [
           practiceLevelService.getLoggedInUserDetails(),
          practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            groupCPIA.loggedInUserObj = data[0].data.userName;
            groupCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(groupCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(groupCPIA.practiceDetails);

            groupCPIA.practiceDetailsPrintObj = {};
            groupCPIA.groupCPIAActivities = {};
            groupCPIA.currentDate = Date.now();

            groupCPIA.groupCPIAActivities = groupLevelService.groupCPIAActivitiesList;
            groupCPIA.mipsCPIAScore = groupLevelService.mipsCPIAScore;
            groupCPIA.totalActivityPointsEarned = groupLevelService.totalActivityPointsEarned;

            groupCPIA.groupName = groupLevelService.groupName;
            groupCPIA.clinicianNpi = groupLevelService.groupTIN;
            groupCPIA.cpiaReportingPeriod = groupLevelService.cpiaReportingPeriod;

            if (typeof groupCPIA.groupCPIAActivities !== 'undefined') {
                groupCPIA.groupCPIAActivities.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                    muStageEnums.measureStages.map(function (measure) {
                        if (measure.measureCode === item.measureCode) {
                            if (count === 0) {
                                groupCPIA.measureStage = muStageEnums.measureStages[0].measureName;
                            }
                            count = count + 1;
                        }
                    })
                });
            }
          
            groupCPIA.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj != null) {
                groupCPIA.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            window.setTimeout(function () {
                var printContents = $("#printGroupLevelCPIADashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            groupCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Group level: " + groupCPIA.AuditGroupName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details for print');
        });
    }

    groupCPIA.backToPrevioudScreen = function () {
        debugger;
        if (localStorage.getItem('setScreenName') != null && localStorage.getItem('setScreenName') != "" && localStorage.getItem('setScreenName') != "undefined") {

            if (localStorage.getItem('setScreenName') == "PracticeLevelDashBoard")
                $state.go('practiceLevelDashBoard');
            else if (localStorage.getItem('setScreenName') == "GroupLevelDashBoard") {
                if (groupCPIA.dashBoarData.type === "Individual") {
                    groupCPIA.setReportingObjTogetBackData.push(groupCPIA.getReportingObjTogetBackData[1]);
                }
                groupCPIA.setReportingObjTogetBackData.push(groupCPIA.dashBoarData); // [0]
                practiceLevelService.setReportingObj(groupCPIA.setReportingObjTogetBackData);
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') == "ClinicianLevelDashBoard") {
                debugger;
                if (groupCPIA.dashBoarData.type === "Group") {
                    groupCPIA.setReportingObjTogetBackData.push(groupCPIA.getReportingObjTogetBackData[1]);
                }
                groupCPIA.setReportingObjTogetBackData.push(groupCPIA.dashBoarData); // [0]
                practiceLevelService.setReportingObj(groupCPIA.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
        }
        else {
            $state.go('practiceLevelDashBoard');
        }
    }
    //Saving user audit details
    groupCPIA.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }
    groupCPIA.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });

}