'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupCPIAController', groupCPIAController);
groupCPIAController.$inject = ['$state', '$scope', '$q', '$filter', 'practiceLevelService', 'groupLevelService', 'muStageEnums', 'practiceLevelConstants'];
function groupCPIAController($state, $scope, $q, $filter, practiceLevelService, groupLevelService, muStageEnums, practiceLevelConstants) {
    var groupCPIA = this;
    groupCPIA.screenTitle = "GroupLevelCPIADashBoard";
    groupCPIA.mipsCPIAScore = 0;
    groupCPIA.totalActivityPointsEarned = 0;
    groupCPIA.model = {};
    groupCPIA.getReportingObjTogetBackData = [];
    groupCPIA.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('CPIAScoreObjectdata'));
   //groupCPIA.dashBoarData = practiceLevelService.reportingObject[0];
    groupCPIA.dashBoarData = groupCPIA.getReportingObjTogetBackData[0];
    
    groupCPIA.cpiaReportingPeriod = [];
    groupCPIA.setReportingObjTogetBackData = [];
    var dateYear = $filter('date')(new Date(groupCPIA.dashBoarData.cpiaFromDate), 'yyyy');
    groupCPIA.init = function () {
        groupCPIA.groupName = groupCPIA.dashBoarData.name;
        groupCPIA.tin = groupCPIA.dashBoarData.tin;
        groupCPIA.getCPIAActivitiesForGroup();
        groupCPIA.productkey = localStorage.getItem('ProductKey');
        groupCPIA.AuditGroupName = groupCPIA.dashBoarData.name;
        groupCPIA.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Group level: " + groupCPIA.AuditGroupName);
    }

    groupCPIA.getCPIAActivitiesForGroup = function () {
        var groupId;
        var fromDate;
        var toDate;
        var tin;
        fromDate = $filter('date')(new Date(groupCPIA.dashBoarData.fromDate), 'yyyy-MM-dd');
        toDate = $filter('date')(new Date(groupCPIA.dashBoarData.toDate), 'yyyy-MM-dd');
        tin = groupCPIA.dashBoarData.tin;
        groupLevelService.getCPIAActivitiesForGroup(tin, fromDate, toDate, dateYear).then(function (response) {
            groupCPIA.cpiaActivities = response.data;
            groupCPIA.cpiaActivities.map(function (item) {
                groupCPIA.totalActivityPointsEarned = groupCPIA.totalActivityPointsEarned + item.points;
                if (item.name == 'Participating with an APM Entity') {
                    item.value = "";
                    item.scalingFactor = "";
                }
                else if (item.name == 'Participating with a PCMH') {
                    item.value = "";
                    item.scalingFactor = "";
                }
            });
            if (groupCPIA.totalActivityPointsEarned > 40) {
                groupCPIA.totalActivityPointsEarned = 40;

            }
            groupCPIA.mipsCPIAScore = (parseFloat(groupCPIA.totalActivityPointsEarned) * 15 / 40).toFixed(2);

        }, function (error) {
            toastr.error("error ocuured while getting Group Activities");
        });

        var firstDate = new Date(groupCPIA.dashBoarData.cpiaFromDate);
        var secondDate = new Date(groupCPIA.dashBoarData.cpiaToDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        groupCPIA.cpiaReportingPeriod = {
            fromDate: $filter('date')(new Date(groupCPIA.dashBoarData.cpiaFromDate), 'MM/dd/yyyy'),
            toDate: $filter('date')(new Date(groupCPIA.dashBoarData.cpiaToDate), 'MM/dd/yyyy'),
            noOfDays: diffDays + 1
        };

        if (typeof groupCPIA.cpiaActivities !== 'undefined') {
            if (groupCPIA.cpiaActivities.length !== 0) {
                groupCPIA.cpiaActivities.map(function (item) {
                    groupCPIA.totalActivityPointsEarned = groupCPIA.totalActivityPointsEarned + item.points;
                });
                if (groupCPIA.totalActivityPointsEarned > 40) {
                    groupCPIA.totalActivityPointsEarned = 40;

                }
                groupCPIA.mipsCPIAScore = (parseFloat(groupCPIA.totalActivityPointsEarned) * 15 / 40).toFixed(2);
            }
        }

        if (diffDays === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            groupCPIA.cpiaReportingPeriod.fromDate = null;
            groupCPIA.cpiaReportingPeriod.toDate = null;
            groupCPIA.cpiaReportingPeriod.noOfDays = null;
        }
    }
    groupCPIA.calculateReportingDays = function () {
        var timestamp1 = new Date(groupCPIA.toDate).getTime();
        var timestamp2 = new Date(groupCPIA.fromDate).getTime();
        var diff = timestamp1 - timestamp2
        var newDate = new Date(diff);
        var one_day = 1000 * 60 * 60 * 24;
        groupCPIA.reportingDuration = diff / one_day;
        groupCPIA.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    groupCPIA.print = function () {
        groupLevelService.setGroupCPIAPrintObj(groupCPIA.cpiaActivities, groupCPIA.mipsCPIAScore, groupCPIA.totalActivityPointsEarned,
            groupCPIA.dashBoarData.name, groupCPIA.dashBoarData.tin, groupCPIA.cpiaReportingPeriod);
        var promises = [
           practiceLevelService.getLoggedInUserDetails(),
          practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            groupCPIA.loggedInUserObj = data[0].data.userName;
            groupCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(groupCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(groupCPIA.practiceDetails);

            groupCPIA.practiceDetailsPrintObj = {};
            groupCPIA.groupCPIAActivities = {};
            groupCPIA.currentDate = Date.now();

            groupCPIA.groupCPIAActivities = groupLevelService.groupCPIAActivitiesList;
            groupCPIA.mipsCPIAScore = groupLevelService.mipsCPIAScore;
            groupCPIA.totalActivityPointsEarned = groupLevelService.totalActivityPointsEarned;

            groupCPIA.groupName = groupLevelService.groupName;
            groupCPIA.clinicianNpi = groupLevelService.groupTIN;
            groupCPIA.cpiaReportingPeriod = groupLevelService.cpiaReportingPeriod;

            if (typeof groupCPIA.groupCPIAActivities !== 'undefined') {
                groupCPIA.groupCPIAActivities.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                    muStageEnums.measureStages.map(function (measure) {
                        if (measure.measureCode === item.measureCode) {
                            if (count === 0) {
                                groupCPIA.measureStage = muStageEnums.measureStages[0].measureName;
                            }
                            count = count + 1;
                        }
                    })
                });
            }
          
            groupCPIA.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj != null) {
                groupCPIA.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            window.setTimeout(function () {
                var printContents = $("#printGroupLevelCPIADashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            groupCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Group level: " + groupCPIA.AuditGroupName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details for print');
        });
    }

    groupCPIA.backToPrevioudScreen = function () {
        debugger;
        if (localStorage.getItem('setScreenName') != null && localStorage.getItem('setScreenName') != "" && localStorage.getItem('setScreenName') != "undefined") {

            if (localStorage.getItem('setScreenName') == "PracticeLevelDashBoard")
                $state.go('practiceLevelDashBoard');
            else if (localStorage.getItem('setScreenName') == "GroupLevelDashBoard") {
                if (groupCPIA.dashBoarData.type === "Individual") {
                    groupCPIA.setReportingObjTogetBackData.push(groupCPIA.getReportingObjTogetBackData[1]);
                }
                groupCPIA.setReportingObjTogetBackData.push(groupCPIA.dashBoarData); // [0]
                practiceLevelService.setReportingObj(groupCPIA.setReportingObjTogetBackData);
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') == "ClinicianLevelDashBoard") {
                debugger;
                if (groupCPIA.dashBoarData.type === "Group") {
                    groupCPIA.setReportingObjTogetBackData.push(groupCPIA.getReportingObjTogetBackData[1]);
                }
                groupCPIA.setReportingObjTogetBackData.push(groupCPIA.dashBoarData); // [0]
                practiceLevelService.setReportingObj(groupCPIA.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
        }
        else {
            $state.go('practiceLevelDashBoard');
        }
    }
    //Saving user audit details
    groupCPIA.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }
    groupCPIA.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });

}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupIndividualCPIAController', groupIndividualCPIAController);
groupIndividualCPIAController.$inject = ['$state', '$scope', '$filter', '$q', 'practiceLevelService', 'clinicianLevelService', 'practiceLevelConstants'];
function groupIndividualCPIAController($state, $scope, $filter, $q, practiceLevelService, clinicianLevelService, practiceLevelConstants) {
    var groupIndividualCPIA = this;
    groupIndividualCPIA.screenTitle = "groupIndividualCPIADashBoard";
    groupIndividualCPIA.model = {};
    groupIndividualCPIA.fromScreenName = localStorage.getItem('setScreenName');
    groupIndividualCPIA.cpiaActivities = [];
    groupIndividualCPIA.getReportingObjTogetBackData = [];
    groupIndividualCPIA.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('CPIAScoreObjectdata'));

    // groupIndividualCPIA.dashBoarData = practiceLevelService.reportingObject;
    groupIndividualCPIA.dashBoarData = groupIndividualCPIA.getReportingObjTogetBackData[0];
    groupIndividualCPIA.cpiaReportingPeriod = [];
    groupIndividualCPIA.setReportingObjTogetBackData = [];


    groupIndividualCPIA.init = function () {
        groupIndividualCPIA.clinicianName = groupIndividualCPIA.dashBoarData.name;
        groupIndividualCPIA.clinicianTIN = groupIndividualCPIA.dashBoarData.tin;
        groupIndividualCPIA.clinicianNPI = groupIndividualCPIA.dashBoarData.npi;
        groupIndividualCPIA.mipsCPIAScore = 0;
        groupIndividualCPIA.totalActivityPointsEarned = 0;
        groupIndividualCPIA.getCPIAActivitiesForgroupIndividual();
        groupIndividualCPIA.productkey = localStorage.getItem('ProductKey');
        //Checking with Group Id to identify whether the name is of Group or Physician.
        if (groupIndividualCPIA.dashBoarData.groupId != null && groupIndividualCPIA.dashBoarData.groupId != "") {
            groupIndividualCPIA.GroupClinicianAuditName = " - for Group: " + groupIndividualCPIA.clinicianName;
        }
        else {
            groupIndividualCPIA.GroupClinicianAuditName = " - for Clinician: " + groupIndividualCPIA.clinicianName;
        }
        groupIndividualCPIA.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashImpActivities + groupIndividualCPIA.GroupClinicianAuditName);
    }
    groupIndividualCPIA.goToDashBoard = function () {
        //get the screen name from the service and redirect to that screen
        if (localStorage.getItem('setScreenName') != null && localStorage.getItem('setScreenName') != "" && localStorage.getItem('setScreenName') != "undefined") {
            if (localStorage.getItem('setScreenName') == "PracticeLevelDashBoard")
                $state.go('practiceLevelDashBoard');
            else if (localStorage.getItem('setScreenName') == "GroupLevelDashBoard") {
                if (groupIndividualCPIA.dashBoarData.type === "Individual") {
                    groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.getReportingObjTogetBackData[1]);
                }
                groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.dashBoarData); // [0]

                practiceLevelService.setReportingObj(groupIndividualCPIA.setReportingObjTogetBackData);
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') == "ClinicianLevelDashBoard") {
                if (groupIndividualCPIA.dashBoarData.type === "Group") {
                    groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.getReportingObjTogetBackData[1]);
                }
                groupIndividualCPIA.setReportingObjTogetBackData.push(groupIndividualCPIA.dashBoarData); // [0]

                practiceLevelService.setReportingObj(groupIndividualCPIA.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
        }
        else {
            $state.go('practiceLevelDashBoard');
        }
    }

    groupIndividualCPIA.getCPIAActivitiesForgroupIndividual = function () {
        var clinicianId;
        var groupId;
        var fromDate;
        var toDate;
        var tin;
        var npi;
        fromDate = groupIndividualCPIA.dashBoarData.fromDate;
        toDate = groupIndividualCPIA.dashBoarData.toDate;
        groupIndividualCPIA.fromDate = $filter('date')(fromDate, 'MM-dd-yyyy');
        groupIndividualCPIA.toDate = $filter('date')(toDate, 'MM-dd-yyyy');
        tin = groupIndividualCPIA.dashBoarData.tin;
        npi = groupIndividualCPIA.dashBoarData.npi;

        var firstDate = new Date(groupIndividualCPIA.dashBoarData.cpiaFromDate);
        var secondDate = new Date(groupIndividualCPIA.dashBoarData.cpiaToDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        groupIndividualCPIA.cpiaReportingPeriod = {
            fromDate: $filter('date')(new Date(groupIndividualCPIA.dashBoarData.cpiaFromDate), 'MM/dd/yyyy'),
            toDate: $filter('date')(new Date(groupIndividualCPIA.dashBoarData.cpiaToDate), 'MM/dd/yyyy'),
            noOfDays: diffDays + 1
        };

        clinicianLevelService.getCPIAActivitiesForGroupClinician(npi, tin, groupIndividualCPIA.fromDate, groupIndividualCPIA.toDate, dateYearIndividual).then(function (response) {
            groupIndividualCPIA.cpiaActivities = response.data;
            if (groupIndividualCPIA.cpiaActivities.length !== 0) {
                groupIndividualCPIA.cpiaActivities.map(function (item) {
                    groupIndividualCPIA.totalActivityPointsEarned = groupIndividualCPIA.totalActivityPointsEarned + item.points;
                    if (item.name == 'Participating with an APM Entity') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                    else if (item.name == 'Participating with a PCMH') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                });
                if (groupIndividualCPIA.totalActivityPointsEarned > 40) {
                    groupIndividualCPIA.totalActivityPointsEarned = 40;
                }
                groupIndividualCPIA.mipsCPIAScore = parseFloat(groupIndividualCPIA.totalActivityPointsEarned) * 15 / 40;
                groupIndividualCPIA.mipsCPIAScore = parseFloat(groupIndividualCPIA.mipsCPIAScore).toFixed(2);
            }
            else {
                groupIndividualCPIA.mipsCPIAScore = 0;
                groupIndividualCPIA.totalActivityPointsEarned = 0;
                groupIndividualCPIA.toDate = null;
                groupIndividualCPIA.fromDate = null;
                groupIndividualCPIA.diffDays = null;
            }
        }, function (error) {
            toastr.error("error ocuured while getting Clinicians Activities");
        });

        if (diffDays === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            groupIndividualCPIA.cpiaReportingPeriod.fromDate = null;
            groupIndividualCPIA.cpiaReportingPeriod.toDate = null;
            groupIndividualCPIA.cpiaReportingPeriod.noOfDays = null;
        }

        if (groupIndividualCPIA.fromDate !== null && groupIndividualCPIA.toDate !== null) {
            groupIndividualCPIA.calculateReportingDays();
        }

        if (groupIndividualCPIA.dashBoarData.type === 'Independent') {
            clinicianId = groupIndividualCPIA.dashBoarData.clinicianId;
        }
    }

    groupIndividualCPIA.calculateReportingDays = function () {
        var timestamp1 = new Date(groupIndividualCPIA.toDate).getTime();
        var timestamp2 = new Date(groupIndividualCPIA.fromDate).getTime();
        var diff = timestamp1 - timestamp2
        var newDate = new Date(diff);
        var one_day = 1000 * 60 * 60 * 24;
        groupIndividualCPIA.reportingDuration = diff / one_day;
        groupIndividualCPIA.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    groupIndividualCPIA.print = function () {
        //set print Object
        clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj(groupIndividualCPIA.cpiaActivities,
            groupIndividualCPIA.clinicianName, groupIndividualCPIA.clinicianNPI, groupIndividualCPIA.clinicianTIN,
            groupIndividualCPIA.mipsCPIAScore, groupIndividualCPIA.totalActivityPointsEarned, groupIndividualCPIA.cpiaReportingPeriod);
        var promises = [
             practiceLevelService.getLoggedInUserDetails(),
             practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            groupIndividualCPIA.loggedInUserObj = data[0].data.userName;
            groupIndividualCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(groupIndividualCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(groupIndividualCPIA.practiceDetails);
            groupIndividualCPIA.practiceDetailsPrintObj = {};
            groupIndividualCPIA.currentDate = Date.now();
            groupIndividualCPIA.clinicianCPIAActivities = clinicianLevelService.clinicianLevelCPIAObj;
            groupIndividualCPIA.mipsCPIAScore = clinicianLevelService.mipsCPIAScore;
            groupIndividualCPIA.totalActivityPointsEarned = clinicianLevelService.totalActivityPointsEarned;

            groupIndividualCPIA.clinicianName = clinicianLevelService.clinicianName;
            groupIndividualCPIA.clinicianNpi = clinicianLevelService.clinicinaNPI;
            groupIndividualCPIA.cpiaReportingPeriod = clinicianLevelService.cpiaReportingPeriod;

            if (typeof groupIndividualCPIA.clinicianCPIAActivities !== 'undefined') {
                groupIndividualCPIA.clinicianCPIAActivities.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');
                });
            }

            groupIndividualCPIA.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj != null) {
                groupIndividualCPIA.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }

            window.setTimeout(function () {
                var printContents = $("#printGroupIndividualCPIADashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            groupIndividualCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + groupIndividualCPIA.GroupClinicianAuditName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }
    //Saving user audit details
    groupIndividualCPIA.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }


    groupIndividualCPIA.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });

}



'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupIndividualCPIAPrintController', groupIndividualCPIAPrintController);
groupIndividualCPIAPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'clinicianLevelService'];
function groupIndividualCPIAPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, clinicianLevelService) {
    var groupIndividualCPIAPrint = this;
    groupIndividualCPIAPrint.init = function () {
        groupIndividualCPIAPrint.practiceDetailsPrintObj = {};
        groupIndividualCPIAPrint.currentDate = Date.now();
        groupIndividualCPIAPrint.clinicianCPIAActivities = clinicianLevelService.clinicianLevelCPIAObj;
        groupIndividualCPIAPrint.mipsCPIAScore = clinicianLevelService.mipsCPIAScore;
        groupIndividualCPIAPrint.totalActivityPointsEarned = clinicianLevelService.totalActivityPointsEarned;

        groupIndividualCPIAPrint.clinicianName = clinicianLevelService.clinicianName;
        groupIndividualCPIAPrint.clinicianNpi = clinicianLevelService.clinicinaNPI;
        groupIndividualCPIAPrint.cpiaReportingPeriod = clinicianLevelService.cpiaReportingPeriod;

        if (typeof groupIndividualCPIAPrint.clinicianCPIAActivities !== 'undefined') {
            groupIndividualCPIAPrint.clinicianCPIAActivities.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');
            });
        }

        groupIndividualCPIAPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            groupIndividualCPIAPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        groupIndividualCPIAPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("groupIndividualCPIAActivities");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printIndividualLevelCPIAGridButton').click();
            }, 100);
        })
    }
    groupIndividualCPIAPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('individualCPIAController', individualCPIAController);
individualCPIAController.$inject = ['$state', '$scope', '$filter', '$q', 'practiceLevelService', 'clinicianLevelService', 'practiceLevelConstants', '$rootScope'];
function individualCPIAController($state, $scope, $filter, $q, practiceLevelService, clinicianLevelService, practiceLevelConstants, $rootScope) {
    var individualCPIA = this;
    individualCPIA.screenTitle = "IndividualCPIADashBoard";
    individualCPIA.model = {};
    individualCPIA.fromScreenName = localStorage.getItem('setScreenName');
    individualCPIA.getReportingObjTogetBackData = [];
    individualCPIA.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('CPIAScoreObjectdata'));

    individualCPIA.cpiaActivities = [];
    //individualCPIA.dashBoarData = practiceLevelService.reportingObject[0];
    //individualCPIA.dashBoarData = practiceLevelService.reportingObject;
    individualCPIA.dashBoarData = individualCPIA.getReportingObjTogetBackData[0];
    individualCPIA.cpiaReportingPeriod = [];
    individualCPIA.setReportingObjTogetBackData = [];


    individualCPIA.init = function () {
        individualCPIA.clinicianName = individualCPIA.dashBoarData.name;
        individualCPIA.clinicianTIN = individualCPIA.dashBoarData.tin;
        individualCPIA.clinicianNPI = individualCPIA.dashBoarData.npi;
        individualCPIA.mipsCPIAScore = 0;
        individualCPIA.totalActivityPointsEarned = 0;
        individualCPIA.getCPIAActivitiesForIndividual();
        individualCPIA.productkey = localStorage.getItem('ProductKey');
        individualCPIA.AuditClinicianName = individualCPIA.dashBoarData.name;
        //not logging audit data when the page is refreshed again after clicking Print button.
        if(sessionStorage.getItem("IndividualCPIAAction") != "printed")
            individualCPIA.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Individual level: " + individualCPIA.AuditClinicianName);
        sessionStorage.setItem("IndividualCPIAAction", "viewed");
    }
    individualCPIA.goToDashBoard = function () {
        //get the screen name from the service and redirect to that screen
        if (localStorage.getItem('setScreenName') != null && localStorage.getItem('setScreenName') != "" && localStorage.getItem('setScreenName') != "undefined") {
            if (localStorage.getItem('setScreenName') == "PracticeLevelDashBoard")
                $state.go('practiceLevelDashBoard');
            else if (localStorage.getItem('setScreenName') == "GroupLevelDashBoard") {
                if (individualCPIA.dashBoarData.type === "Individual") {
                    individualCPIA.setReportingObjTogetBackData.push(individualCPIA.getReportingObjTogetBackData[1]);
                }
                individualCPIA.setReportingObjTogetBackData.push(individualCPIA.dashBoarData); // [0]

                //practiceLevelService.setReportingObj(individualCPIA.setReportingObjTogetBackData);
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') == "ClinicianLevelDashBoard") {
                if (individualCPIA.dashBoarData.type === "Group") {
                    individualCPIA.setReportingObjTogetBackData.push(individualCPIA.getReportingObjTogetBackData[1]);
                }
                individualCPIA.setReportingObjTogetBackData.push(individualCPIA.dashBoarData); // [0]

                //practiceLevelService.setReportingObj(individualCPIA.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
            localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(individualCPIA.setReportingObjTogetBackData));

        }
        else {
            $state.go('practiceLevelDashBoard');
        }
    }

    individualCPIA.getCPIAActivitiesForIndividual = function () {
        var clinicianId;
        var groupId;
        var fromDate;
        var toDate;
        var tin;
        var npi;
        fromDate = individualCPIA.dashBoarData.cpiaFromDate;
        toDate = individualCPIA.dashBoarData.cpiatoDate;
        individualCPIA.fromDate = $filter('date')(fromDate, 'MM-dd-yyyy');
        individualCPIA.toDate = $filter('date')(toDate, 'MM-dd-yyyy');
        tin = individualCPIA.dashBoarData.tin;
        npi = individualCPIA.dashBoarData.npi;

        var firstDate = new Date(individualCPIA.dashBoarData.cpiaFromDate);
        var secondDate = new Date(individualCPIA.dashBoarData.cpiaToDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        individualCPIA.cpiaReportingPeriod = {
            fromDate: $filter('date')(new Date(individualCPIA.dashBoarData.cpiaFromDate), 'MM/dd/yyyy'),
            toDate: $filter('date')(new Date(individualCPIA.dashBoarData.cpiaToDate), 'MM/dd/yyyy'),
            noOfDays: diffDays + 1
        };
        
        if (individualCPIA.fromDate !== null && individualCPIA.toDate !== null) {
            individualCPIA.calculateReportingDays();
        }
        
        if (individualCPIA.dashBoarData.type === 'Independent') {
            clinicianId = individualCPIA.dashBoarData.clinicianId;
        }

        clinicianLevelService.getCPIAActivitiesForClinician(npi, tin, individualCPIA.fromDate, individualCPIA.toDate).then(function (response) {
            individualCPIA.cpiaActivities = response.data

            if (individualCPIA.cpiaActivities.length !== 0) {
                individualCPIA.cpiaActivities.map(function (item) {
                    individualCPIA.totalActivityPointsEarned = individualCPIA.totalActivityPointsEarned + item.points;
                    if (item.name == 'Participating with an APM Entity') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                    else if (item.name == 'Participating with a PCMH') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                });
                if (individualCPIA.totalActivityPointsEarned > 40) {
                    individualCPIA.totalActivityPointsEarned = 40;
                }
                individualCPIA.mipsCPIAScore = parseFloat(individualCPIA.totalActivityPointsEarned) * 15 / 40;
                individualCPIA.mipsCPIAScore = parseFloat(individualCPIA.mipsCPIAScore).toFixed(2);
            }
            else {
                individualCPIA.mipsCPIAScore = 0;
                individualCPIA.totalActivityPointsEarned = 0;
            }
        }, function (error) {
            toastr.error("error ocuured while getting Clinicians Activities");
        });

        if (diffDays === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            individualCPIA.cpiaReportingPeriod.fromDate = null;
            individualCPIA.cpiaReportingPeriod.toDate = null;
            individualCPIA.cpiaReportingPeriod.noOfDays = null;
        }
    }

    individualCPIA.calculateReportingDays = function () {
        var timestamp1 = new Date(individualCPIA.toDate).getTime();
        var timestamp2 = new Date(individualCPIA.fromDate).getTime();
        var diff = timestamp1 - timestamp2
        var newDate = new Date(diff);
        var one_day = 1000 * 60 * 60 * 24;
        individualCPIA.reportingDuration = diff / one_day;
        individualCPIA.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    individualCPIA.print = function () {
        //set print Object
        clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj(individualCPIA.cpiaActivities,
            individualCPIA.clinicianName, individualCPIA.clinicianNPI, individualCPIA.clinicianTIN,
            individualCPIA.mipsCPIAScore, individualCPIA.totalActivityPointsEarned, individualCPIA.cpiaReportingPeriod);
        var promises = [
             practiceLevelService.getLoggedInUserDetails(),
             practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            individualCPIA.loggedInUserObj = data[0].data.userName;
            individualCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(individualCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(individualCPIA.practiceDetails);
            toastr.remove();
            sessionStorage.setItem("IndividualCPIAAction", "printed");
            individualCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Individual level: " + individualCPIA.AuditClinicianName);
            $state.go('individualLevelCPIAPrint');
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }
    //Saving user audit details
    individualCPIA.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }

    individualCPIA.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}



'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('individualCPIAPrintController', individualCPIAPrintController);
individualCPIAPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'clinicianLevelService'];
function individualCPIAPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, clinicianLevelService) {

    var individualLevelCPIAPrint = this;

    individualLevelCPIAPrint.init = function () {
        individualLevelCPIAPrint.practiceDetailsPrintObj = {};
        individualLevelCPIAPrint.currentDate = Date.now();
        individualLevelCPIAPrint.clinicianCPIAActivities = clinicianLevelService.clinicianLevelCPIAObj;
        individualLevelCPIAPrint.mipsCPIAScore = clinicianLevelService.mipsCPIAScore;
        individualLevelCPIAPrint.totalActivityPointsEarned = clinicianLevelService.totalActivityPointsEarned;

        individualLevelCPIAPrint.clinicianName = clinicianLevelService.clinicianName;
        individualLevelCPIAPrint.clinicianNpi = clinicianLevelService.clinicinaNPI;
        individualLevelCPIAPrint.cpiaReportingPeriod = clinicianLevelService.cpiaReportingPeriod;

        if (typeof individualLevelCPIAPrint.clinicianCPIAActivities !== 'undefined') {
            individualLevelCPIAPrint.clinicianCPIAActivities.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');
            });
        }

        individualLevelCPIAPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            individualLevelCPIAPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        individualLevelCPIAPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("individualCPIAActivities");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printIndividualLevelCPIAGridButton').click();
            }, 100);
        })
    }
    individualLevelCPIAPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupCPIAPrintController', groupCPIAPrintController);
groupCPIAPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'groupLevelService'];
function groupCPIAPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, groupLevelService) {

    var groupLevelCPIAPrint = this;

    //clinicianLevelPrint.practiceObject =
    groupLevelCPIAPrint.init = function () {

        groupLevelCPIAPrint.practiceDetailsPrintObj = {};
        groupLevelCPIAPrint.groupCPIAActivities = {};
        groupLevelCPIAPrint.currentDate = Date.now();

        groupLevelCPIAPrint.groupCPIAActivities = groupLevelService.groupCPIAActivitiesList;
        groupLevelCPIAPrint.mipsCPIAScore = groupLevelService.mipsCPIAScore;
        groupLevelCPIAPrint.totalActivityPointsEarned = groupLevelService.totalActivityPointsEarned;

        groupLevelCPIAPrint.groupName = groupLevelService.groupName;
        groupLevelCPIAPrint.clinicianNpi = groupLevelService.groupTIN;
        groupLevelCPIAPrint.cpiaReportingPeriod = groupLevelService.cpiaReportingPeriod;

        if (typeof groupLevelCPIAPrint.groupCPIAActivities !== 'undefined') {
            groupLevelCPIAPrint.groupCPIAActivities.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                muStageEnums.measureStages.map(function (measure) {
                    if (measure.measureCode === item.measureCode) {
                        if (count === 0) {
                            groupLevelCPIAPrint.measureStage = muStageEnums.measureStages[0].measureName;
                        }
                        count = count + 1;
                    }
                })
            });
        }
        //if (clinicianLevelPrint.groupObj.length() !== 0) {
        //    clinicianLevelPrint.groupObj.map(function (group) {
        //        group.fromDate = $filter('date')(group.fromDate, 'MM-dd-yyyy');
        //        group.toDate = $filter('date')(group.toDate, 'MM-dd-yyyy');
        //    })
        //}
        //clinicianLevelPrint.printObj = practiceLevelService.practiceLevelPrintObject
        groupLevelCPIAPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            groupLevelCPIAPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        groupLevelCPIAPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("groupCPIAActivities");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printGroupLevelCPIAGridButton').click();
            }, 100);
        })
    }
    groupLevelCPIAPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}