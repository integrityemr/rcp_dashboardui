﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupIndividualCPIAPrintController', groupIndividualCPIAPrintController);
groupIndividualCPIAPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'clinicianLevelService'];
function groupIndividualCPIAPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, clinicianLevelService) {
    var groupIndividualCPIAPrint = this;
    groupIndividualCPIAPrint.init = function () {
        groupIndividualCPIAPrint.practiceDetailsPrintObj = {};
        groupIndividualCPIAPrint.currentDate = Date.now();
        groupIndividualCPIAPrint.clinicianCPIAActivities = clinicianLevelService.clinicianLevelCPIAObj;
        groupIndividualCPIAPrint.mipsCPIAScore = clinicianLevelService.mipsCPIAScore;
        groupIndividualCPIAPrint.totalActivityPointsEarned = clinicianLevelService.totalActivityPointsEarned;

        groupIndividualCPIAPrint.clinicianName = clinicianLevelService.clinicianName;
        groupIndividualCPIAPrint.clinicianNpi = clinicianLevelService.clinicinaNPI;
        groupIndividualCPIAPrint.cpiaReportingPeriod = clinicianLevelService.cpiaReportingPeriod;

        if (typeof groupIndividualCPIAPrint.clinicianCPIAActivities !== 'undefined') {
            groupIndividualCPIAPrint.clinicianCPIAActivities.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');
            });
        }

        groupIndividualCPIAPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            groupIndividualCPIAPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        groupIndividualCPIAPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("groupIndividualCPIAActivities");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printIndividualLevelCPIAGridButton').click();
            }, 100);
        })
    }
    groupIndividualCPIAPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}