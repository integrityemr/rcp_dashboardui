﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('individualCPIAController', individualCPIAController);
individualCPIAController.$inject = ['$state', '$scope', '$filter', '$q', 'practiceLevelService', 'clinicianLevelService', 'practiceLevelConstants', '$rootScope'];
function individualCPIAController($state, $scope, $filter, $q, practiceLevelService, clinicianLevelService, practiceLevelConstants, $rootScope) {
    var individualCPIA = this;
    individualCPIA.screenTitle = "IndividualCPIADashBoard";
    individualCPIA.model = {};
    individualCPIA.fromScreenName = localStorage.getItem('setScreenName');
    individualCPIA.getReportingObjTogetBackData = [];
    individualCPIA.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('CPIAScoreObjectdata'));

    individualCPIA.cpiaActivities = [];
    //individualCPIA.dashBoarData = practiceLevelService.reportingObject[0];
    //individualCPIA.dashBoarData = practiceLevelService.reportingObject;
    individualCPIA.dashBoarData = individualCPIA.getReportingObjTogetBackData[0];
    var dateYearIndividual = $filter('date')(new Date(individualCPIA.dashBoarData.cpiaFromDate), 'yyyy');
   // dateYearIndividual = '2018';
    individualCPIA.yearValue = dateYearIndividual;
    individualCPIA.cpiaReportingPeriod = [];
    individualCPIA.setReportingObjTogetBackData = [];


    individualCPIA.init = function () {
        individualCPIA.clinicianName = individualCPIA.dashBoarData.name;
        individualCPIA.clinicianTIN = individualCPIA.dashBoarData.tin;
        individualCPIA.clinicianNPI = individualCPIA.dashBoarData.npi;
        individualCPIA.mipsCPIAScore = 0;
        individualCPIA.totalActivityPointsEarned = 0;
        individualCPIA.getCPIAActivitiesForIndividual();
        individualCPIA.productkey = localStorage.getItem('ProductKey');
        individualCPIA.AuditClinicianName = individualCPIA.dashBoarData.name;
        //not logging audit data when the page is refreshed again after clicking Print button.
        if(sessionStorage.getItem("IndividualCPIAAction") != "printed")
            individualCPIA.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Individual level: " + individualCPIA.AuditClinicianName);
        sessionStorage.setItem("IndividualCPIAAction", "viewed");
    }
    individualCPIA.goToDashBoard = function () {
        //get the screen name from the service and redirect to that screen
        if (localStorage.getItem('setScreenName') != null && localStorage.getItem('setScreenName') != "" && localStorage.getItem('setScreenName') != "undefined") {
            if (localStorage.getItem('setScreenName') == "PracticeLevelDashBoard")
                $state.go('practiceLevelDashBoard');
            else if (localStorage.getItem('setScreenName') == "GroupLevelDashBoard") {
                if (individualCPIA.dashBoarData.type === "Individual") {
                    individualCPIA.setReportingObjTogetBackData.push(individualCPIA.getReportingObjTogetBackData[1]);
                }
                individualCPIA.setReportingObjTogetBackData.push(individualCPIA.dashBoarData); // [0]

                //practiceLevelService.setReportingObj(individualCPIA.setReportingObjTogetBackData);
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') == "ClinicianLevelDashBoard") {
                if (individualCPIA.dashBoarData.type === "Group") {
                    individualCPIA.setReportingObjTogetBackData.push(individualCPIA.getReportingObjTogetBackData[1]);
                }
                individualCPIA.setReportingObjTogetBackData.push(individualCPIA.dashBoarData); // [0]

                //practiceLevelService.setReportingObj(individualCPIA.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
            localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(individualCPIA.setReportingObjTogetBackData));

        }
        else {
            $state.go('practiceLevelDashBoard');
        }
    }

    individualCPIA.getCPIAActivitiesForIndividual = function () {
        var clinicianId;
        var groupId;
        var fromDate;
        var toDate;
        var tin;
        var npi;
        fromDate = individualCPIA.dashBoarData.cpiaFromDate;
        toDate = individualCPIA.dashBoarData.cpiaToDate;
        individualCPIA.fromDate = $filter('date')(fromDate, 'MM-dd-yyyy');
        individualCPIA.toDate = $filter('date')(toDate, 'MM-dd-yyyy');
        tin = individualCPIA.dashBoarData.tin;
        npi = individualCPIA.dashBoarData.npi;

        var firstDate = new Date(individualCPIA.dashBoarData.cpiaFromDate);
        var secondDate = new Date(individualCPIA.dashBoarData.cpiaToDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        individualCPIA.cpiaReportingPeriod = {
            fromDate: $filter('date')(new Date(individualCPIA.dashBoarData.cpiaFromDate), 'MM/dd/yyyy'),
            toDate: $filter('date')(new Date(individualCPIA.dashBoarData.cpiaToDate), 'MM/dd/yyyy'),
            noOfDays: diffDays + 1
        };
        
        if (individualCPIA.fromDate !== null && individualCPIA.toDate !== null) {
            individualCPIA.calculateReportingDays();
        }
        
        if (individualCPIA.dashBoarData.type === 'Independent') {
            clinicianId = individualCPIA.dashBoarData.clinicianId;
        }

        var selectedInDividualYear = individualCPIA.yearValue;
        clinicianLevelService.getCPIAActivitiesForClinician(npi, tin, individualCPIA.fromDate, individualCPIA.toDate, selectedInDividualYear).then(function (response) {
            individualCPIA.cpiaActivities = response.data

            if (individualCPIA.cpiaActivities.length !== 0) {
                individualCPIA.cpiaActivities.map(function (item) {
                    individualCPIA.totalActivityPointsEarned = individualCPIA.totalActivityPointsEarned + item.points;
                    if (item.name == 'Participating with an APM Entity') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                    else if (item.name == 'Participating with a PCMH') {
                        item.value = "";
                        item.scalingFactor = "";
                    }
                });
                if (individualCPIA.totalActivityPointsEarned > 40) {
                    individualCPIA.totalActivityPointsEarned = 40;
                }
                individualCPIA.mipsCPIAScore = parseFloat(individualCPIA.totalActivityPointsEarned) * 15 / 40;
                individualCPIA.mipsCPIAScore = parseFloat(individualCPIA.mipsCPIAScore).toFixed(2);
            }
            else {
                individualCPIA.mipsCPIAScore = 0;
                individualCPIA.totalActivityPointsEarned = 0;
            }
        }, function (error) {
            toastr.error("error ocuured while getting Clinicians Activities");
        });

        if (diffDays === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            individualCPIA.cpiaReportingPeriod.fromDate = null;
            individualCPIA.cpiaReportingPeriod.toDate = null;
            individualCPIA.cpiaReportingPeriod.noOfDays = null;
        }
    }

    individualCPIA.calculateReportingDays = function () {
        var timestamp1 = new Date(individualCPIA.toDate).getTime();
        var timestamp2 = new Date(individualCPIA.fromDate).getTime();
        var diff = timestamp1 - timestamp2
        var newDate = new Date(diff);
        var one_day = 1000 * 60 * 60 * 24;
        individualCPIA.reportingDuration = diff / one_day;
        individualCPIA.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    individualCPIA.print_OLD = function () {
        //set print Object
        clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj(individualCPIA.cpiaActivities,
            individualCPIA.clinicianName, individualCPIA.clinicianNPI, individualCPIA.clinicianTIN,
            individualCPIA.mipsCPIAScore, individualCPIA.totalActivityPointsEarned, individualCPIA.cpiaReportingPeriod);
        var promises = [
             practiceLevelService.getLoggedInUserDetails(),
             practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            individualCPIA.loggedInUserObj = data[0].data.userName;
            individualCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(individualCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(individualCPIA.practiceDetails);
            toastr.remove();
            sessionStorage.setItem("IndividualCPIAAction", "printed");
            individualCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Individual level: " + individualCPIA.AuditClinicianName);
            $state.go('individualLevelCPIAPrint');
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }

    //NEW PRINT FUNCTION


    individualCPIA.print = function () {
        clinicianLevelService.setClinicianLevelPrintCPIADashBoardObj(individualCPIA.cpiaActivities,
            individualCPIA.clinicianName, individualCPIA.clinicianNPI, individualCPIA.clinicianTIN,
            individualCPIA.mipsCPIAScore, individualCPIA.totalActivityPointsEarned, individualCPIA.cpiaReportingPeriod);
        var promises = [
           practiceLevelService.getLoggedInUserDetails(),
          practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            individualCPIA.loggedInUserObj = data[0].data.userName;
            individualCPIA.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(individualCPIA.loggedInUserObj);
            practiceLevelService.setPracticeDetails(individualCPIA.practiceDetails);

            individualCPIA.practiceDetailsPrintObj = {};
            individualCPIA.groupCPIAActivities = {};
            individualCPIA.currentDate = Date.now();

            individualCPIA.clinicianName = clinicianLevelService.clinicianName;
            individualCPIA.clinicianNpi = clinicianLevelService.clinicinaNPI;
           
            individualCPIA.userObject = practiceLevelService.userObject;

           
            if (clinicianLevelService.practiceDetailsPrintObj != null) {
                individualCPIA.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            window.setTimeout(function () {
                var printContents = $("#printIndividualLevelCPIADashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
          //  individualCPIA.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashImpActivities + " for Group level: " + groupCPIA.AuditGroupName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details for print');
        });
    }

    //Saving user audit details
    individualCPIA.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }

    individualCPIA.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}


