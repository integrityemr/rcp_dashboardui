﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupCPIAPrintController', groupCPIAPrintController);
groupCPIAPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'groupLevelService'];
function groupCPIAPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, groupLevelService) {

    var groupLevelCPIAPrint = this;

    //clinicianLevelPrint.practiceObject =
    groupLevelCPIAPrint.init = function () {

        groupLevelCPIAPrint.practiceDetailsPrintObj = {};
        groupLevelCPIAPrint.groupCPIAActivities = {};
        groupLevelCPIAPrint.currentDate = Date.now();

        groupLevelCPIAPrint.groupCPIAActivities = groupLevelService.groupCPIAActivitiesList;
        groupLevelCPIAPrint.mipsCPIAScore = groupLevelService.mipsCPIAScore;
        groupLevelCPIAPrint.totalActivityPointsEarned = groupLevelService.totalActivityPointsEarned;

        groupLevelCPIAPrint.groupName = groupLevelService.groupName;
        groupLevelCPIAPrint.clinicianNpi = groupLevelService.groupTIN;
        groupLevelCPIAPrint.cpiaReportingPeriod = groupLevelService.cpiaReportingPeriod;

        var dateYeargroupLevel = $filter('date')(new Date(groupLevelCPIAPrint.cpiaReportingPeriod.fromDate), 'yyyy');
        // dateYearIndividual = '2018';
        groupLevelCPIAPrint.yearValue = dateYeargroupLevel;
        

        if (typeof groupLevelCPIAPrint.groupCPIAActivities !== 'undefined') {
            groupLevelCPIAPrint.groupCPIAActivities.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                muStageEnums.measureStages.map(function (measure) {
                    if (measure.measureCode === item.measureCode) {
                        if (count === 0) {
                            groupLevelCPIAPrint.measureStage = muStageEnums.measureStages[0].measureName;
                        }
                        count = count + 1;
                    }
                })
            });
        }
        //if (clinicianLevelPrint.groupObj.length() !== 0) {
        //    clinicianLevelPrint.groupObj.map(function (group) {
        //        group.fromDate = $filter('date')(group.fromDate, 'MM-dd-yyyy');
        //        group.toDate = $filter('date')(group.toDate, 'MM-dd-yyyy');
        //    })
        //}
        //clinicianLevelPrint.printObj = practiceLevelService.practiceLevelPrintObject
        groupLevelCPIAPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            groupLevelCPIAPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        groupLevelCPIAPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("groupCPIAActivities");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printGroupLevelCPIAGridButton').click();
            }, 100);
        })
    }
    groupLevelCPIAPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}