'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('clinicianLevelController', clinicianLevelController);
clinicianLevelController.$inject = ['$state', '$scope', '$filter', '$q', 'dashBoardEnums', 'practiceLevelService', 'practiceInfoService', 'clinicianLevelService', 'performanceYearEnums', 'muStageEnums', 'practiceLevelConstants'];
function clinicianLevelController($state, $scope, $filter, $q, dashBoardEnums, practiceLevelService, practiceInfoService, clinicianLevelService, performanceYearEnums, muStageEnums, practiceLevelConstants) {
    var clinicianLevel = this;

    clinicianLevel.init = function () {
        clinicianLevel.BackBtn = true;
        if (localStorage.getItem('emergencyAccess') === "true") {
            clinicianLevel.loadInit();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role === undefined || res.data.role === null || res.data.role === "") {
                        toastr.error('You are not authorized.');
                        // $state.go('Home');
                    }
                    else {
                        if (res.data.role === "ADMINISTRATOR") {
                            clinicianLevel.loadInit();
                        }
                        else if (res.data.role === "ELIGIBLE CLINICIAN") {
                            clinicianLevel.getIndividualDashboardInit(res.data.userId);
                        }
                        else {
                            clinicianLevel.loadInit();
                        }
                    }
                }
            });
        }
    }

    clinicianLevel.getIndividualDashboardInit = function (UserId) {
        clinicianLevel.screenTitle = "IndividualDashBoard";
        clinicianLevel.model = {};
        clinicianLevel.getReportingObjTogetBackData = [];
        clinicianLevel.getListOfGroupsForSelectedClinicianForIndividualDashBoard(UserId)

    }
    clinicianLevel.loadInit = function () {
        clinicianLevel.screenTitle = "IndividualDashBoard";
        clinicianLevel.model = {};
        clinicianLevel.getReportingObjTogetBackData = [];
        clinicianLevel.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
        clinicianLevel.DashBoardRowObject = clinicianLevel.getReportingObjTogetBackData[0];
        clinicianLevel.aciStage = clinicianLevel.DashBoardRowObject.measureCode;
        clinicianLevel.setReportingObjTogetBackData = [];
        //clinicianLevel.DashBoardRowObject = practiceLevelService.reportingObject;
        //clinicianLevel.DashBoardRowObject = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
        var date = new Date();
        clinicianLevel.currentDate = new Date();
        clinicianLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        clinicianLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
        clinicianLevel.getReportingPeriodList();
        clinicianLevel.getClinicianData();
        clinicianLevel.getPerformanceYearList();
        clinicianLevel.getListOfGroupsForSelectedClinician();
        clinicianLevel.productkey = localStorage.getItem('ProductKey');
        clinicianLevel.BackBtn = false;
        clinicianLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + clinicianLevel.AuditClinicianName);
    }
    //to get the list of performance year from enums
    clinicianLevel.getPerformanceYearList = function () {
        clinicianLevel.performanceYearList = performanceYearEnums.performanceYearList;
        var currentYear = performanceYearEnums.performanceYearList.filter(function (i) { return i.performanceYear === $filter('date')(new Date(), 'yyyy') })[0];
        if (currentYear != null)
            clinicianLevel.selectedPerformanceYearId = currentYear;
        else
        clinicianLevel.selectedPerformanceYearId = clinicianLevel.performanceYearList[0];
    }


    clinicianLevel.updateGroupList = function () {
        clinicianLevel.groupDataOfClinician.map(function (item) {
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            if (item.fromDate === null && item.toDate === null) {
                item.reportingPeriodId = 0;
            }
            else {
                var individualdiffDays = clinicianLevel.dateDifference(item.fromDate, item.toDate);
                if (individualdiffDays > 363) {
                    item.reportingPeriodId = 1;

                }
                else if (individualdiffDays > 89) {
                    item.reportingPeriodId = 3;
                    item.disableToDateField = false;
                    item.disableFromDateFiled = false;
                }
                else if (individualdiffDays === 89) {
                    item.reportingPeriodId = 2;
                    item.disableToDateField = true;
                    item.disableFromDateFiled = false;
                }
                else {
                    item.reportingPeriodId = 0;
                    item.disableFromDateFiled = false;
                    item.disableToDateField = false;
                }
            }
            if (item.reportingPeriodId === 1) {
                item.reportingPeriodName = 'Full Year';
            }
            if (item.reportingPeriodId === 2) {
                item.reportingPeriodName = '90 Days';
            }
            if (item.reportingPeriodId === 3) {
                item.reportingPeriodName = 'Date Range';
            }
            if (clinicianLevel.clinicianData !== null && clinicianLevel.clinicianData !== undefined && clinicianLevel.clinicianData !== "")
                item.ClinincianName = clinicianLevel.clinicianData[0].name;


        });
        clinicianLevel.groupListForClinician = angular.copy(clinicianLevel.groupDataOfClinician);
        clinicianLevel.addCssForGroupsList();
    }

    clinicianLevel.addCssForGroupsList = function () {
        clinicianLevel.originalClinicianData = [];
        clinicianLevel.groupDataWithCSS = [];
        clinicianLevel.groupDataOfClinician.map(function (item) {
            clinicianLevel.originalClinicianData.push(item);
            if (item.cpiaScore >= 15) {
                item.classGreen = true;
                item.classYellow = false;
                item.classRed = false;
            }
            else if (item.cpiaScore < 7.5) {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = true;
            }
            else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
                item.classGreen = false;
                item.classYellow = true;
                item.classRed = false;
            }
            else {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = false;
            }

            var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
            if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
                item.classGreenACI = true;
                item.classRedACI = false;
                item.classYellowACI = false;
            }
            if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
                item.classGreenACI = false;
                item.classRedACI = false;
                item.classYellowACI = true;
            }
            if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            if (item.aciScore === 0) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }

            item.type = 'Individual'; //'Group';
            clinicianLevel.groupDataWithCSS.push(item);
        });
        clinicianLevel.groupDataOfClinician = angular.copy(clinicianLevel.groupDataWithCSS);
    }

    clinicianLevel.goToPracticeDashBoard = function () {
        if (clinicianLevel.screenTitle === "IndividualDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
    }
    clinicianLevel.getClinicianData = function () {
        clinicianLevel.clinicianData = [];
        if (typeof clinicianLevel.DashBoardRowObject !== 'undefined') {
            clinicianLevel.DashBoardRowObject.fromDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.fromDate), 'yyyy-MM-dd');
            clinicianLevel.DashBoardRowObject.toDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.toDate), 'yyyy-MM-dd');

            if (clinicianLevel.DashBoardRowObject.fromDate === "0001-01-01" && clinicianLevel.DashBoardRowObject.toDate === "0001-01-01") {
                clinicianLevel.DashBoardRowObject.fromDate = null;
                clinicianLevel.DashBoardRowObject.toDate = null;
                clinicianLevel.DashBoardRowObject.measureCode = 0;
                clinicianLevel.clinicianData = null;
            }
            else {
                clinicianLevel.clinicianData.push(clinicianLevel.DashBoardRowObject);
                clinicianLevel.groupDataOfClinician = clinicianLevel.clinicianData;
                clinicianLevel.updateGroupList();
            }
        }
        else {
            clinicianLevel.clinicianData.push(clinicianLevel.DashBoardRowObject);
        }
        clinicianLevel.AuditClinicianName = clinicianLevel.DashBoardRowObject.name;
        if (clinicianLevel.clinicianData !== null)
            clinicianLevel.clinicianData[0].type = "Independent";
    }

    clinicianLevel.goToACIMeasureReport = function (item) {
        if (item.fromDate === null && item.toDate === null && item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }

        if (item.type === "Individual") {
            item.tin = item.groupTIN;
        }

        item.measureCode = item.measureCode;
        clinicianLevel.setReportingObjTogetBackData.push(item); // [0]

        if (item.type === "Group") {
            clinicianLevel.setReportingObjTogetBackData.push(clinicianLevel.DashBoardRowObject); // [1]
        }

        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("AciScoreObjectdata", JSON.stringify(item));
        // practiceLevelService.setScreenName('ClinicianLevelDashBoard');
        var screenName = 'ClinicianLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);
        $state.go("amcMeasureReport");
        // practiceLevelService.setReportingObj(clinicianLevel.setReportingObjTogetBackData);
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(clinicianLevel.setReportingObjTogetBackData));
    }

    clinicianLevel.getListOfGroupsForSelectedClinician = function () {
        var clinicianId = clinicianLevel.DashBoardRowObject.clinicianId;
        var type = clinicianLevel.DashBoardRowObject.type;
        var fromDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.fromDate), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.toDate), 'yyyy-MM-dd');
        var tin = clinicianLevel.DashBoardRowObject.tin;
        var npi = clinicianLevel.DashBoardRowObject.npi;
        var measureCode = clinicianLevel.DashBoardRowObject.measureCode;

        clinicianLevelService.getGroupsForClinician(measureCode, tin, npi, fromDate, toDate).then(function (response) {
            clinicianLevel.groupDataOfClinician = response.data;
            clinicianLevel.updateGroupList();
        }, function (err) {
            toastr.error('server error while getting groups');
        });

    }


    clinicianLevel.getListOfGroupsForSelectedClinicianForIndividualDashBoard = function (UserId) {
        clinicianLevelService.getClinicianMeasuresForIndividualDashBoard(UserId).then(function (response) {
            clinicianLevel.DashBoardRowObject = response.data;
            localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(response.data));
            clinicianLevel.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
            clinicianLevel.DashBoardRowObject = clinicianLevel.getReportingObjTogetBackData[0];
            clinicianLevel.aciStage = clinicianLevel.DashBoardRowObject.measureCode;
            clinicianLevel.setReportingObjTogetBackData = [];
            //clinicianLevel.DashBoardRowObject = practiceLevelService.reportingObject;
            //clinicianLevel.DashBoardRowObject = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
            var date = new Date();
            clinicianLevel.currentDate = new Date();
            clinicianLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
            clinicianLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
            clinicianLevel.getReportingPeriodList();
            clinicianLevel.getClinicianData();
            clinicianLevel.getPerformanceYearList();
            clinicianLevel.getListOfGroupsForSelectedClinician();

            clinicianLevel.productkey = localStorage.getItem('ProductKey');
            clinicianLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + clinicianLevel.AuditClinicianName);
            // clinicianLevel.getClinicianData();
            // clinicianLevel.updateGroupList();
        }, function (err) {
            toastr.error('server error while getting Individual Details');
        });

    }

    clinicianLevel.getReportingPeriodList = function () {
        clinicianLevel.reportingPeriodList = dashBoardEnums.reportingPeriod;
    }

    clinicianLevel.dateDifference = function (fromDate, toDate) {
        var firstDate = new Date(fromDate);
        var secondDate = new Date(toDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }

    clinicianLevel.validateDates = function (item) {
        var activityStartDatesList = [];
        if (item.type === "Individual") {
            practiceLevelService.getActivityStartDatesOfIndividual(item.npi, item.groupTIN).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }
        else if (item.type === "Independent") {
            practiceLevelService.getActivityStartDatesOfIndependent(item.npi, item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }
        else {
            practiceLevelService.getActivityStartDatesOfGroup(item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }

        var registryStartDatesList = [];
        practiceLevelService.getCPIARegistryStartDates().then(function (response) {
            registryStartDatesList = response.data;
            var syndromicCount = 0;
            var immunizationCount = 0;
            var qualiRegistryCount = 0;
            var diffDays = 0;
            //check length of 
            angular.forEach(registryStartDatesList, function (value, index) {
                switch (value.registryCode) {
                    case 1:
                        diffDays = clinicianLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { syndromicCount += 1; }
                        break;
                    case 2:
                        diffDays = clinicianLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { immunizationCount += 1; }
                        break;
                    case 3:
                        break;
                    case 4:
                        diffDays = clinicianLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { qualiRegistryCount += 1; }
                        break;
                }
            });
            if (syndromicCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (immunizationCount > 0) {
                toastr.warning("For receiving credit, the  start date for immunization registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (qualiRegistryCount > 0) {
                toastr.warning("For receiving credit, the  start date for Clinical data registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
        }, function (error) {
            toastr.error("server error while getting Scores");
        });

    }

    clinicianLevel.updateToDate = function (item, dropbox) {
        
        if (item.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period");
            return false
        }
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        if (item.reportingPeriodId === 1) { //Full Year         
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(clinicianLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(clinicianLevel.selectedPerformanceYearId.performanceYear, 11, 31);
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }
            if (dropbox == true)
                item.fromDate = new Date();
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
                item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90 days");
                    return false;
                }
            }

            if (item.fromDate && item.toDate) {
                var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
                var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
                if (item.type === 'Independent' || item.type === 'Individual') {
                    practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                        item.aciScore = response.data.aciScore;
                        //item.cpiaScore = response.data.cpiaScore;
                        item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                        clinicianLevel.addCSSForGroupRow(item);
                    }, function (error) {
                        toastr.error("server error while getting Scores");
                    });
                }
                //if (item.type === 'Individual') {
                //    practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                //        item.aciScore = response.data.aciScore;
                //        item.cpiaScore = response.data.cpiaScore;
                //        item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                //        item.totalPerfomanceGoal = response.data.totalPerfomanceGoal;
                //        clinicianLevel.addCSSForGroupRow(item);
                //    }, function (error) {
                //        toastr.error("server error while getting Scores");
                //    });
                //}
                if (item.type === 'Group') {
                    practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                        item.aciScore = response.data.aciScore;
                        //item.cpiaScore = response.data.cpiaScore;
                        item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                        clinicianLevel.addCSSForGroupRow(item);
                    }, function (error) {
                        toastr.error("server error while getting Scores");
                    });
                }
            }
        }

        var itemfromDate = new Date(item.fromDate);
        var itemtoDate = new Date(item.toDate);

        if (itemfromDate.getFullYear() !== itemtoDate.getFullYear()) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }
        
        var perfYear = clinicianLevel.selectedPerformanceYearId.performanceYear;
        
        if ((itemfromDate.getFullYear() !== perfYear) || (itemtoDate.getFullYear() !== perfYear)) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        clinicianLevel.validateDates(item);

        if (item.fromDate && item.toDate) {
            if (item.reportingPeriodId === 0) {
                toastr.error("Please select valid Reporting Period");
                return false
            }
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            if (item.type === 'Individual') {
                practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    clinicianLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            var aciReportingPeriodObj = {};
            if (item.type === 'Individual') {
                aciReportingPeriodObj = {
                    type: item.type,
                    npi: item.npi,
                    tin: item.groupTIN,
                    fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                    toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
                };
            } else {
                aciReportingPeriodObj = {
                    type: item.type,
                    npi: item.npi,
                    tin: item.tin,
                    fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                    toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
                };
            }

            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    var auditClinicianName = '';
                    //storing clinician name if the type is Independent else storing Group name & clinician name if group is selected.
                    if (item.type === "Individual") {//here type Individual will come for clinician belonging to the different Groups.
                        auditClinicianName = clinicianLevel.AuditClinicianName + " - Group name: " + item.name;;
                    }
                    else { //here type would be Independent for the clinician
                        auditClinicianName = clinicianLevel.AuditClinicianName;
                    }
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + auditClinicianName + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + auditClinicianName + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + auditClinicianName + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        clinicianLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("server error while updating Reporting Period Dates");
            });
        }

    }

    clinicianLevel.addCSSForGroupRow = function (item) {
        var rowItem = {};
        if (item.reportingPeriodId === 1) {
            item.reportingPeriodName = 'Full Year';
        }
        if (item.reportingPeriod === 2) {
            item.reportingPeriodName = '90 Days';
        }
        if (item.reportingPeriodId === 3) {
            item.reportingPeriodName = 'Date Range';
        }
        if (item.cpiaScore >= 15) {
            item.classGreen = true;
            item.classYellow = false;
            item.classRed = false;
        }
        else if (item.cpiaScore < 7.5) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = true;
        }
        else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
            item.classGreen = false;
            item.classYellow = true;
            item.classRed = false;
        }
        else {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;
        }

        var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
        if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
            item.classGreenACI = true;
            item.classRedACI = false;
            item.classYellowACI = false;
        }
        if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = true;
        }
        if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        if (item.aciScore === 0) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }

        rowItem = angular.copy(item);
    }

    clinicianLevel.goToCPIAActivities = function (item) {
        if (item.cpiaScore === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            return false;
        }
        item.measureCode = clinicianLevel.aciStage;
        //practiceLevelService.setScreenName('ClinicianLevelDashBoard');
        var screenName = 'ClinicianLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);
        clinicianLevel.setReportingObjTogetBackData.push(item);


        if (item.type === 'Group') {
            $state.go("groupCPIAActivities");
            item.tin = item.groupTIN;
            clinicianLevel.setReportingObjTogetBackData.push(clinicianLevel.DashBoardRowObject);
        }
        if (item.type === 'Independent') {
            //localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(item));
            //practiceLevelService.setReportingObj(item);
            $state.go("individualCPIAActivities");
        }
        // New added 
        if (item.type === 'Individual') {
            item.tin = item.groupTIN;
            item.name = item.name + " - (" + item.groupTIN + ") " + item.ClinincianName;
            $state.go("groupIndividualCPIAActivities");
            clinicianLevel.setReportingObjTogetBackData.push(clinicianLevel.DashBoardRowObject);
        }
        //practiceLevelService.setReportingObj(item);
        //practiceLevelService.setReportingObj(clinicianLevel.setReportingObjTogetBackData);
        localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(clinicianLevel.setReportingObjTogetBackData));
    }

    clinicianLevel.print = function () {
        //set print Object
        clinicianLevel.updateGroupList();
        delete clinicianLevel.clinicianData[0]['existingFromDate'];
        delete clinicianLevel.clinicianData[0]['existingToDate'];
        clinicianLevelService.setClinicianLevelPrintDashBoardObj(clinicianLevel.clinicianData, clinicianLevel.groupListForClinician);

        var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            clinicianLevel.loggedInUserObj = data[0].data.userName;
            clinicianLevel.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(clinicianLevel.loggedInUserObj);
            practiceLevelService.setPracticeDetails(clinicianLevel.practiceDetails);

            clinicianLevel.practiceDetailsPrintObj = {};
            clinicianLevel.groupObj = {};
            clinicianLevel.currentDate = Date.now();

            clinicianLevel.clinicianObj = clinicianLevelService.clinicianPrintObj;
            clinicianLevel.groupObj = clinicianLevelService.groupPrintObj;

            clinicianLevel.clinicianName = clinicianLevel.clinicianObj[0].name;
            clinicianLevel.clinicianNpi = clinicianLevel.clinicianObj[0].npi;

            if (typeof clinicianLevel.clinicianObj !== 'undefined') {
                clinicianLevel.clinicianObj.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                    muStageEnums.measureStages.map(function (measure) {
                        if (measure.measureCode === item.measureCode) {
                            if (count === 0) {
                                clinicianLevel.measureStage = muStageEnums.measureStages[0].measureName;
                            }
                            count = count + 1;
                        }
                    })
                });
            }
            if (typeof clinicianLevel.groupObj.length !== 'undefined') {
                clinicianLevel.groupObj.map(function (group) {
                    group.fromDate = $filter('date')(group.fromDate, 'MM-dd-yyyy');
                    group.toDate = $filter('date')(group.toDate, 'MM-dd-yyyy');
                })
            }
            clinicianLevel.printObj = practiceLevelService.practiceLevelPrintObject
            clinicianLevel.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                clinicianLevel.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }


            window.setTimeout(function () {
                var printContents = $("#printclinicianLevelDashBoardGrid").html();
                printContents = printContents.replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingFromDate</th>', '').replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingToDate</th>', '');
                var actualPrintContents = printContents.replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingFromDate</th>', '').replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingToDate</th>', '');
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + actualPrintContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            clinicianLevel.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + clinicianLevel.AuditClinicianName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }


    clinicianLevel.backToPrevioudScreen = function () {

        if (clinicianLevel.screenName === "PracticeLevelDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
        if (clinicianLevel.screenName === "ClinicianLevelDashBoard") {
            $state.go('clinicianLevelDashBoard');
        }
        if (clinicianLevel.screenName === "GroupLevelDashBoard") {
            $state.go('groupLevelDashBoard');
        }
        //clinicianLevel.setReportingObjTogetBackData.push(item);   // remove if any back screen other than the practice level
        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
    }
    //Saving user audit details
    clinicianLevel.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }
    clinicianLevel.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('clinicianLevelPrintController', clinicianLevelPrintController);
clinicianLevelPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'clinicianLevelService'];
function clinicianLevelPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, clinicianLevelService) {

    var clinicianLevelPrint = this;

    clinicianLevelPrint.init = function () {
        clinicianLevelPrint.practiceDetailsPrintObj = {};
        clinicianLevelPrint.groupObj = {};
        clinicianLevelPrint.currentDate = Date.now();

        clinicianLevelPrint.clinicianObj = clinicianLevelService.clinicianPrintObj;
        clinicianLevelPrint.groupObj = clinicianLevelService.groupPrintObj;

        clinicianLevelPrint.clinicianName = clinicianLevelPrint.clinicianObj[0].name;
        clinicianLevelPrint.clinicianNpi = clinicianLevelPrint.clinicianObj[0].npi;

        if (typeof clinicianLevelPrint.clinicianObj !== 'undefined') {
            clinicianLevelPrint.clinicianObj.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                muStageEnums.measureStages.map(function (measure) {
                    if (measure.measureCode === item.measureCode) {
                        if (count === 0) {
                            clinicianLevelPrint.measureStage = muStageEnums.measureStages[0].measureName;
                        }
                        count = count + 1;
                    }
                })
            });
        }
        if (typeof clinicianLevelPrint.groupObj.length !== 'undefined') {
            clinicianLevelPrint.groupObj.map(function (group) {
                group.fromDate = $filter('date')(group.fromDate, 'MM-dd-yyyy');
                group.toDate = $filter('date')(group.toDate, 'MM-dd-yyyy');
            })
        }
        clinicianLevelPrint.printObj = practiceLevelService.practiceLevelPrintObject
        clinicianLevelPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            clinicianLevelPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        clinicianLevelPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("clinicianLevelDashBoard");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printClinicianLevelGridButton').click();
            }, 100);
        })
    }
    clinicianLevelPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupLevelController', groupLevelController);
groupLevelController.$inject = ['$state', '$scope', '$filter', '$q', 'dashBoardEnums', 'performanceYearEnums', 'practiceLevelService', 'groupLevelService', 'muStageEnums', 'practiceLevelConstants'];
function groupLevelController($state, $scope, $filter, $q, dashBoardEnums, performanceYearEnums, practiceLevelService, groupLevelService, muStageEnums, practiceLevelConstants) {
    var groupLevel = this;
    groupLevel.screenTitle = "GroupLevelDashBoard";
    groupLevel.model = {};
    groupLevel.getReportingObjTogetBackData = [];
    groupLevel.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));

    //groupLevel.DashBoardRowObject = practiceLevelService.reportingObject;
    //groupLevel.DashBoardRowObject = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));

    groupLevel.DashBoardRowObject = groupLevel.getReportingObjTogetBackData[0];
    groupLevel.aciStage = groupLevel.DashBoardRowObject.measureCode;
    groupLevel.setReportingObjTogetBackData = [];

    groupLevel.init = function () {
        groupLevel.showGrayBgForClinician = true;
        groupLevel.getClinicianData();

        var date = new Date();
        groupLevel.currentDate = new Date();
        groupLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        groupLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);

        groupLevel.showGroup = true;
        groupLevel.checkGroup = true;
        groupLevel.showClinicianColumns = false;

        groupLevel.currentDate = new Date();
        groupLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        groupLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
        groupLevel.getReportingPeriodList();
        groupLevel.getPerformanceYearList();
        groupLevel.getGroupData();
        groupLevel.productkey = localStorage.getItem('ProductKey');
        groupLevel.AuditGroupName = groupLevel.DashBoardRowObject.name;
        groupLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + groupLevel.AuditGroupName);
    }

    groupLevel.goToPracticeDashBoard = function () {
        if (groupLevel.screenTitle === "GroupLevelDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
    }

    groupLevel.goToACIMeasureReport = function (item) {
        if (item.measureCode === 0 && item.aciScore === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        if (groupLevel.showGroup) { // Group selected
            item.fromDate = groupLevel.DashBoardRowObject.fromDate;
            item.toDate = groupLevel.DashBoardRowObject.toDate;
            item.measureCode = groupLevel.aciStage;
            item.tin = groupLevel.DashBoardRowObject.tin;
            //item.idValue = groupLevel.DashBoardRowObject.idValue;
        }

        item.groupName = groupLevel.DashBoardRowObject.name;
        item.groupNameWithTIN = groupLevel.DashBoardRowObject.name + '-' + groupLevel.DashBoardRowObject.tin;
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("AciScoreObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('GroupLevelDashBoard');
        var screenName = 'GroupLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        $state.go("amcMeasureReport");
        groupLevel.setReportingObjTogetBackData.push(item);

        if (item.type === "Individual") {
            groupLevel.setReportingObjTogetBackData.push(groupLevel.DashBoardRowObject);
        }


        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(groupLevel.setReportingObjTogetBackData));
    }

    groupLevel.getReportingPeriodList = function () {
        groupLevel.reportingPeriodList = dashBoardEnums.reportingPeriod;
    }
    groupLevel.getPerformanceYearList = function () {
        groupLevel.performanceYearList = performanceYearEnums.performanceYearList;
        var currentYear = performanceYearEnums.performanceYearList.filter(function (i) { return i.performanceYear == $filter('date')(new Date(), 'yyyy') })[0];
        if (currentYear != null)
            groupLevel.selectedPerformanceYearId = currentYear;
        else
        groupLevel.selectedPerformanceYearId = groupLevel.performanceYearList[0];
    }
    groupLevel.showGroupsGrid = function () {
        groupLevel.showGroup = true;
        groupLevel.checkGroup = true;
        groupLevel.showClinicianColumns = false;
        groupLevel.showGroupColumns = true;
        groupLevel.getGroupData();
        groupLevel.removeCSSForClinicianData();
        groupLevel.getClinicianData();
    }

    groupLevel.showClinicianGrid = function () {
        groupLevel.showGroup = false;
        groupLevel.checkGroup = false;
        groupLevel.showClinicianColumns = true;
        groupLevel.showGroupColumns = false;
        groupLevel.removeCSSForGroupData();
        groupLevel.updateClinicianDataWithDates();
        // groupLevel.addCSSForClinicianCPIASCore();
        groupLevel.getParticipantClinicianData();
    }

    groupLevel.updateClinicianDataWithDates = function () {
        groupLevel.clinicianListForGroup.map(function (item) {            
            //groupLevel.groupDashBoardData[0] = groupLevel.clinicianListForGroup[0];
            item.tin = groupLevel.groupDashBoardData[0].tin;
            //item.fromDate = groupLevel.clinicianListForGroup[0].fromDate;
            //item.toDate = groupLevel.clinicianListForGroup[0].toDate;
            if (item.fromDate === null && item.toDate === null) {
                item.reportingPeriodId = 0;
            }
            else {
                var groupIndividualdiffDays = groupLevel.dateDifference(item.fromDate, item.toDate);
                if (groupIndividualdiffDays > 363)
                    item.reportingPeriodId = 1;
                else if (groupIndividualdiffDays > 89)
                    item.reportingPeriodId = 3;
                else if (groupIndividualdiffDays === 89)
                    item.reportingPeriodId = 2;
                else
                    item.reportingPeriodId = 0;
            }
            if (item.reportingPeriodId === 1) {
                item.reportingPeriodName = 'Full Year';
                item.disableFromDateFiled = true;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 2) {
                item.reportingPeriodName = '90 Days';
                item.disableFromDateFiled = false;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 3) {
                item.reportingPeriodName = 'Date Range';
                item.disableFromDateFiled = false;
                item.disableToDateField = false;
            }
            item.type = "Individual";

        });
    }

    groupLevel.removeCSSForGroupData = function () {
        groupLevel.groupDashBoardData = [];
        groupLevel.originalGroupData.map(function (item) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;

            item.classGreenACI = false;
            item.classYellowACI = false;
            item.classRedACI = false;
            groupLevel.groupDashBoardData.push(item);

        })
    }
    groupLevel.removeCSSForClinicianData = function () {
        groupLevel.clinicianDashBoardData = [];
        groupLevel.clinicianListForGroup.map(function (item) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;

            item.classGreenACI = false;
            item.classYellowACI = false;
            item.classRedACI = false;
            groupLevel.clinicianDashBoardData.push(item);
        })        
        groupLevel.clinicianListForGroup = angular.copy(groupLevel.clinicianDashBoardData);
    }
    groupLevel.getGroupData = function () {
        groupLevel.groupDashBoardData = [];
        groupLevel.groupDashBoardData = angular.copy(groupLevel.DashBoardRowObject);
        groupLevel.addCSSForGroupCPIASCore();
        groupLevel.addCSSForClinicianCPIASCore();
    }

    groupLevel.addCSSForGroupCPIASCore = function () {
        groupLevel.originalGroupData = [];
        groupLevel.originalGroupData.push(groupLevel.groupDashBoardData);
        groupLevel.groupDataWithCSS = [];

        if (groupLevel.groupDashBoardData.cpiaScore >= 15) {
            groupLevel.groupDashBoardData.classGreen = true;
            groupLevel.groupDashBoardData.classYellow = false;
            groupLevel.groupDashBoardData.classRed = false;
        }
        else if (groupLevel.groupDashBoardData.cpiaScore < 7.5) {
            groupLevel.groupDashBoardData.classGreen = false;
            groupLevel.groupDashBoardData.classYellow = false;
            groupLevel.groupDashBoardData.classRed = true;
        }
        else if (groupLevel.groupDashBoardData.cpiaScore < 15 && groupLevel.groupDashBoardData.cpiaScore >= 7.5) {
            groupLevel.groupDashBoardData.classGreen = false;
            groupLevel.groupDashBoardData.classYellow = true;
            groupLevel.groupDashBoardData.classRed = false;
        }
        else {
            groupLevel.groupDashBoardData.classGreen = false;
            groupLevel.groupDashBoardData.classYellow = false;
            groupLevel.groupDashBoardData.classRed = false;
        }


        var PerformanceGoal_Calculated = (groupLevel.groupDashBoardData.totalPerfomanceGoal) * 90 / 100;
        if (groupLevel.groupDashBoardData.totalACIPointsEarned >= groupLevel.groupDashBoardData.totalPerfomanceGoal) {
            groupLevel.groupDashBoardData.classGreenACI = true;
            groupLevel.groupDashBoardData.classRedACI = false;
            groupLevel.groupDashBoardData.classYellowACI = false;
        }
        if ((groupLevel.groupDashBoardData.totalACIPointsEarned < groupLevel.groupDashBoardData.totalPerfomanceGoal) && (groupLevel.groupDashBoardData.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            groupLevel.groupDashBoardData.classGreenACI = false;
            groupLevel.groupDashBoardData.classRedACI = false;
            groupLevel.groupDashBoardData.classYellowACI = true;
        }
        if (groupLevel.groupDashBoardData.totalACIPointsEarned < PerformanceGoal_Calculated) {
            groupLevel.groupDashBoardData.classGreenACI = false;
            groupLevel.groupDashBoardData.classRedACI = true;
            groupLevel.groupDashBoardData.classYellowACI = false;
        }

        if (groupLevel.groupDashBoardData.aciScore === 0) {
            groupLevel.groupDashBoardData.classGreenACI = false;
            groupLevel.groupDashBoardData.classRedACI = true;
            groupLevel.groupDashBoardData.classYellowACI = false;
        }
        groupLevel.groupDataWithCSS.push(groupLevel.groupDashBoardData);

        groupLevel.groupDashBoardData = angular.copy(groupLevel.groupDataWithCSS);
    }


    groupLevel.addCSSForClinicianCPIASCore = function () {
        groupLevel.originalClinicianData = [];
        groupLevel.clinicianDataWithCSS = [];
        groupLevel.clinicianListForGroup.map(function (item) {
            groupLevel.originalClinicianData.push(item);
            if (item.cpiaScore >= 15) {
                item.classGreen = true;
                item.classYellow = false;
                item.classRed = false;
            }
            else if (item.cpiaScore < 7.5) {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = true;
            }
            else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
                item.classGreen = false;
                item.classYellow = true;
                item.classRed = false;
            }
            else {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = false;
            }

            var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
            if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
                item.classGreenACI = true;
                item.classRedACI = false;
                item.classYellowACI = false;
            }
            if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
                item.classGreenACI = false;
                item.classRedACI = false;
                item.classYellowACI = true;
            }
            if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            if (item.aciScore === 0) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }


            groupLevel.clinicianDataWithCSS.push(item);
        });
    }

    groupLevel.getClinicianData = function () {
        
        if (groupLevel.showGroup === false) {
            groupLevel.showClinicianColumns = true;
        }
        else {
            groupLevel.showClinicianColumns = false;
        }
        groupLevel.clinicianListForGroup = [];

        var groupId = groupLevel.DashBoardRowObject.idValue;
        var type = groupLevel.DashBoardRowObject.type;
        var fromDate = $filter('date')(new Date(groupLevel.DashBoardRowObject.fromDate), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(groupLevel.DashBoardRowObject.toDate), 'yyyy-MM-dd');
        var tin = groupLevel.DashBoardRowObject.tin;
        groupLevelService.getClinicianForGroup(groupLevel.aciStage, tin, fromDate, toDate).then(function (response) {            
            groupLevel.clinicianListForGroup = response.data;
            groupLevel.updateClinicianDataWithDates();
        }, function (error) {
            toastr.error("error ocuured while getting Clinicians List For the group");
        });
    }

    groupLevel.getParticipantClinicianData = function () {
        groupLevel.originalClinicianData = [];
        groupLevel.clinicianDataWithCSS = [];
        groupLevel.clinicianListForGroup.map(function (item) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');

            practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                groupLevel.originalClinicianData.push(item);
                item.aciScore = response.data.aciScore;
                item.cpiaScore = response.data.cpiaScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                groupLevel.addCSSForClinicianCPIASCore();
                if (response.data.aciScore === 0) {
                    item.disableFromDateFiled = true;
                    item.disableToDateField = true;
                }
                else {
                    item.disableFromDateFiled = false;
                    item.disableToDateField = false;
                }
            }, function (error) {
                //Removing the toaster need to update
                // toastr.error("unable load Participant Clinician data");
            });
        });

    }

    groupLevel.dateDifference = function (fromDate, toDate) {
        var firstDate = new Date(fromDate);
        var secondDate = new Date(toDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }

    groupLevel.validateDates = function (item) {
        var activityStartDatesList = [];
        if (item.type === "Individual") {
            practiceLevelService.getActivityStartDatesOfIndividual(item.npi, item.tin).then(function (response) {
                if (response.data.item2 === null || response.data.item2 === "" || response.data.item2 === undefined) {
                    if ((activityStartDatesList !== null || activityStartDatesList !== "")) {
                        activityStartDatesList = response.data;
                        var selectedDate = new Date(item.fromDate);
                        var count = 0;
                        angular.forEach(activityStartDatesList, function (value, index) {
                            var activityDate = new Date(value.activityStartDate);
                            if (activityDate > selectedDate) {
                                count += 1;
                            }
                        });
                        if (count > 0) {
                            toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                            //return false;
                        }
                    }
                }
                else {
                    toastr.error(response.data.item2);
                }
            }, function (error) {
                toastr.error("unable to load clinicians ACI Scores");
            });
        } else if (item.type === "Independent") {
            practiceLevelService.getActivityStartDatesOfIndividual(item.npi, item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }
        else {
            practiceLevelService.getActivityStartDatesOfGroup(item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }

        var registryStartDatesList = [];
        practiceLevelService.getCPIARegistryStartDates().then(function (response) {
            registryStartDatesList = response.data;
            var syndromicCount = 0;
            var immunizationCount = 0;
            var qualiRegistryCount = 0;
            var diffDays = 0;
            //check length of 
            angular.forEach(registryStartDatesList, function (value, index) {
                switch (value.registryCode) {
                    case 1:
                        diffDays = groupLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { syndromicCount += 1; }
                        break;
                    case 2:
                        diffDays = groupLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { immunizationCount += 1; }
                        break;
                    case 3:
                        break;
                    case 4:
                        diffDays = groupLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { qualiRegistryCount += 1; }
                        break;
                }
            });
            if (syndromicCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (immunizationCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (qualiRegistryCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
        }, function (error) {
            toastr.error("server error while getting Scores");
        });

    }
  
    groupLevel.updateToDate = function (item, dropbox) {        
        if (item.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period");
            return false
        }
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        if (item.reportingPeriodId === 1) { //Full Year           
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 11, 31);
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }
            if (dropbox == true)
                item.fromDate = new Date();
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
              item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);               
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90 days");
                    return false;
                }
            }
           
        }

        var itemfromDate = new Date(item.fromDate);
        var itemtoDate = new Date(item.toDate);

        if (itemfromDate.getFullYear() !== itemtoDate.getFullYear()) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        var perfYear = groupLevel.selectedPerformanceYearId.performanceYear;
       
        if ((itemfromDate.getFullYear() !== perfYear) || (itemtoDate.getFullYear() !== perfYear)) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        groupLevel.validateDates(item);

        if (item.fromDate && item.toDate) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load clinicians ACI Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load group ACI Scores");
                });
            }
            var aciReportingPeriodObj = {
                type: item.type,
                npi: item.npi,
                tin: item.tin,
                fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
            };
            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    var auditGroupName = '';
                    //storing Group Name and Individual Name if the individual is selected else storing only group name
                    if (item.type === "Individual") { //here type Individual will come for the clinicians participating in that Group.
                        auditGroupName = groupLevel.AuditGroupName + " - Individual reporting for provider name: " + item.name;
                    }
                    else {
                        auditGroupName = groupLevel.AuditGroupName;
                    }
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        groupLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("unable to update reporting period dates");
            });
        }
    }


    groupLevel.addCSSForGroupRow = function (item) {
        var rowItem = {};
        if (item.reportingPeriodId === 1) {
            item.reportingPeriodName = 'Full Year';
        }
        if (item.reportingPeriodId === 2) {
            item.reportingPeriodName = '90 Days';
        }
        if (item.reportingPeriodId === 3) {
            item.reportingPeriodName = 'Date Range';
        }
        if (item.cpiaScore >= 15) {
            item.classGreen = true;
            item.classYellow = false;
            item.classRed = false;
        }
        else if (item.cpiaScore < 7.5) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = true;
        }
        else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
            item.classGreen = false;
            item.classYellow = true;
            item.classRed = false;
        }
        else {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;
        }
        var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
        if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
            item.classGreenACI = true;
            item.classRedACI = false;
            item.classYellowACI = false;
        }
        if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = true;
        }
        if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        if (item.aciScore === 0) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        rowItem = angular.copy(item);
    }

    groupLevel.onToDateChange = function (item) {
        if (item.fromDate !== null && item.toDate !== null) {
            groupLevel.getCPIAACIScores(item);
        }
        if (item.reportingPeriodId === 0) {
            return false
        }
        if (item.reportingPeriodId === 1) { //Full Year
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 11, 31);
            groupLevel.validateDates(item);
            //get that particular row's CPIA and ACI Score``
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }

            groupLevel.validateDates(item);

            item.fromDate = new Date(item.fromDate);
            item.toDate = angular.copy(item.fromDate);
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
                item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90 days");
                    return false;
                }
                groupLevel.validateDates(item);
            }
        }

        if (item.fromDate && item.toDate) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load clinicians ACI Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load group ACI Scores");
                });
            }
            var aciReportingPeriodObj = {
                type: item.type,
                npi: item.npi,
                tin: item.tin,
                fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
            };
            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    var auditGroupName = '';
                    //storing Group Name and Individual Name if the individual is selected else storing only group name
                    if (item.type === "Individual") { //here type Individual will come for the clinicians participating in that Group.
                        auditGroupName = groupLevel.AuditGroupName + " - Individual reporting for provider name: " + item.name;
                    }
                    else {
                        auditGroupName = groupLevel.AuditGroupName;
                    }
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        groupLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("unable to update Reporting Period Dates");
            });
        }
    }

    groupLevel.getCPIAACIScores = function (item) {
        if (item.type === 'Independent') {
            practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, item.fromDate, item.toDate).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('unable to load CPIA and ACI Scores for Individual');
            });
        }
        if (item.type === 'Group') {
            practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, item.fromDate, item.toDate, item.performanceYear).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('unable to load CPIA and ACI Scores for Group');
            });
        }
    }
    groupLevel.goToCPIAActivities = function (item) {
        if (item.cpiaScore === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            return false;
        }

        groupLevel.setReportingObjTogetBackData.push(item);
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('GroupLevelDashBoard');
        var screenName = 'GroupLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        if (item.type === 'Group') {
            $state.go("groupCPIAActivities");
        }
        if (item.type === 'Independent') {
            $state.go("individualCPIAActivities");
            groupLevel.setReportingObjTogetBackData.push(groupLevel.DashBoardRowObject);
        }
        // New added 
        if (item.type === 'Individual') {
            item.name = groupLevel.DashBoardRowObject.name + " - (" + groupLevel.DashBoardRowObject.tin + ") " + item.name;
            $state.go("groupIndividualCPIAActivities");
            groupLevel.setReportingObjTogetBackData.push(groupLevel.DashBoardRowObject);
        }

        localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(groupLevel.setReportingObjTogetBackData));
        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
    }

    //to print the report
    groupLevel.print = function () {
        //set print Object
        delete groupLevel.groupDashBoardData[0]['existingFromDate'];
        delete groupLevel.groupDashBoardData[0]['existingToDate'];
        groupLevelService.setPracticeLevelPrintDashBoardObj(groupLevel.groupDashBoardData, groupLevel.clinicianListForGroup, groupLevel.showGroup);
        var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            groupLevel.loggedInUserObj = data[0].data.userName;
            groupLevel.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(groupLevel.loggedInUserObj);
            practiceLevelService.setPracticeDetails(groupLevel.practiceDetails);
            groupLevel.currentDate = Date.now();
            groupLevel.practiceDetailsPrintObj = {};
            groupLevel.clinicianList = {};
            groupLevel.clinicianList = groupLevelService.clinicianList;
            groupLevel.isShowGroup = groupLevelService.isShowGroup;
            if (typeof groupLevelService.groupsList !== 'undefined') {
                groupLevel.groupsList = angular.copy(groupLevelService.groupsList);
                groupLevel.groupsList.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                    if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                        if (count === 0) {
                            groupLevel.measureStage = muStageEnums.measureStages[0].measureName;
                        }
                        count = count + 1;
                    }
                });
            }
            groupLevel.clinicianList.map(function (value) {
                value.fromDate = $filter('date')(value.fromDate, 'MM-dd-yyyy');
                value.toDate = $filter('date')(value.toDate, 'MM-dd-yyyy');

                if (value.reportingPeriodId === 1) {
                    value.reportingPeriodName = 'Full Year';
                }
                if (value.reportingPeriodId === 2) {
                    value.reportingPeriodName = '90 Days';
                }
                if (value.reportingPeriodId === 3) {
                    value.reportingPeriodName = 'Date Range';
                }
            });


            groupLevel.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                groupLevel.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            window.setTimeout(function () {
                var printContents = $("#printGroupLevelDashBoardGrid").html();
                printContents = printContents.replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingFromDate</th>', '').replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingToDate</th>', '');
                var actualPrintContents = printContents.replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingFromDate</th>', '').replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingToDate</th>', '');
                var popupWin = window.open('', '_blank', 'width=1300,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + actualPrintContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            groupLevel.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + groupLevel.AuditGroupName);

            groupLevel.init();

        }, function (error) {
            toastr.error('error while getting loggged in user details');
        });
    }

    groupLevel.backToPrevioudScreen = function () {

        if (groupCPIA.screenName === "PracticeLevelDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
        if (groupCPIA.screenName === "ClinicianLevelDashBoard") {
            $state.go('clinicianLevelDashBoard');
        }
        if (clinicianLevel.screenName === "GroupLevelDashBoard") {
            $state.go('groupLevelDashBoard');
        }
        //groupLevel.setReportingObjTogetBackData.push(item);   // remove if any back screen other than the practice level
        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
    }
    //Saving user audit details
    groupLevel.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }
    groupLevel.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupLevelPrintController', groupLevelPrintController);
groupLevelPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'groupLevelService'];
function groupLevelPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, groupLevelService) {
    var groupLevelPrint = this;
    groupLevelPrint.init = function () {
        groupLevelPrint.currentDate = Date.now();
        groupLevelPrint.practiceDetailsPrintObj = {};
        groupLevelPrint.clinicianList = {};
        groupLevelPrint.clinicianList = groupLevelService.clinicianList;
        groupLevelPrint.isShowGroup = groupLevelService.isShowGroup;
        if (typeof groupLevelService.groupsList !== 'undefined') {
            groupLevelPrint.groupsList = angular.copy(groupLevelService.groupsList);
            groupLevelPrint.groupsList.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                    if (count === 0) {
                        groupLevelPrint.measureStage = muStageEnums.measureStages[0].measureName;
                    }
                    count = count + 1;
                }
            });
        }
        debugger;
        groupLevelPrint.clinicianList.map(function (value) {
            debugger;
            value.fromDate = $filter('date')(value.fromDate, 'MM-dd-yyyy');
            value.toDate = $filter('date')(value.toDate, 'MM-dd-yyyy');

            if (value.reportingPeriodId === 1) {
                value.reportingPeriodName = 'Full Year';
            } 
            if (value.reportingPeriodId === 2) {
                value.reportingPeriodName = '90 Days';
            } 
            if (value.reportingPeriodId === 3) {
                value.reportingPeriodName = 'Date Range';
            }
        });
      
        //if (typeof groupLevelPrint.clinicianList.length() !== 0) {
        //    groupLevelPrint.clinicianList.map(function (clinician) {
        //        clinician.fromDate = $filter('date')(clinician.fromDate, 'MM-dd-yyyy');
        //        clinician.toDate = $filter('date')(clinician.toDate, 'MM-dd-yyyy');
        //    });
        //}
        //groupLevelPrint.printObj = practiceLevelService.groupLevelPrintObject
        groupLevelPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            groupLevelPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }
        $rootScope.print = true;
        groupLevelPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("groupLevelDashBoard");
        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printGroupLevelGridButton').click();
            }, 100);
        })
    }
    groupLevelPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('practiceLevelController', practiceLevelController);
practiceLevelController.$inject = ['$state', '$scope', '$q', '$filter', 'dashBoardEnums', 'performanceYearEnums', 'practiceLevelService', 'practiceInfoService', 'muStageEnums', 'practiceLevelConstants'];
function practiceLevelController($state, $scope, $q, $filter, dashBoardEnums, performanceYearEnums, practiceLevelService, practiceInfoService, muStageEnums, practiceLevelConstants) {
    var practiceLevel = this;
    practiceLevel.model = {};
    practiceLevelService.setReportingObjTogetData = [];

    practiceLevel.init = function () {
        if (localStorage.getItem('emergencyAccess') === "true") {
            practiceLevel.LoadInit();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role === undefined || res.data.role === null || res.data.role === "") {
                        toastr.error('You are not authorized.');
                        // $state.go('Home');
                    }
                    else {
                        if (res.data.role === "ADMINISTRATOR") {
                            practiceLevel.LoadInit();
                        }
                        else if (res.data.role === "ELIGIBLE CLINICIAN") {
                            $state.go('clinicianLevelDashBoard');
                        }
                        else {
                            practiceLevel.LoadInit();
                        }

                    }
                }

            });
        }
        //  practiceLevel.checkUserRolesandPermission();


    }

    practiceLevel.LoadInit = function () {
        var date = new Date();
        practiceLevel.currentDate = new Date();
        practiceLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        practiceLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
        practiceLevel.getPracticeLevelData();
        practiceLevel.getReportingPeriodList();
        practiceLevel.getPerformanceYearList();
        practiceLevel.productkey = localStorage.getItem('ProductKey');
        practiceLevelService.getPracticeDetails().then(function (response) {
            practiceLevel.PracticeName = response.data.name;
        });
        practiceLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashPracLevel);
    }
    //to get the list of reporting periods from Enums
    practiceLevel.getReportingPeriodList = function () {
        practiceLevel.reportingPeriodList = dashBoardEnums.reportingPeriod;
    }
    practiceLevel.checkUserRolesandPermission = function () {
        if (localStorage.getItem('emergencyAccess') === "true") {
            //do code
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role.trim().toUpperCase() !== "ADMINISTRATOR" && res.data.role.trim().toUpperCase() !== "ELIGIBLE CLINICIAN") {
                        toastr.error('You are not authorized to this.');
                    }
                }

            });
        }

    }



    //to get the list of performance year from enums
    practiceLevel.getPerformanceYearList = function () {                
        practiceLevel.performanceYearList = performanceYearEnums.performanceYearList;
        var currentYear = performanceYearEnums.performanceYearList.filter(function (i) { return i.performanceYear == $filter('date')(new Date(), 'yyyy') })[0];
        if (currentYear != null)
            practiceLevel.selectedPerformanceYearId = currentYear;
        else
            practiceLevel.selectedPerformanceYearId = practiceLevel.performanceYearList[0];
    }

    //on to date change if all the fileds are valid updating the ACI score
    practiceLevel.onToDateChange = function (item) {
        if (item.fromDate !== null && item.toDate !== null) {
            practiceLevel.getCPIAACIScores(item);
        }
    }

    //to get the aci and cpia scores for a  particular row when user changes the dates
    practiceLevel.getCPIAACIScores = function (item) {
        var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
        if (item.type === 'Independent') {
            if (item.tin === '') {
                item.tin = null;
            }
            practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('server error while getting CPIA and ACI Scores for Individual');
            });
        }
        if (item.type === 'Group') {
            practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, item.fromDate, item.toDate, item.performanceYear).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('server error while getting CPIA and ACI Scores for Group');
            });
        }
    }

    //to get the difference between from date and to date
    practiceLevel.dateDifference = function (fromDate, toDate) {
        var firstDate = new Date(fromDate);
        var secondDate = new Date(toDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }

    // validating dates and showing validation alerts 
    practiceLevel.validateDates = function (item) {
        var activityStartDatesList = [];
        if (item.type === "Independent") {
            practiceLevelService.getActivityStartDatesOfIndependent(item.npi, item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");

                }
            }, function (error) {
               // toastr.error("server error while getting Activity start dates");
            });
        }
        else {
            practiceLevelService.getActivityStartDatesOfGroup(item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
               // toastr.error("server error while getting Activity start dates");
            });
        }

        var registryStartDatesList = [];
        practiceLevelService.getCPIARegistryStartDates().then(function (response) {
            registryStartDatesList = response.data;
            var syndromicCount = 0;
            var immunizationCount = 0;
            var qualiRegistryCount = 0;
            var diffDays = 0;
            //check length of 
            angular.forEach(registryStartDatesList, function (value, index) {
                switch (value.registryCode) {
                    case 1:
                        diffDays = practiceLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { syndromicCount += 1; }
                        break;
                    case 2:
                        diffDays = practiceLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { immunizationCount += 1; }
                        break;
                    case 3:
                        break;
                    case 4:
                        diffDays = practiceLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { qualiRegistryCount += 1; }
                        break;
                }
            });
            if (syndromicCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (immunizationCount > 0) {
                toastr.warning("For receiving credit, the  start date for immunization registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (qualiRegistryCount > 0) {
                toastr.warning("For receiving credit, the  start date for Clinical data registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
        }, function (error) {
           // toastr.error("server error while getting Activity start dates");
        });

    }
  
    // to update teh to date when user changed from date based on the reporting period selected
    practiceLevel.updateToDate = function (item, dropbox) {        
        if (item.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period ");
            return false;
        }
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        
        if (item.reportingPeriodId === 1) { //Full Year
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(practiceLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(practiceLevel.selectedPerformanceYearId.performanceYear, 11, 31);
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }
            if (dropbox == true)
                item.fromDate = new Date();
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
               item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {               
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);               
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.ceil(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90days.");
                    return false;
                }
            }
            
        }

        var itemfromDate = new Date(item.fromDate);
        var itemtoDate = new Date(item.toDate);

        if (itemfromDate.getFullYear() !== itemtoDate.getFullYear()) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        var perfYear = practiceLevel.selectedPerformanceYearId.performanceYear;
       
        if ((itemfromDate.getFullYear() !== perfYear) || (itemtoDate.getFullYear() !== perfYear)) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }
        //practiceLevel.performanceYearList[0].yearId

        practiceLevel.validateDates(item);

        if (item.fromDate && item.toDate) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                if (item.tin === '') {
                    item.disableACIScore = true;
                }
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    practiceLevel.addCSSForIndividualRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    practiceLevel.addCSSForIndividualRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }

            var aciReportingPeriodObj = {
                type: item.type,
                npi: item.npi,
                tin: item.tin,
                fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
            };
            
            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {                
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashPracLevel + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashPracLevel + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashPracLevel + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        practiceLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("server error while updating Reporting Period Dates");
            });
        }
    }

    //to get the list of items to display in the dashboard
    practiceLevel.getPracticeLevelData = function () {
        //get the same list on calling Service method
        practiceLevelService.getDashBoardDataForPractice().then(function (response) {
            response.data.map(function (item) {
                if (item.fromDate === '0001-01-01T00:00:00') {
                    item.fromDate = null
                    item.disableFromDateFiled = true;
                }
                if (item.toDate === '0001-01-01T00:00:00') {
                    item.toDate = null
                    item.disableToDateField = true;
                }
            });
            practiceLevel.dashBoardData = response.data;
            practiceLevel.updateDashBoardData();
        }, function (err) {
            toastr.error("server error while getting DashBoard Data");
        });
    }

    //adding css for the particular row when user changes the dates
    practiceLevel.addCSSForIndividualRow = function (item) {
        var rowItem = {};
        if (item.reportingPeriodId === 1) {
            item.reportingPeriodName = 'Full Year';
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
        }
        if (item.reportingPeriodId === 2) {
            item.reportingPeriodName = '90 Days';
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
        }
        if (item.reportingPeriodId === 3) {
            item.reportingPeriodName = 'Date Range';
        }
        if (item.cpiaScore >= 15) {
            item.classGreen = true;
            item.classYellow = false;
            item.classRed = false;
        }
        else if (item.cpiaScore < 7.5) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = true;
        }
        else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
            item.classGreen = false;
            item.classYellow = true;
            item.classRed = false;
        }
        else {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;
        }

        var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
        if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
            item.classGreenACI = true;
            item.classRedACI = false;
            item.classYellowACI = false;
        }
        if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = true;
        }
        if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        if (item.aciScore === 0) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        rowItem = angular.copy(item);
    }

    //to add the styles to the dashBoard data
    practiceLevel.updateDashBoardData = function () {

        practiceLevel.updatedData = [];
        practiceLevel.dashBoardData.map(function (item) {
            if (item.reportingPeriodId === 1) {
                item.reportingPeriodName = 'Full Year';
                item.disableFromDateFiled = true;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 2) {
                item.reportingPeriodName = '90 Days';
                item.disableFromDateFiled = false;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 3) {
                item.reportingPeriodName = 'Date Range';
                item.disableFromDateFiled = false;
                item.disableToDateField = false;
            }
            if (item.cpiaScore >= 15) {
                item.classGreen = true;
                item.classYellow = false;
                item.classRed = false;
            }
            else if (item.cpiaScore < 7.5) {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = true;
            }
            else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
                item.classGreen = false;
                item.classYellow = true;
                item.classRed = false;
            }
            else {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = false;
            }

            var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
            if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
                item.classGreenACI = true;
                item.classRedACI = false;
                item.classYellowACI = false;
            }
            if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
                item.classGreenACI = false;
                item.classRedACI = false;
                item.classYellowACI = true;
            }
            if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            if (item.aciScore === 0) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            
            practiceLevel.updatedData.push(item);
        });
    }

    //navigating to the ACI Measure Report screen
    practiceLevel.goToACIMeasureReport = function (item) {
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'PracticeLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        //practiceLevelService.setReportingObj(item);
        practiceLevelService.setReportingObjTogetData.push(item); //setReportingObjTogetData - list
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(practiceLevelService.setReportingObjTogetData));
        //practiceLevelService.setReportingObj(practiceLevelService.setReportingObjTogetData);

        $state.go("amcMeasureReport");

    }

    practiceLevel.goToGroupLevelDashBoard = function (item) {
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'PracticeLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        //show the Group DashBoard
        if (item.type === 'Group') {
            item.groupName = item.name;
            //practiceLevelService.setReportingObj(item);
            $state.go("groupLevelDashBoard");
        }
            //show Clinician DashBoard
        else {
            $state.go("clinicianLevelDashBoard");
        }
        practiceLevelService.setReportingObjTogetData.push(item);
        //practiceLevelService.setReportingObj(practiceLevelService.setReportingObjTogetData);
        localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(practiceLevelService.setReportingObjTogetData));
    }

    practiceLevel.goToCPIAActivities = function (item) {
        if (item.cpiaScore === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            return false;
        }
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'PracticeLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        if (item.type === 'Group') {
            $state.go("groupCPIAActivities");
        }
        if (item.type === 'Independent') {
            $state.go("individualCPIAActivities");
        }
        practiceLevelService.setReportingObjTogetData.push(item);
        //practiceLevelService.setReportingObj(practiceLevelService.setReportingObjTogetData);
        localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(practiceLevelService.setReportingObjTogetData));
    }

    //to print the report
    practiceLevel.print = function () {
        //set print Object
        practiceLevelService.setPracticeLevelPrintDashBoardObj(practiceLevel.updatedData);

        var promises = [
             practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            practiceLevel.loggedInUserObj = data[0].data.userName;
            practiceLevel.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(practiceLevel.loggedInUserObj);
            practiceLevelService.setPracticeDetails(practiceLevel.practiceDetails);
            practiceLevel.currentDate = Date.now()
            practiceLevelService.practiceLevelPrintObject.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                    if (count === 0) {
                        practiceLevel.measureStage = muStageEnums.measureStages[0].measureName;
                    }
                    count = count + 1;
                }
            });

            practiceLevel.printObj = practiceLevelService.practiceLevelPrintObject
            practiceLevel.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                practiceLevel.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }


            window.setTimeout(function () {
                var printContents = $("#printPracticeLevelDashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            practiceLevel.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashPracLevel);
        }, function (error) {
            toastr.error('error while getting loggged in user details');
        });
        practiceLevel.practiceDetailsPrintObj = {};

    }

    //Saving user audit details
    practiceLevel.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }
    practiceLevel.init();

    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}
'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('practiceLevelPrintController', practiceLevelPrintController);
practiceLevelPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService'];
function practiceLevelPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService) {

    var practiceLevelPrint = this;
    
    practiceLevelPrint.init = function () {
        practiceLevelPrint.practiceDetailsPrintObj = {};
        practiceLevelPrint.currentDate = Date.now()
        practiceLevelService.practiceLevelPrintObject.map(function (item) {
            var count = 0;
            item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
            item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

            if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                if (count === 0) {
                    practiceLevelPrint.measureStage = muStageEnums.measureStages[0].measureName;
                }
                count = count + 1;
            }
        });

        practiceLevelPrint.printObj = practiceLevelService.practiceLevelPrintObject
        practiceLevelPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            practiceLevelPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        practiceLevelPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("practiceLevelDashBoard");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printPraciceLevelGridButton').click();
            }, 100);
        })
    }
    practiceLevelPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}