﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('clinicianLevelPrintController', clinicianLevelPrintController);
clinicianLevelPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'clinicianLevelService'];
function clinicianLevelPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, clinicianLevelService) {

    var clinicianLevelPrint = this;

    clinicianLevelPrint.init = function () {
        clinicianLevelPrint.practiceDetailsPrintObj = {};
        clinicianLevelPrint.groupObj = {};
        clinicianLevelPrint.currentDate = Date.now();

        clinicianLevelPrint.clinicianObj = clinicianLevelService.clinicianPrintObj;
        clinicianLevelPrint.groupObj = clinicianLevelService.groupPrintObj;

        clinicianLevelPrint.clinicianName = clinicianLevelPrint.clinicianObj[0].name;
        clinicianLevelPrint.clinicianNpi = clinicianLevelPrint.clinicianObj[0].npi;

        if (typeof clinicianLevelPrint.clinicianObj !== 'undefined') {
            clinicianLevelPrint.clinicianObj.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                muStageEnums.measureStages.map(function (measure) {
                    if (measure.measureCode === item.measureCode) {
                        if (count === 0) {
                            clinicianLevelPrint.measureStage = muStageEnums.measureStages[0].measureName;
                        }
                        count = count + 1;
                    }
                })
            });
        }
        if (typeof clinicianLevelPrint.groupObj.length !== 'undefined') {
            clinicianLevelPrint.groupObj.map(function (group) {
                group.fromDate = $filter('date')(group.fromDate, 'MM-dd-yyyy');
                group.toDate = $filter('date')(group.toDate, 'MM-dd-yyyy');
            })
        }
        clinicianLevelPrint.printObj = practiceLevelService.practiceLevelPrintObject
        clinicianLevelPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            clinicianLevelPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        clinicianLevelPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("clinicianLevelDashBoard");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printClinicianLevelGridButton').click();
            }, 100);
        })
    }
    clinicianLevelPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}