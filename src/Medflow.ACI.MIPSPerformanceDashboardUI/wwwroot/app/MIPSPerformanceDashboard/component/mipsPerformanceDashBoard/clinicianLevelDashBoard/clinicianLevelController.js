﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('clinicianLevelController', clinicianLevelController);
clinicianLevelController.$inject = ['$state', '$scope', '$filter', '$q', 'dashBoardEnums', 'practiceLevelService', 'practiceInfoService', 'clinicianLevelService', 'performanceYearEnums', 'muStageEnums', 'practiceLevelConstants'];
function clinicianLevelController($state, $scope, $filter, $q, dashBoardEnums, practiceLevelService, practiceInfoService, clinicianLevelService, performanceYearEnums, muStageEnums, practiceLevelConstants) {
    var clinicianLevel = this;

    clinicianLevel.init = function () {
        clinicianLevel.BackBtn = true;
        if (localStorage.getItem('emergencyAccess') === "true") {
            clinicianLevel.loadInit();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role === undefined || res.data.role === null || res.data.role === "") {
                        toastr.error('You are not authorized.');
                        // $state.go('Home');
                    }
                    else {
                        if (res.data.role === "ADMINISTRATOR") {
                            clinicianLevel.loadInit();
                        }
                        else if (res.data.role === "ELIGIBLE CLINICIAN") {
                            clinicianLevel.getIndividualDashboardInit(res.data.userId);
                        }
                        else {
                            clinicianLevel.loadInit();
                        }
                    }
                }
            });
        }
    }

    clinicianLevel.getIndividualDashboardInit = function (UserId) {
        clinicianLevel.screenTitle = "IndividualDashBoard";
        clinicianLevel.model = {};
        clinicianLevel.getReportingObjTogetBackData = [];
        clinicianLevel.getListOfGroupsForSelectedClinicianForIndividualDashBoard(UserId)

    }
    clinicianLevel.loadInit = function () {
        clinicianLevel.screenTitle = "IndividualDashBoard";
        clinicianLevel.model = {};
        clinicianLevel.getReportingObjTogetBackData = [];
        clinicianLevel.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
        clinicianLevel.DashBoardRowObject = clinicianLevel.getReportingObjTogetBackData[0];
        clinicianLevel.aciStage = clinicianLevel.DashBoardRowObject.measureCode;
        clinicianLevel.setReportingObjTogetBackData = [];
        //clinicianLevel.DashBoardRowObject = practiceLevelService.reportingObject;
        //clinicianLevel.DashBoardRowObject = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
        var date = new Date();
        clinicianLevel.currentDate = new Date();
        clinicianLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        clinicianLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
        clinicianLevel.selectedPerformanceYear =null;
        clinicianLevel.getPerformanceYearList();
        clinicianLevel.getReportingPeriodList();
        clinicianLevel.getClinicianData();
        clinicianLevel.getListOfGroupsForSelectedClinician();
        clinicianLevel.productkey = localStorage.getItem('ProductKey');
        clinicianLevel.BackBtn = false;
        clinicianLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + clinicianLevel.AuditClinicianName);
    }
    //to get the list of performance year from enums
    clinicianLevel.getPerformanceYearList = function () {
        clinicianLevel.performanceYearList = performanceYearEnums.performanceYearList;        
        var currentYear = performanceYearEnums.performanceYearList.find(i=>i.performanceYear == $filter('date')(new Date(), 'yyyy'));
        clinicianLevel.selectedPerformanceYear = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.fromDate), 'yyyy');
        if (currentYear != null) {
            clinicianLevel.selectedPerformanceYearId = currentYear;
            //clinicianLevel.selectedPerformanceYear = currentYear["performanceYear"];
        }
        else
            clinicianLevel.selectedPerformanceYearId = clinicianLevel.performanceYearList[0];
    }


    clinicianLevel.updateGroupList = function () {
        clinicianLevel.groupDataOfClinician.map(function (item) {
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            if (item.fromDate === null && item.toDate === null) {
                item.reportingPeriodId = 0;
            }
            else {
                var individualdiffDays = clinicianLevel.dateDifference(item.fromDate, item.toDate);
                if (individualdiffDays > 363) {
                    item.reportingPeriodId = 1;

                }
                else if (individualdiffDays > 89) {
                    item.reportingPeriodId = 3;
                    item.disableToDateField = false;
                    item.disableFromDateFiled = false;
                }
                else if (individualdiffDays === 89) {
                    item.reportingPeriodId = 2;
                    item.disableToDateField = true;
                    item.disableFromDateFiled = false;
                }
                else {
                    item.reportingPeriodId = 0;
                    item.disableFromDateFiled = false;
                    item.disableToDateField = false;
                }
            }
            if (item.reportingPeriodId === 1) {
                item.reportingPeriodName = 'Full Year';
            }
            if (item.reportingPeriodId === 2) {
                item.reportingPeriodName = '90 Days';
            }
            if (item.reportingPeriodId === 3) {
                item.reportingPeriodName = 'Date Range';
            }
            if (clinicianLevel.clinicianData !== null && clinicianLevel.clinicianData !== undefined && clinicianLevel.clinicianData !== "")
                item.ClinincianName = clinicianLevel.clinicianData[0].name;


        });
        clinicianLevel.groupListForClinician = angular.copy(clinicianLevel.groupDataOfClinician);
        clinicianLevel.addCssForGroupsList();
    }

    clinicianLevel.addCssForGroupsList = function () {
        clinicianLevel.originalClinicianData = [];
        clinicianLevel.groupDataWithCSS = [];
        clinicianLevel.groupDataOfClinician.map(function (item) {
            clinicianLevel.originalClinicianData.push(item);
            if (item.cpiaScore >= 15) {
                item.classGreen = true;
                item.classYellow = false;
                item.classRed = false;
            }
            else if (item.cpiaScore < 7.5) {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = true;
            }
            else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
                item.classGreen = false;
                item.classYellow = true;
                item.classRed = false;
            }
            else {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = false;
            }

            var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
            if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
                item.classGreenACI = true;
                item.classRedACI = false;
                item.classYellowACI = false;
            }
            if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
                item.classGreenACI = false;
                item.classRedACI = false;
                item.classYellowACI = true;
            }
            if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            if (item.aciScore === 0) {
                item.classGreenACI = false;
                item.classYellowACI = false;
                if (clinicianLevel.selectedPerformanceYear == "2018") {
                    item.classRedACI = true;
                    item.classGrayACI = false;
                }
                else {
                    item.classRedACI = false;
                    item.classGrayACI = true;
                }
            }

            item.type = 'Individual'; //'Group';
            clinicianLevel.groupDataWithCSS.push(item);
        });
        clinicianLevel.groupDataOfClinician = angular.copy(clinicianLevel.groupDataWithCSS);
    }

    clinicianLevel.goToPracticeDashBoard = function () {
        if (clinicianLevel.screenTitle === "IndividualDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
    }
    clinicianLevel.getClinicianData = function () {
        clinicianLevel.clinicianData = [];
        //clinicianLevel.selectedPerformanceYear = clinicianLevel.selectedPerformanceYearId.performanceYear;
        if (typeof clinicianLevel.DashBoardRowObject !== 'undefined') {
            clinicianLevel.DashBoardRowObject.fromDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.fromDate), 'yyyy-MM-dd');
            clinicianLevel.DashBoardRowObject.toDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.toDate), 'yyyy-MM-dd');

            if (clinicianLevel.DashBoardRowObject.fromDate === "0001-01-01" && clinicianLevel.DashBoardRowObject.toDate === "0001-01-01") {
                clinicianLevel.DashBoardRowObject.fromDate = null;
                clinicianLevel.DashBoardRowObject.toDate = null;
                clinicianLevel.DashBoardRowObject.measureCode = 0;
                clinicianLevel.clinicianData = null;
            }
            else {
                clinicianLevel.clinicianData.push(clinicianLevel.DashBoardRowObject);
                clinicianLevel.groupDataOfClinician = clinicianLevel.clinicianData;
                clinicianLevel.updateGroupList();
            }
        }
        else {
            clinicianLevel.clinicianData.push(clinicianLevel.DashBoardRowObject);
        }
        clinicianLevel.AuditClinicianName = clinicianLevel.DashBoardRowObject.name;
        if (clinicianLevel.clinicianData !== null)
            clinicianLevel.clinicianData[0].type = "Independent";
    }

    clinicianLevel.goToACIMeasureReport = function (item) {
        if (item.fromDate === null && item.toDate === null && item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }

        if (item.type === "Individual") {
            item.tin = item.groupTIN;
        }

        item.measureCode = item.measureCode;
        clinicianLevel.setReportingObjTogetBackData.push(item); // [0]

        if (item.type === "Group") {
            clinicianLevel.setReportingObjTogetBackData.push(clinicianLevel.DashBoardRowObject); // [1]
        }

        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("AciScoreObjectdata", JSON.stringify(item));
        // practiceLevelService.setScreenName('ClinicianLevelDashBoard');
        var screenName = 'ClinicianLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);
        $state.go("amcMeasureReport");
        // practiceLevelService.setReportingObj(clinicianLevel.setReportingObjTogetBackData);
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(clinicianLevel.setReportingObjTogetBackData));
    }

    clinicianLevel.getListOfGroupsForSelectedClinician = function () {
        var clinicianId = clinicianLevel.DashBoardRowObject.clinicianId;
        var type = clinicianLevel.DashBoardRowObject.type;
        var fromDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.fromDate), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(clinicianLevel.DashBoardRowObject.toDate), 'yyyy-MM-dd');
        var tin = clinicianLevel.DashBoardRowObject.tin;
        var npi = clinicianLevel.DashBoardRowObject.npi;
        var measureCode = clinicianLevel.DashBoardRowObject.measureCode;

        clinicianLevelService.getGroupsForClinician(measureCode, tin, npi, fromDate, toDate, clinicianLevel.selectedPerformanceYear).then(function (response) {
            clinicianLevel.groupDataOfClinician = response.data;
            clinicianLevel.updateGroupList();
        }, function (err) {
            toastr.error('server error while getting groups');
        });

    }


    clinicianLevel.getListOfGroupsForSelectedClinicianForIndividualDashBoard = function (UserId) {
        clinicianLevelService.getClinicianMeasuresForIndividualDashBoard(UserId).then(function (response) {
            clinicianLevel.DashBoardRowObject = response.data;
            localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(response.data));
            clinicianLevel.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
            clinicianLevel.DashBoardRowObject = clinicianLevel.getReportingObjTogetBackData[0];
            clinicianLevel.aciStage = clinicianLevel.DashBoardRowObject.measureCode;
            clinicianLevel.setReportingObjTogetBackData = [];
            //clinicianLevel.DashBoardRowObject = practiceLevelService.reportingObject;
            //clinicianLevel.DashBoardRowObject = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));
            var date = new Date();
            clinicianLevel.currentDate = new Date();
            clinicianLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
            clinicianLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
            clinicianLevel.getReportingPeriodList();
            clinicianLevel.getClinicianData();
            clinicianLevel.getPerformanceYearList();
            clinicianLevel.getListOfGroupsForSelectedClinician();

            clinicianLevel.productkey = localStorage.getItem('ProductKey');
            clinicianLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + clinicianLevel.AuditClinicianName);
            // clinicianLevel.getClinicianData();
            // clinicianLevel.updateGroupList();
        }, function (err) {
            toastr.error('server error while getting Individual Details');
        });

    }

    clinicianLevel.getReportingPeriodList = function () {
        clinicianLevel.reportingPeriodList = dashBoardEnums.reportingPeriod;
    }

    clinicianLevel.dateDifference = function (fromDate, toDate) {
        var firstDate = new Date(fromDate);
        var secondDate = new Date(toDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }

    clinicianLevel.validateDates = function (item) {
        var activityStartDatesList = [];
        if (item.type === "Individual") {
            practiceLevelService.getActivityStartDatesOfIndividual(item.npi, item.groupTIN).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }
        else if (item.type === "Independent") {
            practiceLevelService.getActivityStartDatesOfIndependent(item.npi, item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }
        else {
            practiceLevelService.getActivityStartDatesOfGroup(item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }

        var registryStartDatesList = [];
        practiceLevelService.getCPIARegistryStartDates().then(function (response) {
            registryStartDatesList = response.data;
            var syndromicCount = 0;
            var immunizationCount = 0;
            var qualiRegistryCount = 0;
            var diffDays = 0;
            //check length of 
            angular.forEach(registryStartDatesList, function (value, index) {
                switch (value.registryCode) {
                    case 1:
                        diffDays = clinicianLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { syndromicCount += 1; }
                        break;
                    case 2:
                        diffDays = clinicianLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { immunizationCount += 1; }
                        break;
                    case 3:
                        break;
                    case 4:
                        diffDays = clinicianLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { qualiRegistryCount += 1; }
                        break;
                }
            });
            if (syndromicCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (immunizationCount > 0) {
                toastr.warning("For receiving credit, the  start date for immunization registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (qualiRegistryCount > 0) {
                toastr.warning("For receiving credit, the  start date for Clinical data registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
        }, function (error) {
            toastr.error("server error while getting Scores");
        });

    }

    clinicianLevel.updateToDate = function (item, dropbox) {
        
        if (item.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period");
            return false
        }
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        if (item.reportingPeriodId === 1) { //Full Year         
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(clinicianLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(clinicianLevel.selectedPerformanceYearId.performanceYear, 11, 31);
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }
            if (dropbox == true)
                item.fromDate = new Date();
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
                item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90 days");
                    return false;
                }
            }

            if (item.fromDate && item.toDate) {
                var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
                var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
                if (item.type === 'Independent' || item.type === 'Individual') {
                    practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                        item.aciScore = response.data.aciScore;
                        //item.cpiaScore = response.data.cpiaScore;
                        item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                        clinicianLevel.addCSSForGroupRow(item);
                    }, function (error) {
                        toastr.error("server error while getting Scores");
                    });
                }
                //if (item.type === 'Individual') {
                //    practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                //        item.aciScore = response.data.aciScore;
                //        item.cpiaScore = response.data.cpiaScore;
                //        item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                //        item.totalPerfomanceGoal = response.data.totalPerfomanceGoal;
                //        clinicianLevel.addCSSForGroupRow(item);
                //    }, function (error) {
                //        toastr.error("server error while getting Scores");
                //    });
                //}
                if (item.type === 'Group') {
                    practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                        item.aciScore = response.data.aciScore;
                        //item.cpiaScore = response.data.cpiaScore;
                        item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                        clinicianLevel.addCSSForGroupRow(item);
                    }, function (error) {
                        toastr.error("server error while getting Scores");
                    });
                }
            }
        }

        var itemfromDate = new Date(item.fromDate);
        var itemtoDate = new Date(item.toDate);

        if (itemfromDate.getFullYear() !== itemtoDate.getFullYear()) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }
        
        var perfYear = clinicianLevel.selectedPerformanceYearId.performanceYear;
        
        if ((itemfromDate.getFullYear() !== perfYear) || (itemtoDate.getFullYear() !== perfYear)) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        clinicianLevel.validateDates(item);

        if (item.fromDate && item.toDate) {
            if (item.reportingPeriodId === 0) {
                toastr.error("Please select valid Reporting Period");
                return false
            }
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            if (item.type === 'Individual') {
                practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    clinicianLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            var aciReportingPeriodObj = {};
            if (item.type === 'Individual') {
                aciReportingPeriodObj = {
                    type: item.type,
                    npi: item.npi,
                    tin: item.groupTIN,
                    fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                    toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
                };
            } else {
                aciReportingPeriodObj = {
                    type: item.type,
                    npi: item.npi,
                    tin: item.tin,
                    fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                    toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
                };
            }

            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    var auditClinicianName = '';
                    //storing clinician name if the type is Independent else storing Group name & clinician name if group is selected.
                    if (item.type === "Individual") {//here type Individual will come for clinician belonging to the different Groups.
                        auditClinicianName = clinicianLevel.AuditClinicianName + " - Group name: " + item.name;;
                    }
                    else { //here type would be Independent for the clinician
                        auditClinicianName = clinicianLevel.AuditClinicianName;
                    }
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + auditClinicianName + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + auditClinicianName + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + auditClinicianName + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        clinicianLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("server error while updating Reporting Period Dates");
            });
        }

    }

    clinicianLevel.addCSSForGroupRow = function (item) {
        var rowItem = {};
        if (item.reportingPeriodId === 1) {
            item.reportingPeriodName = 'Full Year';
        }
        if (item.reportingPeriod === 2) {
            item.reportingPeriodName = '90 Days';
        }
        if (item.reportingPeriodId === 3) {
            item.reportingPeriodName = 'Date Range';
        }
        if (item.cpiaScore >= 15) {
            item.classGreen = true;
            item.classYellow = false;
            item.classRed = false;
        }
        else if (item.cpiaScore < 7.5) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = true;
        }
        else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
            item.classGreen = false;
            item.classYellow = true;
            item.classRed = false;
        }
        else {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;
        }

        var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
        if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
            item.classGreenACI = true;
            item.classRedACI = false;
            item.classYellowACI = false;
        }
        if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = true;
        }
        if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        if (item.aciScore === 0) {
            item.classGreenACI = false;
            item.classYellowACI = false;
            if (clinicianLevel.selectedPerformanceYear == "2018") {
                item.classRedACI = true;
                item.classGrayACI = false;
            }
            else {
                item.classRedACI = false;
                item.classGrayACI = true;
            }

        }

        rowItem = angular.copy(item);
    }

    clinicianLevel.goToCPIAActivities = function (item) {
        if (item.cpiaScore === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            return false;
        }
        item.measureCode = clinicianLevel.aciStage;
        //practiceLevelService.setScreenName('ClinicianLevelDashBoard');
        var screenName = 'ClinicianLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);
        clinicianLevel.setReportingObjTogetBackData.push(item);


        if (item.type === 'Group') {
            $state.go("groupCPIAActivities");
            item.tin = item.groupTIN;
            clinicianLevel.setReportingObjTogetBackData.push(clinicianLevel.DashBoardRowObject);
        }
        if (item.type === 'Independent') {
            //localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(item));
            //practiceLevelService.setReportingObj(item);
            $state.go("individualCPIAActivities");
        }
        // New added 
        if (item.type === 'Individual') {
            item.tin = item.groupTIN;
            item.name = item.name + " - (" + item.groupTIN + ") " + item.ClinincianName;
            $state.go("groupIndividualCPIAActivities");
            clinicianLevel.setReportingObjTogetBackData.push(clinicianLevel.DashBoardRowObject);
        }
        //practiceLevelService.setReportingObj(item);
        //practiceLevelService.setReportingObj(clinicianLevel.setReportingObjTogetBackData);
        localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(clinicianLevel.setReportingObjTogetBackData));
    }

    clinicianLevel.print = function () {
        //set print Object
        clinicianLevel.updateGroupList();
        delete clinicianLevel.clinicianData[0]['existingFromDate'];
        delete clinicianLevel.clinicianData[0]['existingToDate'];
        clinicianLevelService.setClinicianLevelPrintDashBoardObj(clinicianLevel.clinicianData, clinicianLevel.groupListForClinician);

        var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            clinicianLevel.loggedInUserObj = data[0].data.userName;
            clinicianLevel.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(clinicianLevel.loggedInUserObj);
            practiceLevelService.setPracticeDetails(clinicianLevel.practiceDetails);

            clinicianLevel.practiceDetailsPrintObj = {};
            clinicianLevel.groupObj = {};
            clinicianLevel.currentDate = Date.now();

            clinicianLevel.clinicianObj = clinicianLevelService.clinicianPrintObj;
            clinicianLevel.groupObj = clinicianLevelService.groupPrintObj;

            clinicianLevel.clinicianName = clinicianLevel.clinicianObj[0].name;
            clinicianLevel.clinicianNpi = clinicianLevel.clinicianObj[0].npi;

            if (typeof clinicianLevel.clinicianObj !== 'undefined') {
                clinicianLevel.clinicianObj.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                    muStageEnums.measureStages.map(function (measure) {
                        if (measure.measureCode === item.measureCode) {
                            if (count === 0) {
                                clinicianLevel.measureStage = muStageEnums.measureStages[0].measureName;
                            }
                            count = count + 1;
                        }
                    })
                });
            }
            if (typeof clinicianLevel.groupObj.length !== 'undefined') {
                clinicianLevel.groupObj.map(function (group) {
                    group.fromDate = $filter('date')(group.fromDate, 'MM-dd-yyyy');
                    group.toDate = $filter('date')(group.toDate, 'MM-dd-yyyy');
                })
            }
            clinicianLevel.printObj = practiceLevelService.practiceLevelPrintObject
            clinicianLevel.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                clinicianLevel.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }


            window.setTimeout(function () {
                var printContents = $("#printclinicianLevelDashBoardGrid").html();
                printContents = printContents.replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingFromDate</th>', '').replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingToDate</th>', '');
                var actualPrintContents = printContents.replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingFromDate</th>', '').replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingToDate</th>', '');
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + actualPrintContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            clinicianLevel.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashIndividualLevel + ": " + clinicianLevel.AuditClinicianName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }


    clinicianLevel.backToPrevioudScreen = function () {

        if (clinicianLevel.screenName === "PracticeLevelDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
        if (clinicianLevel.screenName === "ClinicianLevelDashBoard") {
            $state.go('clinicianLevelDashBoard');
        }
        if (clinicianLevel.screenName === "GroupLevelDashBoard") {
            $state.go('groupLevelDashBoard');
        }
        //clinicianLevel.setReportingObjTogetBackData.push(item);   // remove if any back screen other than the practice level
        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
    }
    //Saving user audit details
    clinicianLevel.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }

    clinicianLevel.onYearChange = function () {
        clinicianLevel.getClinicianData();
        clinicianLevel.getListOfGroupsForSelectedClinician();
    }
    clinicianLevel.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}