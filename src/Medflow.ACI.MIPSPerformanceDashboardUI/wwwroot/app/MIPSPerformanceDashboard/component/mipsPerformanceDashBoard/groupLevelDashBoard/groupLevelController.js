﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupLevelController', groupLevelController);
groupLevelController.$inject = ['$state', '$scope', '$filter', '$q', 'dashBoardEnums', 'performanceYearEnums', 'practiceLevelService', 'groupLevelService', 'muStageEnums', 'practiceLevelConstants'];
function groupLevelController($state, $scope, $filter, $q, dashBoardEnums, performanceYearEnums, practiceLevelService, groupLevelService, muStageEnums, practiceLevelConstants) {
    var groupLevel = this;
    groupLevel.screenTitle = "GroupLevelDashBoard";
    groupLevel.model = {};
    groupLevel.getReportingObjTogetBackData = [];
    groupLevel.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));

    //groupLevel.DashBoardRowObject = practiceLevelService.reportingObject;
    //groupLevel.DashBoardRowObject = JSON.parse(localStorage.getItem('GroupIndpendentObjectdata'));

    groupLevel.DashBoardRowObject = groupLevel.getReportingObjTogetBackData[0];
    groupLevel.aciStage = groupLevel.DashBoardRowObject.measureCode;
    groupLevel.setReportingObjTogetBackData = [];

    groupLevel.init = function () {
        groupLevel.showGrayBgForClinician = true;
        groupLevel.selectedPerformanceYear = null;
        groupLevel.getPerformanceYearList();
        groupLevel.getClinicianData();

        var date = new Date();
        groupLevel.currentDate = new Date();
        groupLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        groupLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);

        groupLevel.showGroup = true;
        groupLevel.checkGroup = true;
        groupLevel.showClinicianColumns = false;

        groupLevel.currentDate = new Date();
        groupLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        groupLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
        groupLevel.getReportingPeriodList();
        groupLevel.getGroupData();
        groupLevel.productkey = localStorage.getItem('ProductKey');
        groupLevel.AuditGroupName = groupLevel.DashBoardRowObject.name;
        groupLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + groupLevel.AuditGroupName);
    }

    groupLevel.goToPracticeDashBoard = function () {
        if (groupLevel.screenTitle === "GroupLevelDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
    }

    groupLevel.goToACIMeasureReport = function (item) {
        if (item.measureCode === 0 && item.aciScore === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        if (groupLevel.showGroup) { // Group selected
            item.fromDate = groupLevel.DashBoardRowObject.fromDate;
            item.toDate = groupLevel.DashBoardRowObject.toDate;
            item.measureCode = groupLevel.aciStage;
            item.tin = groupLevel.DashBoardRowObject.tin;
            //item.idValue = groupLevel.DashBoardRowObject.idValue;
        }

        item.groupName = groupLevel.DashBoardRowObject.name;
        item.groupNameWithTIN = groupLevel.DashBoardRowObject.name + '-' + groupLevel.DashBoardRowObject.tin;
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("AciScoreObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('GroupLevelDashBoard');
        var screenName = 'GroupLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        $state.go("amcMeasureReport");
        groupLevel.setReportingObjTogetBackData.push(item);

        if (item.type === "Individual") {
            groupLevel.setReportingObjTogetBackData.push(groupLevel.DashBoardRowObject);
        }


        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(groupLevel.setReportingObjTogetBackData));
    }

    groupLevel.getReportingPeriodList = function () {
        groupLevel.reportingPeriodList = dashBoardEnums.reportingPeriod;
    }
    groupLevel.getPerformanceYearList = function () {
        groupLevel.performanceYearList = performanceYearEnums.performanceYearList;
        var currentYear = performanceYearEnums.performanceYearList.filter(function (i) { return i.performanceYear == $filter('date')(new Date(), 'yyyy') })[0];
        groupLevel.selectedPerformanceYear = $filter('date')(new Date(groupLevel.DashBoardRowObject.fromDate), 'yyyy')
        var selectedYear = parseInt(groupLevel.selectedPerformanceYear);
        if (currentYear["performanceYear"] == selectedYear)
        {
            groupLevel.selectedPerformanceYearId = currentYear;
            //groupLevel.selectedPerformanceYear = currentYear["performanceYear"]
        }
        else
            groupLevel.selectedPerformanceYearId = groupLevel.performanceYearList[0];
    }
    groupLevel.showGroupsGrid = function () {
        groupLevel.showGroup = true;
        groupLevel.checkGroup = true;
        groupLevel.showClinicianColumns = false;
        groupLevel.showGroupColumns = true;
        groupLevel.getGroupData();
        groupLevel.removeCSSForClinicianData();
        groupLevel.getClinicianData();
    }

    groupLevel.showClinicianGrid = function () {
        groupLevel.showGroup = false;
        groupLevel.checkGroup = false;
        groupLevel.showClinicianColumns = true;
        groupLevel.showGroupColumns = false;
        groupLevel.removeCSSForGroupData();
        groupLevel.updateClinicianDataWithDates();
        // groupLevel.addCSSForClinicianCPIASCore();
        groupLevel.getParticipantClinicianData();
    }

    groupLevel.updateClinicianDataWithDates = function () {
        groupLevel.clinicianListForGroup.map(function (item) {            
            //groupLevel.groupDashBoardData[0] = groupLevel.clinicianListForGroup[0];
            item.tin = groupLevel.groupDashBoardData[0].tin;
            //item.fromDate = groupLevel.clinicianListForGroup[0].fromDate;
            //item.toDate = groupLevel.clinicianListForGroup[0].toDate;
            if (item.fromDate === null && item.toDate === null) {
                item.reportingPeriodId = 0;
            }
            else {
                var groupIndividualdiffDays = groupLevel.dateDifference(item.fromDate, item.toDate);
                if (groupIndividualdiffDays > 363)
                    item.reportingPeriodId = 1;
                else if (groupIndividualdiffDays > 89)
                    item.reportingPeriodId = 3;
                else if (groupIndividualdiffDays === 89)
                    item.reportingPeriodId = 2;
                else
                    item.reportingPeriodId = 0;
            }
            if (item.reportingPeriodId === 1) {
                item.reportingPeriodName = 'Full Year';
                item.disableFromDateFiled = true;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 2) {
                item.reportingPeriodName = '90 Days';
                item.disableFromDateFiled = false;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 3) {
                item.reportingPeriodName = 'Date Range';
                item.disableFromDateFiled = false;
                item.disableToDateField = false;
            }
            item.type = "Individual";

        });
    }

    groupLevel.removeCSSForGroupData = function () {
        groupLevel.groupDashBoardData = [];
        groupLevel.originalGroupData.map(function (item) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;

            item.classGreenACI = false;
            item.classYellowACI = false;
            item.classRedACI = false;
            item.classGrayACI = false;
            groupLevel.groupDashBoardData.push(item);

        })
    }
    groupLevel.removeCSSForClinicianData = function () {
        groupLevel.clinicianDashBoardData = [];
        groupLevel.clinicianListForGroup.map(function (item) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;

            item.classGreenACI = false;
            item.classYellowACI = false;
            item.classRedACI = false;
            groupLevel.clinicianDashBoardData.push(item);
        })        
        groupLevel.clinicianListForGroup = angular.copy(groupLevel.clinicianDashBoardData);
    }
    groupLevel.getGroupData = function () {
        groupLevel.groupDashBoardData = [];
        groupLevel.groupDashBoardData = angular.copy(groupLevel.DashBoardRowObject);
        groupLevel.addCSSForGroupCPIASCore();
        groupLevel.addCSSForClinicianCPIASCore();
    }

    groupLevel.addCSSForGroupCPIASCore = function () {
        groupLevel.originalGroupData = [];
        groupLevel.originalGroupData.push(groupLevel.groupDashBoardData);
        groupLevel.groupDataWithCSS = [];

        if (groupLevel.groupDashBoardData.cpiaScore >= 15) {
            groupLevel.groupDashBoardData.classGreen = true;
            groupLevel.groupDashBoardData.classYellow = false;
            groupLevel.groupDashBoardData.classRed = false;
        }
        else if (groupLevel.groupDashBoardData.cpiaScore < 7.5) {
            groupLevel.groupDashBoardData.classGreen = false;
            groupLevel.groupDashBoardData.classYellow = false;
            groupLevel.groupDashBoardData.classRed = true;
        }
        else if (groupLevel.groupDashBoardData.cpiaScore < 15 && groupLevel.groupDashBoardData.cpiaScore >= 7.5) {
            groupLevel.groupDashBoardData.classGreen = false;
            groupLevel.groupDashBoardData.classYellow = true;
            groupLevel.groupDashBoardData.classRed = false;
        }
        else {
            groupLevel.groupDashBoardData.classGreen = false;
            groupLevel.groupDashBoardData.classYellow = false;
            groupLevel.groupDashBoardData.classRed = false;
        }


        var PerformanceGoal_Calculated = (groupLevel.groupDashBoardData.totalPerfomanceGoal) * 90 / 100;
        if (groupLevel.groupDashBoardData.totalACIPointsEarned >= groupLevel.groupDashBoardData.totalPerfomanceGoal) {
            groupLevel.groupDashBoardData.classGreenACI = true;
            groupLevel.groupDashBoardData.classRedACI = false;
            groupLevel.groupDashBoardData.classYellowACI = false;
        }
        if ((groupLevel.groupDashBoardData.totalACIPointsEarned < groupLevel.groupDashBoardData.totalPerfomanceGoal) && (groupLevel.groupDashBoardData.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            groupLevel.groupDashBoardData.classGreenACI = false;
            groupLevel.groupDashBoardData.classRedACI = false;
            groupLevel.groupDashBoardData.classYellowACI = true;
        }
        if (groupLevel.groupDashBoardData.totalACIPointsEarned < PerformanceGoal_Calculated) {
            groupLevel.groupDashBoardData.classGreenACI = false;
            groupLevel.groupDashBoardData.classRedACI = true;
            groupLevel.groupDashBoardData.classYellowACI = false;
        }

        if (groupLevel.groupDashBoardData.aciScore === 0) {
            groupLevel.groupDashBoardData.classGreenACI = false;
            
            groupLevel.groupDashBoardData.classYellowACI = false;
            if (groupLevel.selectedPerformanceYear == "2018") {
                groupLevel.groupDashBoardData.classRedACI = true;
                groupLevel.groupDashBoardData.classGrayACI = false;
            }
            else {
                groupLevel.groupDashBoardData.classRedACI = false;
                groupLevel.groupDashBoardData.classGrayACI = true;
            }
        }
        groupLevel.groupDataWithCSS.push(groupLevel.groupDashBoardData);

        groupLevel.groupDashBoardData = angular.copy(groupLevel.groupDataWithCSS);
    }


    groupLevel.addCSSForClinicianCPIASCore = function () {
        groupLevel.originalClinicianData = [];
        groupLevel.clinicianDataWithCSS = [];
        groupLevel.clinicianListForGroup.map(function (item) {
            groupLevel.originalClinicianData.push(item);
            if (item.cpiaScore >= 15) {
                item.classGreen = true;
                item.classYellow = false;
                item.classRed = false;
            }
            else if (item.cpiaScore < 7.5) {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = true;
            }
            else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
                item.classGreen = false;
                item.classYellow = true;
                item.classRed = false;
            }
            else {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = false;
            }

            var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
            if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
                item.classGreenACI = true;
                item.classRedACI = false;
                item.classYellowACI = false;
            }
            if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
                item.classGreenACI = false;
                item.classRedACI = false;
                item.classYellowACI = true;
            }
            if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            if (item.aciScore === 0) {
                item.classGreenACI = false;
                item.classYellowACI = false;
                if (groupLevel.selectedPerformanceYear == "2018")
                {
                    item.classRedACI = true;
                    item.classGrayACI = false;
                }
                else {
                    item.classRedACI = false;
                    item.classGrayACI = true;
                }
            }


            groupLevel.clinicianDataWithCSS.push(item);
        });
    }

    groupLevel.getClinicianData = function () {
        
        if (groupLevel.showGroup === false) {
            groupLevel.showClinicianColumns = true;
        }
        else {
            groupLevel.showClinicianColumns = false;
        }
        groupLevel.clinicianListForGroup = [];

        var groupId = groupLevel.DashBoardRowObject.idValue;
        var type = groupLevel.DashBoardRowObject.type;
        var fromDate = $filter('date')(new Date(groupLevel.DashBoardRowObject.fromDate), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(groupLevel.DashBoardRowObject.toDate), 'yyyy-MM-dd');
        var tin = groupLevel.DashBoardRowObject.tin;
        //groupLevel.selectedPerformanceYear = $filter('date')(new Date(groupLevel.DashBoardRowObject.fromDate), 'yyyy');
        //if (groupLevel.selectedPerformanceYear = groupLevel.performanceYearList[0].performanceYear)
        //{

        //}
        groupLevelService.getClinicianForGroup(groupLevel.aciStage, tin, fromDate, toDate, groupLevel.selectedPerformanceYear).then(function (response) {
            groupLevel.clinicianListForGroup = response.data;
            groupLevel.updateClinicianDataWithDates();
        }, function (error) {
            toastr.error("error ocuured while getting Clinicians List For the group");
        });
    }

    groupLevel.getParticipantClinicianData = function () {
        groupLevel.originalClinicianData = [];
        groupLevel.clinicianDataWithCSS = [];
        groupLevel.clinicianListForGroup.map(function (item) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');

            practiceLevelService.getGroupIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate,item.performanceYear).then(function (response) {
                groupLevel.originalClinicianData.push(item);
                item.aciScore = response.data.aciScore;
                item.cpiaScore = response.data.cpiaScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                groupLevel.addCSSForClinicianCPIASCore();
                if (response.data.aciScore === 0) {
                    item.disableFromDateFiled = true;
                    item.disableToDateField = true;
                }
                else {
                    item.disableFromDateFiled = false;
                    item.disableToDateField = false;
                }
            }, function (error) {
                //Removing the toaster need to update
                // toastr.error("unable load Participant Clinician data");
            });
        });

    }

    groupLevel.dateDifference = function (fromDate, toDate) {
        var firstDate = new Date(fromDate);
        var secondDate = new Date(toDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }

    groupLevel.validateDates = function (item) {
        var activityStartDatesList = [];
        if (item.type === "Individual") {
            practiceLevelService.getActivityStartDatesOfIndividual(item.npi, item.tin).then(function (response) {
                if (response.data.item2 === null || response.data.item2 === "" || response.data.item2 === undefined) {
                    if ((activityStartDatesList !== null || activityStartDatesList !== "")) {
                        activityStartDatesList = response.data;
                        var selectedDate = new Date(item.fromDate);
                        var count = 0;
                        angular.forEach(activityStartDatesList, function (value, index) {
                            var activityDate = new Date(value.activityStartDate);
                            if (activityDate > selectedDate) {
                                count += 1;
                            }
                        });
                        if (count > 0) {
                            toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                            //return false;
                        }
                    }
                }
                else {
                    toastr.error(response.data.item2);
                }
            }, function (error) {
                toastr.error("unable to load clinicians ACI Scores");
            });
        } else if (item.type === "Independent") {
            practiceLevelService.getActivityStartDatesOfIndividual(item.npi, item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }
        else {
            practiceLevelService.getActivityStartDatesOfGroup(item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
                toastr.error("server error while getting Scores");
            });
        }

        var registryStartDatesList = [];
        practiceLevelService.getCPIARegistryStartDates().then(function (response) {
            registryStartDatesList = response.data;
            var syndromicCount = 0;
            var immunizationCount = 0;
            var qualiRegistryCount = 0;
            var diffDays = 0;
            //check length of 
            angular.forEach(registryStartDatesList, function (value, index) {
                switch (value.registryCode) {
                    case 1:
                        diffDays = groupLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { syndromicCount += 1; }
                        break;
                    case 2:
                        diffDays = groupLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { immunizationCount += 1; }
                        break;
                    case 3:
                        break;
                    case 4:
                        diffDays = groupLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { qualiRegistryCount += 1; }
                        break;
                }
            });
            if (syndromicCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (immunizationCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (qualiRegistryCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
        }, function (error) {
            toastr.error("server error while getting Scores");
        });

    }
  
    groupLevel.updateToDate = function (item, dropbox) {        
        if (item.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period");
            return false
        }
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        if (item.reportingPeriodId === 1) { //Full Year           
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 11, 31);
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }
            if (dropbox == true)
                item.fromDate = new Date();
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
              item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);               
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90 days");
                    return false;
                }
            }
           
        }

        var itemfromDate = new Date(item.fromDate);
        var itemtoDate = new Date(item.toDate);

        if (itemfromDate.getFullYear() !== itemtoDate.getFullYear()) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        var perfYear = groupLevel.selectedPerformanceYearId.performanceYear;
       
        if ((itemfromDate.getFullYear() !== perfYear) || (itemtoDate.getFullYear() !== perfYear)) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        groupLevel.validateDates(item);

        if (item.fromDate && item.toDate) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load clinicians ACI Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load group ACI Scores");
                });
            }
            var aciReportingPeriodObj = {
                type: item.type,
                npi: item.npi,
                tin: item.tin,
                fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
            };
            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    var auditGroupName = '';
                    //storing Group Name and Individual Name if the individual is selected else storing only group name
                    if (item.type === "Individual") { //here type Individual will come for the clinicians participating in that Group.
                        auditGroupName = groupLevel.AuditGroupName + " - Individual reporting for provider name: " + item.name;
                    }
                    else {
                        auditGroupName = groupLevel.AuditGroupName;
                    }
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        groupLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("unable to update reporting period dates");
            });
        }
    }


    groupLevel.addCSSForGroupRow = function (item) {
        var rowItem = {};
        if (item.reportingPeriodId === 1) {
            item.reportingPeriodName = 'Full Year';
        }
        if (item.reportingPeriodId === 2) {
            item.reportingPeriodName = '90 Days';
        }
        if (item.reportingPeriodId === 3) {
            item.reportingPeriodName = 'Date Range';
        }
        if (item.cpiaScore >= 15) {
            item.classGreen = true;
            item.classYellow = false;
            item.classRed = false;
        }
        else if (item.cpiaScore < 7.5) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = true;
        }
        else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
            item.classGreen = false;
            item.classYellow = true;
            item.classRed = false;
        }
        else {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;
        }
        var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
        if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
            item.classGreenACI = true;
            item.classRedACI = false;
            item.classYellowACI = false;
        }
        if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = true;
        }
        if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        if (item.aciScore === 0) {
            item.classGreenACI = false;
            item.classYellowACI = false;
            if (groupLevel.selectedPerformanceYear == "2018")
            {
                item.classRedACI = true;
                item.classGrayACI = false;
            }
            else {
                item.classRedACI = false;
                item.classGrayACI = true;
            }
        }
        rowItem = angular.copy(item);
    }

    groupLevel.onToDateChange = function (item) {
        if (item.fromDate !== null && item.toDate !== null) {
            groupLevel.getCPIAACIScores(item);
        }
        if (item.reportingPeriodId === 0) {
            return false
        }
        if (item.reportingPeriodId === 1) { //Full Year
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(groupLevel.selectedPerformanceYearId.performanceYear, 11, 31);
            groupLevel.validateDates(item);
            //get that particular row's CPIA and ACI Score``
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }

            groupLevel.validateDates(item);

            item.fromDate = new Date(item.fromDate);
            item.toDate = angular.copy(item.fromDate);
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
                item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90 days");
                    return false;
                }
                groupLevel.validateDates(item);
            }
        }

        if (item.fromDate && item.toDate) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load clinicians ACI Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    //item.cpiaScore = response.data.cpiaScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    groupLevel.addCSSForGroupRow(item);
                }, function (error) {
                    toastr.error("unable to load group ACI Scores");
                });
            }
            var aciReportingPeriodObj = {
                type: item.type,
                npi: item.npi,
                tin: item.tin,
                fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
            };
            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    var auditGroupName = '';
                    //storing Group Name and Individual Name if the individual is selected else storing only group name
                    if (item.type === "Individual") { //here type Individual will come for the clinicians participating in that Group.
                        auditGroupName = groupLevel.AuditGroupName + " - Individual reporting for provider name: " + item.name;
                    }
                    else {
                        auditGroupName = groupLevel.AuditGroupName;
                    }
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + auditGroupName + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        groupLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("unable to update Reporting Period Dates");
            });
        }
    }

    groupLevel.getCPIAACIScores = function (item) {
        if (item.type === 'Independent') {
            practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, item.fromDate, item.toDate).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('unable to load CPIA and ACI Scores for Individual');
            });
        }
        if (item.type === 'Group') {
            practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, item.fromDate, item.toDate, item.performanceYear).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('unable to load CPIA and ACI Scores for Group');
            });
        }
    }
    groupLevel.goToCPIAActivities = function (item) {
        if (item.cpiaScore === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            return false;
        }

        groupLevel.setReportingObjTogetBackData.push(item);
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('GroupLevelDashBoard');
        var screenName = 'GroupLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        if (item.type === 'Group') {
            $state.go("groupCPIAActivities");
        }
        if (item.type === 'Independent') {
            $state.go("individualCPIAActivities");
            groupLevel.setReportingObjTogetBackData.push(groupLevel.DashBoardRowObject);
        }
        // New added 
        if (item.type === 'Individual') {
            item.name = groupLevel.DashBoardRowObject.name + " - (" + groupLevel.DashBoardRowObject.tin + ") " + item.name;
            $state.go("groupIndividualCPIAActivities");
            groupLevel.setReportingObjTogetBackData.push(groupLevel.DashBoardRowObject);
        }

        localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(groupLevel.setReportingObjTogetBackData));
        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
    }

    //to print the report
    groupLevel.print = function () {
        //set print Object
        delete groupLevel.groupDashBoardData[0]['existingFromDate'];
        delete groupLevel.groupDashBoardData[0]['existingToDate'];
        groupLevelService.setPracticeLevelPrintDashBoardObj(groupLevel.groupDashBoardData, groupLevel.clinicianListForGroup, groupLevel.showGroup);
        var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            groupLevel.loggedInUserObj = data[0].data.userName;
            groupLevel.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(groupLevel.loggedInUserObj);
            practiceLevelService.setPracticeDetails(groupLevel.practiceDetails);
            groupLevel.currentDate = Date.now();
            groupLevel.practiceDetailsPrintObj = {};
            groupLevel.clinicianList = {};
            groupLevel.clinicianList = groupLevelService.clinicianList;
            groupLevel.isShowGroup = groupLevelService.isShowGroup;
            if (typeof groupLevelService.groupsList !== 'undefined') {
                groupLevel.groupsList = angular.copy(groupLevelService.groupsList);
                groupLevel.groupsList.map(function (item) {
                    var count = 0;
                    item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                    item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                    if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                        if (count === 0) {
                            groupLevel.measureStage = muStageEnums.measureStages[0].measureName;
                        }
                        count = count + 1;
                    }
                });
            }
            groupLevel.clinicianList.map(function (value) {
                value.fromDate = $filter('date')(value.fromDate, 'MM-dd-yyyy');
                value.toDate = $filter('date')(value.toDate, 'MM-dd-yyyy');

                if (value.reportingPeriodId === 1) {
                    value.reportingPeriodName = 'Full Year';
                }
                if (value.reportingPeriodId === 2) {
                    value.reportingPeriodName = '90 Days';
                }
                if (value.reportingPeriodId === 3) {
                    value.reportingPeriodName = 'Date Range';
                }
            });


            groupLevel.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                groupLevel.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            window.setTimeout(function () {
                var printContents = $("#printGroupLevelDashBoardGrid").html();
                printContents = printContents.replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingFromDate</th>', '').replace('<th ng-hide="true" aria-hidden="true" class="ng-hide">ExistingToDate</th>', '');
                var actualPrintContents = printContents.replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingFromDate</th>', '').replace('<th class=\"ng-hide\" aria-hidden=\"true\" ng-hide=\"true\">ExistingToDate</th>', '');
                var popupWin = window.open('', '_blank', 'width=1300,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + actualPrintContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            groupLevel.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashGroupLevel + " - " + groupLevel.AuditGroupName);

            groupLevel.init();

        }, function (error) {
            toastr.error('error while getting loggged in user details');
        });
    }

    groupLevel.backToPrevioudScreen = function () {

        if (groupCPIA.screenName === "PracticeLevelDashBoard") {
            $state.go('practiceLevelDashBoard');
        }
        if (groupCPIA.screenName === "ClinicianLevelDashBoard") {
            $state.go('clinicianLevelDashBoard');
        }
        if (clinicianLevel.screenName === "GroupLevelDashBoard") {
            $state.go('groupLevelDashBoard');
        }
        //groupLevel.setReportingObjTogetBackData.push(item);   // remove if any back screen other than the practice level
        //practiceLevelService.setReportingObj(groupLevel.setReportingObjTogetBackData);
    }
    //Saving user audit details
    groupLevel.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }

    groupLevel.onYearChange = function () {
        groupLevel.getGroupData();
    }
    groupLevel.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}