﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('groupLevelPrintController', groupLevelPrintController);
groupLevelPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService', 'groupLevelService'];
function groupLevelPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService, groupLevelService) {
    var groupLevelPrint = this;
    groupLevelPrint.init = function () {
        groupLevelPrint.currentDate = Date.now();
        groupLevelPrint.practiceDetailsPrintObj = {};
        groupLevelPrint.clinicianList = {};
        groupLevelPrint.clinicianList = groupLevelService.clinicianList;
        groupLevelPrint.isShowGroup = groupLevelService.isShowGroup;
        if (typeof groupLevelService.groupsList !== 'undefined') {
            groupLevelPrint.groupsList = angular.copy(groupLevelService.groupsList);
            groupLevelPrint.groupsList.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                    if (count === 0) {
                        groupLevelPrint.measureStage = muStageEnums.measureStages[0].measureName;
                    }
                    count = count + 1;
                }
            });
        }
        debugger;
        groupLevelPrint.clinicianList.map(function (value) {
            debugger;
            value.fromDate = $filter('date')(value.fromDate, 'MM-dd-yyyy');
            value.toDate = $filter('date')(value.toDate, 'MM-dd-yyyy');

            if (value.reportingPeriodId === 1) {
                value.reportingPeriodName = 'Full Year';
            } 
            if (value.reportingPeriodId === 2) {
                value.reportingPeriodName = '90 Days';
            } 
            if (value.reportingPeriodId === 3) {
                value.reportingPeriodName = 'Date Range';
            }
        });
      
        //if (typeof groupLevelPrint.clinicianList.length() !== 0) {
        //    groupLevelPrint.clinicianList.map(function (clinician) {
        //        clinician.fromDate = $filter('date')(clinician.fromDate, 'MM-dd-yyyy');
        //        clinician.toDate = $filter('date')(clinician.toDate, 'MM-dd-yyyy');
        //    });
        //}
        //groupLevelPrint.printObj = practiceLevelService.groupLevelPrintObject
        groupLevelPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            groupLevelPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }
        $rootScope.print = true;
        groupLevelPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("groupLevelDashBoard");
        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printGroupLevelGridButton').click();
            }, 100);
        })
    }
    groupLevelPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}