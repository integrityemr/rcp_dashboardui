﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('practiceLevelPrintController', practiceLevelPrintController);
practiceLevelPrintController.$inject = ['$state', '$scope', '$rootScope', '$filter', 'muStageEnums', 'practiceLevelService'];
function practiceLevelPrintController($state, $scope, $rootScope, $filter, muStageEnums, practiceLevelService) {

    var practiceLevelPrint = this;
    
    practiceLevelPrint.init = function () {
        practiceLevelPrint.practiceDetailsPrintObj = {};
        practiceLevelPrint.currentDate = Date.now()
        practiceLevelService.practiceLevelPrintObject.map(function (item) {
            var count = 0;
            item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
            item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

            if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                if (count === 0) {
                    practiceLevelPrint.measureStage = muStageEnums.measureStages[0].measureName;
                }
                count = count + 1;
            }
        });

        practiceLevelPrint.printObj = practiceLevelService.practiceLevelPrintObject
        practiceLevelPrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            practiceLevelPrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        practiceLevelPrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("practiceLevelDashBoard");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printPraciceLevelGridButton').click();
            }, 100);
        })
    }
    practiceLevelPrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}