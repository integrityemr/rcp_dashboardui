﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('practiceLevelController', practiceLevelController);
practiceLevelController.$inject = ['$state', '$scope', '$q', '$filter', 'dashBoardEnums', 'performanceYearEnums', 'practiceLevelService', 'practiceInfoService', 'muStageEnums', 'practiceLevelConstants'];
function practiceLevelController($state, $scope, $q, $filter, dashBoardEnums, performanceYearEnums, practiceLevelService, practiceInfoService, muStageEnums, practiceLevelConstants) {
    var practiceLevel = this;
    practiceLevel.model = {};
    practiceLevelService.setReportingObjTogetData = [];

    practiceLevel.init = function () {
        if (localStorage.getItem('emergencyAccess') === "true") {
            practiceLevel.LoadInit();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role === undefined || res.data.role === null || res.data.role === "") {
                        toastr.error('You are not authorized.');
                        // $state.go('Home');
                    }
                    else {
                        if (res.data.role === "ADMINISTRATOR") {
                            practiceLevel.LoadInit();
                        }
                        else if (res.data.role === "ELIGIBLE CLINICIAN") {
                            $state.go('clinicianLevelDashBoard');
                        }
                        else {
                            practiceLevel.LoadInit();
                        }

                    }
                }

            });
        }
        //  practiceLevel.checkUserRolesandPermission();


    }

    practiceLevel.LoadInit = function () {
        var date = new Date();
        practiceLevel.currentDate = new Date();
        practiceLevel.yearEndDate = new Date(date.getFullYear(), 11, 31);
        practiceLevel.yearStartDate = new Date(date.getFullYear(), 0, 1);
        practiceLevel.selectedperformanceyear = 2019;
        practiceLevel.getPerformanceYearList();
        practiceLevel.getPracticeLevelData();
        practiceLevel.getReportingPeriodList();
        practiceLevel.productkey = localStorage.getItem('ProductKey');
        practiceLevelService.getPracticeDetails().then(function (response) {
            practiceLevel.PracticeName = response.data.name;
        });
        practiceLevel.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSPerfDashPracLevel);
    }
    //to get the list of reporting periods from Enums
    practiceLevel.getReportingPeriodList = function () {
        practiceLevel.reportingPeriodList = dashBoardEnums.reportingPeriod;
    }

    //to get data year wise
    practiceLevel.onYearChange = function () {
        practiceLevel.getPracticeLevelData();
    }
    practiceLevel.checkUserRolesandPermission = function () {
        if (localStorage.getItem('emergencyAccess') === "true") {
            //do code
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role.trim().toUpperCase() !== "ADMINISTRATOR" && res.data.role.trim().toUpperCase() !== "ELIGIBLE CLINICIAN") {
                        toastr.error('You are not authorized to this.');
                    }
                }

            });
        }

    }



    //to get the list of performance year from enums
    practiceLevel.getPerformanceYearList = function () {                
        practiceLevel.performanceYearList = performanceYearEnums.performanceYearList;
        var currentYear = performanceYearEnums.performanceYearList.filter(function (i) { return i.performanceYear == $filter('date')(new Date(), 'yyyy') })[0];
        if (currentYear != null) {
            practiceLevel.selectedPerformanceYearId = currentYear
            practiceLevel.selectedperformanceyear = currentYear["performanceYear"];
        }
        else
            practiceLevel.selectedPerformanceYearId = practiceLevel.performanceYearList[1];
    }

    //on to date change if all the fileds are valid updating the ACI score
    practiceLevel.onToDateChange = function (item) {
        if (item.fromDate !== null && item.toDate !== null) {
            practiceLevel.getCPIAACIScores(item);
        }
    }

    //to get the aci and cpia scores for a  particular row when user changes the dates
    practiceLevel.getCPIAACIScores = function (item) {
        var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
        if (item.type === 'Independent') {
            if (item.tin === '') {
                item.tin = null;
            }
            practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('server error while getting CPIA and ACI Scores for Individual');
            });
        }
        if (item.type === 'Group') {
            practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, item.fromDate, item.toDate, item.performanceYear).then(function (response) {
                //item.cpiaScore = response.data.cpiaScore;
                item.aciSCore = response.data.aciScore;
                item.totalACIPointsEarned = response.data.totalACIPointsEarned;
            }, function (error) {
                toastr.error('server error while getting CPIA and ACI Scores for Group');
            });
        }
    }

    //to get the difference between from date and to date
    practiceLevel.dateDifference = function (fromDate, toDate) {
        var firstDate = new Date(fromDate);
        var secondDate = new Date(toDate);
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }

    // validating dates and showing validation alerts 
    practiceLevel.validateDates = function (item) {
        var activityStartDatesList = [];
        if (item.type === "Independent") {
            practiceLevelService.getActivityStartDatesOfIndependent(item.npi, item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");

                }
            }, function (error) {
               // toastr.error("server error while getting Activity start dates");
            });
        }
        else {
            practiceLevelService.getActivityStartDatesOfGroup(item.tin).then(function (response) {
                activityStartDatesList = response.data;
                var selectedDate = new Date(item.fromDate);
                var count = 0;
                angular.forEach(activityStartDatesList, function (value, index) {
                    var activityDate = new Date(value.activityStartDate);
                    if (activityDate > selectedDate) {
                        count += 1;
                    }
                });
                if (count > 0) {
                    toastr.warning("Start date of MIPS reporting period must be on/ after the activity start date (activity eligible for ACI bonus) for receiving credit.");
                }
            }, function (error) {
               // toastr.error("server error while getting Activity start dates");
            });
        }

        var registryStartDatesList = [];
        practiceLevelService.getCPIARegistryStartDates().then(function (response) {
            registryStartDatesList = response.data;
            var syndromicCount = 0;
            var immunizationCount = 0;
            var qualiRegistryCount = 0;
            var diffDays = 0;
            //check length of 
            angular.forEach(registryStartDatesList, function (value, index) {
                switch (value.registryCode) {
                    case 1:
                        diffDays = practiceLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { syndromicCount += 1; }
                        break;
                    case 2:
                        diffDays = practiceLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { immunizationCount += 1; }
                        break;
                    case 3:
                        break;
                    case 4:
                        diffDays = practiceLevel.dateDifference(item.fromDate, value.startDate);
                        if (diffDays > 59) { qualiRegistryCount += 1; }
                        break;
                }
            });
            if (syndromicCount > 0) {
                toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (immunizationCount > 0) {
                toastr.warning("For receiving credit, the  start date for immunization registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
            if (qualiRegistryCount > 0) {
                toastr.warning("For receiving credit, the  start date for Clinical data registry reporting must be within 60 days from the start date of MIPS reporting period");
            }
        }, function (error) {
           // toastr.error("server error while getting Activity start dates");
        });

    }
  
    // to update teh to date when user changed from date based on the reporting period selected
    practiceLevel.updateToDate = function (item, dropbox) {        
        if (item.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period ");
            return false;
        }
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        
        if (item.reportingPeriodId === 1) { //Full Year
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
            item.fromDate = new Date(practiceLevel.selectedPerformanceYearId.performanceYear, 0, 1);
            item.toDate = new Date(practiceLevel.selectedPerformanceYearId.performanceYear, 11, 31);
        }
        else if (item.reportingPeriodId === 2) { //90 Days
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
            if (item.fromDate === undefined || item.fromDate === null) { return false; }
            if (dropbox == true)
                item.fromDate = new Date();
            var reqTime = item.fromDate.getTime();
            item.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);
        }
        else {
            item.disableFromDateFiled = false;
            item.disableToDateField = false;
            if (item.fromDate === undefined || item.fromDate === null ||
               item.toDate === undefined || item.toDate === null) {
                return false;
            }
            else {               
                var firstDate = new Date(item.fromDate);
                var secondDate = new Date(item.toDate);               
                var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                var diffDays = Math.ceil(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                if (diffDays < 89) {
                    toastr.error("Reporting period must be greater than or equal to 90days.");
                    return false;
                }
            }
            
        }

        var itemfromDate = new Date(item.fromDate);
        var itemtoDate = new Date(item.toDate);

        if (itemfromDate.getFullYear() !== itemtoDate.getFullYear()) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }

        var perfYear = practiceLevel.selectedPerformanceYearId.performanceYear;
       
        if ((itemfromDate.getFullYear() !== perfYear) || (itemtoDate.getFullYear() !== perfYear)) {
            toastr.error("Reporting period is not within the performance year.");
            return false;
        }
        //practiceLevel.performanceYearList[0].yearId

        practiceLevel.validateDates(item);

        if (item.fromDate && item.toDate) {
            var fromDate = $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(item.toDate), 'yyyy-MM-dd');
            if (item.type === 'Independent') {
                if (item.tin === '') {
                    item.disableACIScore = true;
                }
                practiceLevelService.getIndividualACICPIAScores(item.measureCode, item.npi, item.tin, fromDate, toDate).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    practiceLevel.addCSSForIndividualRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }
            if (item.type === 'Group') {
                practiceLevelService.getGroupACICPIAScores(item.measureCode, item.tin, fromDate, toDate, item.performanceYear).then(function (response) {
                    item.aciScore = response.data.aciScore;
                    item.totalACIPointsEarned = response.data.totalACIPointsEarned;
                    practiceLevel.addCSSForIndividualRow(item);
                }, function (error) {
                    toastr.error("server error while getting Scores");
                });
            }

            var aciReportingPeriodObj = {
                type: item.type,
                npi: item.npi,
                tin: item.tin,
                fromDate: $filter('date')(new Date(item.fromDate), 'yyyy-MM-dd'),
                toDate: $filter('date')(new Date(item.toDate), 'yyyy-MM-dd')
            };
            
            practiceLevelService.updateACIReportingPeriod(aciReportingPeriodObj).then(function (response) {                
                if (response.data === "true") {
                    toastr.success("Reporting period has been updated");
                    //Logging the audit based on the reporting date (From/To) changes.
                    var auditFromDate = $filter('date')(new Date(item.existingFromDate), 'MM/dd/yyyy');
                    var auditToDate = $filter('date')(new Date(item.existingToDate), 'MM/dd/yyyy');
                    var fromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    var toDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                    //based on the date changed, audit is logged for that particular date either From or To.
                    var auditAction = '';
                    if (auditFromDate !== fromDate && auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashPracLevel + " has changed for Start date " + auditFromDate + " and End date " + auditToDate;
                    }
                    else if (auditFromDate !== fromDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashPracLevel + " has changed for Start date " + auditFromDate;
                    }
                    else if (auditToDate !== toDate) {
                        auditAction = "Reporting period in " + practiceLevelConstants.MIPSPerfDashPracLevel + " has changed for End date " + auditToDate;
                    }
                    if (auditAction !== '' && auditAction !== undefined)
                        practiceLevel.LogUserAuditData(practiceLevelConstants.ChangeActionType, auditAction);
                    //assigning updated (From and To) dates to the existing (From and To) dates
                    item.existingFromDate = $filter('date')(new Date(item.fromDate), 'MM/dd/yyyy');
                    item.existingToDate = $filter('date')(new Date(item.toDate), 'MM/dd/yyyy');
                } else {
                    toastr.error(response.data);
                    return false;
                }
            }, function (error) {
                toastr.error("server error while updating Reporting Period Dates");
            });
        }
    }

    //to get the list of items to display in the dashboard
    practiceLevel.getPracticeLevelData = function () {
        //get the same list on calling Service method
        var selectedYear = practiceLevel.selectedPerformanceYearId;
        practiceLevel.selectedperformanceyear = practiceLevel.selectedPerformanceYearId.performanceYear;
       // alert(selectedYear.performanceYear);
        practiceLevelService.getDashBoardDataForPractice(selectedYear.performanceYear).then(function (response) {
            response.data.map(function (item) {
                if (item.fromDate === '0001-01-01T00:00:00') {
                    item.fromDate = null
                    item.disableFromDateFiled = true;
                }
                if (item.toDate === '0001-01-01T00:00:00') {
                    item.toDate = null
                    item.disableToDateField = true;
                }
            });
            practiceLevel.dashBoardData = response.data;
            practiceLevel.updateDashBoardData();
        }, function (err) {
            toastr.error("server error while getting DashBoard Data");
        });
    }

    //adding css for the particular row when user changes the dates
    practiceLevel.addCSSForIndividualRow = function (item) {
        var rowItem = {};
        if (item.reportingPeriodId === 1) {
            item.reportingPeriodName = 'Full Year';
            item.disableFromDateFiled = true;
            item.disableToDateField = true;
        }
        if (item.reportingPeriodId === 2) {
            item.reportingPeriodName = '90 Days';
            item.disableToDateField = true;
            item.disableFromDateFiled = false;
        }
        if (item.reportingPeriodId === 3) {
            item.reportingPeriodName = 'Date Range';
        }
        if (item.cpiaScore >= 15) {
            item.classGreen = true;
            item.classYellow = false;
            item.classRed = false;
        }
        else if (item.cpiaScore < 7.5) {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = true;
        }
        else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
            item.classGreen = false;
            item.classYellow = true;
            item.classRed = false;
        }
        else {
            item.classGreen = false;
            item.classYellow = false;
            item.classRed = false;
        }

        var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
        if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
            item.classGreenACI = true;
            item.classRedACI = false;
            item.classYellowACI = false;
        }
        if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = true;
        }
        if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
            item.classGreenACI = false;
            item.classRedACI = true;
            item.classYellowACI = false;
        }
        if (item.aciScore === 0) {
            item.classGreenACI = false;
            item.classRedACI = false;
            item.classYellowACI = false;
            item.classGrayACI = true;
        }
        rowItem = angular.copy(item);
    }

    //to add the styles to the dashBoard data
    practiceLevel.updateDashBoardData = function () {

        practiceLevel.updatedData = [];
        practiceLevel.dashBoardData.map(function (item) {
            if (item.reportingPeriodId === 1) {
                item.reportingPeriodName = 'Full Year';
                item.disableFromDateFiled = true;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 2) {
                item.reportingPeriodName = '90 Days';
                item.disableFromDateFiled = false;
                item.disableToDateField = true;
            }
            if (item.reportingPeriodId === 3) {
                item.reportingPeriodName = 'Date Range';
                item.disableFromDateFiled = false;
                item.disableToDateField = false;
            }
            if (item.cpiaScore >= 15) {
                item.classGreen = true;
                item.classYellow = false;
                item.classRed = false;
            }
            else if (item.cpiaScore < 7.5) {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = true;
            }
            else if (item.cpiaScore < 15 && item.cpiaScore >= 7.5) {
                item.classGreen = false;
                item.classYellow = true;
                item.classRed = false;
            }
            else {
                item.classGreen = false;
                item.classYellow = false;
                item.classRed = false;
            }

            var PerformanceGoal_Calculated = (item.totalPerfomanceGoal) * 90 / 100;
            if (item.totalACIPointsEarned >= item.totalPerfomanceGoal) {
                item.classGreenACI = true;
                item.classRedACI = false;
                item.classYellowACI = false;
            }
            if ((item.totalACIPointsEarned < item.totalPerfomanceGoal) && (item.totalACIPointsEarned > PerformanceGoal_Calculated)) {
                item.classGreenACI = false;
                item.classRedACI = false;
                item.classYellowACI = true;
            }
            if (item.totalACIPointsEarned < PerformanceGoal_Calculated) {
                item.classGreenACI = false;
                item.classRedACI = true;
                item.classYellowACI = false;
            }
            if (item.aciScore === 0)
            {
                item.classGreenACI = false;
                item.classYellowACI = false;
                if (practiceLevel.selectedperformanceyear == 2019)
                {
                    item.classRedACI = false;
                    item.classGrayACI = true
                }
               else
                {
                    item.classGrayACI = false;
                    item.classRedACI = true;
                }
                
            }
            
            practiceLevel.updatedData.push(item);
        });
    }

    //navigating to the ACI Measure Report screen
    practiceLevel.goToACIMeasureReport = function (item) {
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'PracticeLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        //practiceLevelService.setReportingObj(item);
        practiceLevelService.setReportingObjTogetData.push(item); //setReportingObjTogetData - list
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(practiceLevelService.setReportingObjTogetData));
        //practiceLevelService.setReportingObj(practiceLevelService.setReportingObjTogetData);

        $state.go("amcMeasureReport");

    }

    practiceLevel.goToGroupLevelDashBoard = function (item) {
        if (item.measureCode === 0) {
            toastr.error("Please set reporting period in ACI configuration screen");
            return false;
        }
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'PracticeLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        //show the Group DashBoard
        if (item.type === 'Group') {
            item.groupName = item.name;
            //practiceLevelService.setReportingObj(item);
            $state.go("groupLevelDashBoard");
        }
            //show Clinician DashBoard
        else {
            $state.go("clinicianLevelDashBoard");
        }
        practiceLevelService.setReportingObjTogetData.push(item);
        //practiceLevelService.setReportingObj(practiceLevelService.setReportingObjTogetData);
        localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(practiceLevelService.setReportingObjTogetData));
    }

    practiceLevel.goToCPIAActivities = function (item) {
        if (item.cpiaScore === 0) {
            toastr.error("Please configure activities in the CPIA configuration screen");
            return false;
        }
        //practiceLevelService.setReportingObj(item);
        //localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(item));
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'PracticeLevelDashBoard';
        localStorage.setItem("setScreenName", screenName);

        if (item.type === 'Group') {
            $state.go("groupCPIAActivities");
        }
        if (item.type === 'Independent') {
            $state.go("individualCPIAActivities");
        }
        practiceLevelService.setReportingObjTogetData.push(item);
        //practiceLevelService.setReportingObj(practiceLevelService.setReportingObjTogetData);
        localStorage.setItem("CPIAScoreObjectdata", JSON.stringify(practiceLevelService.setReportingObjTogetData));
    }

    //to print the report
    practiceLevel.print = function () {
        //set print Object
        practiceLevelService.setPracticeLevelPrintDashBoardObj(practiceLevel.updatedData);

        var promises = [
             practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            practiceLevel.loggedInUserObj = data[0].data.userName;
            practiceLevel.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(practiceLevel.loggedInUserObj);
            practiceLevelService.setPracticeDetails(practiceLevel.practiceDetails);
            practiceLevel.currentDate = Date.now()
            practiceLevelService.practiceLevelPrintObject.map(function (item) {
                var count = 0;
                item.fromDate = $filter('date')(item.fromDate, 'MM-dd-yyyy');
                item.toDate = $filter('date')(item.toDate, 'MM-dd-yyyy');

                if (muStageEnums.measureStages[0].measureCode === item.measureCode) {
                    if (count === 0) {
                        practiceLevel.measureStage = muStageEnums.measureStages[0].measureName;
                    }
                    count = count + 1;
                }
            });

            practiceLevel.printObj = practiceLevelService.practiceLevelPrintObject
            practiceLevel.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                practiceLevel.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }


            window.setTimeout(function () {
                var printContents = $("#printPracticeLevelDashBoardGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            practiceLevel.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSPerfDashPracLevel);
        }, function (error) {
            toastr.error('error while getting loggged in user details');
        });
        practiceLevel.practiceDetailsPrintObj = {};

    }

    //Saving user audit details
    practiceLevel.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }
    practiceLevel.init();

    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}