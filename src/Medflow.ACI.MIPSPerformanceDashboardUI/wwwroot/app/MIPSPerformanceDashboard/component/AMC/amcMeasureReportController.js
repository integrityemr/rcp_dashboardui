﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('amcMeasureReportController', amcMeasureReportController);
amcMeasureReportController.$inject = ['$state', '$scope', '$q', '$filter', 'amcService', 'practiceLevelService', 'enums', 'muMeasureService', 'categoryDescriptionEnum', 'practiceLevelConstants', 'aciService', 'aciPerformanceYearEnums'];
function amcMeasureReportController($state, $scope, $q, $filter, amcService, practiceLevelService, enums, muMeasureService, categoryDescriptionEnum, practiceLevelConstants, aciService,aciPerformanceYearEnums) {
    var amcMeasure = this;
    amcMeasure.disableFields = true;
    amcMeasure.getReportingObjTogetBackData = [];
    amcMeasure.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('AciScoreObjectdata'));
    //amcMeasure.dashBoarData = practiceLevelService.reportingObject;
    //amcMeasure.dashBoarData = JSON.parse(localStorage.getItem('AciScoreObjectdata'));

    amcMeasure.dashBoarData = amcMeasure.getReportingObjTogetBackData[0];
    amcMeasure.fromScreenName = localStorage.getItem('setScreenName');
    amcMeasure.categoryDescription = categoryDescriptionEnum.categoryDescription;
    amcMeasure.noCount = 0;
    amcMeasure.stageName = "";
    amcMeasure.setReportingObjTogetBackData = [];
    amcMeasure.setReportingObjTogetData = [];
    amcMeasure.isCEHRT = false;
    amcMeasure.isSecuityAys = false;
    amcMeasure.selectedYear = null;
    amcMeasure.ReportingType = null;
    amcMeasure.isBaseScoreValid = false;
    amcMeasure.init = function () {
        amcMeasure.totalMeasurePoints = 0;
        amcMeasure.aciScore = 0;
        amcMeasure.active = false;
        amcMeasure.showGroupDropDown = false;
        amcMeasure.showClinicianDropDown = false;
        amcMeasure.showBoth = false;
        amcMeasure.getMeasureSet();
        amcMeasure.getAmcMeasureData();
        amcMeasure.productkey = localStorage.getItem('ProductKey');
        amcMeasure.cehrtinit();
        amcMeasure.getSRADate();
    }

   
    amcMeasure.goBackToPreviousScreen = function () {
        //get the screen name from the service and redirect to that screen
        if (localStorage.getItem('setScreenName') !== null && localStorage.getItem('setScreenName') !== "" && localStorage.getItem('setScreenName') !== "undefined") {
            if (localStorage.getItem('setScreenName') === "PracticeLevelDashBoard") {
                $state.go('practiceLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') === "GroupLevelDashBoard") {
                if (amcMeasure.dashBoarData.type === "Individual") {
                    amcMeasure.setReportingObjTogetBackData.push(amcMeasure.getReportingObjTogetBackData[1]);
                }
                else {
                    amcMeasure.setReportingObjTogetBackData.push(amcMeasure.dashBoarData);
                    //practiceLevelService.setReportingObj(amcMeasure.dashBoarData);
                }
                //practiceLevelService.setReportingObj(amcMeasure.setReportingObjTogetBackData);
                localStorage.setItem("GroupIndpendentObjectdata", JSON.stringify(amcMeasure.setReportingObjTogetBackData));
                $state.go('groupLevelDashBoard');
            }
            else if (localStorage.getItem('setScreenName') === "ClinicianLevelDashBoard") {
                if (amcMeasure.dashBoarData.type === "Group") {
                    amcMeasure.setReportingObjTogetBackData.push(amcMeasure.getReportingObjTogetBackData[1]);
                }
                else {
                    amcMeasure.setReportingObjTogetBackData.push(amcMeasure.dashBoarData);
                }
                practiceLevelService.setReportingObj(amcMeasure.setReportingObjTogetBackData);
                $state.go('clinicianLevelDashBoard');
            }
            else {
                $state.go('practiceLevelDashBoard');
            }
        }
        //if (amcMeasure.fromScreenName === "PracticeLevelDashBoard") {
        //    $state.go('practiceLevelDashBoard');
        //}
        //if (amcMeasure.fromScreenName === "ClinicianLevelDashBoard") {
        //    //practiceLevelService.setClinicianDashBoardObj(amcMeasure.dashBoarData);
        //    $state.go('clinicianLevelDashBoard');
        //}
        //if (amcMeasure.fromScreenName === "GroupLevelDashBoard") {
        //    $state.go('groupLevelDashBoard');
        //}
    }

    amcMeasure.getAmcMeasureData = function () {
        amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
        if (typeof amcMeasure.dashBoarData !== 'undefined') {
            //muStageCode = 3 ACI MeasureSet Stage3 if 2 then ACI AlternateMeasureSet (2017 only, Stage 2)
            if (amcMeasure.dashBoarData.type === 'Group') {
                amcMeasure.groupTIN = amcMeasure.dashBoarData.groupTIN;
                amcMeasure.getCliniciansAndGroups();
            }

            var physicianId = 0;
            var idValue = amcMeasure.dashBoarData.idValue;

            amcMeasure.type = amcMeasure.dashBoarData.type;
            amcMeasure.stageCode = amcMeasure.dashBoarData.measureCode;
            amcMeasure.selectedPhysicianId = physicianId;
            amcMeasure.selectedPhysicianNPI = amcMeasure.dashBoarData.npi;
            amcMeasure.fromDateInDashBoard = $filter('date')(new Date(amcMeasure.dashBoarData.fromDate), 'yyyy-MM-dd');
            amcMeasure.selectedYear = $filter('date')(new Date(amcMeasure.dashBoarData.fromDate), 'yyyy');
            amcMeasure.toDateInDashBoard = $filter('date')(new Date(amcMeasure.dashBoarData.toDate), 'yyyy-MM-dd');
            amcMeasure.idValue = idValue;
            amcMeasure.tin = amcMeasure.dashBoarData.tin;
            amcMeasure.npi = amcMeasure.dashBoarData.npi;
            amcMeasure.calculateReportingDays();


            if (amcMeasure.fromScreenName === "ClinicianLevelDashBoard") {
                if (amcMeasure.type === "Group") {
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    if (amcMeasure.dashBoarData.groupTIN !== "" && amcMeasure.dashBoarData.individualTIN !== "" && amcMeasure.dashBoarData.npi !== "") {
                        amcMeasure.groupId = amcMeasure.dashBoarData.groupId;
                        amcMeasure.selectedGroupId = amcMeasure.groupId;
                        amcMeasure.groupTIN = amcMeasure.dashBoarData.groupTIN;
                        //amcMeasure.groupTIN = amcMeasure.groupTIN;
                        //amcMeasure.tin = amcMeasure.dashBoarData.individualTIN;
                        amcMeasure.showGroupDropDown = false;
                        amcMeasure.showClinicianDropDown = false;
                        amcMeasure.showBoth = true;
                    }
                    amcMeasure.getCliniciansAndGroups();
                    // group - todo
                    amcService.getMeasureReportForGroup(amcMeasure.stageCode, amcMeasure.selectedGroupId, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.groupTIN).then(function (response) {
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Group';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;
                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
                    });
                }

                else if (amcMeasure.type === "Individual" || amcMeasure.type === "Participant")
                {
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcMeasure.groupId = amcMeasure.dashBoarData.groupReportingId;
                    amcMeasure.selectedGroupId = amcMeasure.groupId
                    amcMeasure.groupTIN = amcMeasure.dashBoarData.groupTIN;
                    amcMeasure.tin = amcMeasure.dashBoarData.groupTIN;
                    amcMeasure.showGroupDropDown = false;
                    amcMeasure.showClinicianDropDown = true;
                    amcMeasure.showBoth = false;
                    amcMeasure.getCliniciansAndGroups();
                    amcService.getMeasureReportForGroupClinician(amcMeasure.stageCode, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.tin, amcMeasure.npi).then(function (response) {
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Group';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;
                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                            amcMeasure.showGroupDropDown = false;
                            amcMeasure.showClinicianDropDown = false;
                            amcMeasure.showBoth = true;
                            if (amcMeasure.selectedYear == 2019)
                            {
                                amcMeasure.showGroupDropDown = true;
                            }
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
                    });
                }
                else {
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcMeasure.groupId = amcMeasure.dashBoarData.groupReportingId;
                    amcMeasure.selectedGroupId = amcMeasure.groupId
                    amcMeasure.groupTIN = amcMeasure.dashBoarData.groupTIN;
                    amcMeasure.tin = amcMeasure.dashBoarData.tin;
                    amcMeasure.showGroupDropDown = false;
                    amcMeasure.showClinicianDropDown = true;
                    amcMeasure.showBoth = false;
                    amcMeasure.getCliniciansAndGroups();
                    amcService.getMeasureReportForClinician(amcMeasure.stageCode, amcMeasure.selectedPhysicianId, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.tin, amcMeasure.npi).then(function (response) {
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Individual';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;
                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
                    });
                }
            }
            else if (amcMeasure.fromScreenName === "PracticeLevelDashBoard") {
                if (amcMeasure.dashBoarData.type === 'Group') {
                    amcMeasure.showGroupDropDown = true;
                    amcMeasure.showClinicianDropDown = false;
                    amcMeasure.showBoth = false;
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcMeasure.npi = null;
                    amcMeasure.groupId = amcMeasure.idValue;
                    amcMeasure.selectedGroupId = amcMeasure.idValue;
                    amcMeasure.groupTIN = amcMeasure.dashBoarData.tin;

                    amcMeasure.getCliniciansAndGroups();

                    amcService.getMeasureReportForGroup(amcMeasure.stageCode, amcMeasure.groupId, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.groupTIN).then(function (response) {
                        //amcMeasure.showGroupDropDown = true;
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Group';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;

                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Group");
                    });
                }
                else if (amcMeasure.dashBoarData.type === "Individual" || amcMeasure.dashBoarData.type === "Participant") {
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcMeasure.groupId = amcMeasure.dashBoarData.groupReportingId;
                    amcMeasure.selectedGroupId = amcMeasure.groupId
                    amcMeasure.groupTIN = amcMeasure.dashBoarData.groupTIN;
                    amcMeasure.selectedPhysicianNPI = amcMeasure.dashBoarData.npi;

                    amcMeasure.showGroupDropDown = false;
                    amcMeasure.showClinicianDropDown = true;
                    amcMeasure.showBoth = false;

                    amcMeasure.getCliniciansAndGroups();
                    amcService.getMeasureReportForGroupClinician(amcMeasure.stageCode, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.tin, amcMeasure.npi).then(function (response) {
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Group';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;
                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
                    });
                }
                else {
                    amcMeasure.groupId = amcMeasure.dashBoarData.groupReportingId;
                    amcMeasure.selectedGroupId = amcMeasure.groupId
                    amcMeasure.groupTIN = amcMeasure.dashBoarData.groupTIN;
                    amcMeasure.selectedPhysicianNPI = amcMeasure.dashBoarData.npi;
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcMeasure.showGroupDropDown = false;
                    amcMeasure.showClinicianDropDown = true;
                    amcMeasure.showBoth = false;

                    amcMeasure.getCliniciansAndGroups();
                    amcService.getMeasureReportForClinician(amcMeasure.stageCode, amcMeasure.selectedPhysicianId, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.tin, amcMeasure.npi).then(function (response) {
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Individual';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;
                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
                    });
                }

            }
            else {
                amcMeasure.npi = null;
                amcMeasure.groupId = amcMeasure.getReportingObjTogetBackData[0].groupId;
                amcMeasure.selectedGroupId = amcMeasure.idValue;
                amcMeasure.groupTIN = amcMeasure.dashBoarData.tin;
                amcMeasure.npi = amcMeasure.dashBoarData.npi;
                amcMeasure.getCliniciansAndGroups();
                if (amcMeasure.dashBoarData.type === "Individual" || amcMeasure.dashBoarData.type === "Independent") {
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcService.getMeasureReportForGroupClinician(amcMeasure.stageCode, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.tin, amcMeasure.npi).then(function (response) {
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Individual';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;
                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                            amcMeasure.showGroupDropDown = false;
                            amcMeasure.showClinicianDropDown = false;
                            if (amcMeasure.dashBoarData.type === "Independent") {
                                amcMeasure.showClinicianDropDown = true;
                            }
                            else {
                                amcMeasure.showBoth = true;
                            }
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
                    });

                }
                else {
                    amcMeasure.selectedYear = $filter('date')(amcMeasure.dashBoarData.fromDate, "yyyy")
                    amcService.getMeasureReportForGroup(amcMeasure.stageCode, amcMeasure.groupId, amcMeasure.fromDateInDashBoard, amcMeasure.toDateInDashBoard, amcMeasure.tin).then(function (response) {
                        amcMeasure.showGroupDropDown = true;
                        amcMeasure.resultSet = response.data;
                        if (response.data !== null) {
                            amcMeasure.ReportingType = 'Group';
                            amcMeasure.measureReportBaseScore = response.data.baseScoreViewModel;
                            amcMeasure.measureReportPerformancePoints = response.data.performancePointsViewModel;
                            amcMeasure.measureReportBonusPoints = response.data.bonusPointsViewModel;

                            amcMeasure.getBaseScoreData();
                            amcMeasure.getPerformanceData();
                            amcMeasure.getBonusData();
                        }
                    }, function (error) {
                        toastr.error("Server Error in Getting ACI Measure Report for Group");
                    });
                }
            }
        }
        amcMeasure.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.MIPSACIMeasureReport + amcMeasure.AuditGroupClinicianName);
    }

    amcMeasure.calculateReportingDays = function () {
        var one_day = 1000 * 60 * 60 * 24;
        amcMeasure.reportingDuration = Math.ceil((new Date(amcMeasure.toDateInDashBoard).getTime() - new Date(amcMeasure.fromDateInDashBoard).getTime()) / (one_day));
        amcMeasure.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    };

    //to get the clinician dropdown data
    amcMeasure.getCliniciansAndGroups = function () {
        amcMeasure.groupList = [];
        amcMeasure.clinicianList = [];

        if (amcMeasure.dashBoarData.type === "Group") {
            amcMeasure.ReportingType = 'Group';
            amcMeasure.groupName = amcMeasure.dashBoarData.name + '-' + amcMeasure.dashBoarData.tin;
            amcMeasure.groupList.push({ groupTIN: amcMeasure.dashBoarData.tin, groupName: amcMeasure.groupName });
            amcMeasure.AuditGroupClinicianName = " for Group: " + amcMeasure.dashBoarData.name;
        }
        else if (amcMeasure.dashBoarData.type === "Individual") {
            amcMeasure.ReportingType = 'Individual';
            if (amcMeasure.dashBoarData.groupId === null) { // grouplevel dashboard
                amcMeasure.groupName = amcMeasure.dashBoarData.groupName + '-' + amcMeasure.dashBoarData.groupTIN;
                amcMeasure.groupList.push({ groupTIN: amcMeasure.dashBoarData.groupTIN, groupName: amcMeasure.groupName });

                amcMeasure.physicianName = amcMeasure.dashBoarData.name + '-' + amcMeasure.dashBoarData.npi;
                amcMeasure.clinicianList.push({ physicianNPI: amcMeasure.dashBoarData.npi, physicianName: amcMeasure.physicianName });
                amcMeasure.physicianNPI = amcMeasure.clinicianList[0].physicianNPI;
                amcMeasure.AuditGroupClinicianName = " for Group: " + amcMeasure.dashBoarData.groupName + " and for clinician: " + amcMeasure.dashBoarData.name;
            }
            else { // clinicianlevel dashboard
                amcMeasure.groupName = amcMeasure.dashBoarData.name + '-' + amcMeasure.dashBoarData.groupTIN;
                amcMeasure.groupList.push({ groupTIN: amcMeasure.dashBoarData.groupTIN, groupName: amcMeasure.groupName });

                amcMeasure.physicianName = amcMeasure.dashBoarData.ClinincianName + '-' + amcMeasure.dashBoarData.npi;
                amcMeasure.clinicianList.push({ physicianNPI: amcMeasure.dashBoarData.npi, physicianName: amcMeasure.physicianName });
                amcMeasure.physicianNPI = amcMeasure.clinicianList[0].physicianNPI;
                amcMeasure.AuditGroupClinicianName = " for Clinician: " + amcMeasure.dashBoarData.ClinincianName + " and for Group: " + amcMeasure.dashBoarData.name;
            }
        }
        else if (amcMeasure.dashBoarData.type === "Independent") {
            amcMeasure.ReportingType = 'Individual';
            amcMeasure.physicianName = amcMeasure.dashBoarData.name + '-' + amcMeasure.dashBoarData.npi;
            amcMeasure.clinicianList.push({ physicianNPI: amcMeasure.dashBoarData.npi, physicianName: amcMeasure.physicianName });
            amcMeasure.physicianNPI = amcMeasure.clinicianList[0].physicianNPI;
            amcMeasure.AuditGroupClinicianName = " for Clinician: " + amcMeasure.dashBoarData.name;
        }

    }
    //to get the measure set dropdwon data
    amcMeasure.getMeasureSet = function () {
        muMeasureService.getMUStages().then(function (response) {
            amcMeasure.measureSetList = response.data;
            if (amcMeasure.measureSetList !== null) {
                amcMeasure.measureSetList = $filter('filter')(amcMeasure.measureSetList, { stageType: enums.stageType.ACI });
                angular.forEach(amcMeasure.measureSetList, function (value, key) {
                    if (value.stageCode === amcMeasure.dashBoarData.measureCode) {
                        amcMeasure.stageName = value.stageName;
                    }
                });
                amcMeasure.copiedMeasureSetList = angular.copy(amcMeasure.measureSetList);
            }
        }, function (error) {
            toastr.error('server error');
        });
    }



    amcMeasure.getBaseScoreData = function () {
        amcMeasure.amcMeasureData = [];
        amcMeasure.measureReportBaseScore.map(function (item) {
            if (item.numerator === null && item.denominator === null && item.performanceGoals === null && item.measureName === null) {
                item.performance = '';
                item.classDanger = null;
                item.showProgressBar = false;

                amcMeasure.amcMeasureData.push(item);
            }
            else if ($filter('uppercase')(item.performance) === 'YES') {
                item.performance = 'YES';
                item.classDanger = false;
                item.showProgressBar = true;
                amcMeasure.amcMeasureData.push(item);

            }
            else if ($filter('uppercase')(item.performance) === 'NO') {
                item.performance = 'NO'
                item.classDanger = true;
                item.showProgressBar = true;
                amcMeasure.amcMeasureData.push(item);
                amcMeasure.noCount += 1;
            }

            else if (item.performance == null) {
                item.classDanger = true;
                item.showProgressBar = true;
                amcMeasure.amcMeasureData.push(item);
                amcMeasure.noCount += 1;
            }

            else if ($filter('uppercase')(item.performance) === 'NO') {
                item.performance = 'NO'
                item.classDanger = true;
                item.showProgressBar = true;
                amcMeasure.amcMeasureData.push(item);
                amcMeasure.noCount += 1;
            }
                // added for 2018 enhancements
            else if ($filter('uppercase')(item.performance) === 'EXCLUDE') {
                item.performance = 'EXCLUDE';
                item.classDanger = false;
                item.showProgressBar = true;
                amcMeasure.amcMeasureData.push(item);
            }
            else {
                amcMeasure.amcMeasureData.push(item);
            }
        });
        amcMeasure.measureReportBaseScore = angular.copy(amcMeasure.amcMeasureData);
        angular.forEach(amcMeasure.measureReportBaseScore, function (value, key) {
            if (key !== 0) {
                value.category = "";
                value.possiblePoints = "";
                value.pointsEarned = "";
            }
            if (key === 0 && amcMeasure.noCount === 0) {
                value.pointsEarned = 50;
                amcMeasure.totalMeasurePoints = 50;
                amcMeasure.isBaseScoreValid = true;
            }
        });
    }
    //check
    amcMeasure.getPerformanceData = function () {
        amcMeasure.amcMeasurePermanceData = [];
        amcMeasure.measureReportPerformancePoints.map(function (item) 
        {
            item.showPerformanceCategory = true;
            if (amcMeasure.selectedYear == 2019 && (item.numerator == 0 || item.denominator == 0 || item.performanceGoals == 0))
            {
                item.classSuccess = false;
                item.classDanger = false;
                item.classWarning = false;
                item.classGray = true;
                if (item.numerator == 0 && item.denominator == 0)
                {
                    item.performance = "0";
                }
                amcMeasure.amcMeasurePermanceData.push(item);
            }
            else {
                if ($filter('uppercase')(item.performance) === 'YES') {
                    item.performance = 'YES';
                    item.classSuccess = true
                    item.classDanger = false;
                    item.classWarning = false;
                    amcMeasure.amcMeasurePermanceData.push(item);
                }
                else if ($filter('uppercase')(item.performance) === 'NO') {
                    item.performance = 'NO'
                    item.classDanger = true;
                    item.classWarning = false;
                    item.classSuccess = false;
                    item.showPerformanceCategory = false;
                    amcMeasure.amcMeasurePermanceData.push(item);
                }
                else if (item.performance === null) {
                    item.performance = "0";
                    amcMeasure.amcMeasurePermanceData.push(item);
                }
                else if (parseInt(item.performance) >= parseInt(item.performanceGoals)) {
                    item.classDanger = false;
                    item.classWarning = false;
                    item.classSuccess = true;
                    item.showPerformanceCategory = false;
                    amcMeasure.amcMeasurePermanceData.push(item);
                }
                else if (parseInt(item.performance) < parseInt(item.performanceGoals)) {
                    var calculation = 90 * parseInt(item.performanceGoals) / 100;
                    if (parseInt(calculation) <= parseInt(item.performance)) {
                        item.classDanger = false;
                        item.classWarning = true;
                        item.classSuccess = false;
                        item.showPerformanceCategory = false;
                        amcMeasure.amcMeasurePermanceData.push(item);
                    }
                    else {
                        item.classDanger = true;
                        item.classWarning = false;
                        item.classSuccess = false;
                        item.showPerformanceCategory = false;
                        amcMeasure.amcMeasurePermanceData.push(item);
                    }
                }
                else {
                    item.classDanger = false;
                    item.classSuccess = true;
                    item.showPerformanceCategory = false;
                    amcMeasure.amcMeasurePermanceData.push(item);
                }
            }
        });
        amcMeasure.measureReportPerformancePoints = angular.copy(amcMeasure.amcMeasurePermanceData);
        angular.forEach(amcMeasure.measureReportPerformancePoints, function (value, key) {
            if (key !== 0) {
                value.category = "";
            }
        });

        angular.forEach(amcMeasure.measureReportPerformancePoints, function (value, key) {
            amcMeasure.totalMeasurePoints = parseFloat(amcMeasure.totalMeasurePoints) + parseFloat(value.pointsEarned);
        });
    }

    amcMeasure.getBonusData = function () {
        amcMeasure.amcMeasureBonusData = [];
        amcMeasure.measureReportBonusPoints.map(function (item) {
            if ($filter('uppercase')(item.performance) === 'YES') {
                item.performance = 'YES';
                item.classDanger = false;
                amcMeasure.amcMeasureBonusData.push(item);
            }
            else if ($filter('uppercase')(item.performance) === 'NO') {
                item.performance = 'NO'
                item.classDanger = true;
                amcMeasure.amcMeasureBonusData.push(item);
            }
            else if (item.performance === null) {
                item.performance = "0";
                amcMeasure.amcMeasureBonusData.push(item);
            }
            else {
                amcMeasure.amcMeasureBonusData.push(item);
            }
        });
        amcMeasure.measureReportBonusPoints = angular.copy(amcMeasure.amcMeasureBonusData);
        angular.forEach(amcMeasure.measureReportBonusPoints, function (value, key) {
            if (key !== 0) {
                value.category = "";
            }
        });

        angular.forEach(amcMeasure.measureReportBonusPoints, function (value, key) {
            amcMeasure.totalMeasurePoints = amcMeasure.totalMeasurePoints + parseInt(value.pointsEarned);
        });
        if (amcMeasure.totalMeasurePoints !== 0) {
            if (amcMeasure.totalMeasurePoints > 100) {
                amcMeasure.totalMeasurePoints = 100;
            }
            amcMeasure.aciScore = amcMeasure.totalMeasurePoints / 4;
        }
        if (amcMeasure.measureReportBaseScore[0].pointsEarned === 0) {
            amcMeasure.aciScore = 0;
            amcMeasure.totalMeasurePoints = 0;
        }
    }

    amcMeasure.exportAmcData = function (isExport) {
        var entities = [];
        if (amcMeasure.selectedYear == 2018) {
            amcMeasure.measureReportBaseScore.map(function (item) {
                entities.push(item);
            })
        }
        amcMeasure.measureReportPerformancePoints.map(function (item) {
            entities.push(item);
        })
        amcMeasure.measureReportBonusPoints.map(function (item) {
            entities.push(item);
        })

        if (amcMeasure.selectedYear == 2019) {
            amcMeasure.measureReportBaseScore.map(function (item) {
                entities.push(item);
            })
        }
        //Binding Total ACI Points and ACI Score in the export
        if (amcMeasure.selectedYear == 2018) {
            entities.push({
                performance: "Total ACI Points Earned: ",
                pointsEarned: (amcMeasure.countedACIScore > 100 ? 100 : amcMeasure.countedACIScore.toFixed(2)) + " / 100"
            });
            entities.push({
                performance: "MIPS-ACI Score: ",
                pointsEarned: (amcMeasure.countedACIScore > 100 ? 25 : (amcMeasure.countedACIScore / 4).toFixed(2)) + " / 25"
            });
        }
        if (amcMeasure.selectedYear == 2018) {
            var mystyle = {
                headers: true,
                columns: [
                    { columnid: 'no', title: "no" },
                    { columnid: 'category', title: "Category" },
                    { columnid: 'measureName', title: "Measure Name" },
                    { columnid: 'numerator', title: 'Numerator' },
                    { columnid: 'denominator', title: 'Denominator' },
                    { columnid: 'possiblePoints', title: 'Possible Points' },
                    { columnid: 'performanceGoals', title: 'Performance Goal' },
                    { columnid: 'performance', title: 'Performance' },
                    { columnid: 'pointsEarned', title: 'Points Earned' }
                ],
            };
        }
        else {
            var mystyle = {
                headers: true,
                columns: [
                    { columnid: 'no', title: "no" },
                    { columnid: 'measureName', title: "Measure Name" },
                    { columnid: 'numerator', title: 'Numerator' },
                    { columnid: 'denominator', title: 'Denominator' },
                    { columnid: 'possiblePoints', title: 'Possible Points' },
                    { columnid: 'performanceGoals', title: 'Performance Goal' },
                    { columnid: 'performance', title: 'Performance' },
                ],
            };
        }
        var fileNameExcel = "amcReport" + $filter('date')(new Date(), "MMddyyyy") + ".xls";
        var fileNameCsv = "amcReport" + $filter('date')(new Date(), "MMddyyyy") + ".csv";

        if (isExport === 'csv') {
            alasql("SELECT * INTO CSV('" + fileNameCsv + "',?) FROM ?", [mystyle, entities]);
        }
        else {
            alasql('SELECT * INTO XLSXML("' + fileNameExcel + '",?) FROM ?', [mystyle, entities]);
        }
        amcMeasure.LogUserAuditData(practiceLevelConstants.ExportActionType, "Exported " + practiceLevelConstants.MIPSACIMeasureReport + amcMeasure.AuditGroupClinicianName);
    }
    amcMeasure.printAMcMeasureReport = function () {
        //set 3 types of objects 
        amcMeasure.measureSetList.map(function (measure) {
            if (measure.stageCode === amcMeasure.stageCode) {
                amcMeasure.selectedStage = measure.stageName;
            }

        })
        if (amcMeasure.dashBoarData.type === 'Independent') {
            amcMeasure.ReportingType = 'Individual';
            amcMeasure.selectedClinicianName = amcMeasure.dashBoarData.name;
            amcMeasure.selectedClinicianNPI = amcMeasure.dashBoarData.npi;
            amcMeasure.selectedClinicianTIN = amcMeasure.dashBoarData.tin;
        }
        if (amcMeasure.dashBoarData.type === 'Group') {
            amcMeasure.ReportingType = 'Individual';
            amcMeasure.selectedGroupName = amcMeasure.dashBoarData.name;
            amcMeasure.selectedGroupTIN = amcMeasure.dashBoarData.tin;
        } else {
            amcMeasure.selectedClinicianName = amcMeasure.dashBoarData.name;
            amcMeasure.selectedClinicianNPI = amcMeasure.dashBoarData.npi;
            amcMeasure.selectedClinicianTIN = amcMeasure.dashBoarData.tin;
            amcMeasure.selectedGroupName = amcMeasure.dashBoarData.groupName;
            amcMeasure.selectedGroupTIN = amcMeasure.dashBoarData.groupTIN;
        }

        amcService.setACIMeasureReportObj(
           amcMeasure.measureReportBaseScore,
           amcMeasure.measureReportPerformancePoints,
           amcMeasure.measureReportBonusPoints,
           amcMeasure.selectedStage,
           amcMeasure.selectedClinicianName,
           amcMeasure.selectedClinicianNPI,
           amcMeasure.selectedClinicianTIN,
           amcMeasure.totalMeasurePoints,
           amcMeasure.aciScore,
           amcMeasure.selectedGroupName,
           amcMeasure.selectedGroupTIN,
           amcMeasure.dashBoarData.type,
           amcMeasure.fromDateInDashBoard,
           amcMeasure.toDateInDashBoard,
           amcMeasure.stageName
           )

        var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            amcMeasure.loggedInUserObj = data[0].data.userName;
            amcMeasure.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(amcMeasure.loggedInUserObj);
            practiceLevelService.setPracticeDetails(amcMeasure.practiceDetails);

            amcMeasure.practiceDetailsPrintObj = {};
            amcMeasure.currentDate = Date.now()
            amcMeasure.active = false;
            amcMeasure.practiceDetailsPrintObj = practiceLevelService.practiceLevelPrintObject;
            amcMeasure.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                amcMeasure.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            amcMeasure.measureReportBaseScore = amcService.measureReportBaseScore;
            amcMeasure.measureReportPerformancePoints = amcService.measureReportPerformancePoints;
            amcMeasure.measureReportBonusPoints = amcService.measureReportBonusPoints;

            amcMeasure.fromDate = $filter('date')(amcService.fromDate, 'MM/dd/yyyy');
            amcMeasure.toDate = $filter('date')(amcService.toDate, 'MM/dd/yyyy');

            amcMeasure.stageName = amcService.stageName;
            if (amcService.type === 'Independent') {
                amcMeasure.groupFlag = true;
                amcMeasure.clinicianName = amcService.selectedClinicianName;
                amcMeasure.clinicianNPI = amcService.selectedClinicianNPI;
                amcMeasure.clinicianTIN = amcService.selectedClinicianTIN;
            }
            else if (amcService.type === 'Group') {
                amcMeasure.selectedGroupName = amcService.selectedGroupName;
                amcMeasure.selectedGroupTIN = amcService.selectedGroupTIN;
            } else {
                amcMeasure.IndividualFlag = true;
                amcMeasure.clinicianName = amcService.selectedClinicianName;
                amcMeasure.clinicianNPI = amcService.selectedClinicianNPI;
                amcMeasure.clinicianTIN = amcService.selectedClinicianTIN;
                amcMeasure.selectedGroupName = amcService.selectedGroupName;
                amcMeasure.selectedGroupTIN = amcService.selectedGroupTIN;
            }
            amcMeasure.totalMeasurePoints = amcService.totalMeasurePoints;
            amcMeasure.aciScore = amcService.aciScore;

            window.setTimeout(function () {
                var printContents = $("#printAMCMeasureGrid").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            amcMeasure.LogUserAuditData(practiceLevelConstants.PrintActionType, "Printed " + practiceLevelConstants.MIPSACIMeasureReport + amcMeasure.AuditGroupClinicianName);
        }, function (error) {
            toastr.error('error while getting loggged in user details and Practice Details');
        });
    }

    //navigating to the ACI Measure Report screen
    amcMeasure.goToANRMeasureReport = function (currentValues) {
        //practiceLevelService.setScreenName('PracticeLevelDashBoard');
        var screenName = 'AMCMeasureReport';
        localStorage.setItem("setScreenName", screenName);
        var modifieditem = null;
        modifieditem = currentValues;
        //practiceLevelService.setReportingObj(item);
        currentValues.fromDate = amcMeasure.getReportingObjTogetBackData[0].fromDate;
        currentValues.toDate = amcMeasure.getReportingObjTogetBackData[0].toDate;
        currentValues.tin = amcMeasure.getReportingObjTogetBackData[0].tin;
        currentValues.npi = amcMeasure.getReportingObjTogetBackData[0].npi;
        currentValues.type = amcMeasure.getReportingObjTogetBackData[0].type;
        currentValues.stageName = amcMeasure.stageName;
        currentValues.name = amcMeasure.getReportingObjTogetBackData[0].ClinincianName;
        currentValues.groupName = amcMeasure.getReportingObjTogetBackData[0].name;
        currentValues.physicianName = amcMeasure.ClinincianName;
        currentValues.groupTIN = amcMeasure.groupTIN;
        currentValues.groupId = amcMeasure.groupId;
        if (amcMeasure.getReportingObjTogetBackData[0].type === "Individual" && amcMeasure.groupId === undefined) {
            currentValues.name = amcMeasure.getReportingObjTogetBackData[0].name;
            currentValues.groupName = amcMeasure.getReportingObjTogetBackData[0].groupName;
            currentValues.npi = amcMeasure.getReportingObjTogetBackData[0].npi;
        }
        if (amcMeasure.getReportingObjTogetBackData[0].type === "Individual" && amcMeasure.groupId !== undefined) {
            //when group clicked first time from Clinician Level, assigning the values in name (Clinician) and group Name properties.
            //when clicked back from ANR it goes to else condition and executes the name and group name correctly as the first time values 
            //were set in the object 'AciScoreObjectdata' and from next time it will read from there only.
            if (amcMeasure.fromScreenName === "ClinicianLevelDashBoard") {
                currentValues.name = amcMeasure.getReportingObjTogetBackData[0].ClinincianName; //Physician Name
                currentValues.groupName = amcMeasure.getReportingObjTogetBackData[0].name; //Group Name
            }
            else {
                currentValues.name = amcMeasure.getReportingObjTogetBackData[0].name; //Physician Name
                currentValues.groupName = amcMeasure.getReportingObjTogetBackData[0].groupName; //Group Name 
                currentValues.physicianName = amcMeasure.ClinincianName;
            }
        }
        if (amcMeasure.getReportingObjTogetBackData[0].type === "Group") {
            currentValues.groupName = amcMeasure.getReportingObjTogetBackData[0].name;
            currentValues.tin = amcMeasure.getReportingObjTogetBackData[0].tin;
        }

        currentValues.stageId = amcMeasure.getReportingObjTogetBackData[0].measureCode;

        amcMeasure.setReportingObjTogetData.push(currentValues); //setReportingObjTogetData - list
        amcMeasure.setReportingObjTogetData.push(modifieditem); //setReportingObjTogetData[1]
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(amcMeasure.setReportingObjTogetData));
        $state.go("anr");
    }
    //Saving user audit details
    amcMeasure.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: practiceLevelConstants.MIPSPerfDash,
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }

    amcMeasure.cehrtinit = function () {
        aciService.getCEHRT().then(function (response) {
            if (response.data != null) {
                if (response.data.cehrtEffectiveDate != null && response.data.isActive && $filter('date')(amcMeasure.fromDateInDashBoard, "yyyy") != '2017') {
                    amcMeasure.isCEHRT = true;
                }
            }
        }, function (error) {
            var message = "Server Error go get CEHRT";
            toastr.error(message);
        });
    }

    amcMeasure.getSRADate = function () {
        aciService.getSRADate(amcMeasure.selectedYear).then(function (response) {
            if (response.data != null) {
                if (response.data.sraViewDate != null) {
                    if ($filter('date')(new Date(amcMeasure.fromDateInDashBoard), 'yyyy') === $filter('date')(new Date(response.data.sraViewDate), 'yyyy'))
                    {
                        amcMeasure.isSecuityAys = true;
                    }
                }
            }           
        }, function () {            
        })
    };

    //Newly Added function for 2019 year
    //amcMeasure.getPerformanceYearList = function () {
    //    amcMeasure.performanceYearList = aciPerformanceYearEnums.performanceYearList;
    //    var currentYear = aciPerformanceYearEnums.performanceYearList.find(i=>i.performanceYear == $filter('date')(new Date(), 'yyyy'));
    //    if (currentYear != null) {
    //        amcMeasure.selectedPerformanceYearId = currentYear;
    //        amcMeasure.selectedYear = amcMeasure.selectedPerformanceYearId["performanceYear"];
    //    }
    //    else
    //       amcMeasure.selectedPerformanceYearId = aciConfig.performanceYearList[0];
    //    amcMeasure.selectedYear = amcMeasure.selectedPerformanceYearId["performanceYear"];
    //}

    amcMeasure.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}