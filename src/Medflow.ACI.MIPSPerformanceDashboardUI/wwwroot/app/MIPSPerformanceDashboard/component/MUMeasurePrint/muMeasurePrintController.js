﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('muMeasurePrintController', muMeasurePrintController);
muMeasurePrintController.$inject = ['$state', '$scope', '$timeout', '$filter', '$rootScope', 'uiGridConstants', 'enums', 'muMeasureService', 'practiceLevelService'];
function muMeasurePrintController($state, $scope, $timeout, $filter, $rootScope, uiGridConstants, enums, muMeasureService, practiceLevelService) {
    var muMeasurePrint = this;
    muMeasurePrint.practiceDetailsPrintObj = {};
    muMeasurePrint.currentDate = Date.now();
    muMeasurePrint.init = function () {
        muMeasurePrint.muMeasurePrintObject = [];
        muMeasureService.muPrintObj.map(function (itemobj) {
            itemobj.muMeasureDistinctViewModel.map(function (item) {
                if (Number.isInteger(parseInt(item.performance))) {
                    item.performance = parseInt(item.performance).toFixed(2);
                }
                muMeasurePrint.muMeasurePrintObject.push(item);
            });
        });
        muMeasurePrint.clinicianName = muMeasureService.physicianName;
        muMeasurePrint.tin = muMeasureService.physicianTIN;
        muMeasurePrint.npi = muMeasureService.physicianNPI;
        muMeasurePrint.fromDate = $filter('date')(new Date(muMeasureService.fromDate), 'MM/dd/yyyy');
        muMeasurePrint.toDate = $filter('date')(new Date(muMeasureService.toDate), 'MM/dd/yyyy');
        muMeasurePrint.muMeasureTitle = muMeasureService.muMeasureTitle;

        muMeasurePrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            muMeasurePrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }

        $rootScope.print = true;
        muMeasurePrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("muMeasureReport");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printMuMeasureButton').click();
            }, 100);
        })

    }
    muMeasurePrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}