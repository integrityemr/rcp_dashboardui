﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('anrController', anrController);
anrController.$inject = ['$state', '$scope', '$q', '$filter', 'anrService', 'amcService', 'practiceLevelService', 'muMeasureService', 'muStageEnums', 'enums', 'practiceLevelConstants'];
function anrController($state, $scope, $q, $filter, anrService, amcService, practiceLevelService, muMeasureService, muStageEnums, enums, practiceLevelConstants) {
    var anrMeasure = this;
    anrMeasure.isGroup = "";
    anrMeasure.isPhysician = "";

    anrMeasure.init = function () {
        anrMeasure.getReportingObjTogetBackData = JSON.parse(localStorage.getItem('AciScoreObjectdata'));
        anrMeasure.dashBoarData = anrMeasure.getReportingObjTogetBackData[0];
        anrMeasure.fromScreenName = localStorage.getItem('setScreenName');
        anrMeasure.anrMeasureCode = anrMeasure.getReportingObjTogetBackData[0].measureCode;
        anrMeasure.anrFromDate = anrMeasure.getReportingObjTogetBackData[0].fromDate;
        anrMeasure.anrToDate = anrMeasure.getReportingObjTogetBackData[0].toDate;
        anrMeasure.tin = anrMeasure.getReportingObjTogetBackData[0].tin;
        anrMeasure.npi = anrMeasure.getReportingObjTogetBackData[0].npi;
        anrMeasure.type = anrMeasure.getReportingObjTogetBackData[0].type; // Group / Individual / Participant
        anrMeasure.groupTIN = anrMeasure.getReportingObjTogetBackData[0].groupTIN;
        anrMeasure.isGroup = true;
        anrMeasure.stageName = anrMeasure.getReportingObjTogetBackData[0].stageName;
        anrMeasure.isRedirect = false;
        if (anrMeasure.type === "Individual") {
            anrMeasure.isGroup = true;
            anrMeasure.isPhysician = true;
            anrMeasure.groupName = anrMeasure.getReportingObjTogetBackData[0].groupName + '-' + anrMeasure.getReportingObjTogetBackData[0].groupTIN;
            anrMeasure.physicianName = anrMeasure.getReportingObjTogetBackData[0].name + '-' + anrMeasure.getReportingObjTogetBackData[0].npi;
            anrMeasure.AuditGroupClinicianName = " for Group: " + anrMeasure.getReportingObjTogetBackData[0].groupName + " and for Clinician: " + anrMeasure.getReportingObjTogetBackData[0].name;
        }
        if (anrMeasure.type === "Independent") {
            anrMeasure.physicianName = anrMeasure.getReportingObjTogetBackData[0].groupName + '-' + anrMeasure.getReportingObjTogetBackData[0].npi;
            anrMeasure.isGroup = false;
            anrMeasure.isPhysician = true;
            anrMeasure.AuditGroupClinicianName = " for Clinician: " + anrMeasure.getReportingObjTogetBackData[0].groupName;
        }


        if (anrMeasure.type === "Group") {
            anrMeasure.groupName = anrMeasure.getReportingObjTogetBackData[0].groupName + '-' + anrMeasure.getReportingObjTogetBackData[0].tin;
            anrMeasure.isGroup = true;
            anrMeasure.AuditGroupClinicianName = " for Group: " + anrMeasure.getReportingObjTogetBackData[0].groupName;
        }
        if (anrMeasure.type === "Physician") {
            anrMeasure.physicianName = anrMeasure.getReportingObjTogetBackData[0].physicianName //+ '-' + anrMeasure.getReportingObjTogetBackData[0].npi;
            anrMeasure.isGroup = false;
            anrMeasure.isPhysician = true;
        }

        anrMeasure.stageId = anrMeasure.getReportingObjTogetBackData[0].stageId;
        anrMeasure.measureName = anrMeasure.getReportingObjTogetBackData[0].measureName;
        anrMeasure.setReportingObjTogetData = [];
        anrMeasure.fromScreenName = localStorage.getItem('setScreenName');
        anrMeasure.setReportingObjTogetBackData = [];

        anrMeasure.calculateReportingDays();
        anrMeasure.GetVendorIsMRN();
        anrMeasure.getanrPatientsData();
        //Logging the audit data when this page has arrived from MIPS Performance Dashboard
        if (anrMeasure.fromScreenName === "AMCMeasureReport") {
            anrMeasure.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.ANRPatientList + ' - From - ' + anrMeasure.anrFromDate + ' - To - ' + anrMeasure.anrToDate + ' - ' + anrMeasure.AuditGroupClinicianName + '- Stage - ' + anrMeasure.stageName + ' - Measure Set - ' + anrMeasure.measureName);
        }

        //Logging the audit data when this page has arrived from  MUMeasure Report
        if (anrMeasure.fromScreenName === "MUMeasureReport" && anrMeasure.physicianName != undefined) {
            anrMeasure.LogUserAuditData(practiceLevelConstants.ViewActionType, "Viewed " + practiceLevelConstants.ANRPatientList + ' - From - ' + anrMeasure.anrFromDate + ' - To - ' + anrMeasure.anrToDate + ' - ' + anrMeasure.physicianName + '- Stage - ' + anrMeasure.stageName + ' - Measure Set - ' + anrMeasure.measureName);
        }
    }

    anrMeasure.calculateReportingDays = function () {
        var one_day = 1000 * 60 * 60 * 24;
        anrMeasure.reportingDuration = Math.ceil((new Date(anrMeasure.anrToDate).getTime() - new Date(anrMeasure.anrFromDate).getTime()) / (one_day));
        anrMeasure.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    };


    anrMeasure.getanrPatientsData = function () {
        anrMeasure.updatedData = [];
        if (anrMeasure.npi === null || anrMeasure.npi === "")
            anrMeasure.npi = 0;

        anrMeasure.anrFromDate = $filter('date')(new Date(anrMeasure.anrFromDate), 'yyyy-MM-dd');
        anrMeasure.anrToDate = $filter('date')(new Date(anrMeasure.anrToDate), 'yyyy-MM-dd');

        anrService.getANRMeasurePatientsData(anrMeasure.stageId, anrMeasure.anrMeasureCode, anrMeasure.anrFromDate, anrMeasure.anrToDate, anrMeasure.tin, anrMeasure.npi, anrMeasure.type).then(function (response) {
            anrMeasure.resultSet = response.data;
            if (response.data !== null) {
                anrMeasure.updatedData = response.data;
            }
            //console.log(response.data);
        }, function (error) {
            toastr.error("Server Error in Getting ACI Measure Report for Clinician ");
        });
    };

    // Added this method to get MRN Configuration status of Vendor for showing MRN column in ANR screen
    anrMeasure.GetVendorIsMRN = function () {
        anrService.GetVendorIsMRNValue().then(function (response) {
            anrMeasure.IsMRN = response.data;
        })
    }

    anrMeasure.goBackToPreviousScreen = function () {
        //get the screen name from the service and redirect to that screen
        if (localStorage.getItem('setScreenName') !== null && localStorage.getItem('setScreenName') !== "" && localStorage.getItem('setScreenName') !== "undefined") {
            if (localStorage.getItem('setScreenName') === "AMCMeasureReport") {

                anrMeasure.getReportingObjTogetBackData[1].measureCode = anrMeasure.getReportingObjTogetBackData[1].stageId; //imp

                if (anrMeasure.getReportingObjTogetBackData[1].type === "Group") {
                    anrMeasure.getReportingObjTogetBackData[1].name = anrMeasure.getReportingObjTogetBackData[1].groupName;
                }
                if (anrMeasure.getReportingObjTogetBackData[1].type === "Independent") {
                    anrMeasure.getReportingObjTogetBackData[1].physicianNPI = anrMeasure.getReportingObjTogetBackData[1].npi;
                    anrMeasure.getReportingObjTogetBackData[1].name = anrMeasure.getReportingObjTogetBackData[1].groupName;
                }
                if (anrMeasure.getReportingObjTogetBackData[1].type === "Physician") {
                    anrMeasure.getReportingObjTogetBackData[1].physicianNPI = anrMeasure.getReportingObjTogetBackData[1].npi;
                }
                if (anrMeasure.getReportingObjTogetBackData[1].type === "Individual") {
                    anrMeasure.getReportingObjTogetBackData[1].groupName = anrMeasure.getReportingObjTogetBackData[1].groupName;
                    anrMeasure.getReportingObjTogetBackData[1].groupTIN = anrMeasure.getReportingObjTogetBackData[1].groupTIN;
                    anrMeasure.getReportingObjTogetBackData[1].physicianName = anrMeasure.getReportingObjTogetBackData[1].physicianName;
                    anrMeasure.getReportingObjTogetBackData[1].name = anrMeasure.getReportingObjTogetBackData[1].name;
                    anrMeasure.getReportingObjTogetBackData[1].groupId = null;
                }
                anrMeasure.setReportingObjTogetBackData.push(anrMeasure.getReportingObjTogetBackData[1]);
                localStorage.setItem("AciScoreObjectdata", JSON.stringify(anrMeasure.setReportingObjTogetBackData));
                $state.go('amcMeasureReport');
            }

            else if (localStorage.getItem('setScreenName') === "MUMeasureReport") {
                $state.go('muMeasureReport');
            }
        }
    }
    //Saving user audit details
    anrMeasure.LogUserAuditData = function (ActionType, Action) {
        var currentDate = new Date();
        var ClientTime = new Date(currentDate.getTime() - (currentDate.getTimezoneOffset() * 60000)).toJSON();
        var data = {
            ScreenName: "Patient List",
            ActionType: ActionType,
            Action: Action,
            CreatedDate: ClientTime
        };
        practiceLevelService.LogUserAudit(data).then(function (res) {
            if (res) {
                return true;
            }
        }, function (res) {
            toastr.error("Audit data did not get captured");
        });
    }

    anrMeasure.init();

    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });


}