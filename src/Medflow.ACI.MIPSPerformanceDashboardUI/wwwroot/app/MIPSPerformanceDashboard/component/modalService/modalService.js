﻿angular.module('aci.modalService', [])
    .service('modalService', ['$uibModal', '$sce', 'constants', '$uibModalStack', 'util',
    function ($uibModal, $sce, constants, $uibModalStack, util) {
        var modalDismissTimer;
        var confirmModalDefaults = {
            backdrop: false,
            keyboard: true,
            modalFade: true,
            templateUrl: constants.uiRoot + 'app/MIPSPerformanceDashboard/component/modalService/confirmModal.tpl.html'
        };

        var confirmModalOptions = {
            closeButtonText: 'Cancel',
            actionButtonText: 'OK',
            headerText: 'Confirm'            
        };        

        this.showConfirmModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) {
                customModalDefaults = {};
            }
            if (!customModalOptions) {
                customModalOptions = {};
            }
            //customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions, confirmModalOptions, confirmModalDefaults);
        };

        this.show = function (customModalDefaults, customModalOptions, defaultModalOptions, modalDefaults) {
            var tempModalDefaults = {};
            var tempModalOptions = {};

            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);
            angular.extend(tempModalOptions, defaultModalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = ['$scope', '$sce', '$uibModalInstance', function ($scope, $sce, $uibModalInstance) {

                    $scope.modalOptions = tempModalOptions;

                    $scope.modalOptions.adobeEvent = {};
                    $scope.modalOptions.ok = function (result) {
                        result = 'ok';
                        $uibModalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (confirmAbandonForm) {
                        if (confirmAbandonForm
                            && confirmAbandonForm.$dirty
                            && !window.confirm(strings.Confirm_Abandon)) {
                            return false;
                        }
                        clearTimeout(modalDismissTimer);
                        $uibModalInstance.dismiss('cancel');
                    };
                }];
            }
            return $uibModal.open(tempModalDefaults).result;
        };

        this.close = function () {
            $uibModalStack.dismissAll();
        };

        this.dirtyForm = false;

        //this.scrollTop = function () {
        //    $('.modal-scroll').scrollTop(0);
        //}
    }]);




//angular.module('aci.mipsPerformanceDashboard').controller('MyModalController', MyModalController);
//MyModalController.$inject = ['$uibModal', '$uibModalStack'];
//function MyModalController($uibModal, $uibModalStack) {
//    var vm = this;
//    vm.message = 'teststset';
//    vm.title = 'title';
//    //vm.items = items;
//    vm.flag = true;
 
//    vm.confirm = function ($event) {
//        if (vm.flag) {
//            $uibModalStack.dismissAll();
//            toastr.success("update");
//        }
//        else {
//            $uibModalStack.dismissAll();
//            toastr.success("Save");
//        }
//    }

//    vm.cancel = function () {
//        $uibModalStack.dismissAll();
//    }
//    return vm;
//};