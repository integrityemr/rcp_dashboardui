﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('RealTimeExportController', dataExportController);
dataExportController.$inject = ['$state', '$scope', 'constants', 'RealTimeDataExportConstants', 'RealTimeExportService', '$window'];
function dataExportController($state, $scope, constants, dataExportConstants, dataExportService, $window) {
    var dataExportCtrl = this;

    dataExportCtrl.fromDate = new Date();
    dataExportCtrl.toDate = new Date();

    dataExportCtrl.physicians = [];
    dataExportCtrl.physicianList = [];
    dataExportCtrl.selectedPhysicianToList = [];

    dataExportCtrl.patientList = [];
    dataExportCtrl.selectedPatientsLst = [];
    dataExportCtrl.selectedPatientsNames = [];
    dataExportCtrl.selectedPatientsAccountLst = [];

    dataExportCtrl.selectedPhysiciansArray = [];
    dataExportCtrl.selectedPhysiciansString = "";
    dataExportCtrl.exportFolderLocation = "";

    dataExportCtrl.settings = {
        showCheckAll: true,
        showUncheckAll: true
    };

    dataExportCtrl.init = function () {
        dataExportCtrl.width = '0%';
        dataExportCtrl.physicianListFn();
        dataExportCtrl.patientListFn();
        dataExportCtrl.FolderPathNotValid = false;
        dataExportCtrl.exportFolderLocation = "";
    }

    dataExportCtrl.physicianListFn = function () { // method to Get physician list
        var url = constants.dataExportServiceUrl + dataExportConstants.PhysicianList;
        dataExportService.getData(url).then(function (res) {
            dataExportCtrl.physicians = res.data;
            angular.forEach(dataExportCtrl.physicians, function (value, index) {
                dataExportCtrl.physicianList.push({ id: value.accountNumber, label: value.lastName + ' ' + value.firstName });
            });
        }, function (res) {
            toastr.error("unable to load practice details"); // we can show custom error when we are getting it from API
        });
    }

    dataExportCtrl.patientListFn = function () {
        var data = {
            FromDate: dataExportCtrl.fromDate,
            ToDate: dataExportCtrl.toDate,
            Physician: dataExportCtrl.selectedPhysiciansString
        };
        var url = constants.dataExportServiceUrl + dataExportConstants.PatientSummaryList;
        dataExportService.postData(url, data).then(function (res) {
            dataExportCtrl.patientList = res.data;
        }, function (res) {
            toastr.error("unable to export data");
        });
    }

    dataExportCtrl.updateSelectAll = function (active) {
        dataExportCtrl.selectedAll = false;
        dataExportCtrl.selectedPatientsNames =[];
        dataExportCtrl.selectedPatientsAccountLst =[];
        if (active) {
            dataExportCtrl.selectedAll = true;
        } else {
            dataExportCtrl.selectedAll = false;
            dataExportCtrl.selectedPatientsLst = [];
            dataExportCtrl.selectedPatientsNames = [];
            dataExportCtrl.selectedPatientsAccountLst = [];
        }
        angular.forEach(dataExportCtrl.patientList, function (patient) {
            patient.isChecked = dataExportCtrl.selectedAll;
            if (dataExportCtrl.selectedAll) {
                 dataExportCtrl.selectedPatientsAccountLst.push(patient.accountNumber);
                dataExportCtrl.selectedPatientsNames.push(patient.lastName + ' ' + patient.firstName);
                dataExportCtrl.selectedPatientsLst.push(patient.id);
            }
        });
    }

    dataExportCtrl.updateSelection = function (accountNumber, active, lastName, firstName,accountdetails) {

        dataExportCtrl.selectedPatientsAccountLst = [];
        dataExportCtrl.selectedPatientsNames = [];
        var fullName = [];
        var accountDetails = [];
        angular.forEach(dataExportCtrl.patientList, function (value, index) {
            if (value.isChecked === true) {
                fullName.push(value.lastName + " " + value.firstName);
                accountDetails.push(value.accountNumber);
            }
        });

        dataExportCtrl.selectedPatientsAccountLst = accountDetails;
        dataExportCtrl.selectedPatientsNames = fullName;
        if (active) {
            dataExportCtrl.selectedPatientsLst.push(accountNumber);
                    }
        else
            dataExportCtrl.selectedPatientsLst.splice(dataExportCtrl.selectedPatientsLst.indexOf(accountNumber), 1);

        if (dataExportCtrl.selectedPatientsLst.length === dataExportCtrl.patientList.length)
            dataExportCtrl.selectedAll = true;
        else
            dataExportCtrl.selectedAll = false;
    }

    dataExportCtrl.ExportData = function () {
        if (dataExportCtrl.exportFolderLocation === "" || dataExportCtrl.exportFolderLocation === "" || dataExportCtrl.exportFolderLocation === undefined) {
            // dataExportCtrl.FolderPathNotValid = true;
            toastr.error("Select Storage Path");
            return false;
        }

        var commaSeperatedString = dataExportCtrl.selectedPatientsLst.join("','");
        commaSeperatedString = "'" + commaSeperatedString + "'";
        var commaseppatientNames = dataExportCtrl.selectedPatientsNames.join("','");
        commaseppatientNames = "'" + commaseppatientNames + "'";
        // dataExportCtrl.selectedPatientsLst.push(patient.id);

        var data = {
            ServerFilePath: dataExportCtrl.exportFolderLocation,
            patientlst: commaSeperatedString,
            patientName: commaseppatientNames
        };

        var url = constants.dataExportServiceUrl + dataExportConstants.Export;
        dataExportService.postData(url, data).then(function (res) {
            //Add below lines in original file      //Add this     

            if (res.data === true) {

                //  dataExportCtrl.fnStart();
                toastr.success("Exported the files successfully");
            }
            if (res.data === false)
                toastr.error("FileName or Storage Path Doesn't Exits");
            ////////////////////////////////////
        });
    }

    dataExportCtrl.DownloadData = function () { 
        // if (dataExportCtrl.exportFolderLocation == "" || dataExportCtrl.exportFolderLocation == "" || dataExportCtrl.exportFolderLocation == undefined) {
        // dataExportCtrl.FolderPathNotValid = true;
        //    toastr.error("Select Storage Path");
        //   return false;
        //}

        var commaSeperatedString = dataExportCtrl.selectedPatientsLst.join("','");
        commaSeperatedString = "'" + commaSeperatedString + "'";
        var commaseppatientNames = dataExportCtrl.selectedPatientsNames.join("','");
        commaseppatientNames = "'" + commaseppatientNames + "'";
        var PatientAccountDetails = dataExportCtrl.selectedPatientsAccountLst.join("','");
        PatientAccountDetails = "'" + PatientAccountDetails + "'";

        var data = {
            ServerFilePath: dataExportCtrl.exportFolderLocation, 
            patientlst: commaSeperatedString,
            patientName: commaseppatientNames,
            patientAccountlst: PatientAccountDetails
        };
        var url = constants.dataExportServiceUrl + dataExportConstants.Download;
        dataExportService.postData(url, data).then(function (res) {
            //Add below lines in original file      //Add this     
            if (res.data.length > 0) {
                angular.forEach(res.data, function (item) {
                    //dataExportCtrl.selectedPhysiciansArray.push(physician.id);

                    var blob = new Blob([item.docData], { type: "application/octet-stream" });
                    var fileName = item.fileName;
                    saveAs(blob, fileName);
                });

                toastr.success("Exported the files successfully");
            }
            else {
                toastr.error("File or Storage Path Doesn't Exits");
            }
                ////////////////////////////////////
        });
    }

    function saveAs(blob, fileName) {
      
            var url = window.URL.createObjectURL(blob);

            var anchorElem = document.createElement("a");
            anchorElem.style = "display: none";
            anchorElem.href = url;
            anchorElem.download = fileName;

            document.body.appendChild(anchorElem);
            anchorElem.click();

            document.body.removeChild(anchorElem);

            // On Edge, revokeObjectURL should be called only after
            // a.click() has completed, atleast on EdgeHTML 15.15048
            setTimeout(function () {
                window.URL.revokeObjectURL(url);
            }, 1000);
       
    };

    dataExportCtrl.clearSearch = function () {
        dataExportCtrl.fromDate = new Date();
        dataExportCtrl.toDate = new Date();
        dataExportCtrl.selectedPhysicianToList = [];
    }

    dataExportCtrl.searchPatients = function (fromDate, toDate) {
        if (dataExportCtrl.validateSearch(fromDate, toDate)) {
            dataExportCtrl.selectedPhysiciansArray = [];
            angular.forEach(dataExportCtrl.selectedPhysicianToList, function (physician) {
                dataExportCtrl.selectedPhysiciansArray.push(physician.id);
            });
            dataExportCtrl.selectedPhysiciansString = dataExportCtrl.selectedPhysiciansArray.join("','");
            if (dataExportCtrl.selectedPhysiciansString !== "")
                dataExportCtrl.selectedPhysiciansString = "'" + dataExportCtrl.selectedPhysiciansString + "'";
            dataExportCtrl.patientListFn();
        }
    }

    dataExportCtrl.validateSearch = function (fromDate, toDate) {
        var returnValue = false;
        if (dataExportCtrl.fromDate !== "" && dataExportCtrl.fromDate !== undefined && dataExportCtrl.fromDate !== null) {
            returnValue = true;
        }
        else {
            toastr.error("Please select FromDate");
            returnValue = false;
        }
        if (dataExportCtrl.toDate !== "" && dataExportCtrl.toDate !== undefined && dataExportCtrl.toDate !== null) {
            returnValue = true;
        }
        else {
            toastr.error("Please select To Date");
            returnValue = false;
        }

        var currentDate = new Date();
        if (typeof dataExportCtrl.fromDate === 'undefined' || typeof dataExportCtrl.toDate === 'undefined') {
            toastr.error("please enter valid date");
            return false;
        }
        if (fromDate > currentDate || toDate > currentDate || fromDate > toDate) {
            toastr.error("please enter valid date");
            return false;
        }
        return returnValue;
    }

    dataExportCtrl.isFilePathEmpty = function () {
        dataExportCtrl.FolderPathNotValid = false;
    }
    dataExportCtrl.isFilePathEmptyBlur = function () {
        if (dataExportCtrl.exportFolderLocation === null || dataExportCtrl.exportFolderLocation === "" || dataExportCtrl.exportFolderLocation === undefined)
            dataExportCtrl.FolderPathNotValid = true;
        else
            dataExportCtrl.FolderPathNotValid = false;

    }

    dataExportCtrl.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}