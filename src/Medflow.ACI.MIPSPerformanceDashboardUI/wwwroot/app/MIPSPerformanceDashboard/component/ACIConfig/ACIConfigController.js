﻿'use strict'
angular.module('aci.mipsPerformanceDashboard')
.controller('ACIConfigController', ACIConfigController);
ACIConfigController.$inject = ['$state', '$scope', 'aciService', 'practiceInfoService', '$q', 'dashBoardEnums', 'configEnums', '$filter', 'util', 'modalService', 'regex', '$timeout', 'nextYearEnum', 'aciPerformanceYearEnums'];
function ACIConfigController($state, $scope, aciService, practiceInfoService, $q, dashBoardEnums,enums, $filter, util, modalService, regex, $timeout, nextYearEnum, aciPerformanceYearEnums) {
    var aciConfig = this;
    aciConfig.enums = enums;
    aciConfig.regex = regex;
    aciConfig.groupReportingId = 0;
    aciConfig.individualReportingId = 0;
    aciConfig.copyToList = [];
    aciConfig.selectedCopyToList = [];
    aciConfig.enableSave = false;
    aciConfig.enableHistory = true;
    aciConfig.noData = false;
    aciConfig.fromDate = null;
    aciConfig.effectiveDate = null;
    aciConfig.cehrtIsActive = false;
    aciConfig.cehrtId = null;
    aciConfig.cehrtShow = false;
    aciConfig.toDate = null;
    aciConfig.reportingDuration = null;
    aciConfig.selectedYear = 2019;
    $scope.submitted = false;
    aciConfig.settings = {
        showCheckAll: false,
        showUncheckAll: false,
        scrollable: false
    };

    aciConfig.init = function () {
        if (localStorage.getItem('emergencyAccess') == "true") {
            aciConfig.loadInit();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role != "ADMINISTRATOR") {
                        toastr.error('You are not authorized.');
                        $state.go('Home');
                    }
                    else {
                        aciConfig.loadInit();
                    }
                }

            });
        }
        aciConfig.cehrtStatus(false);
    }
    aciConfig.cehrtStatus = function (isActive) {
        $('.toggle').toggles({
            on: isActive,
            height: 20
        });
    }
    aciConfig.cehrtStatusChange = function () {
        var status = $('.toggle-on').hasClass('active')
        if (status)
            aciConfig.cehrtIsActive = true
        else
            aciConfig.cehrtIsActive = false
    }
    aciConfig.loadInit = function () {
        var date = new Date();
        //aciConfig.fromDate = new Date();
        //aciConfig.toDate = new Date();
        var currentDate = new Date();
        aciConfig.yearEndDate = new Date(date.getFullYear(), 11, 31);
        aciConfig.yearStartDate = new Date(date.getFullYear(), 0, 1);

        aciConfig.pastyearEndDate = new Date(date.getFullYear() - 1, 11, 31);
        aciConfig.pastyearStartDate = new Date(date.getFullYear() - 1, 0, 1);

        aciConfig.updateToDate(aciConfig.fromDate);
        aciConfig.typeOptions = [
            // { name: 'Select Reporting Type',  value: 0,},
             { name: 'Group', value: enums.cpiaType.group },
             { name: 'Individual', value: enums.cpiaType.individual }
        ];

        aciConfig.getReportingPeriodList();

        var promises = [
            aciService.getGroupList(),
            aciService.getACICliniciansList(),
            aciService.getACIConfigurationCountDetails(),
            //aciService.getMeasuresList(currentDate.getFullYear()),
            aciService.getMeasuresList(),
            aciService.enabledPublicHealthPoints(1001),
            aciService.enabledPublicHealthPoints(1002),
            aciService.enableQualityRegistryPoints(),
            aciService.createAuditLogACIConfiguration()
        ];

        $q.all(promises).then(function (data) {
            //aciConfig.cpiaRadioType = enums.cpiaType.simple;
            //aciConfig.cpiaType = enums.cpiaType.simple;
            aciConfig.cpiaType = enums.cpiaType.group;

            //aciConfig.getMeasureSetByCPIAType(aciConfig.cpiaType);
            //if (aciConfig.configuredMeasureSetId) {
            //    aciConfig.stageId = aciConfig.configuredMeasureSetId;
            //}
           
            aciConfig.groups = data[0].data;
            aciConfig.groups.unshift({
                groupReportingId: 0,
                groupReportingName: 'Select Group',
            });
            aciConfig.clinicians = data[1].data;
            aciConfig.clinicians.unshift({
                physicianName: 'Select Clinician',
                individualReportingId: 0
            });
            aciConfig.reportingCount = data[2].data;
            aciConfig.measuresList = data[3].data;
            aciConfig.measuresList.unshift({
                stageCode: 0, id: 0, name: 'Select Stage', stageType: "select"
            });
            aciConfig.syndromicScore = data[4].data;
            aciConfig.immunizationScore = data[5].data;
            aciConfig.griScore = data[6].data;
            aciConfig.defaultMeasuresList = angular.copy(aciConfig.measuresList);

            aciConfig.groupReportingId = aciConfig.groups[0].groupReportingId;
            aciConfig.individualReportingId = aciConfig.clinicians[0].individualReportingId;
            //New Added for 2019 -Default Pi Numeric Performance measure name and Measure Set
            aciConfig.selectedPerformanceyearMeasureSet = aciConfig.measuresList.filter(function (item) {
                return item.id == 6;
            });
            aciConfig.stageName = aciConfig.measuresList[1].name;
            //aciConfig.stageName = stage[6].name;
            //if (aciConfig.cpiaType === enums.cpiaType.simple) {
            //    aciConfig.selectedClinicianId = 0;
            //    aciConfig.selectedGroupId = 0;
            //    aciConfig.selectedType = null;
            //}
            aciService.getMeasureSetByCPIAType(aciConfig.cpiaType, aciConfig.selectedType, aciConfig.selectedClinicianId, aciConfig.selectedGroupId, aciConfig.tin, aciConfig.npi, aciConfig.selectedYear).then(function (res) {
                aciConfig.stageId = res.data;
                aciConfig.onMeasureSetChange();
                if (aciConfig.stageId === 0) {
                    aciConfig.stageId = aciConfig.measuresList[0].id;
                }
                aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId,aciConfig.selectedYear);
            })


        }, function (error) {
            toastr.error("ACI Configurations not updated");
        });
        //New Added for performance year
        aciConfig.getPerformanceYearList();
        aciConfig.getMeasureName(aciConfig.stageId);
        aciConfig.onMeasureSetChange();
    }


    aciConfig.getReportingPeriodList = function () {
        aciConfig.reportingPeriodList = dashBoardEnums.reportingPeriod;
        aciConfig.reportingPeriodId = 0;

    }

    aciConfig.updateToDateNew = function (item, dropbox) {
        if (aciConfig.reportingPeriodId === 0) {
            toastr.error("Please select valid Reporting Period ");
            return false;
        }

        if (aciConfig.reportingPeriodId === 1) { //Full Year

            aciConfig.disableToDateField = true;
            aciConfig.disableFromDateFiled = true;
            if (aciConfig.selectedPerformanceYearId.performanceYear === 2018) {
                aciConfig.fromDate = angular.copy(aciConfig.pastyearStartDate);
                aciConfig.toDate = angular.copy(aciConfig.pastyearEndDate);
            }

            if (aciConfig.selectedPerformanceYearId.performanceYear === 2019) {
                aciConfig.fromDate = angular.copy(aciConfig.yearStartDate);
                aciConfig.toDate = angular.copy(aciConfig.yearEndDate);
            }
            var firstDate = new Date(aciConfig.toDate);
            var secondDate = new Date(aciConfig.fromDate);
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            aciConfig.reportingDuration = diffDays + 1;

            if (aciConfig.reportingDuration === 365) {
                aciConfig.reportingPeriodId = 1
            }
            else if (aciConfig.reportingDuration === 90) {
                aciConfig.reportingPeriodId = 2
            }
            else {
                aciConfig.reportingPeriodId = 3
            }
        }

        if (aciConfig.reportingPeriodId === 2) { //90 Days
            aciConfig.disableToDateField = true;
            aciConfig.disableFromDateFiled = false;
            // if (aciConfig.fromDate === undefined || aciConfig.fromDate === null) { return false; }

            aciConfig.fromDate = new Date();
            var reqTime = aciConfig.fromDate.getTime();
            aciConfig.toDate = new Date(reqTime + 89 * 24 * 60 * 60 * 1000);

            var firstDate = new Date(aciConfig.toDate);
            var secondDate = new Date(aciConfig.fromDate);
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
            aciConfig.reportingDuration = diffDays + 1;

            if (aciConfig.reportingDuration === 365) {
                aciConfig.reportingPeriodId = 1
            }
            else if (aciConfig.reportingDuration === 90) {
                aciConfig.reportingPeriodId = 2
            }
            else {
                aciConfig.reportingPeriodId = 3
            }
        }
        if (aciConfig.reportingPeriodId === 3) { //90 Days
            aciConfig.disableFromDateFiled = false;
            aciConfig.disableToDateField = false;
            aciConfig.fromDate = "";
            aciConfig.toDate = "";
            aciConfig.reportingDuration = "";
            aciConfig.reportingPeriodId = 0;
        }





    }

    //Newly Added function for 2019 year
    aciConfig.getPerformanceYearList = function () {
        aciConfig.performanceYearList = aciPerformanceYearEnums.performanceYearList;
        var currentYear = aciPerformanceYearEnums.performanceYearList.find(i=>i.performanceYear == $filter('date')(new Date(), 'yyyy'));
        if (currentYear != null) {
            aciConfig.selectedPerformanceYearId = currentYear;
            aciConfig.selectedYear = aciConfig.selectedPerformanceYearId["performanceYear"];
        }
        else
            aciConfig.selectedPerformanceYearId = aciConfig.performanceYearList[0];
            aciConfig.selectedYear = aciConfig.selectedPerformanceYearId["performanceYear"];
    }
    aciConfig.getGroupReportingName = function (groupReportingId) {
        if (aciConfig.groups) {
            aciConfig.groups.map(function (group) {
                if (group.groupReportingId === groupReportingId) {
                    aciConfig.selectedGroupReportingName = group.groupReportingName;
                    aciConfig.selectedAuditGroupReportingName = group.groupReportingName;
                }
            })
        }
    }

    $scope.$watch('aciConfig.groupReportingId', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            aciConfig.getGroupReportingName(newVal);
        }
    });

    aciConfig.getPhysicianName = function (individualReportingId) {
        if (aciConfig.clinicians) {
            aciConfig.clinicians.map(function (group) {
                if (group.individualReportingId === individualReportingId) {
                    aciConfig.selectedPhysicianName = group.titleName;
                    aciConfig.selectedAuditPhysicianName = group.titleName;
                    //TO DO remove
                    //aciConfig.selectedAuditPhysicianName = group.clinicianName + "-" + group.individualReportingId;
                }
            })
        }
    }

    $scope.$watch('aciConfig.individualReportingId', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            aciConfig.getPhysicianName(newVal);
        }
    });


    aciConfig.getMeasureName = function (stageId) {
        if (aciConfig.measuresList) {
            aciConfig.measuresList.map(function (measure) {
                if (aciConfig.selectedYear == "2019")
                {
                    aciConfig.stageId = 6;
                }
                else if (measure.id === stageId) {   
                    aciConfig.selectedMeasureName = measure.name;
                }
            })
        }
    }

    $scope.$watch('aciConfig.stageId', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            aciConfig.getMeasureName(newVal);
        }
    });
    aciConfig.loadPerformanceMeasures = function (cpiaType, stageId) {

        aciConfig.getSRADate();
        aciConfig.enableSave = false;
        var groupId = null;
        var individualId = null;

        if (cpiaType === enums.cpiaType.group) {
            groupId = aciConfig.groupReportingId;
            //aciConfig.individualReportingId = aciConfig.clinicians[0].individualReportingId;
            aciConfig.selectedClinician = aciConfig.clinicians[0];
            aciService.ACIMeasureSetList(stageId, cpiaType, individualId, groupId,aciConfig.selectedYear).then(function (response) {
                aciConfig.aciPerformanceMeasures = angular.copy(response.data);
                aciConfig.PerformanceyearDate = aciConfig.aciPerformanceMeasures.fromDate;  
                aciConfig.aciDefaultPerformanceMeasures = angular.copy(response.data);
                aciConfig.cpiaType = cpiaType;
                aciConfig.stageId = stageId;
                aciConfig.onMeasureSetChange();
                aciConfig.setFromToDates();

                var stage = $filter('filter')(aciConfig.measuresList, {
                    id: stageId
                }, true);
                aciConfig.stageName = stage[0].name;
                //if (aciConfig.selectedYear == 2019)
                //{
                //    aciConfig.stageName = aciConfig.measuresList[1].name;
                //}
                //aciConfig.stageName = stage[0].name;
                aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'aciGoalPoints');
                aciConfig.defaultTotalMeasureGoal = angular.copy(aciConfig.totalMeasureGoal);
                aciConfig.updateCopyList();
                aciService.eligibleCPIAPointsforGroupsandIndividual(cpiaType, groupId, 0, 0, 0, aciConfig.selectedYear).then(function (response) {
                    aciConfig.cpiaPoints = response.data;
                }, function () {
                    toastr.error("unable to load group Measures");
                })
            }, function () {
                toastr.error("unable to load group Measures");
            })
        }
        else if (cpiaType === enums.cpiaType.individual) {
            aciConfig.clinicians.map(function (clinician) {
                if (clinician.physicianTIN === aciConfig.tin && clinician.physicianNPI === aciConfig.npi) {
                    aciConfig.clinicianType = clinician.type;
                    aciConfig.selectedTIN = clinician.physicianTIN;
                    aciConfig.selectedNPI = clinician.physicianNPI;
                }
            })
            if (aciConfig.clinicianType === 'Participant') {
                aciService.getACIMeasureSetListByTINAndNPI(aciConfig.selectedNPI, aciConfig.selectedTIN, stageId, aciConfig.cpiaType,aciConfig.selectedYear).then(function (response) {
                    aciConfig.aciPerformanceMeasures = angular.copy(response.data);
                    aciConfig.aciDefaultPerformanceMeasures = angular.copy(response.data);
                    aciConfig.cpiaType = cpiaType;
                    aciConfig.stageId = stageId;
                    aciConfig.onMeasureSetChange();
                    aciConfig.setFromToDates();
                    var stage = $filter('filter')(aciConfig.measuresList, { id: stageId }, true);
                    aciConfig.stageName = stage[0].name;
                    aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'aciGoalPoints');
                    aciConfig.defaultTotalMeasureGoal = angular.copy(aciConfig.totalMeasureGoal);
                    aciConfig.updateCopyList();
                    aciService.eligibleCPIAPointsforGroupsandIndividual(cpiaType, 0, 0, aciConfig.selectedNPI, aciConfig.selectedTIN,aciConfig.selectedYear).then(function (response) {
                        aciConfig.cpiaPoints = response.data;
                    }, function () {
                        toastr.error("unable to load Participant Measures");
                    })

                }, function (error) {
                    toastr.error("unable to load Participant Measures");
                })
            }
            else {
                individualId = aciConfig.individualReportingId;
                aciService.ACIMeasureSetList(stageId, cpiaType, individualId, groupId, aciConfig.selectedYear).then(function (response) {
                    aciConfig.aciPerformanceMeasures = angular.copy(response.data);
                    aciConfig.aciDefaultPerformanceMeasures = angular.copy(response.data);
                    aciConfig.cpiaType = cpiaType;
                    aciConfig.stageId = stageId;
                    aciConfig.onMeasureSetChange();
                    aciConfig.setFromToDates();

                    var stage = $filter('filter')(aciConfig.measuresList, {
                        id: stageId
                    }, true);
                    aciConfig.stageName = stage[0].name;
                    aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'aciGoalPoints');
                    aciConfig.defaultTotalMeasureGoal = angular.copy(aciConfig.totalMeasureGoal);
                    aciConfig.updateCopyList();
                    aciService.eligibleCPIAPointsforGroupsandIndividual(cpiaType, 0, individualId, 0, 0, 0,aciConfig.selectedYear).then(function (response) {
                        aciConfig.cpiaPoints = response.data;
                    }, function () {
                        toastr.error("unable to load individual Measures");
                    })
                }, function () {
                    toastr.error("unable to load individual Measures");
                })
            }

        }
    
            else {//simpletype
                aciService.ACIMeasureSetList(stageId, cpiaType, individualId, groupId, aciConfig.selectedYear).then(function (response)
                {
                    aciConfig.aciPerformanceMeasures = angular.copy(response.data);
                        aciConfig.aciDefaultPerformanceMeasures = angular.copy(response.data);
                        aciConfig.cpiaType = cpiaType;
                        aciConfig.stageId = stageId;
                        aciConfig.onMeasureSetChange();
                        aciConfig.setFromToDates();
                        var stage = $filter('filter')(aciConfig.measuresList, {
                            id: stageId
                        }, true);
                        aciConfig.stageName = stage[0].name;
                        aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'aciGoalPoints');
                        aciConfig.defaultTotalMeasureGoal = angular.copy(aciConfig.totalMeasureGoal);
                        aciConfig.updateCopyList();
                        aciService.eligibleCPIAPoints(cpiaType, aciConfig.selectedYear).then(function (response) {
                            aciConfig.cpiaPoints = response.data;
                        }, function () {
                            toastr.error("unable to load performance measures");
                        })
                    }, function () {
                        toastr.error("unable to load performance measures");
                    })
                }
        }


    aciConfig.setFromToDates = function () {
        aciService.getACIConfigurationCountDetails().then(function (result) {
            aciConfig.reportingCount = result.data;
            if (aciConfig.cpiaType === enums.cpiaType.simple) {
                if (aciConfig.reportingCount.simpleCount == 0) {
                    var year = aciConfig.fromDate.getFullYear();
                    if (year !== nextYearEnum.nextYear) { //2018 year
                        aciConfig.fromDate = null;
                        aciConfig.toDate = null;
                        aciConfig.reportingDuration = null;
                    }
                }
                else {
                    //toget the latest saved FromDate and ToDates to load for the next Time
                    aciConfig.setFromToDatesByCPIAType();
                }
            }
            else if (aciConfig.cpiaType === enums.cpiaType.individual) {
                if (aciConfig.selectedClinician.type === "Participant") {
                    if (aciConfig.reportingCount.participantCount == 0) {
                        aciConfig.fromDate = null;
                        aciConfig.toDate = null;
                        aciConfig.reportingDuration = null;
                    }
                    else {
                        //toget the latest saved FromDate and ToDates to load for the next Time
                        aciConfig.setFromToDatesByCPIAType();
                    }
                }

                else if (aciConfig.selectedClinician.type === "Independent") {
                    if (aciConfig.reportingCount.individualReportingCount == 0) {
                        aciConfig.fromDate = null;
                        aciConfig.toDate = null;
                        aciConfig.reportingDuration = null;
                    }
                    else {
                        //toget the latest saved FromDate and ToDates to load for the next Time
                        aciConfig.setFromToDatesByCPIAType();
                    }
                }
                //else {
                //    aciConfig.fromDate = null;
                //    aciConfig.toDate = null;
                //    aciConfig.reportingDuration = null;
                //}
            }
            else if (aciConfig.cpiaType === enums.cpiaType.group) {
                if (aciConfig.reportingCount.groupreportingCount == 0) {
                    aciConfig.fromDate = null;
                    aciConfig.toDate = null;
                    aciConfig.reportingDuration = null;
                }
                else {
                    //toget the latest saved FromDate and ToDates to load for the next Time
                    aciConfig.setFromToDatesByCPIAType();
                }
            }
        }, function (error) {
            var message = "unable to load...";
            if (error.data) {
                message = error.data.error.Message;
            }
            toastr.error(message);
        });
    }
    aciConfig.setFromToDatesByCPIAType = function () {
        var one_day = 1000 * 60 * 60 * 24;
        var measure = angular.copy(aciConfig.aciDefaultPerformanceMeasures[0]);
        if (typeof measure !== 'undefined') {
            if (measure.fromDate !== null) {
                var year1 = new Date(measure.fromDate);
                var year = year1.getFullYear();
                if (year !== nextYearEnum.nextYear) { //2018 year
                    aciConfig.fromDate = measure.fromDate !== null ? new Date(measure.fromDate) : null;
                    aciConfig.toDate = measure.toDate !== null ? new Date(measure.toDate) : null;
                    aciConfig.existingFromDate = aciConfig.fromDate;
                    aciConfig.existingToDate = aciConfig.toDate;
                }
                else {
                    aciConfig.fromDate = measure.fromDate !== null ? new Date(measure.fromDate) : null;
                    aciConfig.toDate = measure.toDate !== null ? new Date(measure.toDate) : null;
                    aciConfig.existingFromDate = aciConfig.fromDate;
                    aciConfig.existingToDate = aciConfig.toDate;
                }
            }
            if (measure.fromDate === null && aciConfig.selectedPerformanceYearId.performanceYear === 2019 ) {
               
                    aciConfig.fromDate = null ;
                    aciConfig.toDate = null;
                    aciConfig.existingFromDate = aciConfig.fromDate;
                    aciConfig.existingToDate = aciConfig.toDate;
                    aciConfig.disableFromDateFiled = false;
                    aciConfig.disableToDateField = false;
               
            }



        }


        if (aciConfig.fromDate != null && aciConfig.toDate != null) {
            aciConfig.reportingDuration = Math.ceil((aciConfig.toDate.getTime() - aciConfig.fromDate.getTime()) / (one_day));
            aciConfig.reportingDuration += 1;

            if (aciConfig.reportingDuration === 365) {
                aciConfig.reportingPeriodId = 1
            }
            else if (aciConfig.reportingDuration === 90) {
                aciConfig.reportingPeriodId = 2
            }
            else {
                aciConfig.reportingPeriodId = 3
            }


        }
        else {
            aciConfig.reportingDuration = null;
            aciConfig.reportingPeriodId = 0;
            
        }


    }

    aciConfig.updateCopyList = function () {
        aciConfig.copyToList = [];
        aciConfig.selectedCopyToList = [];
        if (aciConfig.cpiaType === enums.cpiaType.group) {
            aciConfig.groups.map(function (group) {
                if (group.groupReportingId !== aciConfig.groupReportingId) {
                    aciConfig.copyToList.push({
                        id: group.groupReportingId, label: group.groupReportingName, auditgroupvalue: group.auditGroupName
                    });
                }
            });
            aciConfig.copyToList.shift();
        }
        var IntialIndivisualReportingId = aciConfig.clinicians != null ? aciConfig.clinicians[0].individualReportingId : 0;
        if (aciConfig.cpiaType === enums.cpiaType.individual) {
            aciConfig.clinicians.map(function (clinician) {
                if (clinician.uniqueId !== aciConfig.uniqueId) {
                    aciConfig.copyToList.push({
                        id: clinician.uniqueId, label: clinician.physicianName, npi: clinician.physicianNPI, tin: clinician.physicianTIN, type: clinician.type
                    });
                }

            });
            aciConfig.copyToList.shift();
        }
    }


    //TODO: Check if not usign
    aciConfig.updatePerformanceMeasuresByType = function () {
        var performanceMeasures = angular.copy(aciConfig.defaultACIPerformanceMeasureSetList);
        aciConfig.aciPerformanceMeasures = angular.copy(performanceMeasures);
        aciConfig.aciDefaultPerformanceMeasures = angular.copy(performanceMeasures);
        aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'performanceGoal');
        aciConfig.updateCopyList();
    }

    aciConfig.getSRADate = function () {
            aciService.getSRADate(aciConfig.selectedYear).then(function (response) {
            aciConfig.defaultSraDate = new Date();
            aciConfig.sraDate = null;
            aciConfig.enableHistory = true;
            if (response.data != null) {
                if (response.data.id > 0) {
                    if (response.data.sraViewDate != null) {
                        aciConfig.sraDate = angular.copy(response.data.sraDate);
                        aciConfig.defaultSraDate = angular.copy(response.data.sraDate);
                    }
                    aciConfig.enableHistory = false;
                }
            }
            $scope.actualSraDate = null;
            $timeout(function () {
                $scope.actualSraDate = angular.copy(aciConfig.defaultSraDate);
            });
            aciConfig.existingSRADate = aciConfig.sraDate;
        }, function () {
            toastr.error("unable to load SRA details");
        })
    };
    aciConfig.onCPIATypeChange = function (cpiaType) {
        aciConfig.cpiaType === cpiaType;
        if (aciConfig.cpiaType == 2 && aciConfig.groupReportingId != 0 && aciConfig.individualReportingId != 0) {
            aciConfig.CopyDiv = false;
        }
        if (cpiaType === enums.cpiaType.simple) {
            aciConfig.selectedClinicianId = 0;
            aciConfig.selectedGroupId = 0;
            aciConfig.selectedType = null;
            aciConfig.fromDate = null;
            aciConfig.toDate = null;
            aciConfig.reportingDuration = null;
        }
        else if (cpiaType === enums.cpiaType.individual) {
            var tin = "";
            var npi = "";
            if (aciConfig.selectedClinician.type == "Participant") {
                tin = aciConfig.tin;
                npi = aciConfig.npi;
            }
            else {
                aciConfig.selectedType = "Independent";
                aciConfig.selectedClinicianId = aciConfig.individualReportingId;
            }
        }
        else if (cpiaType === enums.cpiaType.group) {
            aciConfig.selectedGroupId = aciConfig.groupReportingId;
            aciConfig.fromDate = null;
            aciConfig.toDate = null;
            aciConfig.reportingDuration = null;
        }
        if (typeof aciConfig.tin === 'undefined') {
            aciConfig.tin = null;
        }
        if (typeof aciConfig.npi === 'undefined') {
            aciConfig.npi = null;
        }
        aciService.getMeasureSetByCPIAType(cpiaType, aciConfig.selectedType, aciConfig.selectedClinicianId, aciConfig.selectedGroupId, aciConfig.tin, aciConfig.npi ,aciConfig.selectedYear).then(function (res) {
            aciConfig.stageId = res.data;
            aciConfig.onMeasureSetChange();
            if (aciConfig.stageId === 0) {
                aciConfig.stageId = aciConfig.measuresList[0].id;
            }
            aciConfig.loadPerformanceMeasures(cpiaType, aciConfig.stageId);
        })

    }

    aciConfig.getMeasureSetByCPIAType = function (cpiaType) {
        if (cpiaType === enums.cpiaType.simple) {
            aciConfig.selectedClinicianId = 0;
            aciConfig.selectedGroupId = 0;
            aciConfig.selectedType = null;
        }
        else if (cpiaType === enums.cpiaType.individual) {
            var tin = "";
            var npi = "";
            if (aciConfig.selectedClinician.type == "Participant") {
                tin = aciConfig.tin;
                npi = aciConfig.npi;
            }
            else {
                aciConfig.selectedType = "Independent";
                aciConfig.selectedClinicianId = aciConfig.individualReportingId;
            }
        }
        else if (cpiaType === enums.cpiaType.group) {
            aciConfig.aciPerformanceMeasures = null;
            aciConfig.selectedGroupId = aciConfig.groupReportingId;
        }
        aciService.getMeasureSetByCPIAType(cpiaType, aciConfig.selectedType, aciConfig.selectedClinicianId, aciConfig.selectedGroupId, aciConfig.tin, aciConfig.npi,aciConfig.selectedYear).then(function (res) {
            aciConfig.stageId = res.data;
            aciConfig.onMeasureSetChange();
            if (aciConfig.stageId === 0) {
                aciConfig.stageId = aciConfig.measuresList[0].id;
            }
        })
    }


    aciConfig.selectMeasureSet = function () {
        aciConfig.reportingPeriodId = 0;

        if (aciConfig.stageId == 0)
        {
            toastr.error("Please select the correct measure set");
            return false;

        }
        if (aciConfig.selectedYear == 2019)
        {
            aciConfig.cpiaType = enums.cpiaType.group;
        }
        aciConfig.onMeasureSetChange();
        aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId);
        aciConfig.getMeasureName(aciConfig.stageId);
    }

    //newly added for 2019
     aciConfig.selectPerformanceSet = function ()
    {
         aciConfig.selectedYear = null;
         aciConfig.aciPerformanceMeasures = null;
         aciConfig.selectedClinicianId = 0;
         aciConfig.selectedGroupId = 0;
         aciConfig.selectedType = null;
         aciConfig.groupReportingId = 0;
         aciConfig.individualReportingId = 0;
        
          aciConfig.selectedYear = aciConfig.selectedPerformanceYearId["performanceYear"];
          if (aciConfig.selectedYear == 2018)
          {
              aciConfig.stageId = 1;
              aciConfig.cpiaRadioType = enums.cpiaType.simple;
              aciConfig.cpiaType = enums.cpiaType.simple;
              aciConfig.selectedPerformanceyearMeasureSet = aciConfig.measuresList.filter(function (item) {
                  return item.id !=6;
              });
              aciService.getMeasureSetByCPIAType(aciConfig.cpiaType, aciConfig.selectedType, aciConfig.selectedClinicianId, aciConfig.selectedGroupId, aciConfig.tin, aciConfig.npi, aciConfig.selectedYear).then(function (res) {
                  aciConfig.stageId = res.data;
                  aciConfig.onMeasureSetChange();
                  if (aciConfig.stageId === 0) {
                      aciConfig.stageId = aciConfig.measuresList[0].id;
                  }
                  aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId, aciConfig.selectedYear);
              });

          }
          else if (aciConfig.selectedYear == 2019)
          {
              var date = new Date();
              aciConfig.fromDate = new Date();
              aciConfig.toDate = new Date();
              var currentDate = new Date();
              aciConfig.cpiaType = enums.cpiaType.group;
              aciConfig.yearEndDate = new Date(date.getFullYear(), 11, 31);
              aciConfig.yearStartDate = new Date(date.getFullYear(), 0, 1);
              aciConfig.updateToDate(aciConfig.fromDate);
              aciConfig.selectedPerformanceyearMeasureSet = aciConfig.measuresList.filter(function (item) {
                  return item.id == 6;
              });
              aciService.getMeasureSetByCPIAType(aciConfig.cpiaType, aciConfig.selectedType, aciConfig.selectedClinicianId, aciConfig.selectedGroupId, aciConfig.tin, aciConfig.npi, aciConfig.selectedYear).then(function (res) {
                  aciConfig.stageId = res.data;
                  aciConfig.onMeasureSetChange();
                  if (aciConfig.stageId === 0) {
                      aciConfig.stageId = aciConfig.measuresList[0].id;
                  }
                  aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId, aciConfig.selectedYear);
              });
          }
        aciConfig.getMeasureName(aciConfig.stageId);
    }


    aciConfig.onMeasureSetChange = function () {
        if (aciConfig.measuresList != null) {
            var selectedMeasure = aciConfig.measuresList.filter(function (i) { return (i.id === aciConfig.stageId)  })[0];
            if (selectedMeasure != null) {
                if (selectedMeasure.code == 3) {
                    aciConfig.cehrtinit();
                    aciConfig.cehrtShow = true;
                }
                else {
                    aciConfig.cehrtShow = false;
                }
            }
            else {
                aciConfig.cehrtShow = false;
            }
        }
    }

    aciConfig.cehrtinit = function () {
        aciService.getCEHRT().then(function (response) {
            if (response.data != null) {
                if (response.data.cehrtEffectiveDate != null) {
                    aciConfig.effectiveDate = new Date(response.data.cehrtEffectiveDate);
                }
                aciConfig.cehrtIsActive = response.data.isActive;
                aciConfig.cehrtId = response.data.id;
                aciConfig.cehrtStatus(aciConfig.cehrtIsActive);
            }
        }, function (error) {
            var message = "Server Error go get CEHRT";
            toastr.error(message);
        });
    }

    aciConfig.onClinicianDropDownChange = function () {
        var aa = aciConfig.selectedClinician;
        aciConfig.reportingPeriodId = 0;
        aciConfig.individualReportingId = aciConfig.selectedClinician.individualReportingId;
        aciConfig.type = aciConfig.selectedClinician.type;
        aciConfig.uniqueId = aciConfig.selectedClinician.uniqueId;
        aciConfig.tin = aciConfig.selectedClinician.physicianTIN;
        aciConfig.npi = aciConfig.selectedClinician.physicianNPI;
        aciConfig.onTypeChange();
    }

    aciConfig.onTypeChange = function () {
        aciConfig.reportingPeriodId = 0;
        //aciConfig.getMeasureSetByCPIAType(aciConfig.cpiaType);
        //aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId);

        //if (aciConfig.stageId == 0)
        //{
        //    toastr.warning("Please select measure set");
        //    return false;
        //}

        //if (aciConfig.cpiaType === enums.cpiaType.individual && (aciConfig.individualReportingId == 0 || typeof aciConfig.individualReportingId == 'undefined'))
        //{
        //    toastr.warning("Please select individual reporting entity");
        //    return false;
        //}
        if (aciConfig.cpiaType === enums.cpiaType.simple) {
            aciConfig.selectedClinicianId = 0;
            aciConfig.selectedGroupId = 0;
            aciConfig.selectedType = null;
        }
        else if (aciConfig.cpiaType === enums.cpiaType.individual) {
            var tin = "";
            var npi = "";
            aciConfig.selectedClinicianId = aciConfig.individualReportingId;
            if (typeof aciConfig.selectedClinician.type !== 'undefined') {
                if (aciConfig.selectedClinician.type == "Participant") {
                    tin = aciConfig.tin;
                    npi = aciConfig.npi;
                    aciConfig.selectedType = "Participant";
                }
                else {
                    aciConfig.selectedType = "Independent";
                    aciConfig.selectedClinicianId = aciConfig.individualReportingId;
                }
                //aciConfig.selectedAuditPhysicianName = aciConfig.selectedClinician.physicianName;
            }
            else {
                aciConfig.selectedType = null;
            }
        }
        else if (aciConfig.cpiaType === enums.cpiaType.group) {
            aciConfig.selectedGroupId = aciConfig.groupReportingId;
        }
        else if (aciConfig.cpiaType === enums.cpiaType.individual)
        {
            aciConfig.selectedClinicianId = aciConfig.individualReportingId;
        }
        aciService.getMeasureSetByCPIAType(aciConfig.cpiaType, aciConfig.selectedType, aciConfig.selectedClinicianId, aciConfig.selectedGroupId, aciConfig.tin, aciConfig.npi,aciConfig.selectedYear).then(function (res) {
            aciConfig.stageId = res.data;
            aciConfig.onMeasureSetChange();
            if (aciConfig.stageId === 0) {
                aciConfig.stageId = aciConfig.measuresList[0].id;
            }
            aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId);
        })
    }

    aciConfig.onFromDateChange = function () {

        if (aciConfig.reportingPeriodId !== 3) {
            aciConfig.updateToDate(aciConfig.fromDate);
        }

        if (aciConfig.toDate !== undefined || aciConfig.toDate !== null) {
            var firstDate = new Date(aciConfig.toDate);
            var secondDate = new Date(aciConfig.fromDate);
            var dateTime1 = firstDate.getTime();
            var dateTime2 = secondDate.getTime();
            var diff = dateTime2 - dateTime1;
            if (diff > 0) {
                toastr.error("endDate is greater than startDate");
                aciConfig.reportingDuration = "";
                return true;
            }
        }



    }

    aciConfig.onToDateChange = function () {
        if (aciConfig.toDate === undefined || aciConfig.toDate === null) {
            aciConfig.reportingDuration = null;
            return false;
        }

        if (aciConfig.fromDate !== undefined || aciConfig.fromDate !== null) {
            var firstDate = new Date(aciConfig.toDate);
            var secondDate = new Date(aciConfig.fromDate);
            var dateTime1 = firstDate.getTime();
            var dateTime2 = secondDate.getTime();
            var diff = dateTime2 - dateTime1;
            if (diff > 0) {
                toastr.error("endDate is greater than startDate");
                aciConfig.reportingDuration = "";
                return true;
            }
        }

        var one_day = 1000 * 60 * 60 * 24;
        aciConfig.reportingDuration = Math.ceil((aciConfig.toDate.getTime() - aciConfig.fromDate.getTime()) / (one_day));
        aciConfig.reportingDuration += 1;
    }

    aciConfig.updateMeasureSetOnDateChange = function () {
        var currentDate = new Date();
        var one_day = 1000 * 60 * 60 * 24;
        var updatedMeasuresList = [];
        aciConfig.reportingDuration = Math.ceil((aciConfig.toDate.getTime() - aciConfig.fromDate.getTime()) / (one_day));
        aciConfig.reportingDuration += 1;
        if (aciConfig.reportingDuration === 365) {
            aciConfig.reportingPeriodId = 1
        }
        else if (aciConfig.reportingDuration === 90) {
            aciConfig.reportingPeriodId = 2
        }
        else {
            aciConfig.reportingPeriodId = 3
        }
        if ((aciConfig.toDate.getFullYear() === aciConfig.fromDate.getFullYear()) && aciConfig.toDate.getFullYear() > currentDate.getFullYear()) {
            updatedMeasuresList.push(aciConfig.defaultMeasuresList[1]);
            aciConfig.measuresList = angular.copy(updatedMeasuresList);
            aciConfig.stageId = aciConfig.measuresList !== undefined ? aciConfig.measuresList[0].id : 0;
            aciConfig.onMeasureSetChange();
            aciConfig.measureSetName = aciConfig.measuresList !== undefined ? aciConfig.measuresList[0].name : '';
            aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId);
        }
        else {
            aciConfig.measuresList = angular.copy(aciConfig.defaultMeasuresList);
            aciConfig.measureSetName = aciConfig.measuresList !== undefined ? aciConfig.measuresList[0].name : '';
        }
    };

    aciConfig.updateToDate = function (fromDate) {
        if (fromDate === undefined || fromDate === null) {
            aciConfig.reportingDuration = null;
            return false;
        }
        aciConfig.toDate = angular.copy(fromDate);

        var endofyear = new Date(fromDate.getFullYear(), 11, 31);
        var one_day = 1000 * 60 * 60 * 24;

        //Calculate difference btw the two dates, and convert to days
        var daysLeftinYear = Math.ceil((endofyear.getTime() - fromDate.getTime()) / (one_day));
        if (daysLeftinYear < 89) {
            aciConfig.toDate.setDate(fromDate.getDate() + daysLeftinYear);
        } else {
            aciConfig.toDate.setDate(fromDate.getDate() + 89);
        }

        aciConfig.reportingDuration = Math.ceil((aciConfig.toDate.getTime() - aciConfig.fromDate.getTime()) / (one_day));
        aciConfig.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
        aciConfig.updateMeasureSetOnDateChange();
    }

    aciConfig.updatePerformanceGoal = function (measure, $event) {
        var value = measure.performanceGoal;
        if (typeof value === 'undefined') {
            measure.aciGoalPoints = 0;
            aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'aciGoalPoints');
        } else {
            value = value.replace(/[^0-9]/g, '');
            value = value.substr(0, 4);
            measure.performanceGoal = parseInt(value);

            if (!aciConfig.enableSave) aciConfig.enableSave = true;
            measure.aciGoalPoints = (measure.performanceGoal * measure.maxACIPoints) / 100;
            aciConfig.totalMeasureGoal = util.sum(aciConfig.aciPerformanceMeasures, 'aciGoalPoints');
        }
    }

    aciConfig.copyMeasures = function () {
        if (aciConfig.fromDate == "" || aciConfig.fromDate == undefined) {
            toastr.error("Please select reporting period dates");
            return false;
        }
        if (aciConfig.toDate == "" || aciConfig.toDate == undefined) {
            toastr.error("Please select reporting period dates");
            return false;
        }

        if (aciConfig.selectedYear == null) {
            toastr.error("Please select the performance year.");
            return false;
        }

        if (aciConfig.selectedPerformanceYearId["performanceYear"] == "2019" && (aciConfig.cpiaType === enums.cpiaType.simple || aciConfig.cpiaType === enums.cpiaType.advanced)) {
            toastr.error("Please select the reporting type.");
            return false;
        }
        if (aciConfig.selectedPerformanceYearId["performanceYear"] == "2019" && (aciConfig.cpiaType === enums.cpiaType.group && ((aciConfig.selectedGroupId == null || aciConfig.selectedGroupId==0)))) {
            toastr.error("Please select the reporting group entity.");
            return false;
        }
        if (aciConfig.selectedPerformanceYearId["performanceYear"] == "2019" && (aciConfig.cpiaType === enums.cpiaType.individual && ((aciConfig.individualReportingId == null || aciConfig.individualReportingId == 0)))) {
            toastr.error("Please select the reporting individual entity.");
            return false;
        }

        var fromdateyear = $filter('date')(aciConfig.fromDate, 'yyyy');

        if (fromdateyear != aciConfig.selectedYear) {
            toastr.error("Please Enter Valid Date.");
            return false;
        }

        var requestObject = [];
        var CopyrequestObject = [];
        var copyMeasuresList = [];
        var groupId = null;
        var individualId = null;
        var Auditmessage = null;


        if (aciConfig.selectedYear == 2019)
        {
            var cpiaType = aciConfig.cpiaType;
        }
        else
        {
            var cpiaType = (aciConfig.cpiaRadioType === enums.cpiaType.simple) ? enums.cpiaType.simple : aciConfig.cpiaType;
        }

        //var cpiaType = (aciConfig.cpiaRadioType === enums.cpiaType.simple) ? enums.cpiaType.simple : aciConfig.cpiaType;

        if (cpiaType === enums.cpiaType.group) {
            groupId = aciConfig.groupReportingId;
            Auditmessage = aciConfig.selectedAuditGroupReportingName;
        } else if (cpiaType === enums.cpiaType.individual) {
            individualId = aciConfig.individualReportingId;
            Auditmessage = aciConfig.selectedAuditPhysicianName;
        }
        var defaultMeasures = angular.copy(aciConfig.aciDefaultPerformanceMeasures);

        defaultMeasures.map(function (value) {

            copyMeasuresList.push(value);

        });

        angular.forEach(copyMeasuresList, function (item, key) {
            copyMeasuresList[key].fromDate = $filter('date')(aciConfig.fromDate, 'MM/dd/yyyy');
            copyMeasuresList[key].toDate = $filter('date')(aciConfig.toDate, 'MM/dd/yyyy');
            copyMeasuresList[key].cpiaTypeId = aciConfig.cpiaType;
            copyMeasuresList[key].selectedYear = aciConfig.selectedYear;
        });

        copyMeasuresList.map(function (value) {
            value.groupReportingId = groupId;
            value.individualReportingId = individualId;
        });

        var copyMeasuresObj = [];
        requestObject = [];

        aciConfig.selectedCopiedClinicians = [];


        aciConfig.clinicians.map(function (item) {
            aciConfig.selectedCopyToList.map(function (clinician) {
                if (item.uniqueId === clinician.id) {
                    aciConfig.selectedCopiedClinicians.push(item);
                }
            });
        });

        //For Groups
        aciConfig.selectedCopiedGroupsList = [];
        aciConfig.groupNamesForAuditLog = "";
        if (cpiaType === enums.cpiaType.group) {

            aciConfig.groups.map(function (item) {
                aciConfig.selectedCopyToList.map(function (group) {
                    if (item.groupReportingId === group.id) {
                        aciConfig.selectedCopiedGroupsList.push(item);
                    }
                });
            });
            aciConfig.actionforgroup = "";
            aciConfig.groupNamesForAuditLog = "";
            if (cpiaType === enums.cpiaType.group) {
                aciConfig.selectedCopiedGroupsList.map(function (selectedGroup) {
                    if (cpiaType === enums.cpiaType.group) {
                        if (aciConfig.groupNamesForAuditLog === "") {
                            aciConfig.groupNamesForAuditLog = selectedGroup.auditGroupName;
                        }
                        else {
                            aciConfig.groupNamesForAuditLog = aciConfig.groupNamesForAuditLog + "," + selectedGroup.auditGroupName;
                        }
                    }
                })
            }
            aciConfig.actionforgroup = "Copied ACI Configuration for Group from " + aciConfig.selectedAuditGroupReportingName + " to " + aciConfig.groupNamesForAuditLog
        }

        //For Individual
        aciConfig.clinicianNamesForAuditLog = "";
        aciConfig.actionforIndividual = "";
        if (cpiaType === enums.cpiaType.individual) {
            if (cpiaType === enums.cpiaType.individual) {
                aciConfig.selectedCopiedClinicians.map(function (selectedindividual) {
                    if (cpiaType === enums.cpiaType.individual) {
                        if (aciConfig.clinicianNamesForAuditLog === "") {
                            aciConfig.clinicianNamesForAuditLog = selectedindividual.clinicianName + "-" + selectedindividual.individualReportingId;
                        }
                        else {
                            aciConfig.clinicianNamesForAuditLog = aciConfig.clinicianNamesForAuditLog + "," + selectedindividual.clinicianName + "-" + selectedindividual.individualReportingId;
                        }
                    }
                })
            }
            aciConfig.actionforIndividual = "Copied ACI Configuration for Individual from " + aciConfig.selectedAuditPhysicianName + " to " + aciConfig.clinicianNamesForAuditLog;
        }


        aciConfig.selectedCopyToList.map(function (copyValue) {
            if (aciConfig.cpiaType === 2)//GROUP
            {
                copyMeasuresList.map(function (value) {

                    if (groupId !== null) {
                        value.groupReportingId = copyValue.id;
                    }
                    value.aciConfigurationId = 0;
                    requestObject.push(value);

                });
                var data = [];
                data = angular.copy(requestObject);
                CopyrequestObject = CopyrequestObject.concat(data);
            }

        })
        aciConfig.selectedCopiedClinicians.map(function (copyValue) {

            requestObject = [];
            copyMeasuresList.map(function (value) {

                if (groupId !== null) {
                    value.groupReportingId = copyValue.id;
                } else if (copyValue.type === "Participant") {
                    value.tin = copyValue.physicianTIN;
                    value.npi = copyValue.physicianNPI;
                    value.type = copyValue.type;
                }
                else {
                    value.individualReportingId = copyValue.individualReportingId;
                    value.type = copyValue.type;
                }
                value.aciConfigurationId = 0;
                requestObject.push(value);

            });
            var data = [];
            data = angular.copy(requestObject);
            CopyrequestObject = CopyrequestObject.concat(data);

        });

        var requestMeasureObject = {}
        requestMeasureObject.aciConfigurationModellist = CopyrequestObject;
        requestMeasureObject.isCopied = "true";

        aciService.createACIMeasureAndSRADate(requestMeasureObject).then(function () {
            if (aciConfig.actionforIndividual !== "") {
                var data = {
                    Action: aciConfig.actionforIndividual,
                    ActionType: 'Copy'
                };
                aciService.auditForACI(data).then(function (res) {
                    if (res) {

                    }
                })
            }
            if (aciConfig.actionforgroup !== "") {
                var data = {
                    Action: aciConfig.actionforgroup,
                    ActionType: 'Copy'
                };
                aciService.auditForACI(data).then(function (res) {
                    if (res) {
                    }
                })
            }
            aciConfig.actionforIndividual = "";
            aciConfig.actionforIndividual = "";
            aciConfig.clinicianNamesForAuditLog = "";
            aciConfig.groupNamesForAuditLog = "";
            toastr.success("ACI configurations copied successfully");
        }, function () {
            //toastr.error("server error occured");
        })

    }

    aciConfig.SaveAudit = function (value) {
        var value = {
            Action: aciConfig.AuditSRADate,
            ActionType: 'Copy'
        };
        aciService.auditForACI(value).then(function (res) {
            if (res) {
                return true;
            }
        });
    }

    aciConfig.saveMeasures = function () {

        var fromdateyear = $filter('date')(aciConfig.fromDate, 'yyyy');
        var sraYear = $filter('date')(aciConfig.sraDate, 'yyyy');

        var firstDate = new Date(aciConfig.toDate);
        var secondDate = new Date(aciConfig.fromDate);
        var dateTime1 = firstDate.getTime();
        var dateTime2 = secondDate.getTime();
        var diff = dateTime2 - dateTime1;
        if (diff > 0) {
            toastr.error("endDate is greater than startDate");
            return false;
        }

        if (fromdateyear != aciConfig.selectedYear)
        {
            toastr.error("Please Enter Valid Date.");
            return false;
        }
        if (aciConfig.effectiveDate === undefined) {
            toastr.error("Please Enter Valid Date.");
            return false;
        }

        if (aciConfig.selectedYear == null) 
        {
            toastr.error("Please select the performance year.");
            return false;
        }

        if (aciConfig.selectedPerformanceYearId["performanceYear"] == "2019" && (aciConfig.cpiaType === enums.cpiaType.simple || aciConfig.cpiaType === enums.cpiaType.advanced))
        {
            toastr.error("Please select the reporting type.");
            return false;
        }
        if (aciConfig.selectedPerformanceYearId["performanceYear"] == "2019" && (aciConfig.cpiaType === enums.cpiaType.group && aciConfig.selectedGroupId == null))
        {
            toastr.error("Please select the reporting group entity.");
            return false;
        }
        if (aciConfig.selectedPerformanceYearId["performanceYear"] == "2019" && (aciConfig.cpiaType === enums.cpiaType.group && aciConfig.individualReportingId == null))
        {
            toastr.error("Please select the reporting individual entity.");
            return false;
        }

        if (aciConfig.sraDate !=null)
        {
            if (sraYear != fromdateyear && aciConfig.sraDate != "") {
                toastr.error("please enter SRA valid date.");
                return false;
            }

        }

      
        //if (aciConfig.fromDate >= aciConfig.sraDate && aciConfig.sraDate <= aciConfig.toDate)
        //{
        //    toastr.error("please enter SRA valid date.");
        //    return false;
        //}
        var response = { valid: true, message: '' };
        var measuresObject = [];
        measuresObject = angular.copy(aciConfig.aciPerformanceMeasures);
        response = aciConfig.validateData(measuresObject);
        if (aciConfig.stageId == 0) {
            toastr.error("please select measure set");
            return false;
        }
        if (!response.valid) {
            toastr.error(response.validationMessage);
            return false;
        }
        $scope.submitted = true;
        var records = [];
        if (aciConfig.selectedYear == 2019) {
            var cpiaType =  aciConfig.cpiaType;
        }
        else
        {
            var cpiaType = (aciConfig.cpiaRadioType === enums.cpiaType.simple) ? enums.cpiaType.simple : aciConfig.cpiaType;
        }
        var groupId = null;
        var individualId = null;

        if (cpiaType === enums.cpiaType.group) {
            groupId = aciConfig.groupReportingId;
        } else if (cpiaType === enums.cpiaType.individual) {
            individualId = aciConfig.individualReportingId;

            aciConfig.clinicians.map(function (clinician) {
                if (clinician.individualReportingId === aciConfig.individualReportingId) {
                    if (clinician.type === 'Participant') {
                        aciConfig.NPI = clinician.physicianNPI;
                        aciConfig.TIN = clinician.physicianTIN;

                        //aciConfig.saveIndependentClinicianACIMeasure(clinician.physicianNPI, clinician.physicianTIN)
                    }

                    //  aciConfig.selectedPhysicianType = clinician.type;
                }
            })
        }

        var syndromicScore = aciConfig.syndromicScore.publicHealthRegistryCount;
        var immunizationScore = aciConfig.immunizationScore.publicHealthRegistryCount;
        var griScore = aciConfig.griScore.qualityRegistryCount;
        var cpiaPoints = aciConfig.cpiaPoints.cpiaCount;
        var confirmationMessage = "";

        if (immunizationScore === 0 && (syndromicScore === 0 && griScore === 0) && cpiaPoints === 0 && aciConfig.totalMeasureGoal < 50) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should also select ACI-eligible improvement activities (CPIA configuration screen) and indicate your possible participation with immunization and public health and/or clinical data registry reporting (GRI dashboard)";
        } else if (immunizationScore === 0 && (syndromicScore === 0 && griScore === 0) && cpiaPoints !== 0 && aciConfig.totalMeasureGoal < 40) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should indicate your possible participation with immunization and public health and/or clinical data registry reporting (GRI dashboard)";
        } else if (immunizationScore === 0 && (syndromicScore !== 0 || griScore !== 0) && cpiaPoints === 0 && aciConfig.totalMeasureGoal < 45) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should also select ACI-eligible improvement activities (CPIA configuration screen) and indicate your possible participation with immunization registry reporting (GRI dashboard)";
        } else if (immunizationScore === 0 && (syndromicScore !== 0 || griScore !== 0) && cpiaPoints !== 0 && aciConfig.totalMeasureGoal < 35) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should indicate your possible participation with immunization registry reporting (GRI dashboard)";
        } else if (immunizationScore !== 0 && (syndromicScore === 0 && griScore === 0) && cpiaPoints === 0 && aciConfig.totalMeasureGoal < 40) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should also select ACI-eligible improvement activities (CPIA configuration screen) and indicate your possible participation with public health and/or clinical data registry reporting (GRI dashboard)";
        } else if (immunizationScore !== 0 && (syndromicScore === 0 && griScore === 0) && cpiaPoints !== 0 && aciConfig.totalMeasureGoal < 30) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should indicate your possible participation with public health and/or clinical data registry reporting (GRI dashboard)";
        } else if (immunizationScore !== 0 && (syndromicScore !== 0 || griScore !== 0) && cpiaPoints === 0 && aciConfig.totalMeasureGoal < 35) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. To maximize your MIPS ACI performance points, you should also select ACI-eligible improvement activities (CPIA configuration screen) ";
        } else if (immunizationScore !== 0 && (syndromicScore !== 0 || griScore !== 0) && cpiaPoints !== 0 && aciConfig.totalMeasureGoal < 25) {
            confirmationMessage = " Warning: your ACI performance goals are below what is needed to maximize your MIPS score. You may either save these goals as it is, or continue editing to increase them. ";
        }
        if (confirmationMessage !== "") {
            toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' style='width:100px' class='btn btn-success'>Yes</button> &nbsp <button type='button' id='confirmationRevertNo' style='width:100px' class='btn btn-danger'>No</button>", confirmationMessage,
              {
                  timeOut: 0,
                  extendedTimeOut: 0,
                  closeButton: false,
                  allowHtml: true,
                  onclick: function (event) {
                      event.preventDefault();

                  },
                  onShown: function (toast) {

                      $("#confirmationRevertYes").click(function () {
                          //console.log('clicked yes');
                          toastr.remove();
                          aciConfig.save(cpiaType, groupId, individualId,aciConfig.selectedYear);
                      });
                      $("#confirmationRevertNo").click(function () {
                          //console.log('clicked No');
                          toastr.remove();
                          return false;

                      });
                  }
              });
        } else {
            aciConfig.save(cpiaType, groupId, individualId, aciConfig.selectedYear);
        }
    }
    //aciConfig.saveIndependentClinicianACIMeasure = function (npi, tin) {
    //    aciConfig.save()
    //}

    aciConfig.save = function (cpiaType, groupId, individualId ,selectedYear) {
        var response = { valid: true, message: '' };
        var syndromicStartDates = aciConfig.syndromicScore.publicHealthRegistryDates;
        var immunizationStartDates = aciConfig.immunizationScore.publicHealthRegistryDates;
        var griStartDates = aciConfig.griScore.qualityRegistryStartDate;
        var cpiaStartDates = aciConfig.cpiaPoints.cpiaActivityStartDate;

        var cpiaStartDatesChk = 0;
        cpiaStartDates.map(function (startDate) {
            var cpiaStartDate = new Date(startDate);
            if (aciConfig.fromDate < cpiaStartDate) {
                cpiaStartDatesChk = 1;
            }
        });
        if (cpiaStartDatesChk === 1)
            toastr.warning("Start date of ACI reporting period must be on/ after the activity start date (CPIA eligible for ACI bonus) for receiving credit.");

        var immunizationStartDatesChk = 0;
        immunizationStartDates.map(function (startDate) {
            //including End Date also so decreased 60 days to 59 days 
            if (util.dateDiff(aciConfig.fromDate, startDate) > 59) {
                immunizationStartDatesChk = 1;
            }
        });
        if (immunizationStartDatesChk === 1)
            toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");

        var griStartDatesChk = 0;
        griStartDates.map(function (startDate) {
            if (util.dateDiff(aciConfig.fromDate, startDate) > 59) {
                griStartDatesChk = 1;
            }
        });
        if (griStartDatesChk === 1)
            toastr.warning("For receiving credit, the  start date for Clinical data registry reporting must be within 60 days from the start date of MIPS reporting period");

        var syndromicStartDatesChk = 0;
        syndromicStartDates.map(function (startDate) {
            if (util.dateDiff(aciConfig.fromDate, startDate) > 59) {
                syndromicStartDatesChk = 1;
            }
        });
        if (syndromicStartDatesChk === 1)
            toastr.warning("For receiving credit, the  start date for syndromic surveillance registry reporting must be within 60 days from the start date of MIPS reporting period");
        var measuresObject = [];
        measuresObject = angular.copy(aciConfig.aciPerformanceMeasures);
        if (measuresObject.length > 0) {
            measuresObject.map(function (measure, index) {
                measuresObject[index].fromDate = $filter('date')(aciConfig.fromDate, 'MM/dd/yyyy');
                measuresObject[index].toDate = $filter('date')(aciConfig.toDate, 'MM/dd/yyyy');
                measuresObject[index].cpiaTypeId = cpiaType;
                measuresObject[index].selectedYear = selectedYear;
            })
            measuresObject.map(function (value) {
                value.groupReportingId = groupId;
                value.individualReportingId = individualId;
                //if (aciConfig.selectedPhysicianType === 'Independent') {
                value.NPI = aciConfig.selectedNPI;
                value.TIN = aciConfig.selectedTIN;
                //    //value.type = aciConfig.selectedPhysicianType;
                //}
                value.type = aciConfig.type;
                value.MeasureName = aciConfig.selectedMeasureName;
                if (aciConfig.selectedGroupReportingName != undefined && aciConfig.selectedGroupReportingName != null && aciConfig.selectedGroupReportingName != "")
                    value.GroupName = aciConfig.selectedGroupReportingName;
                value.auditGroupName = aciConfig.selectedGroupReportingName;
                if (aciConfig.selectedClinician != undefined && aciConfig.selectedClinician != null && aciConfig.selectedClinician != "" && aciConfig.selectedClinician.physicianName != undefined && aciConfig.selectedClinician.physicianName != null && aciConfig.selectedClinician.physicianName != "")
                    value.ClinicianName = aciConfig.selectedClinician.physicianName;
                //value.auditClinicianName = aciConfig.selectedClinician.physicianName;
                //value aciConfig.selectedPhysicianType;
            });

            var requestObject = {}
            requestObject.aciConfigurationModellist = measuresObject;
            if (aciConfig.sraDate !== null) {
                if (aciConfig.sraDate === undefined) {
                    requestObject.sraHistoryModel = { sraDate: aciConfig.sraDate }
                }
                else {
                    var aciStartDate = $filter('date')(aciConfig.sraDate, 'MM/dd/yyyy');
                    requestObject.sraHistoryModel = { sraDate: aciStartDate }
                }
            }
            var effectedDate = $filter('date')(aciConfig.effectiveDate, 'MM/dd/yyyy');
            requestObject.cehrtModel = { Id: aciConfig.cehrtId, IsActive: aciConfig.cehrtIsActive, cehrtEffectiveDate: effectedDate, isCEHRT: aciConfig.cehrtShow };
            response = aciConfig.validateData(measuresObject);

            if (!response.valid) {
                toastr.error(response.validationMessage);
                return false;
            }

            aciService.createACIMeasureAndSRADate(requestObject).then(function (data) {
                var message = "ACI configurations saved successfully";
                toastr.success(message);
                $scope.submitted = false;
                if (aciConfig.cpiaType === enums.cpiaType.group) {
                    aciConfig.cpiaRadioType = enums.cpiaType.advanced;
                    aciConfig.cpiaType = enums.cpiaType.group;
                }
                aciConfig.updateSRAHistory();
                aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.stageId);
                aciConfig.SaveAudit();
            }, function (error) {
                var message = "ACI configurations not saved successfully";
                if (error.data) {
                    message = error.data.error.Message;
                }
                toastr.error(message);
            });

        }

        aciConfig.SaveAudit = function () {
            if (aciConfig.existingFromDate != null && aciConfig.existingFromDate != "") {
                aciConfig.existingFromDate = $filter('date')(aciConfig.existingFromDate, 'yyyy-MM-dd');
            }

            if (aciConfig.existingToDate != null && aciConfig.existingToDate != "") {
                aciConfig.existingToDate = $filter('date')(aciConfig.existingToDate, 'yyyy-MM-dd');
            }

            if (measuresObject[0].fromDate != null && measuresObject[0].fromDate != "") {
                aciConfig.ComparefromDate = $filter('date')(measuresObject[0].fromDate.replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2"), 'yyyy-MM-dd');
            }

            if (measuresObject[0].toDate != null && measuresObject[0].toDate != "") {
                aciConfig.ComparetoDate = $filter('date')(measuresObject[0].toDate.replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2"), 'yyyy-MM-dd');
            }


            if (aciConfig.ComparefromDate !== aciConfig.existingFromDate && aciConfig.ComparetoDate !== aciConfig.existingToDate) {
                if (aciConfig.cpiaType === enums.cpiaType.group) {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Group-" + aciConfig.selectedAuditGroupReportingName + "-FromDate-" + aciConfig.existingFromDate + "-ToDate-" + aciConfig.existingToDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed for Group.";
                    }
                }
                else if (aciConfig.cpiaType === enums.cpiaType.individual) {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Clinician-" + aciConfig.selectedAuditPhysicianName + "-FromDate " + aciConfig.existingFromDate + "-ToDate-" + aciConfig.existingToDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed for Clinician.";
                    }
                }
                else {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Simple FromDate-" + aciConfig.existingFromDate + "-ToDate-" + aciConfig.existingToDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed for Simple FromDate.";
                    }
                }
            }
            else if (aciConfig.ComparefromDate !== aciConfig.existingFromDate && aciConfig.ComparetoDate === aciConfig.existingToDate) {
                if (aciConfig.cpiaType === enums.cpiaType.group) {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Group-" + aciConfig.selectedAuditGroupReportingName + "-FromDate-" + aciConfig.existingFromDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed for Group.";
                    }
                }
                else if (aciConfig.cpiaType === enums.cpiaType.individual) {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Clinician-" + aciConfig.selectedAuditPhysicianName + "-FromDate-" + aciConfig.existingFromDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed for Clinician.";
                    }
                }
                else {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Simple FromDate-" + aciConfig.existingFromDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Simple FromDate.";
                    }
                }
            }
            else if (aciConfig.ComparetoDate !== aciConfig.existingToDate && aciConfig.ComparefromDate === aciConfig.existingFromDate) {
                if (aciConfig.cpiaType === enums.cpiaType.group) {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Group-" + aciConfig.selectedAuditGroupReportingName + "-ToDate-" + aciConfig.existingToDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Group.";
                    }
                }
                else if (aciConfig.cpiaType === enums.cpiaType.individual) {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed " + aciConfig.selectedMeasureName + " for Clinician " + aciConfig.selectedAuditPhysicianName + " ToDate " + aciConfig.existingToDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed for Clinician.";
                    }
                }
                else {
                    if (aciConfig.existingFromDate != "" && aciConfig.existingFromDate != undefined && aciConfig.existingFromDate != null) {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Simple ToDate-" + aciConfig.existingToDate + ".";
                    } else {
                        aciConfig.AuditFormDate = "Reporting period changed-" + aciConfig.selectedMeasureName + "-for Simple FromDate.";
                    }
                }
            }
            aciConfig.existingFromDate = measuresObject[0].fromDate;
            aciConfig.existingToDate = measuresObject[0].toDate;


            if (aciConfig.AuditFormDate !== "" && aciConfig.AuditFormDate != null && aciConfig.AuditFormDate != undefined) {
                var data = {
                    Action: aciConfig.AuditFormDate,
                    ActionType: 'Change'
                };
                aciConfig.AuditFormDate = "";
                aciService.auditForACI(data).then(function (res) {
                    if (res) {
                    }
                })

            }
            aciConfig.AuditFormDate = "";
            aciConfig.existingSRADate = new Date(aciConfig.existingSRADate);
            if (aciConfig.existingSRADate != null && aciConfig.existingSRADate != "") {
                aciConfig.existingSRADate = $filter('date')(aciConfig.existingSRADate, 'yyyy-MM-dd');
            }
            if (aciConfig.sraDate != null && aciConfig.sraDate != "") {
                aciConfig.sraDate = $filter('date')(aciConfig.sraDate, 'yyyy-MM-dd');
            }
            if (aciConfig.cpiaType === enums.cpiaType.group && aciConfig.existingSRADate !== aciConfig.sraDate) {
                aciConfig.AuditSRADate = "SRA date for Group-Measure Set-" + aciConfig.selectedMeasureName + "-Group-" + aciConfig.selectedAuditGroupReportingName + " has been changed from-" + aciConfig.existingSRADate;
            }
            if (aciConfig.cpiaType === enums.cpiaType.individual && aciConfig.existingSRADate !== aciConfig.sraDate) {
                aciConfig.AuditSRADate = "SRA date for Individual-Measure Set-" + aciConfig.selectedMeasureName + "-for Clinician " + aciConfig.selectedAuditPhysicianName + " has been changed from-" + aciConfig.existingSRADate;
            }
            if (aciConfig.cpiaType === enums.cpiaType.simple && aciConfig.existingSRADate !== aciConfig.sraDate) {
                aciConfig.AuditSRADate = "SRA date for Simple-Measure Set-" + aciConfig.selectedMeasureName + "-has been changed from-" + aciConfig.existingSRADate;
            }
            if (aciConfig.AuditSRADate !== "" && aciConfig.AuditSRADate != null && aciConfig.AuditSRADate != undefined) {
                var data = {
                    Action: aciConfig.AuditSRADate,
                    ActionType: 'Change'
                };
                aciConfig.AuditSRADate = "";
                aciConfig.existingSRADate = aciConfig.sraDate;
                aciService.auditForACI(data).then(function (res) {
                    if (res) {
                        return true;
                    }
                });
            }
            aciConfig.AuditSRADate = "";
        }
    }

    aciConfig.validateData = function (measures) {
        var message = 'success';
        var isValid = true;
        var currentDate = new Date();
        if (typeof aciConfig.fromDate === 'undefined' ||
            typeof aciConfig.toDate === 'undefined') {
            isValid = false;
            message = 'Please select/enter data in the ACI configuration fields.';
            return { valid: isValid, validationMessage: message };
        }
        if (aciConfig.fromDate === null || aciConfig.toDate === null) {
            isValid = false;
            message = 'Please select/enter data in the ACI configuration fields.';
        }
        else if (new Date(aciConfig.fromDate).getFullYear() !== new Date(aciConfig.toDate).getFullYear()) {
            isValid = false;
            message = 'Reporting period is not within the calendar year';
        }
        else if (aciConfig.reportingDuration < 89) {
            isValid = false;
            message = 'Reporting period must be greater than or equal to 90 days';
        }
        else if (new Date(aciConfig.fromDate) > new Date(aciConfig.toDate.toDateString())) {
            message = 'Start date of ACI reporting period must be on/ after the activity start date (CPIA eligible for ACI bonus) for receiving credit.';
            isValid = false;
        }
        else if (aciConfig.cpiaType == enums.cpiaType.group) {
            if (aciConfig.groupReportingId === 0) {
                isValid = false;
                message = 'Please select group';
            }

        }
        else if (aciConfig.cpiaType == enums.cpiaType.individual) {
            if (aciConfig.individualReportingId === 0) {
                isValid = false;
                message = 'Please select clinician';
            }

        }
        var copyMeasuresList = measures.map(function (measure) {
            if (typeof measure.performanceGoal === 'string' || measure.performanceGoal === null) {
                message = 'performance goal percentage must be between 0 and 100';
                isValid = false;
            }
        });



        return { valid: isValid, validationMessage: message };
    }

    aciConfig.setFullCalender = function () {
        aciConfig.fromDate = angular.copy(aciConfig.yearStartDate);
        aciConfig.toDate = angular.copy(aciConfig.yearEndDate);
        var one_day = 1000 * 60 * 60 * 24;
        aciConfig.reportingDuration = Math.ceil((aciConfig.toDate.getTime() - aciConfig.fromDate.getTime()) / (one_day));
        aciConfig.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    }

    aciConfig.cancelMeasures = function () {
        aciConfig.aciPerformanceMeasures = aciConfig.aciDefaultPerformanceMeasures;
        aciConfig.totalMeasureGoal = angular.copy(aciConfig.defaultTotalMeasureGoal);
        aciConfig.loadPerformanceMeasures(aciConfig.cpiaType, aciConfig.measuresList[0].id);

        aciConfig.showHistoryTable = false;
    }

    aciConfig.updateSRAHistory = function () {
        if (aciConfig.showHistoryTable) aciConfig.getSRAHistory();
    };
    aciConfig.getSRAHistory = function () {
        aciService.getSRAHistoryList(aciConfig.selectedYear).then(function (response) {
            aciConfig.sraHistory = angular.copy(response.data);

            if (aciConfig.sraHistory.length === 0) {
                aciConfig.showHistoryTable = false;
                aciConfig.noData = true;
            } else {
                aciConfig.showHistoryTable = true;
                aciConfig.noData = false;
            }
        }, function () {
            toastr.error("unable to load SRA History");
        })
    }

    aciConfig.deleteSRAHistory = function (id, $index) {
        aciService.deleteSRAHistory(id).then(function (response) {
            if (response.data == true) {     
                aciConfig.sraHistory.splice($index, 1);
                aciConfig.getSRADate();
                toastr.success("SRA date deleted");
            }
        }, function (error) {
            toastr.error("Server error");
        });
    }
    aciConfig.openConfirmationModal = function (message) {
        var modalOptions = {
            headerText: 'Total Numeric Performance Measure - Confirmation',
            closeButtonText: 'No',
            actionButtonText: 'Yes',
            bodyText: message
        };

        return modalService.showConfirmModal({}, modalOptions);
    }

    aciConfig.init();

    aciConfig.getSample = function () {
        return "alert";
    }
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}