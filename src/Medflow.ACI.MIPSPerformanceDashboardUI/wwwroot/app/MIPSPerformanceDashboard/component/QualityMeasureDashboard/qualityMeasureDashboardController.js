﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('qualityMeasureDashboardController', qualityMeasureDashboardController);
qualityMeasureDashboardController.$inject = ['$state', '$scope', '$filter', 'qualityDashboardService', 'qualityConfigurationService', 'practiceLevelService', '$q', 'practiceInfoService'];
function qualityMeasureDashboardController($state, $scope, $filter, qualityDashboardService, qualityConfigurationService, practiceLevelService, $q, practiceInfoService) {
    var qualitydashboardCtrl = this;
    qualitydashboardCtrl.ReportingDetails = JSON.parse(localStorage.getItem('ReportingDetails'));
    qualitydashboardCtrl.disableFields = true;
    qualitydashboardCtrl.ReportingYear = null;
    qualitydashboardCtrl.PerformanceYears = [];
    qualitydashboardCtrl.DashboardMeasuresData = [];
    qualitydashboardCtrl.ImprovementScore = [];
    qualitydashboardCtrl.IsImprovementScore = true;
    qualitydashboardCtrl.ReportingMeasuresTableData = [];
    qualitydashboardCtrl.MeasuresDashboardTableData = [];
    qualitydashboardCtrl.OutComeAndHighpriorityBonus = 0;
    qualitydashboardCtrl.cehrtBonusPoints = 0;
    qualitydashboardCtrl.type = qualitydashboardCtrl.ReportingDetails["reportingType"];// curently its static
    qualitydashboardCtrl.fromdate = "01/01/2018";
    qualitydashboardCtrl.todate = "12/31/2018";
    //qualitydashboardCtrl.fromdate = $filter('date')(new Date(new Date), 'yyyy-MM-dd');// curently its static
    //qualitydashboardCtrl.todate = $filter('date')(new Date(new Date), 'yyyy-MM-dd');// curently its static
    qualitydashboardCtrl.ReportingEntityName = qualitydashboardCtrl.ReportingDetails["ReportingEntityName"]; // curently its static
    qualitydashboardCtrl.TIN = qualitydashboardCtrl.ReportingDetails["TIN"];// curently its static
    qualitydashboardCtrl.NPI = qualitydashboardCtrl.ReportingDetails["NPI"];// curently its static
    qualitydashboardCtrl.ReportingPeriod = qualitydashboardCtrl.fromdate + ' - ' + qualitydashboardCtrl.todate;
    // Group or Individual  // it needs to get from qulaityscore click of mips dashboard screen
    qualitydashboardCtrl.totalAchievmentPts = 0;
    qualitydashboardCtrl.OutComeAndHighpriorityBonus = 0;
    qualitydashboardCtrl.cehrtBonusPoints = 0;
    qualitydashboardCtrl.totalQtyPtsEarned = 0;
    qualitydashboardCtrl.MIPSQualityScore = 0;
    qualitydashboardCtrl.ImprovementScore = 0;
    qualitydashboardCtrl.QualityPerformanceScore = 0;
    qualitydashboardCtrl.QualityMeasureDecline = [];
    qualitydashboardCtrl.exportedData = [];
    qualitydashboardCtrl.exportedColumns = [];
    qualitydashboardCtrl.MeasureType = "";
    qualitydashboardCtrl.UserName = "";
    qualitydashboardCtrl.PracticeDetails = [];
    qualitydashboardCtrl.init = function () {
        qualitydashboardCtrl.getPerformanceYearList();
        qualitydashboardCtrl.setTab(1);
        qualitydashboardCtrl.isshowNational = false;
        qualitydashboardCtrl.isshowPerfomance = true;
        qualitydashboardCtrl.RepPerformancePercButton = "Performance % - Performance Goal";
    }

    qualitydashboardCtrl.setTab = function (tabId) {
        qualitydashboardCtrl.DashboardMeasuresData = [];
        qualitydashboardCtrl.MeasuresDashboardTableData = [];
        if (tabId == 1) {
            qualitydashboardCtrl.ReportingYear = qualitydashboardCtrl.selectedPerformanceYearId.performanceYear;
            if (qualitydashboardCtrl.type == "Group") {
                qualitydashboardCtrl.GetReportingMeasuresCalculationsforGroup();
            }
            else {
                qualitydashboardCtrl.GetReportingMeasuresCalculationsforIndividual();
            }
            qualitydashboardCtrl.MeasureType = "Reporting";
        }
        else {
            if (qualitydashboardCtrl.type == "Group") {
                qualitydashboardCtrl.GetMonitringMeasuresCalculationsforGroup();
            }
            else {
                qualitydashboardCtrl.GetMonitringMeasuresCalculationsforIndividual();
            }
            qualitydashboardCtrl.MeasureType = "Monitoring";
        }
        return qualitydashboardCtrl.tab = tabId;
    };

    qualitydashboardCtrl.isSet = function (tabId) {
        return qualitydashboardCtrl.tab === tabId;
    };

    qualitydashboardCtrl.GetMonitringMeasuresCalculationsforGroup = function () {
        // call service methods which is having api call
        // NPI-TIN/TIN , Reporting Period,fromdate and todate.
        var data = {
            NPI: "",
            TIN: qualitydashboardCtrl.TIN,
            Performanceyear: qualitydashboardCtrl.selectedPerformanceYearId.performanceYear,
            ReportingType: "Group",
        }
        qualitydashboardCtrl.DashboardMeasuresData = [];
        qualityDashboardService.GetMonitoringMeasuresDataGroup(data).then(function (result) {
            if (result.data.length != 0) {
                qualitydashboardCtrl.DashboardMeasuresData = result.data;
                qualitydashboardCtrl.loadPerfomamceGoal();
            }
            else {
                toastr.error("Quality measure configurations, not set up for the reporting entity-Please select the quality measures in the quality configuration screen");
            }
        });
    }

    qualitydashboardCtrl.GetImprovementScoreData = function () {
        var data = {
            NPI: "",
            TIN: qualitydashboardCtrl.TIN,
            Performanceyear: qualitydashboardCtrl.selectedPerformanceYearId.performanceYear,
            ReportingType: "Group",
        }
        qualitydashboardCtrl.ImprovementScore = [];
        qualityDashboardService.GetImprovementScore(data).then(function (result) {
            if (result.data.length != 0) {
                qualitydashboardCtrl.ImprovementScore = result.data;
                var currentyearscore = (qualitydashboardCtrl.totalAchievmentPts / 60) * 100;
                var priorScore = (qualitydashboardCtrl.ImprovementScore / 60) * 100;
                if (priorScore < currentyearscore) {
                    qualitydashboardCtrl.ImprovementScore = ((currentyearscore - priorScore) / priorScore) * 100 + '%';
                    qualitydashboardCtrl.IsImprovementScore = true;
                }
                else {
                    qualitydashboardCtrl.ImprovementScore = 0;
                    qualitydashboardCtrl.IsImprovementScore = false;
                }
            }
        });
    }

    qualitydashboardCtrl.GetMonitringMeasuresCalculationsforIndividual = function () {
        var data = {
            TIN: qualitydashboardCtrl.TIN,
            NPI: qualitydashboardCtrl.NPI,
            Performanceyear: qualitydashboardCtrl.selectedPerformanceYearId.performanceYear,
            ReportingType: "Individual",
        }
        qualitydashboardCtrl.DashboardMeasuresData = [];
        qualityDashboardService.GetMonitoringMeasuresDataIndividual(data).then(function (result) {
            if (result.data.length != 0) {
                qualitydashboardCtrl.DashboardMeasuresData = result.data;
                qualitydashboardCtrl.loadPerfomamceGoal();
            }
            else {
                toastr.error("Quality measure configurations, not set up for the reporting entity-Please select the quality measures in the quality configuration screen");
            }
        });
    }
    qualitydashboardCtrl.GetReportingMeasuresCalculationsforGroup = function () {
        //// call service methods which is having api call
        var data = {
            NPI: "",
            TIN: qualitydashboardCtrl.TIN,
            PerformanceYear: qualitydashboardCtrl.selectedPerformanceYearId.performanceYear,
            ReportingType: "Group",
        }
        qualitydashboardCtrl.DashboardMeasuresData = [];
        var promises = [
            qualityDashboardService.GetReportingMeasuresDataGroup(data),
            qualityConfigurationService.GetQualityMeasureDecline()
        ];
        $q.all(promises).then(function (result) {
            if (result[0].data.length > 0) {
                qualitydashboardCtrl.DashboardMeasuresData = result[0].data;
            }
            else {
                toastr.error("Quality measure configurations, not set up for the reporting entity-Please select the quality measures in the quality configuration screen");
            }
            if (result[1].data.length > 0) {
                qualitydashboardCtrl.QualityMeasureDecline = result[1].data;
            }
            qualitydashboardCtrl.loadPerfomamceGoal();
            qualitydashboardCtrl.CalulateOutComeAndHighPriorityBonus();
            qualitydashboardCtrl.CalulateCEHRTBonus();
            qualitydashboardCtrl.GetTotalAchievmentPts();
            qualitydashboardCtrl.GetTotalQualityPtsEarned();
            qualitydashboardCtrl.GetQualityPerformanceScore();
        })
    }
    qualitydashboardCtrl.GetReportingMeasuresCalculationsforIndividual = function () {
        //// call service methods which is having api call
        var data = {
            NPI: qualitydashboardCtrl.NPI,
            TIN: qualitydashboardCtrl.TIN,
            PerformanceYear: qualitydashboardCtrl.selectedPerformanceYearId.performanceYear,
            ReportingType: "Individual",
        }
        qualitydashboardCtrl.DashboardMeasuresData = [];
        var promises = [
            qualityDashboardService.GetReportingMeasuresDataIndividual(data),
            qualityConfigurationService.GetQualityMeasureDecline()
        ];
        $q.all(promises).then(function (result) {
            if (result[0].data.length > 0) {
                qualitydashboardCtrl.DashboardMeasuresData = result[0].data;
            }
            else {
                toastr.error("Quality measure configurations, not set up for the reporting entity-Please select the quality measures in the quality configuration screen");
            }
            if (result[1].data.length > 0) {
                qualitydashboardCtrl.QualityMeasureDecline = result[1].data;
            }
            qualitydashboardCtrl.loadPerfomamceGoal();
            qualitydashboardCtrl.CalulateOutComeAndHighPriorityBonus();
            qualitydashboardCtrl.CalulateCEHRTBonus();
            qualitydashboardCtrl.GetTotalAchievmentPts();
            qualitydashboardCtrl.GetTotalQualityPtsEarned();
            qualitydashboardCtrl.GetQualityPerformanceScore();
        })
    }

    qualitydashboardCtrl.getPerformanceYearList = function () {
        qualitydashboardCtrl.PerformanceYears = [
        { yearId: 1, performanceYear: 2018 },
         { yearId: 2, performanceYear: 2019 }
        ]
        var currentYear = qualitydashboardCtrl.PerformanceYears.find(i=>i.performanceYear == $filter('date')(new Date(), 'yyyy'));
        if (currentYear != null)
            qualitydashboardCtrl.selectedPerformanceYearId = currentYear;
        else
            qualitydashboardCtrl.selectedPerformanceYearId = qualitydashboardCtrl.PerformanceYears[0];
    }

    qualitydashboardCtrl.loadNational = function () {
        qualitydashboardCtrl.isshowPerfomance = false;
        qualitydashboardCtrl.isshowNational = true;
        qualitydashboardCtrl.PerformancePercButton = "Performance % - National Average";
        qualitydashboardCtrl.MeasuresDashboardTableData = [];

        qualitydashboardCtrl.DashboardMeasuresData.map(function (item) {
            if (item.isInverse) {
                if (item.performancePercentage <= item.nationalAverage) {
                    item.classSuccess = true;
                    item.classDanger = false;
                    item.classWarning = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
                else {
                    item.classWarning = false;
                    item.classDanger = true
                    item.classSuccess = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
            }
            else {
                if (item.performancePercentage >= item.nationalAverage) {
                    item.classSuccess = true;
                    item.classDanger = false;
                    item.classWarning = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
                else if (item.performancePercentage > (item.nationalAverage) * (90 / 100)) {
                    item.classSuccess = false;
                    item.classDanger = false;
                    item.classWarning = true;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
                else {
                    item.classWarning = false;
                    item.classDanger = true
                    item.classSuccess = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
            }
        });
    }

    qualitydashboardCtrl.loadPerfomamceGoal = function () {
        qualitydashboardCtrl.isshowNational = false;
        qualitydashboardCtrl.isshowPerfomance = true;
        qualitydashboardCtrl.PerformancePercButton = "Performance % - Performance Goal";
        qualitydashboardCtrl.MeasuresDashboardTableData = [];
        qualitydashboardCtrl.DashboardMeasuresData.map(function (item) {
            if (item.isInverse) {
                if (item.performancePercentage <= item.performanceGoals) {
                    item.classSuccess = true;
                    item.classDanger = false;
                    item.classWarning = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
                else {
                    item.classSuccess = false;
                    item.classWarning = false;
                    item.classDanger = true;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
            }
            else {
                if (item.performancePercentage >= item.performanceGoals) {
                    item.classSuccess = true;
                    item.classDanger = false;
                    item.classWarning = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
                else if (item.performancePercentage > (item.performanceGoals) * (90 / 100)) {
                    item.classSuccess = false;
                    item.classDanger = false;
                    item.classWarning = true;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
                else {
                    item.classSuccess = false;
                    item.classDanger = true;
                    item.classWarning = false;
                    qualitydashboardCtrl.MeasuresDashboardTableData.push(item);
                }
            }
            qualitydashboardCtrl.setAchivementPonints(item);
        });

    }

    qualitydashboardCtrl.GetTotalAchievmentPts = function () {
        if (qualitydashboardCtrl.DashboardMeasuresData != null && qualitydashboardCtrl.DashboardMeasuresData != "" && qualitydashboardCtrl.DashboardMeasuresData != undefined) {
            qualitydashboardCtrl.measuresAcievmentPts = [];
            angular.forEach(qualitydashboardCtrl.DashboardMeasuresData, function (value) {
                qualitydashboardCtrl.measuresAcievmentPts.push(value.achievementPoints);
            });
            var achvmntPts = qualitydashboardCtrl.measuresAcievmentPts;
            var ttlAchvmntPts = 0;
            var maxTtlAchvmntPts = 60;
            var i, j, k;
            //getting the top 6 maximum numbers from the list of measure achievment points
            if (achvmntPts.length > 6) {
                for (i = 0; i < achvmntPts.length - 1; i++) {
                    for (j = i + 1; j < achvmntPts.length; j++) {
                        var a = parseFloat(achvmntPts[i]);
                        var b = parseFloat(achvmntPts[j]);
                        if (a < b) {
                            achvmntPts[i] = b;
                            achvmntPts[j] = a;
                        }
                    }
                }
            }
            //if >= 6 measures are configured then sum up all those else considering the top 6 for the sum. 
            var len = achvmntPts.length > 6 ? 6 : achvmntPts.length;
            for (k = 0; k < len; k++) {
                ttlAchvmntPts += parseFloat(achvmntPts[k]);
            }
            qualitydashboardCtrl.totalAchievmentPts = !isNaN(ttlAchvmntPts) && angular.isNumber(ttlAchvmntPts) ? ttlAchvmntPts.toFixed(2) : 0;
            //trim the value to 60 if the total points exceed more than 60
            qualitydashboardCtrl.totalAchievmentPts = qualitydashboardCtrl.totalAchievmentPts > maxTtlAchvmntPts ? maxTtlAchvmntPts.toFixed(2) : qualitydashboardCtrl.totalAchievmentPts;
        }
    }

    qualitydashboardCtrl.GetTotalQualityPtsEarned = function () {
        var ttlQtyPtsEarned = 0;
        var maxTtlQtyPtsEarned = 60;
        ttlQtyPtsEarned = parseFloat(qualitydashboardCtrl.totalAchievmentPts) +
            parseFloat(qualitydashboardCtrl.OutComeAndHighpriorityBonus) + parseFloat(qualitydashboardCtrl.cehrtBonusPoints);
        qualitydashboardCtrl.totalQtyPtsEarned = ttlQtyPtsEarned.toFixed(2);
        //trim the value to 60 if the total points exceed more than 60
        qualitydashboardCtrl.totalQtyPtsEarned = ttlQtyPtsEarned > maxTtlQtyPtsEarned ? maxTtlQtyPtsEarned.toFixed(2) : ttlQtyPtsEarned.toFixed(2);
    }
    // Start
    qualitydashboardCtrl.GetQualityPerformanceScore = function () {
        var ttlQualityPtsEarned = qualitydashboardCtrl.totalQtyPtsEarned;
        var TtlAvailabilityPtsEarned = 60;
        var numeratorscore = ((ttlQualityPtsEarned / TtlAvailabilityPtsEarned) * 100) + qualitydashboardCtrl.ImprovementScore;
        qualitydashboardCtrl.QualityPerformanceScore = numeratorscore;
        qualitydashboardCtrl.MIPSQualityScore = qualitydashboardCtrl.QualityPerformanceScore / 2
    }

    qualitydashboardCtrl.CalulateOutComeAndHighPriorityBonus = function () {
        var bonuscount = 0;
        var IsHighpriorityCount = 0;
        var IsOutComeCount = 0;

        var IsOutComewithHighCount = $filter('filter')(qualitydashboardCtrl.DashboardMeasuresData, { isOutCome: true, isHighPriority: true }, true).length;
        var IsOutComeonlyCount = $filter('filter')(qualitydashboardCtrl.DashboardMeasuresData, { isOutCome: true, isHighPriority: false }, true).length;

        var IsHighpriorityCount = $filter('filter')(qualitydashboardCtrl.DashboardMeasuresData, { isHighPriority: true, isOutCome: false }, true).length;
        var IsOutComeCount = IsOutComewithHighCount + IsOutComeonlyCount;
        //var totalMeasurescount = qualitydashboardCtrl.DashboardMeasuresData.length;

        // verify all the conditions for having the bonus points for both inverse and nonInverse measures.
        if (IsHighpriorityCount == 1 && IsOutComeCount == 1) {
            bonuscount = 1;
        }
        else if (IsHighpriorityCount == 0 && IsOutComeCount == 0) {
            bonuscount = 0;
        }
        else if ((IsHighpriorityCount == 1 && IsOutComeCount == 0) || (IsHighpriorityCount == 0 && IsOutComeCount == 1)) {
            bonuscount = 0;
        }
        else {
            qualitydashboardCtrl.DashboardMeasuresData.map(function (item) {
                if (item.isInverse == true) {
                    if ((item.isOutCome == true && item.isHighPriority == true)) {
                        if (item.denominator >= 20) {
                            bonuscount = bonuscount + 2;
                        }
                        else {
                            bonuscount = bonuscount + 0;
                        }
                    }
                    else if ((item.isOutCome == true && item.isHighPriority == false)) {
                        if (item.denominator >= 20) {
                            bonuscount = bonuscount + 2;
                        }
                        else {
                            bonuscount = bonuscount + 0;
                        }
                    }
                    else if (item.isHighPriority == true && item.isOutCome == false) {
                        if (item.denominator >= 20) {
                            bonuscount = bonuscount + 1;
                        }
                        else {
                            bonuscount = bonuscount + 0;
                        }
                    }
                }
                else {
                    if ((item.isOutCome == true && item.isHighPriority == true)) {
                        if (item.denominator >= 20 && item.performancePercentage > 0) {
                            bonuscount = bonuscount + 2;
                        }
                        else if (item.denominator >= 20 && item.performancePercentage == 0) {
                            bonuscount = bonuscount + 0;
                        }
                        else {
                            bonuscount = bonuscount + 0;
                        }
                    }
                    else if ((item.isOutCome == true && item.isHighPriority == false)) {
                        if (item.denominator >= 20 && item.performancePercentage > 0) {
                            bonuscount = bonuscount + 2;
                        }
                        else if (item.denominator >= 20 && item.performancePercentage == 0) {
                            bonuscount = bonuscount + 0;
                        }
                        else {
                            bonuscount = bonuscount + 0;
                        }
                    }
                    else if (item.isHighPriority == true && item.isOutCome == false) {
                        if (item.denominator >= 20 && item.performancePercentage > 0) {
                            bonuscount = bonuscount + 1;
                        }
                        else if (item.denominator >= 20 && item.performancePercentage == 0) {
                            bonuscount = bonuscount + 0;
                        }
                        else {
                            bonuscount = bonuscount + 0;
                        }
                    }
                }

            });

            //Taking bonus points for only additonal measures only (OutCome/HighPriority)
            if (IsOutComeCount >= 1) {
                bonuscount = bonuscount - 2;
            }
            if (IsHighpriorityCount >= 1 && IsOutComeCount == 0) {
                bonuscount = bonuscount - 1;
            }
            // fixing the bouns points to 6 
            qualitydashboardCtrl.OutComeAndHighpriorityBonus = bonuscount;
            if (qualitydashboardCtrl.OutComeAndHighpriorityBonus >= 6) {
                qualitydashboardCtrl.OutComeAndHighpriorityBonus = 6;
            }
            if (qualitydashboardCtrl.OutComeAndHighpriorityBonus <= 0) {
                qualitydashboardCtrl.OutComeAndHighpriorityBonus = 0;
            }

        }
    }

    qualitydashboardCtrl.CalulateCEHRTBonus = function () {
        var MeasuresCount = qualitydashboardCtrl.DashboardMeasuresData.length;
        if (MeasuresCount <= 6) {
            qualitydashboardCtrl.cehrtBonusPoints = MeasuresCount;
        }
        else {
            qualitydashboardCtrl.cehrtBonusPoints = 6;
        }
    }

    qualitydashboardCtrl.GobackToNavScreen = function () {
        $state.go('qualitymeasureDashBoardNav');
    }

    qualitydashboardCtrl.exportMonitoringMeasure = function () {
        qualitydashboardCtrl.exportedData = [];
        qualitydashboardCtrl.exportedColumns = [];
        qualitydashboardCtrl.exportedColumns.push(
            { columnid: 'qualityId', name: "Quality ID" },
                { columnid: 'measureName', name: "Measure Name" },
                { columnid: 'numerator', name: "Numerator" },
                { columnid: 'denominator', name: 'Denominator' },
                { columnid: 'performancePercentage', name: 'Performance percentage' },
                { columnid: 'performanceGoals', name: 'Performance goal' },
                { columnid: 'nationalAverage', name: 'National Average' },
                { columnid: 'achievementPoints', name: 'Achievement points' }
        );
        qualitydashboardCtrl.DashboardMeasuresData.map(function (item) {
            qualitydashboardCtrl.exportedData.push(item);
        })

        window.gridExportToExcel = (function () {
            //getting the user and practice details
            var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
            ];

            $q.all(promises).then(function (data) {
                // if the data for fetching the username and practice details is null or empty then show the message as unable to export.
                if ((data != null && data != "") && (data[0].data != null && data[0].data != "") && (data[1].data != null && data[1].data != "")) {
                    qualitydashboardCtrl.UserName = data[0].data.userName;
                    qualitydashboardCtrl.PracticeDetails = data[1].data;
                    var anchorTag = document.createElement("a");
                    document.body.appendChild(anchorTag);
                    anchorTag.style = "display: none";
                    var value = generateExportTemplateData();
                    var blob = new Blob([value], { type: "application/vnd.ms-excel" });
                    var url = window.URL.createObjectURL(blob);
                    anchorTag.href = url;
                    var fileName = qualitydashboardCtrl.MeasureType == "Reporting" ? "Quality Reporting Measures" : "Quality Monitoring Measures";
                    anchorTag.download = fileName + ".xls";
                    anchorTag.click();
                    window.URL.revokeObjectURL(url);
                    document.body.removeChild(anchorTag);
                }
                else {
                    toastr.error("Unable to export. Please try again.");
                }
            }, function (error) {
                toastr.error("Unable to export. Please try again.");
            });
        }());
    }

    function generateExportTemplateData() {
        var template = "";
        var title = "Sheet1";
        var dateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm:ss"); //formatting the current date time
        var practiceName = qualitydashboardCtrl.PracticeDetails.name;
        var addressLine1 = qualitydashboardCtrl.PracticeDetails.addressline1 != null && qualitydashboardCtrl.PracticeDetails.addressline1 != undefined
                && qualitydashboardCtrl.PracticeDetails.addressline1 != "" ? qualitydashboardCtrl.PracticeDetails.addressline1 + ', ' : '';
        var addressLine2 = qualitydashboardCtrl.PracticeDetails.addressline2 != null && qualitydashboardCtrl.PracticeDetails.addressline2 != undefined
                && qualitydashboardCtrl.PracticeDetails.addressline2 != "" ? qualitydashboardCtrl.PracticeDetails.addressline2 + ', ' : '';
        var city = qualitydashboardCtrl.PracticeDetails.city != null && qualitydashboardCtrl.PracticeDetails.city != undefined
                        && qualitydashboardCtrl.PracticeDetails.city != "" ? qualitydashboardCtrl.PracticeDetails.city + ', ' : '';
        var state = qualitydashboardCtrl.PracticeDetails.state != null && qualitydashboardCtrl.PracticeDetails.state != undefined
                        && qualitydashboardCtrl.PracticeDetails.state != "" ? qualitydashboardCtrl.PracticeDetails.state + ', ' : '';
        var county = qualitydashboardCtrl.PracticeDetails.county != null && qualitydashboardCtrl.PracticeDetails.county != undefined
                        && qualitydashboardCtrl.PracticeDetails.county != "" ? qualitydashboardCtrl.PracticeDetails.county + ', ' : '';
        var zip = qualitydashboardCtrl.PracticeDetails.zip != null && qualitydashboardCtrl.PracticeDetails.zip != undefined
                        && qualitydashboardCtrl.PracticeDetails.zip != "" ? qualitydashboardCtrl.PracticeDetails.zip : '';

        var practiceAddress = addressLine1 + '' + addressLine2 + '' + city + '' + state + '' + county + '' + zip;

        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" \
	    xmlns="http://www.w3.org/TR/REC-html40"><head> \
	    <meta charset="utf-8" /> \
	    <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets> ';
        template += ' <x:ExcelWorksheet><x:Name>' + title + '</x:Name><x:WorksheetOptions><x:DisplayGridlines/>     </x:WorksheetOptions> \
	    </x:ExcelWorksheet>';
        template += '</x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>';
        //Main Heading
        template += '<thead><tr><td colspan="8" style="text-align:center; background-color:#5b9bd5;color:white;font-size:18px; border: 1px solid black">';
        template += 'Quality Measure Report</td></tr></thead>';
        //Practice & User section
        template += '<thead><tr><td style="font-size:14px; border: 1px solid black"><b>Practice Name</b></td>';
        template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + practiceName + '</td></tr>';
        template += '<tr><td style="font-size:14px; border: 1px solid black"><b>Practice Address</b></td>';
        template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + practiceAddress + '</td></tr>';
        template += '<tr><td style="font-size:14px; border: 1px solid black"><b>Date-Time(Export)</b></td>';
        template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + dateTime + '</td></tr>';
        template += '<tr><td style="font-size:14px; border: 1px solid black;"><b>User Name(Export)</b></td>';
        template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + qualitydashboardCtrl.UserName + '</td></tr>';
        template += '<tr><td style="font-size:14px; border: 1px solid black"><b>Reporting Period</b></td>';
        template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + qualitydashboardCtrl.ReportingPeriod + '</td></tr>';
        //Displaying the Group/Individual name based on the entity type selected
        if (qualitydashboardCtrl.type == "Group") {
            template += '<tr><td style="font-size:14px; border: 1px solid black"><b>Group Name - Group TIN</b></td>';
            template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + qualitydashboardCtrl.ReportingEntityName;
            template += ' - ' + qualitydashboardCtrl.TIN + '</td></tr>';
        }
        else {
            template += '<tr><td style="font-size:14px; border: 1px solid black"><b>Individual Clinician Name - NPI -TIN</b></td>';
            template += '<td colspan="7" style="font-size:14px; border: 1px solid black; text-align: left">' + qualitydashboardCtrl.ReportingEntityName;
            template += ' - ' + qualitydashboardCtrl.NPI + ' - ' + qualitydashboardCtrl.TIN + '</td></tr>';
        }

        //Measure Type Heading based on the measure type selected
        template += '<thead><tr><td colspan="8" style="text-align:center; background-color:#4472c4;color:white;font-size:14px; border: 1px solid black">';
        if (qualitydashboardCtrl.MeasureType == "Monitoring")
            template += 'Monitoring Measures</td></tr></thead>';
        else
            template += 'Reporting Measures</td></tr></thead>';

        //Displaying the measures (Monitoring/Reporting) data 
        template += '<colgroups>';
        qualitydashboardCtrl.exportedColumns.forEach(function (col) {
            template += '<col style="width:auto"></col>';
        });
        template += '<thead><tr>';
        qualitydashboardCtrl.exportedColumns.forEach(function (col) {
            template += '<th style="background-color: #ed7d31; color:white; border: 1px solid black; font-size: 14px">' + col.name + '</th>';
        });
        template += '</tr></thead> <tbody>';
        qualitydashboardCtrl.exportedData.forEach(function (d) {
            template += '<tr>';
            qualitydashboardCtrl.exportedColumns.forEach(function (col) {
                var value = d[col.columnid];
                template += '<td style="border: 1px solid black;">';
                template += value != null ? value.toString() : "";
                template += '</td>';
            });
            template += '</tr></tbody>';
        });
        //Displaying the calculation scores only if the measure type is Reporting
        if (qualitydashboardCtrl.MeasureType == "Reporting") {
            template += '<thead><tr><td style="font-size:14px;"><b>Total Achievment Points = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.totalAchievmentPts + '</td></tr>';
            template += '<tr><td style="font-size:14px;"><b>Outcome And High Priority Bonus = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.OutComeAndHighpriorityBonus + '</td></tr>';
            template += '<tr><td style="font-size:14px;"><b>CEHRT Reporting Bonus = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.cehrtBonusPoints + '</td></tr>';
            template += '<tr><td style="font-size:14px;"><b>Total Quality Points Earned = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.totalQtyPtsEarned + '</td></tr>';
            template += '<tr><td style="font-size:14px;"><b>Improvement Score = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.ImprovementScore + '</td></tr>';
            template += '<tr><td style="font-size:14px;"><b>Quality Performance Score = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.QualityPerformanceScore + '</td></tr>';
            template += '<tr><td style="font-size:14px;"><b>MIPS Quality Score = </b></td>';
            template += '<td style="font-size:14px; text-align:left">' + qualitydashboardCtrl.MIPSQualityScore + '</td></tr></thead>';
        }
        template += '</table></body></html>';
        return template;
    }

    qualitydashboardCtrl.Print = function () {
        //Displaying the Group/Individual name based on the entity type selected
        if (qualitydashboardCtrl.type == "Group") {
            qualitydashboardCtrl.selectedEntityName = qualitydashboardCtrl.ReportingEntityName + " - " + qualitydashboardCtrl.TIN;
        }
        else {
            qualitydashboardCtrl.selectedEntityName = qualitydashboardCtrl.ReportingEntityName + " - " + qualitydashboardCtrl.NPI + " - " + qualitydashboardCtrl.TIN;
        }
        //getting the practice logo path
        practiceInfoService.GetPracticeLogoPath().then(function (result) {
            if (result != null && result != "") {
                qualitydashboardCtrl.PracticeLogoPath = result.data;
            }
        });

        //getting the user and practice details
        var promises = [
        practiceLevelService.getLoggedInUserDetails(),
        practiceLevelService.getPracticeDetails(),
        ];

        $q.all(promises).then(function (data) {
            // if the data for fetching the username and practice details is null or empty then show the message as unable to export.
            if ((data != null && data != "") && (data[0].data != null && data[0].data != "") && (data[1].data != null && data[1].data != "")) {
                qualitydashboardCtrl.UserName = data[0].data.userName;
                qualitydashboardCtrl.PracticeDetails = data[1].data != null ? data[1].data : [];
                qualitydashboardCtrl.printDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm:ss"); //formatting the current date time
                window.setTimeout(function () {
                    var printContents = $("#printQualityMeasureGrid").html();
                    var popupWin = window.open('', '_blank', 'width=1320,height=650');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css"/></head><body onload="window.print(); self.close();">' + printContents + '</body></html>');
                    popupWin.document.close();
                }, 1);
            }
            else {
                toastr.error("Unable to print. Please try again.");
            }
        }, function (error) {
            toastr.error("Unable to print. Please try again.");
        });
    };

    qualitydashboardCtrl.init();
    qualitydashboardCtrl.setAchivementPonints = function (qultyMeaure) {
        var point = null;
        if (qualitydashboardCtrl.QualityMeasureDecline != null) {
            if (qultyMeaure.performancePercentage > 0 && qultyMeaure.performancePercentage <= 100) {
                var selectedMeasure = $filter('filter')(qualitydashboardCtrl.QualityMeasureDecline, { pqrs: qultyMeaure.qualityId }, true);
                var isDecile = false;
                var isQualityGoalpointCreated = false;
                for (var i = 0, len = selectedMeasure.length; i < len; i++) {
                    switch (qultyMeaure.qualityId) {
                        case "001": //CMS "122"
                        case "192": // CMS "132"
                        case "238": //CMS "156"
                            if (qultyMeaure.performancePercentage <= selectedMeasure[i].minRange && qultyMeaure.performancePercentage >= selectedMeasure[i].maxRange) {
                                point = qualitydashboardCtrl.setDecile(qultyMeaure, selectedMeasure[i]);
                                isQualityGoalpointCreated = true;
                            }
                            break;
                        default:
                            if (qultyMeaure.performancePercentage >= selectedMeasure[i].minRange && qultyMeaure.performancePercentage <= selectedMeasure[i].maxRange) {
                                point = qualitydashboardCtrl.setDecile(qultyMeaure, selectedMeasure[i]);
                                isQualityGoalpointCreated = true;
                            }
                            break;
                    }
                    if (isQualityGoalpointCreated) {
                        break;
                    }
                }
                if (!isQualityGoalpointCreated && selectedMeasure.length > 0) {
                    point = 3;
                }
            }
        }
        qultyMeaure.achievementPoints = point;
    }
    qualitydashboardCtrl.setDecile = function (qultyMeaure, item) {
        var qualityGoalPoints = Math.abs(item.decileValue + ((qultyMeaure.performancePercentage - item.minRange) / (item.maxRange - item.minRange)))
        if (qualityGoalPoints > 10)
            qualityGoalPoints = 10;

        return $filter('number')(qualityGoalPoints, 2);
    }
};