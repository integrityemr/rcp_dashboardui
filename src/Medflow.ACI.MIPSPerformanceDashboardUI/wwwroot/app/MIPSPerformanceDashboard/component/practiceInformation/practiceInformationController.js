﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('practiceInformationController', practiceInformationController);
practiceInformationController.$inject = ['$state', '$scope', 'uiGridConstants', 'practiceInfoService', '$q', 'enums', 'regexPracticInfo', '$filter'];
function practiceInformationController($state, $scope, uiGridConstants, practiceInfoService, $q, enums, regexPracticInfo, $filter) {
    var practiceInfo = this;
    practiceInfo.groupBillingId = null;
    practiceInfo.groupName = "";
    practiceInfo.groupTIN = "";
    practiceInfo.attributesList = [];
    practiceInfo.selectedAttributesToList = [];
    practiceInfo.selectedAttributes = [];
    practiceInfo.isGroup = true;
    practiceInfo.nonParticipants = [];
    practiceInfo.newGroupInfo = [];
    practiceInfo.disableArrowButton = true;
    practiceInfo.isGroupEdit = false;
    practiceInfo.isDataModified = true;
    practiceInfo.newGroupInfo = [];
    practiceInfo.IsGroupNameNotValid = false;
    practiceInfo.IsGroupTINNotValid = false;
    practiceInfo.AuditInfo = [];
    practiceInfo.AuditInfoIndividual = [];

    practiceInfo.settings = {
        showCheckAll: false,
        showUncheckAll: false
    };

    practiceInfo.loadInit = function () {
        if (localStorage.getItem('emergencyAccess') === "true") {
            practiceInfo.init();
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role != "ADMINISTRATOR") {
                        toastr.error('You are not authorized.');
                        $state.go('Home');
                    }
                    else {
                        practiceInfo.init();
                    }
                }

            });
        }
         
    }
    practiceInfo.init = function () {

        practiceInfo.attributes = [];
        practiceInfo.clinicians = [];

        practiceInfo.groupBillingList = [];
        practiceInfo.groupBillingListCopy = [];
        practiceInfo.attributesList = [];

        practiceInfo.cliniciansGridDef();
        //get all default data
        var promises = [practiceInfoService.getAttributes(), practiceInfoService.getClinicians(), practiceInfoService.getGroupBillingList(), practiceInfoService.createAuditLog()];

        $q.all(promises).then(function (data) {
            practiceInfo.attributes = data[0].data;
            practiceInfo.clinicians = data[1].data;
            practiceInfo.cliniciansGrid.data = practiceInfo.clinicians;
            practiceInfo.groupBillingList = data[2].data;
            practiceInfo.groupBillingListCopy = angular.copy(practiceInfo.groupBillingList);

            angular.forEach(practiceInfo.attributes, function (value, index) {
                practiceInfo.attributesList.push({ id: value.id, label: value.name });
            });

        }, function (error) {
            toastr.error("unable to load practice information");
        });


    }

    practiceInfo.cliniciansGridDef = function () { // Clinicians grid
        practiceInfo.cliniciansGrid = {
            enableRowSelection: true,
            enableColumnMenus: false,
            enableRowHeaderSelection: false,
            gridMenuShowHideColumns: false,
            multiSelect: true,
            modifierKeysToMultiSelect: false,
            noUnselect: false,

        };

        practiceInfo.cliniciansGrid.onRegisterApi = function (gridApi) {
            practiceInfo.gridApi = gridApi;
            practiceInfo.cliniciansGrid.gridApi = gridApi;
            practiceInfo.cliniciansGrid.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                practiceInfo.validateData();
            });
        }
        practiceInfo.cliniciansGrid.columnDefs =
            [
               //{ name: 'npi', displayName: "", visible: false, enableHiding: false, enableSorting: true },
               { name: 'clinician', displayName: "Clinician", enableHiding: false },
               { name: 'npi', displayName: "NPI", enableHiding: false },
               { name: 'tin', displayName: "TIN", enableHiding: false }
            ];
        practiceInfo.cliniciansGrid.data = [];
    }

    practiceInfo.redirectToIndividual = function () {
        practiceInfo.isGroup = false;
        practiceInfo.showIndividualBillingList();
        practiceInfo.disableArrowButton = true;
        practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
        practiceInfo.groupName = "";
        practiceInfo.groupTIN = "";
        practiceInfo.selectedAttributesToList = [];
    }
    practiceInfo.redirectToGroup = function () {
        practiceInfo.isGroup = true;
        practiceInfo.showGroupBillingList();
        practiceInfo.disableArrowButton = true;
        practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
        practiceInfo.groupName = "";
        practiceInfo.groupTIN = "";
        practiceInfo.selectedAttributesToList = [];
    }

    practiceInfo.showIndividualBillingList = function () {
        practiceInfoService.getIndividualBillingList().then(function (data) {
            practiceInfo.individualBillingList = data.data;
            practiceInfo.individualBillingListCopy = angular.copy(data.data);
        }, function (error) {
            toastr.error("unable to load Individual Billing List");
        });
    }

    practiceInfo.showGroupBillingList = function () {
        practiceInfoService.getGroupBillingList().then(function (data) {
            practiceInfo.groupBillingList = data.data;
        }, function (error) {
            toastr.error("unable to load Group Billing List");
        });
    }

    practiceInfo.moveGroupInfo = function () {
        practiceInfo.selectedRowsFromCliniciansList = practiceInfo.cliniciansGrid.gridApi.selection.getSelectedRows();
        practiceInfo.selectedAttributes = [];
        angular.forEach(practiceInfo.selectedAttributesToList, function (value, index) {
            var attributeInfo = [];
            angular.forEach(practiceInfo.attributes, function (value1, index1) {
                if (value1.id === value.id) {
                    attributeInfo.push(practiceInfo.attributes[index1]);
                }
            });
            practiceInfo.selectedAttributes.push({ id: attributeInfo[0].id, name: attributeInfo[0].name });
        });
        //practiceInfo.isDataModified = false;
        if (practiceInfo.isGroup) {
            if (practiceInfo.groupTIN.length < 9) {
                toastr.error("Invalid Tin format. Please provide the tin in XXXXXXXXX format");
            }
            else if (/^[\d]{3}-?[\d]{2}-?[\d]{4}$/.test(practiceInfo.groupTIN) === false) {
                toastr.error("Invalid Tin format. Please provide the tin in XXXXXXXXX format");
            }
            else if (practiceInfo.selectedRowsFromCliniciansList.length < 2) {
                toastr.error("Minimum two clinicians are required in a group");
            }
            else {
                practiceInfo.isDataModified = false;
                if (practiceInfo.isGroupEdit) {
                    practiceInfo.AuditInfo.push(practiceInfo.groupName);
                    practiceInfo.groupBillingList[practiceInfo.selectedIndex].groupBillingId = practiceInfo.groupBillingId;
                    practiceInfo.groupBillingList[practiceInfo.selectedIndex].groupName = practiceInfo.groupName;
                    practiceInfo.groupBillingList[practiceInfo.selectedIndex].tin = practiceInfo.groupTIN;
                    practiceInfo.groupBillingList[practiceInfo.selectedIndex].attributes = practiceInfo.selectedAttributes;
                    //adding clinicians to the list
                    practiceInfo.groupBillingList[practiceInfo.selectedIndex].participants = practiceInfo.selectedRowsFromCliniciansList;

                    practiceInfo.groupBillingList[practiceInfo.selectedIndex].nonParticipants = [];
                    angular.forEach(practiceInfo.clinicians, function (value, index) {
                        var count = 0;
                        angular.forEach(practiceInfo.selectedRowsFromCliniciansList, function (value1, index1) {
                            if (value.npi != value1.npi && value.tin != value1.tin) {
                                //practiceInfo.groupBillingList[practiceInfo.selectedIndex].nonParticipants.push(value);
                                count = count + 1;
                            }
                        });
                        if (count === practiceInfo.selectedRowsFromCliniciansList.length) {
                            practiceInfo.groupBillingList[practiceInfo.selectedIndex].nonParticipants.push(value);
                        }
                    });
                    practiceInfo.disableArrowButton = true;
                    practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
                    practiceInfo.groupName = "";
                    practiceInfo.groupTIN = "";
                    practiceInfo.selectedAttributesToList = [];
                    practiceInfo.selectedAttributes = [];
                    practiceInfo.isGroupEdit = false;
                }
                else {
                    practiceInfo.AuditInfo.push(practiceInfo.groupName);
                    practiceInfo.nonParticipants = [];
                    angular.forEach(practiceInfo.clinicians, function (value, index) {
                        var count = 0;
                        angular.forEach(practiceInfo.selectedRowsFromCliniciansList, function (value1, index1) {
                            if (value.npi != value1.npi) {
                                count = count + 1;
                            }
                        });
                        if (count === practiceInfo.selectedRowsFromCliniciansList.length) {
                            practiceInfo.nonParticipants.push(value);
                        }
                    });

                    practiceInfo.newGroupInfo.unshift({
                        groupBillingId: '0', groupName: practiceInfo.groupName,
                        tin: practiceInfo.groupTIN, attributes: practiceInfo.selectedAttributes,
                        participants: practiceInfo.selectedRowsFromCliniciansList,
                        nonParticipants: practiceInfo.nonParticipants
                    });
                    practiceInfo.groupBillingList.unshift(practiceInfo.newGroupInfo[0]);
                    practiceInfo.newGroupInfo = [];
                    practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
                    practiceInfo.disableArrowButton = true;
                    practiceInfo.groupName = "";
                    practiceInfo.groupTIN = "";
                    practiceInfo.selectedAttributesToList = [];
                }
            }

        }
        else {
            practiceInfo.isDataModified = false;
            if (practiceInfo.isIndividualEdit) {
                practiceInfo.individualBillingList.cliniciansBillingIndipendently[practiceInfo.selectedIndex].attributesList = practiceInfo.selectedAttributes;
                practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
                practiceInfo.disableArrowButton = true;
                practiceInfo.selectedAttributesToList = [];
                angular.forEach(practiceInfo.individualBillingList.cliniciansBillingIndipendently, function (value, index) {
                    practiceInfo.AuditInfoIndividual.push(value.clinician + "-" + value.id);
                });
            }
            else {
                var tinCliniciansList = [];
                var errorMessage = "";
                angular.forEach(practiceInfo.selectedRowsFromCliniciansList, function (value, index) {
                    if (value.tin !== null && value.tin !== '') {
                        tinCliniciansList.push(value);
                    }
                    else {
                        errorMessage = "Unable to add clinicians without TIN to the billing list";
                    }
                });
                if (errorMessage !== "") {
                    toastr.error(errorMessage);
                    if (practiceInfo.selectedRowsFromCliniciansList.length === 1) {
                        practiceInfo.isDataModified = true;
                    }
                }
                angular.forEach(tinCliniciansList, function (value, index) {
                    angular.forEach(practiceInfo.individualBillingList.cliniciansBillingIndipendently, function (value2, index2) {
                        if (value2.npi === value.npi && value2.tin === value.tin) {
                            value2.attributesList = practiceInfo.selectedAttributes;
                        }
                    });
                });
                practiceInfo.newIndividualCliniciansList = [];
                angular.forEach(tinCliniciansList, function (value, index) {
                    var count = 0;
                    angular.forEach(practiceInfo.individualBillingList.cliniciansBillingIndipendently, function (value1, index1) {
                        if (value.npi === value1.npi && value.tin === value1.tin) {
                            count = count + 1;
                        }
                    });
                    if (count === 0) {
                        practiceInfo.newIndividualCliniciansList.push(value);
                    }
                });

                angular.forEach(practiceInfo.newIndividualCliniciansList, function (value, index) {
                    value.attributesList = practiceInfo.selectedAttributes;
                    practiceInfo.individualBillingList.cliniciansBillingIndipendently.push(value);
                });

                practiceInfo.individualBillingList.cliniciansNotBillingIndipendently = [];
                angular.forEach(practiceInfo.clinicians, function (value, index) {
                    var count = 0;
                    angular.forEach(practiceInfo.individualBillingList.cliniciansBillingIndipendently, function (value1, index1) {
                        if (value.npi !== value1.npi && value.tin !== value1.tin) {
                            count = count + 1;
                        }
                    });
                    if (count === practiceInfo.individualBillingList.cliniciansBillingIndipendently.length) {
                        practiceInfo.individualBillingList.cliniciansNotBillingIndipendently.push(value);
                    }
                });

                angular.forEach(tinCliniciansList, function (value, index) {
                    practiceInfo.AuditInfoIndividual.push(value.clinician+"-"+value.id);
                });
                practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
                practiceInfo.disableArrowButton = true;
                practiceInfo.selectedAttributesToList = [];
                practiceInfo.selectedAttributes = [];
            }
        }

    }

    practiceInfo.saveBillingInfo = function () {
        practiceInfo.isDataModified = true;
        if (practiceInfo.isGroup) {
            var data = practiceInfo.groupBillingList;
            if (practiceInfo.AuditInfo.length <= 1) {
                practiceInfo.groupBillingList[0].AuditInfo = practiceInfo.AuditInfo.join();
            } else {
                practiceInfo.groupBillingList[0].AuditInfo = practiceInfo.AuditInfo.slice(0, -1).join(", ") + " and " + practiceInfo.AuditInfo[practiceInfo.AuditInfo.length - 1];
            }
            data.AuditInfo = practiceInfo.groupBillingList[0].AuditInfo;
            practiceInfoService.saveGroupReporting(data).then(function (res) {
                if (res) {
                    toastr.success("Group configurations saved successfully.");
                    practiceInfo.init();
                    practiceInfo.showGroupBillingList();
                    practiceInfo.cliniciansGrid.gridApi = practiceInfo.gridApi;
                    practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
                    practiceInfo.groupName = "";
                    practiceInfo.groupTIN = "";
                    practiceInfo.selectedAttributesToList = [];
                    practiceInfo.selectedAttributes = [];
                    practiceInfo.disableArrowButton = true;
                    practiceInfo.isDataModified = true;
                    practiceInfo.AuditInfo = [];
                }
                else {
                    toastr.error("Group configurations not saved");
                }
            }, function (res) {
                toastr.error("unable to load save Group configurations");
            });
        }
        else {
            var data = practiceInfo.individualBillingList;
            if (practiceInfo.AuditInfoIndividual.length <= 1) {
                practiceInfo.individualBillingList.AuditInfoIndividual = practiceInfo.AuditInfoIndividual.join();
            } else {
                practiceInfo.individualBillingList.AuditInfoIndividual = practiceInfo.AuditInfoIndividual.slice(0, -1).join(", ") + " and " + practiceInfo.AuditInfoIndividual[practiceInfo.AuditInfoIndividual.length - 1];
            }
            data.AuditInfoIndividual = practiceInfo.individualBillingList.AuditInfoIndividual;
            practiceInfoService.saveIndividualReporting(data).then(function (res) {
                if (res) {
                    toastr.success("Individual configurations saved successfully.");
                    practiceInfo.init();
                    practiceInfo.showIndividualBillingList();
                    practiceInfo.cliniciansGrid.gridApi = practiceInfo.gridApi;
                    practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
                    practiceInfo.groupName = "";
                    practiceInfo.groupTIN = "";
                    practiceInfo.selectedAttributesToList = [];
                    practiceInfo.selectedAttributes = [];
                    practiceInfo.disableArrowButton = true;
                    practiceInfo.isDataModified = true;
                    practiceInfo.AuditInfoIndividual = [];
                    practiceInfo.individualBillingList.AuditInfoIndividual = "";

                }
                else {
                    toastr.error("Individual configurations not saved");
                }
            }, function (res) {
                toastr.error("unable to load save Individual configurations");
            });
        }
    }

    practiceInfo.dropSuccessHandler = function ($event, index, array, arrayname) {
        practiceInfo.isDataModified = false;
        if (arrayname === 'participantArray') {
            if (practiceInfo.groupBillingList[practiceInfo.selectedIndex].participants.length === 2) {
                toastr.error("Minimum two clinicians are required in the participants list.");
            }
            else {
                practiceInfo.groupBillingList[practiceInfo.selectedIndex].participants.splice(index, 1);
                practiceInfo.groupBillingList[practiceInfo.selectedIndex].nonParticipants.push(array);
            }
        }
        else {
            practiceInfo.groupBillingList[practiceInfo.selectedIndex].nonParticipants.splice(index, 1);
            practiceInfo.groupBillingList[practiceInfo.selectedIndex].participants.push(array);
        }
    };

    practiceInfo.dropSuccessHandlerIndividual = function ($event, index, array, arrayname) {
        practiceInfo.isDataModified = false;
        if (arrayname === 'participantArray') {
            practiceInfo.individualBillingList.cliniciansBillingIndipendently.splice(index, 1);
            array.attributesList = [];
            practiceInfo.individualBillingList.cliniciansNotBillingIndipendently.push(array);
            practiceInfo.AuditInfoIndividual.push(array.clinician);

        }
        else {
            if (array.tin === null || array.tin === "") {
                toastr.error("Select clinician with TIN");
            }
            else {
                practiceInfo.selectedAttributes = [];
                angular.forEach(practiceInfo.selectedAttributesToList, function (value, index) {
                    var attributeInfo = $filter("filter")(practiceInfo.attributes, { id: value.id });
                    practiceInfo.selectedAttributes.push({ id: attributeInfo[0].id, name: attributeInfo[0].name });
                });
                practiceInfo.individualBillingList.cliniciansNotBillingIndipendently.splice(index, 1);
                array.attributesList = practiceInfo.selectedAttributes;
                practiceInfo.individualBillingList.cliniciansBillingIndipendently.push(array);
                practiceInfo.AuditInfoIndividual.push(array.clinician);
            }
        }
    };

    practiceInfo.getIndex = function (index, event) {
        practiceInfo.selectedIndex = index;
        event.preventDefault();
    }

    practiceInfo.isGroupNameValid = function ($event) {
        if (practiceInfo.groupName != undefined) {
            if (!practiceInfo.isGroupEdit) {
                practiceInfo.groupBillingListValidateCopy = practiceInfo.groupBillingList;
            }
            var GroupData = $filter('filter')(practiceInfo.groupBillingListValidateCopy,
                function (group) {
                    var oldValue = $filter('uppercase')(group.groupName);
                    var newValue = $filter('uppercase')(practiceInfo.groupName);
                    return oldValue == newValue;
                }, true);
            if (GroupData.length === 0) {
                practiceInfo.IsGroupNameNotValid = false;
                practiceInfo.validateData();
            }
            else {
                toastr.error("Group name already exists");
                practiceInfo.IsGroupNameNotValid = true;
                practiceInfo.disableArrowButton = true;
                //$event.preventDefault();
                //$event.stopPropagation();
                //angular.element(document.getElementById("groupNameTxt")).focus();
                //return false;
            }
        }
        else {
            practiceInfo.IsGroupNameNotValid = false;
            practiceInfo.validateData();
        }
    }
    practiceInfo.isGroupTINValid = function ($event) {
        if (practiceInfo.groupTIN != undefined) {
            if (!practiceInfo.isGroupEdit) {
                practiceInfo.groupBillingListValidateCopy = practiceInfo.groupBillingList;
            }
            var GroupData = $filter('filter')(practiceInfo.groupBillingListValidateCopy, { tin: practiceInfo.groupTIN }, true);
            if (GroupData.length === 0) {
                practiceInfo.IsGroupTINNotValid = false;
                practiceInfo.validateData();
            }
            else {
                toastr.error("Group TIN already exists");
                practiceInfo.disableArrowButton = true;
                practiceInfo.IsGroupTINNotValid = true;
                //$event.preventDefault();
                //$event.stopPropagation();
                //angular.element(document.getElementById("groupTINTxt")).focus();
            }
        }
        else {
            practiceInfo.IsGroupTINNotValid = false;
            practiceInfo.validateData();
        }
    }

    practiceInfo.setgroupTIN = function (event) {
        if (event.which != 8) {
            if (practiceInfo.groupTIN != undefined && practiceInfo.groupTIN.length != 0) {
                var enteredCharacter = event.key;
                if (enteredCharacter != "-") {
                    if (practiceInfo.groupTIN.length === 3) {
                        practiceInfo.groupTIN = practiceInfo.groupTIN + '-';
                    }
                    else if (practiceInfo.groupTIN.length === 6) {
                        practiceInfo.groupTIN = practiceInfo.groupTIN + '-';
                    }
                }
                else if (enteredCharacter === "-") {
                    practiceInfo.groupTIN = practiceInfo.groupTIN.slice(0, -1);
                }
            }
        }
    }

    practiceInfo.validateData = function () {
        if (practiceInfo.isGroup) {
            // Group 
            //practiceInfo.IsGroupNameNotValid = false;
            if (practiceInfo.cliniciansGrid.gridApi.selection.getSelectedRows().length != 0 && practiceInfo.groupName != undefined
                && practiceInfo.groupName != "" && practiceInfo.groupTIN != undefined && practiceInfo.groupTIN != ""
                && !practiceInfo.IsGroupNameNotValid && !practiceInfo.IsGroupTINNotValid) {
                practiceInfo.disableArrowButton = false;
            }
            else {
                practiceInfo.disableArrowButton = true;
            }
        }
        else {
            // Individual
            if (practiceInfo.cliniciansGrid.gridApi.selection.getSelectedRows().length != 0) {
                practiceInfo.disableArrowButton = false;
                practiceInfo.isIndividualEdit = false;
            }
            else {
                practiceInfo.disableArrowButton = true;
            }
        }
    }

    practiceInfo.selectAllRows = function () {
        practiceInfo.cliniciansGrid.gridApi.selection.selectAllRows();
        practiceInfo.validateData();
        practiceInfo.isIndividualEdit = false;
    }

    practiceInfo.clearData = function () {
        practiceInfo.cliniciansGrid.gridApi = practiceInfo.gridApi;
        practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
        practiceInfo.groupName = "";
        practiceInfo.groupTIN = "";
        practiceInfo.selectedAttributesToList = [];
        practiceInfo.selectedAttributes = [];
        practiceInfo.disableArrowButton = true;
        practiceInfo.groupBillingList = angular.copy(practiceInfo.groupBillingListCopy);
        practiceInfo.individualBillingList = angular.copy(practiceInfo.individualBillingListCopy);
        practiceInfo.isDataModified = true;
        practiceInfo.isGroupEdit = false;
        practiceInfo.IsGroupNameNotValid = false;
        practiceInfo.IsGroupTINNotValid = false;
        practiceInfo.AuditInfo = [];
        practiceInfo.AuditInfoIndividual = [];


    }

    practiceInfo.editGroup = function (index) {
        practiceInfo.cliniciansGrid.gridApi = practiceInfo.gridApi;
        practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
        practiceInfo.disableArrowButton = false;
        practiceInfo.groupName = "";
        practiceInfo.groupTIN = "";
        practiceInfo.selectedAttributesToList = [];
        practiceInfo.selectedAttributes = [];

        practiceInfo.isGroupEdit = true;
        practiceInfo.selectedIndex = index;
        practiceInfo.cliniciansGrid.gridApi = practiceInfo.gridApi;
        practiceInfo.groupBillingId = practiceInfo.groupBillingList[index].groupBillingId;
        practiceInfo.groupName = practiceInfo.groupBillingList[index].groupName;
        practiceInfo.groupTIN = practiceInfo.groupBillingList[index].tin;
        practiceInfo.groupBillingListValidateCopy = angular.copy(practiceInfo.groupBillingList);
        practiceInfo.groupBillingListValidateCopy.splice(index, 1);

        angular.forEach(practiceInfo.groupBillingList[index].attributes, function (value, index) {
            practiceInfo.selectedAttributesToList.push({ id: value.id });
        });
        angular.forEach(practiceInfo.groupBillingList[index].participants, function (value, index) {

            angular.forEach(practiceInfo.cliniciansGrid.data, function (value1, index1) {
                if (value.npi === value1.npi && value.tin === value1.tin) {
                    practiceInfo.cliniciansGrid.gridApi.selection.selectRow(value1);
                }
            })
        });
    }

    practiceInfo.editIndividual = function (index) {
        practiceInfo.disableArrowButton = true;
        practiceInfo.cliniciansGrid.gridApi.selection.clearSelectedRows();
        practiceInfo.selectedAttributesToList = [];

        practiceInfo.isIndividualEdit = true;
        practiceInfo.selectedIndex = index;

        angular.forEach(practiceInfo.individualBillingList.cliniciansBillingIndipendently[index].attributesList, function (value, index) {
            practiceInfo.selectedAttributesToList.push({ id: value.id });
        });
        angular.forEach(practiceInfo.cliniciansGrid.data, function (value, index) {
            if (practiceInfo.individualBillingList.cliniciansBillingIndipendently[practiceInfo.selectedIndex].npi === value.npi
                && practiceInfo.individualBillingList.cliniciansBillingIndipendently[practiceInfo.selectedIndex].tin === value.tin) {
                practiceInfo.cliniciansGrid.gridApi.selection.selectRow(value);
            }
        })
    }

    practiceInfo.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}