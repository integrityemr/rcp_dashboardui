﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('qppExportController', qppExportController);
qppExportController.$inject = ['$state', '$scope', '$filter', 'constants', '$q', 'cpiaService', 'aciService', '$window', 'QPPperformanceYearEnums'];
function qppExportController($state, $scope, $filter, constants, $q, cpiaService, aciService, $window, QPPperformanceYearEnums) {
    var qppExportCtrl = this;
    qppExportCtrl.Entitieslist = [];
    qppExportCtrl.Listitems = [];
    qppExportCtrl.Categorieslist = [];
    qppExportCtrl.selectedCategoryitems = [];
    qppExportCtrl.selecteditems = [];
    qppExportCtrl.DateTime = '';
    qppExportCtrl.PracticeUserName = '';
    qppExportCtrl.PerformanceYears = [];
    qppExportCtrl.ConfigStatus = [];

    qppExportCtrl.settings = {
        showCheckAll: true,
        showUncheckAll: true
    };

    //to display scrollable bar for the ReportingEntity (group/Individual names) dropdown list
    qppExportCtrl.reportingEntitySettings = {
        showCheckAll: true,
        showUncheckAll: true,
        scrollableheight: '400px',
        scrollable: true
    }

    qppExportCtrl.init = function () {

        qppExportCtrl.GetEntities();
        qppExportCtrl.GetCategories();
        qppExportCtrl.bindResults();
        qppExportCtrl.DateTime = (new Date().toLocaleString("en-US"))
        cpiaService.GetUserDetails().then(function (value) {
            qppExportCtrl.PracticeUserName = value.data.userName;
        })
        qppExportCtrl.getPerformanceYearList();
        var reqBody = {
            UserName: qppExportCtrl.PracticeUserName,
            ScreenName: "QPP Audit Log",
            ActionType: "Opend",
            Action:"Viewed the QPP Export Screen",
            CreatedDate: qppExportCtrl.DateTime
        }
        cpiaService.saveQppAuditLogData(reqBody).then(function (response) {
        });
    }

    // get the list of performance year from enums
    qppExportCtrl.getPerformanceYearList = function () {
        qppExportCtrl.PerformanceYears = QPPperformanceYearEnums.QppperformanceYearList;
        var currentYear = QPPperformanceYearEnums.QppperformanceYearList.filter(function (i) { return i.performanceYear == $filter('date')(new Date(), 'yyyy') })[0];
        if (currentYear != null)
            qppExportCtrl.selectedPerformanceYearId = currentYear;
        else
            qppExportCtrl.selectedPerformanceYearId = qppExportCtrl.PerformanceYears[0];
    }

    qppExportCtrl.GetEntities = function () {
        qppExportCtrl.entities = [];
        qppExportCtrl.entities.push({ code: 0, name: 'Select Entity' });
        cpiaService.GetEntities().then(function (response) {
            qppExportCtrl.Entitieslist = response.data;
            angular.forEach(qppExportCtrl.Entitieslist, function (value, key) {
                qppExportCtrl.entities.push({ code: value.code, name: value.name });
            });
            qppExportCtrl.code = qppExportCtrl.entities[0].code;
        }, function () {
            toastr.error("unable to load Entities");
        })
    }

    qppExportCtrl.onTypeChange = function (reportingType) {
        if (reportingType == 401) {
            qppExportCtrl.getGroups();
        }
        else {
            qppExportCtrl.GetClinicians()
        }
    }

    qppExportCtrl.getGroups = function () {
        debugger;
        cpiaService.GetGroupsQppExport().then(function (response) {
            debugger;
            qppExportCtrl.groups = response.data;
            qppExportCtrl.Listitems = [];
            qppExportCtrl.selecteditems = [];
            angular.forEach(qppExportCtrl.groups, function (value, index) {
                qppExportCtrl.Listitems.push({ id: value.id + '_' + value.tin, label: value.name });
            });
        }, function () {
            toastr.error("unable to load Groups");
        })
    }

    qppExportCtrl.GetClinicians = function () {
        cpiaService.GetCliniciansQppExport().then(function (response) {
            qppExportCtrl.clinicians = response.data;
            qppExportCtrl.Listitems = [];
            qppExportCtrl.selecteditems = [];
            angular.forEach(qppExportCtrl.clinicians, function (value, index) {
                //display independent and participant individuals. Appended type to the list.
                qppExportCtrl.Listitems.push({ id: value.id + '_' + value.physicianTIN + '_' + value.physicianNPI + '_' + value.type, label: value.name });
            });
        }, function () {
            toastr.error("unable to load clinicians");
        })

    }

    qppExportCtrl.GetCategories = function () {
        cpiaService.GetCategoriesList().then(function (response) {
            qppExportCtrl.Categories = response.data;
            qppExportCtrl.Categorieslist = [];
            qppExportCtrl.selectedCategoryitems = [];
            angular.forEach(qppExportCtrl.Categories, function (value, index) {
                qppExportCtrl.Categorieslist.push({ id: value.code, label: value.name });
            });
        }, function () {
            toastr.error("unable to load Categories");
        })
    }

    qppExportCtrl.Export = function () {
        // validations for all the dropdowns       
        if (qppExportCtrl.code == 0) {
            toastr.error("Please select Reporting Entity");
        }
        else if (qppExportCtrl.selecteditems == "") {
            toastr.error("Please select Reporting Entity Names");
        }
        else if (qppExportCtrl.selectedCategoryitems == "") {
            toastr.error("Please select Reporting Entity Categories");
        }
        else if (qppExportCtrl.selectedPerformanceYearId == "") {
            toastr.error("Please select Performance year");
        }
        else {
            qppExportCtrl.selecteditemsArray = [];
            qppExportCtrl.selectedcategoriesArray = [];
            qppExportCtrl.selIndependents = [];
            qppExportCtrl.selParticipants = [];
            qppExportCtrl.selReportingEntities = [];
            var selecteditemslistIds = qppExportCtrl.selecteditems;

            angular.forEach(selecteditemslistIds, function (item) {
                qppExportCtrl.selecteditemsArray.push(item.id);
            });
            qppExportCtrl.selecteditemsString = qppExportCtrl.selecteditemsArray.join(",");

            var selectedCatCodes = qppExportCtrl.selectedCategoryitems;
            angular.forEach(selectedCatCodes, function (item) {
                qppExportCtrl.selectedcategoriesArray.push(item.id);
            });
            qppExportCtrl.selectedCategoriesString = qppExportCtrl.selectedcategoriesArray.join(",");

            //separating the participants and independent objects.
            angular.forEach(qppExportCtrl.selecteditemsArray, function (value, index) {
                if (value.split('_')[3] == 'Participant') {
                    qppExportCtrl.selParticipants.push(value.split('_')[2] + "_" + value.split('_')[1]);
                }
                else if (value.split('_')[3] == 'Independent') {
                    qppExportCtrl.selIndependents.push(value.split('_')[0] + "_" + value.split('_')[1] + "_" + value.split('_')[2]);
                }
            });
            qppExportCtrl.selParticipants = qppExportCtrl.selParticipants.join(',');
            qppExportCtrl.selIndependents = qppExportCtrl.selIndependents.join(',');
            //assigning selected group data if the independent and participant individual object is empty. (i.e. no individuals selected in UI)
            if ((qppExportCtrl.selIndependents == null || qppExportCtrl.selIndependents == "") && (qppExportCtrl.selParticipants == null || qppExportCtrl.selParticipants == "")) {
                qppExportCtrl.selReportingEntities = qppExportCtrl.selecteditemsString; //for group
            }
            else {
                qppExportCtrl.selReportingEntities = qppExportCtrl.selIndependents; //for Individual
            }

            var data = {
                ReportingEntityCode: qppExportCtrl.code,
                ReportingEntityNames: qppExportCtrl.selReportingEntities, //can be empty in case of only participant individuals are selected in UI
                ReportingCategoryCode: qppExportCtrl.selectedCategoriesString,
                PerformanceYear: qppExportCtrl.selectedPerformanceYearId.performanceYear,
                IsParticipant: qppExportCtrl.selParticipants == null || qppExportCtrl.selParticipants == undefined || qppExportCtrl.selParticipants == "" ? false : true,
                ReportingParticipants: qppExportCtrl.selParticipants //will have data only when participant individuals are selected in UI
            }
            // call api for getting status about configuration then based on status call the Export API
            cpiaService.GetConfigStatus(data).then(function (response) {
                qppExportCtrl.ConfigStatus = response.data;
                var result = $filter('filter')(qppExportCtrl.ConfigStatus, { isConfigured: false }, true); // get items with isConfigured equals to false
                if (result.length < 1) {
                    qppExportCtrl.GetQPPExportStatus(data);
                }
                else {
                    toastr.error("No Configurations done for selected items.");
                }
            }, function () {
                toastr.error("unable to get config status");
            })
        }
    }

    qppExportCtrl.bindResults = function () {
        cpiaService.GetExportresults().then(function (response) {
            qppExportCtrl.exportresults = response.data;
        }, function () {
            toastr.error("unable to load export results");
        })

    }

    qppExportCtrl.GetQPPExportStatus = function (dataObject) {
        // call Export API and get status if its exported or not. based on that show the success and error message 
        var data = {
            ReportingEntities: dataObject.ReportingEntityNames,
            ReportingCategory: dataObject.ReportingCategoryCode,
            ReportingEntityType: dataObject.ReportingEntityCode,
            PerformanceYear: dataObject.PerformanceYear,
            IsParticipant: dataObject.IsParticipant,
            ReportingParticipants: dataObject.ReportingParticipants
        }
        // call api for getting status about configuration then based on status call the Export API
        cpiaService.QppExport(data).then(function (res) {
            if (res.data != null) {
                angular.forEach(res.data, function (value, key) {
                    var categoryList = "";
                    var jsonData = angular.fromJson(value);
                    //getting the categories binded in the json and joining it using a comma separator
                    angular.forEach(jsonData.measurementSets, function (value, key) {
                        categoryList += value.category + ",";
                    })
                    categoryList = categoryList.replace(/,\s*$/, ""); //removing the last comma from the string
                    var dateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm:ss"); //formatting the current date time
                    //generating the file name for Group and Individual Entity types
                    var fileName = "";
                    if (angular.lowercase(jsonData.entityType) === "group") {
                        fileName = jsonData.taxpayerIdentificationNumber + '_' + categoryList + '_' + dateTime + '.json';
                    }
                    else if (angular.lowercase(jsonData.entityType) === "individual") {
                        fileName = jsonData.taxpayerIdentificationNumber + '_' + jsonData.nationalProviderIdentifier + '_' + categoryList + '_' + dateTime + '.json';
                    }
                    //to download the files in the browser
                    var data = "data:application/json;charset=utf-8," + value;
                    var fileDwnld = document.createElement('a');
                    fileDwnld.href = data;
                    fileDwnld.download = fileName;
                    document.body.appendChild(fileDwnld);
                    fileDwnld.click();
                    document.body.removeChild(fileDwnld);
                })
                qppExportCtrl.SaveExportStatus(data);
            }

        }, function () {
            toastr.error("unable to  export QPP json(s)");
        })
    }

    qppExportCtrl.clearFields = function () {
        qppExportCtrl.code = 0;
        qppExportCtrl.selecteditems = "";
        qppExportCtrl.selectedCategoryitems = "";
    }

    qppExportCtrl.SaveExportStatus = function (data) {
        cpiaService.QppExportStatusSave(data).then(function (response) {
            if (response.data) {
                qppExportCtrl.bindResults(); // bind the data to table after export status insert.

                var entityname = "";
                var reportEntityName = [];
                var reportCategoryName = [];
                angular.forEach(qppExportCtrl.entities, function (value, key) {
                    if (value.code == qppExportCtrl.code) {
                        entityname = value.name;
                    }
                });
                angular.forEach(qppExportCtrl.Listitems, function (value, key) {
                    for (var i = 0; i < qppExportCtrl.selecteditems.length; i++) {
                        if (qppExportCtrl.selecteditems[i].id == value.id) {
                            reportEntityName.push(value.label);
                        }
                    }
                })
                angular.forEach(qppExportCtrl.Categorieslist, function (value, key) {
                    for (var i = 0; i < qppExportCtrl.selectedCategoryitems.length; i++) {
                        if (qppExportCtrl.selectedCategoryitems[i].id == value.id) {
                            reportCategoryName.push(value.label);
                        }
                    }
                })
                entityname;
                reportEntityName = reportEntityName.join(",");
                reportCategoryName = reportCategoryName.join(",");
                qppExportCtrl.selectedPerformanceYearId.performanceYear;

                var reqBody = {
                    UserName: qppExportCtrl.PracticeUserName,
                    ScreenName: "QPP Audit Log",
                    ActionType: "Click",
                    Action: "Exported the QPP Details of Report Entity:" + entityname + "Reporting Name:" + reportEntityName + " Reporting Catagory:" + reportCategoryName + "Performance Year: " + qppExportCtrl.selectedPerformanceYearId.performanceYear + "",
                    CreatedDate: qppExportCtrl.DateTime
                }
                cpiaService.saveQppAuditLogData(reqBody).then(function (response) {
                });
                toastr.success("QPP Json(s) are exported successfully");

            }
            else {
                qppExportCtrl.bindResults(); 
                toastr.error("unable to Save export status");
            }

        }, function () {
            toastr.error("unable to Save export status");
        })
    }

    qppExportCtrl.init();
}