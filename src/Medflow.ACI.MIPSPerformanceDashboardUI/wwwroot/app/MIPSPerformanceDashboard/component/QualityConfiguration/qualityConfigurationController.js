﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('qualityConfigurationController', qualityConfigurationController);
qualityConfigurationController.$inject = ['$state', '$scope', '$filter', 'qualityConfigurationService', 'qualityConfigurationConstants', 'cpiaService', 'constants', '$q'];
function qualityConfigurationController($state, $scope, $filter, qualityConfigurationService, qualityConfigurationConstants, cpiaService, constants, $q) {
    var qualityConfig = this;
    qualityConfig.performanceYear = '';
    qualityConfig.reportingType = '';
    qualityConfig.reportingEntity = '';
    qualityConfig.MonitoringTypeId = '';
    qualityConfig.PerformanceYearStatic = [];
    qualityConfig.reportingTypeStatic = [];
    qualityConfig.reportingEntityStatic = [];
    qualityConfig.MonitoringTypeIdstatic = [];
    qualityConfig.Listitems = [];
    qualityConfig.selecteditems = [];
    qualityConfig.qualityConfigObj = [];
    qualityConfig.SaveQualityConfigObj = [];
    qualityConfig.QrdaYearPoints = 0;
    qualityConfig.QualityConfigRequestBodyObj = [];
    qualityConfig.QualityConfigGetDataObj = [];
    qualityConfig.SaveUpdateLabel = "Save";
    qualityConfig.QualityMeasureDecline = [];
    qualityConfig.selectedCopyEntities = [];
    qualityConfig.copyReportingType = '';
    qualityConfig.reportingEntitiesList = [];
    qualityConfig.getConfigMeasuresForEntities = [];
    qualityConfig.outComeMeasureCount = 0;
    qualityConfig.highPriorityMeasureCount = 0;
    qualityConfig.mandatoryMeasure = "No";
    qualityConfig.bonusMeasureScore = 0;

    //for multi select dropdown
    qualityConfig.settings = {
        showCheckAll: true,
        showUncheckAll: true,
        scrollable: true
    };

    qualityConfig.init = function () {
        qualityConfig.SaveUpdateLabel = "Save";
        qualityConfig.GetQualityConfigMasterData();

        qualityConfigurationService.getPerformancYear().then(function (result) {
            qualityConfig.PerformanceYearStatic.push({ "id": 0, "year": "select year" });
            angular.forEach(result.data, function (value, index) {
                qualityConfig.PerformanceYearStatic.push({ id: value.id,year: value.year });
            });
        });
        qualityConfigurationService.getreportType().then(function (result) {
            qualityConfig.reportingTypeStatic.push({ "id": 0, "name": "select Reporting" });
            angular.forEach(result.data, function (value, index) {
                qualityConfig.reportingTypeStatic.push({ id: value.id, name: value.name });
            });
        });
        qualityConfigurationService.GetMonitoringType().then(function (result) {
            qualityConfig.MonitoringTypeIdstatic.push({ "id": 0, "name": "select" });
            angular.forEach(result.data, function (value, index) {
                qualityConfig.MonitoringTypeIdstatic.push({ id: value.id, name: value.name });
            });
        });
        qualityConfig.performanceYear = 0;
        qualityConfig.reportingType = 0;
        qualityConfig.copyReportingType = 0;
        qualityConfigurationService.GetQualityMeasureDecline().then(function (result) {
            if (result.data != null) {
                qualityConfig.QualityMeasureDecline = result.data;
            }
        });
    }
    qualityConfig.GetQualityConfigMasterData = function () {
        qualityConfigurationService.GetQualityMeasureMasterData().then(function (result) {
            qualityConfig.qualityConfigObj = [];
            angular.forEach(result.data, function (value, key) {
                qualityConfig.qualityConfigObj.push({
                    Id:0,
                    cms: value.cms,
                    cqm: value.cqm,
                    highPriority: value.highPriority,
                    MeasureId: value.id,
                    inverseMeasure: value.inverseMeasure,
                    measureName: value.measureName,
                    measureType: value.measureType,
                    MonitoringTypeId: 0,
                    IndividualReportingId: "",
                    PerformanceGoal: 0,
                    qualityGoalpoints: "",
                    pqrs: value.pqrs,
                    practiceQRDAMeasureConfigId: value.practiceQRDAMeasureConfigId
                });
            });
        })
    }
    qualityConfig.clearMeasurMasterdata = function () {
        angular.forEach(qualityConfig.qualityConfigObj, function (value, key) {
            value['MonitoringTypeId'] = 0,
            value['PerformanceGoal'] = 0,
            value['qualityGoalpoints'] = ""           
        });
    }
    qualityConfig.OnTypeChange = function (value) {
        if (value == 2) {
            qualityConfig.getGroups();
        } if(value == 1) {
            qualityConfig.GetClinicians();
        }
    }
    qualityConfig.PerformanceGoalClick = function (qultyMeaure) {        
        if (!qultyMeaure.MeasureId > 0)
        {
            toastr.warning("Measure Id cannot be null or empty");
            return false;
        }
        qultyMeaure.qualityGoalpoints = null;
        if (qultyMeaure.PerformanceGoal > 0 && qultyMeaure.PerformanceGoal <= 100) {
            var selectedMeasure = $filter('filter')(qualityConfig.QualityMeasureDecline, { pqrs: qultyMeaure.pqrs }, true);
            var isDecile = false;
            var isQualityGoalpointCreated = false;
            angular.forEach(selectedMeasure, function (item, key) {
                var isDecile = false;
                switch (qultyMeaure.pqrs) {
                    case "001": //CMS "122"
                    case "192": // CMS "132"
                    case "238": //CMS "156"
                        if (qultyMeaure.PerformanceGoal <= item.minRange && qultyMeaure.PerformanceGoal >= item.maxRange) {
                            isDecile = true;
                        }
                        break;
                    default:
                        if (qultyMeaure.PerformanceGoal >= item.minRange && qultyMeaure.PerformanceGoal <= item.maxRange) {
                            isDecile = true;
                        }
                        break;

                }
                if (isDecile) {
                    qultyMeaure.qualityGoalpoints = qualityConfig.setDecile(qultyMeaure, item);
                    isQualityGoalpointCreated = true;
                }
            });
            if (!isQualityGoalpointCreated)
            {
                qultyMeaure.qualityGoalpoints = 3;
            }
        }
    }

    qualityConfig.setDecile = function (qultyMeaure, item) {
        var qualityGoalPoints = Math.abs(item.decileValue + ((qultyMeaure.PerformanceGoal - item.minRange) / (item.maxRange - item.minRange)))
        if (qualityGoalPoints > 10)
            qualityGoalPoints = 10;

        return $filter('number')(qualityGoalPoints, 2);
    }

    qualityConfig.GetQualityMeasures = function () {
        qualityConfig.QualityConfigGetDataObj = [];
        var reportingEntityGrouplId = "";
        var reportingEntityIndividualId = "";
        var tinValue = "";
        var npiValue = "";
        qualityConfig.performanceYear;
        qualityConfig.reportingType;
        if (qualityConfig.performanceYear != "" && qualityConfig.reportingEntity != "" && qualityConfig.reportingEntity != null) {           
            if (qualityConfig.reportingType == 1) {
                angular.forEach(qualityConfig.Listitems, function (value, index) {
                    if (qualityConfig.reportingEntity == value.id) {
                        reportingEntityIndividualId  = value.commonID;
                        tinValue = value.TIN;
                        npiValue = value.NPI;
                        if (value.type == "Independent") {
                            var reqBody = {
                                "PerformanceYearId": qualityConfig.performanceYear,
                                "ReportingTypeId": qualityConfig.reportingType,
                                "GroupReportingId": 0,
                                "IndividualReportingId": reportingEntityIndividualId,
                                "TIN": "",
                                "NPI": ""
                            }
                            qualityConfig.QualityConfigGetDataObj = qualityConfig.qualityConfigObj;
                            qualityConfigurationService.GetQualityMeasureData(reqBody).then(function (res) {
                                if (res.data.length) {
                                    qualityConfig.SaveUpdateLabel = "Update";
                                    qualityConfig.clearMeasurMasterdata();
                                    angular.forEach(qualityConfig.QualityConfigGetDataObj, function (value, index) {
                                        angular.forEach(res.data, function (data, index) {
                                            if (value.practiceQRDAMeasureConfigId == data.practiceQRDAMeasureConfigId) {
                                                if (data.monitoringTypeId == 1) {
                                                    if ($filter('lowercase')(value.measureType) == "outcome")
                                                        qualityConfig.outComeMeasureCount++
                                                    if (value.highPriority == true)
                                                        qualityConfig.highPriorityMeasureCount++;
                                                }
                                                value['Id'] = data.id
                                                value['MonitoringTypeId'] = data.monitoringTypeId
                                                value['PerformanceGoal'] = data.performanceGoal
                                                value['qualityGoalpoints'] = data.qualityGoalPoints
                                                qualityConfig.QrdaYearPoints = data.qrdaPriorYearBonusPoints
                                            }
                                            //assigning the data to new object which will be used in copy functionality.
                                            qualityConfig.getConfigMeasuresForEntities = res.data; 
                                        })
                                    })
                                    if (qualityConfig.outComeMeasureCount >= 1 || qualityConfig.highPriorityMeasureCount>=1)
                                        qualityConfig.mandatoryMeasure = "Yes";
                                    if (qualityConfig.outComeMeasureCount > 1)
                                        qualityConfig.bonusMeasureScore += (qualityConfig.outComeMeasureCount - 1) * 2;
                                    if (qualityConfig.highPriorityMeasureCount)
                                        qualityConfig.bonusMeasureScore += (qualityConfig.highPriorityMeasureCount - 1) * 1;
                                    if (qualityConfig.bonusMeasureScore > 6)
                                        qualityConfig.bonusMeasureScore = 6;
                                } else {
                                    qualityConfig.SaveUpdateLabel = "Save";
                                    qualityConfig.getConfigMeasuresForEntities = [];
                                    qualityConfig.clearMeasurMasterdata();
                                }
                            })
                        }
                        if (value.type == "Participant") {
                            var reqBody = {
                                "PerformanceYearId": qualityConfig.performanceYear,
                                "ReportingTypeId": qualityConfig.reportingType,
                                "GroupReportingId": 0,
                                "IndividualReportingId": 0,
                                "TIN": tinValue,
                                "NPI": npiValue
                            }
                            qualityConfig.QualityConfigGetDataObj = qualityConfig.qualityConfigObj;
                            qualityConfigurationService.GetQualityMeasureData(reqBody).then(function (res) {
                                if (res.data.length) {
                                    qualityConfig.SaveUpdateLabel = "Update";
                                    qualityConfig.clearMeasurMasterdata();
                                    angular.forEach(qualityConfig.QualityConfigGetDataObj, function (value, index) {
                                        angular.forEach(res.data, function (data, index) {
                                            if (value.MeasureId == data.practiceDestinationMapQRDAMeasureId) {
                                                //Rajesh                                                
                                                if (data.monitoringTypeId == 1) {
                                                    if ($filter('lowercase')(value.measureType) == "outcome")
                                                        qualityConfig.outComeMeasureCount++
                                                    if (value.highPriority == true)
                                                        qualityConfig.highPriorityMeasureCount++;
                                                }
                                                value['Id'] = data.id
                                                value['MonitoringTypeId'] = data.monitoringTypeId
                                                value['PerformanceGoal'] = data.performanceGoal
                                                value['qualityGoalpoints'] = data.qualityGoalPoints
                                                qualityConfig.QrdaYearPoints = data.qrdaPriorYearBonusPoints
                                            }
                                            //assigning the data to new object which will be used in copy functionality.
                                            qualityConfig.getConfigMeasuresForEntities = res.data;
                                        })
                                    })
                                    if (qualityConfig.outComeMeasureCount >= 1 || qualityConfig.highPriorityMeasureCount>=1)
                                        qualityConfig.mandatoryMeasure = "Yes";
                                    if (qualityConfig.outComeMeasureCount > 1)
                                        qualityConfig.bonusMeasureScore += (qualityConfig.outComeMeasureCount - 1) * 2;
                                    if (qualityConfig.highPriorityMeasureCount)
                                        qualityConfig.bonusMeasureScore += (qualityConfig.highPriorityMeasureCount - 1) * 1;
                                    if (qualityConfig.bonusMeasureScore > 6)
                                        qualityConfig.bonusMeasureScore = 6;
                                } else {
                                    qualityConfig.SaveUpdateLabel = "Save";
                                    qualityConfig.getConfigMeasuresForEntities = [];
                                    qualityConfig.clearMeasurMasterdata();
                                }
                            })
                        }
                    }
                })
            } else {
                angular.forEach(qualityConfig.Listitems, function (value, index) {
                    if (qualityConfig.reportingEntity == value.id) {
                        reportingEntityGrouplId = value.commonID;
                        var reqBody = {
                            "PerformanceYearId": qualityConfig.performanceYear,
                            "ReportingTypeId": qualityConfig.reportingType,
                            "GroupReportingId": reportingEntityGrouplId,
                            "IndividualReportingId": 0,
                            "TIN": "",
                            "NPI": ""
                        }
                        qualityConfig.QualityConfigGetDataObj = qualityConfig.qualityConfigObj;
                        qualityConfigurationService.GetQualityMeasureData(reqBody).then(function (res) {
                            if (res.data.length) {
                                qualityConfig.SaveUpdateLabel = "Update";
                                qualityConfig.clearMeasurMasterdata();
                                angular.forEach(qualityConfig.QualityConfigGetDataObj, function (value, index) {
                                    angular.forEach(res.data, function (data, index) {
                                        if (value.practiceQRDAMeasureConfigId == data.practiceQRDAMeasureConfigId) {
                                            //Rajesh
                                            if (data.monitoringTypeId == 1) {
                                                if ($filter('lowercase')(value.measureType) == "outcome")
                                                    qualityConfig.outComeMeasureCount++
                                                if (value.highPriority == true)
                                                    qualityConfig.highPriorityMeasureCount++;
                                            }
                                            value['Id'] = data.id
                                            value['MonitoringTypeId'] = data.monitoringTypeId
                                            value['PerformanceGoal'] = data.performanceGoal
                                            value['qualityGoalpoints'] = data.qualityGoalPoints
                                            qualityConfig.QrdaYearPoints = data.qrdaPriorYearBonusPoints
                                        }
                                        //assigning the data to new object which will be used in copy functionality.
                                        qualityConfig.getConfigMeasuresForEntities = res.data;
                                    })
                                })
                                if (qualityConfig.outComeMeasureCount >= 1 || qualityConfig.highPriorityMeasureCount>=1)
                                    qualityConfig.mandatoryMeasure = "Yes";
                                if (qualityConfig.outComeMeasureCount > 1)
                                    qualityConfig.bonusMeasureScore += (qualityConfig.outComeMeasureCount - 1) * 2;
                                if (qualityConfig.highPriorityMeasureCount)
                                    qualityConfig.bonusMeasureScore += (qualityConfig.highPriorityMeasureCount - 1) * 1;
                                if (qualityConfig.bonusMeasureScore > 6)
                                    qualityConfig.bonusMeasureScore = 6;
                               
                            } else {
                                qualityConfig.SaveUpdateLabel = "Save";
                                qualityConfig.getConfigMeasuresForEntities = [];
                                qualityConfig.clearMeasurMasterdata();
                            }
                        })
                    }
                })
            }
        } else {

        }
    }
    qualityConfig.getGroups = function () {
        cpiaService.GetGroupsQppExport().then(function (response) {
            qualityConfig.groups = response.data;
            //making reporting entity (listItems variable) null only on the page load
            if (qualityConfig.copyReportingType != null && qualityConfig.copyReportingType != '' && qualityConfig.copyReportingType != undefined) {
                qualityConfig.reportingEntitiesList = [];
                qualityConfig.selectedCopyEntities = [];
            }
            else {
                qualityConfig.Listitems = [];
            }
            qualityConfig.selecteditems = [];
            angular.forEach(qualityConfig.groups, function (value, index) {
                qualityConfig.Listitems.push({ id: value.id + '+' + value.name, label: value.name, TIN: value.tin, commonID: value.id });
                //for copy - reporting entity dropdown
                qualityConfig.reportingEntitiesList.push({ id: value.id + '_' + value.tin, label: value.name });
            });
        }, function () {
            toastr.error("unable to load Groups");
        })
    }
    qualityConfig.GetClinicians = function () {
        cpiaService.GetCliniciansQppExport().then(function (response) {
            qualityConfig.clinicians = response.data;
            //making reporting entity (listItems variable) null only on the page load
            if (qualityConfig.copyReportingType != null && qualityConfig.copyReportingType != '' && qualityConfig.copyReportingType != undefined) {
                qualityConfig.reportingEntitiesList = [];
                qualityConfig.selectedCopyEntities = [];
            }
            else {
                qualityConfig.Listitems = [];
            }
            qualityConfig.selecteditems = [];
            angular.forEach(qualityConfig.clinicians, function (value, index) {
                qualityConfig.Listitems.push({ id: value.id + '+' + value.name, commonID: value.id, TIN: value.physicianTIN, NPI: value.physicianNPI, label: value.name, type: value.type });
                //for copy - reporting entity dropdown
                qualityConfig.reportingEntitiesList.push({ id: value.id + '_' + value.physicianTIN + '_' + value.physicianNPI + '_' + value.type, label: value.name });
            });
        }, function () {
            toastr.error("unable to load clinicians");
        })
    }
    qualityConfig.Savedata = function () {
        if (qualityConfig.performanceYear != 0) {
            if (qualityConfig.reportingType != 0) {
                if (qualityConfig.reportingEntity != "") {
                    if (qualityConfig.reportingEntity != null) {

                        qualityConfig.SaveQualityConfigObj = [];
                        var ReportingtypeName = "";
                        var IndividualReportId = 0;
                        var GroupReportId = 0;
                        var NPIValue = null;
                        var TINValue = null;
                        if (qualityConfig.reportingType == 1) {
                            GroupReportId = 0;
                            angular.forEach(qualityConfig.Listitems, function (value, index) {
                                if (qualityConfig.reportingEntity == value.id) {
                                    IndividualReportId = value.commonID;
                                    ReportingtypeName = value.type;
                                    NPIValue = value.NPI;
                                    TINValue = value.TIN;
                                }
                            });
                        } else {
                            IndividualReportId = 0;
                            ReportingtypeName = "Group";
                            angular.forEach(qualityConfig.Listitems, function (value, index) {
                                if (qualityConfig.reportingEntity == value.id) {
                                    GroupReportId = value.commonID;
                                    NPIValue = null;
                                    TINValue = value.TIN;
                                }
                            })
                        }
                        if (qualityConfig.SaveUpdateLabel == "Save") {
                            angular.forEach(qualityConfig.qualityConfigObj, function (value, index) {
                                value['ReportingTypeId'] = qualityConfig.reportingType;
                                value['PerformanceYearId'] = qualityConfig.performanceYear;
                                value['QrdaYearBonusPoints'] = qualityConfig.QrdaYearPoints;
                                if (value.MonitoringTypeId != 0 && value.PerformanceGoal != 0) {
                                    qualityConfig.SaveQualityConfigObj.push({
                                        MeasureId: value.MeasureId,
                                        MonitoringTypeId: value.MonitoringTypeId,
                                        PerformanceGoal: value.PerformanceGoal,
                                        PerformanceYearId: value.PerformanceYearId,
                                        ReportingTypeId: value.ReportingTypeId,
                                        qualityGoalpoints: value.qualityGoalpoints,
                                        QrdaYearBonusPoints: value.QrdaYearBonusPoints,
                                        Type: ReportingtypeName,
                                        IndividualReportingId: IndividualReportId,
                                        GroupReportingId: GroupReportId,
                                        PracticeQRDAMeasureConfigId: value.practiceQRDAMeasureConfigId,
                                        NPI: NPIValue,
                                        TIN: TINValue
                                    });
                                }
                            });
                            qualityConfig.QualityConfigRequestBodyObj = [];
                            angular.forEach(qualityConfig.SaveQualityConfigObj, function (value, index) {
                                var reqBodyObj =
                                  {
                                      "Id": 0,
                                      "PracticeDestinationMapQRDAMeasureId": value.MeasureId,
                                      "PracticeQRDAMeasureConfigId": value.PracticeQRDAMeasureConfigId,
                                      "ReportingTypeId": value.ReportingTypeId,
                                      "PerformanceYearId": value.PerformanceYearId,
                                      "IndividualReportingId": value.IndividualReportingId,
                                      "GroupReportingId": value.GroupReportingId,
                                      "NPI": value.NPI,
                                      "TIN": value.TIN,
                                      "PerformanceGoal": value.PerformanceGoal,
                                      "QualityGoalPoints": value.qualityGoalpoints,
                                      "QRDAPriorYearBonusPoints": value.QrdaYearBonusPoints,
                                      "Type": value.Type,
                                      "MonitoringTypeId": value.MonitoringTypeId
                                  }
                                qualityConfig.QualityConfigRequestBodyObj.push(reqBodyObj)
                            })
                            if (qualityConfig.QualityConfigRequestBodyObj.length) {
                                qualityConfigurationService.SaveQualityMeasureData(qualityConfig.QualityConfigRequestBodyObj).then(function (res) {
                                    if (res.data == true) {
                                        toastr.success("Quality Measures Saved Successfully");
                                        qualityConfig.clear();
                                    }
                                })
                            } else {
                                toastr.error("Please Select Quality Measures");
                            }
                        } else {
                            angular.forEach(qualityConfig.qualityConfigObj, function (value, index) {
                                value['ReportingTypeId'] = qualityConfig.reportingType;
                                value['PerformanceYearId'] = qualityConfig.performanceYear;
                                value['QrdaYearBonusPoints'] = qualityConfig.QrdaYearPoints;
                                if (value.MonitoringTypeId != 0 && value.PerformanceGoal != 0) {
                                    qualityConfig.SaveQualityConfigObj.push({
                                        Id: value.Id,
                                        MeasureId: value.MeasureId,
                                        PracticeQRDAMeasureConfigId: value.practiceQRDAMeasureConfigId,
                                        MonitoringTypeId: value.MonitoringTypeId,
                                        PerformanceGoal: value.PerformanceGoal,
                                        PerformanceYearId: value.PerformanceYearId,
                                        ReportingTypeId: value.ReportingTypeId,
                                        qualityGoalpoints: value.qualityGoalpoints,
                                        QrdaYearBonusPoints: value.QrdaYearBonusPoints,
                                        Type: ReportingtypeName,
                                        IndividualReportingId: IndividualReportId,
                                        GroupReportingId: GroupReportId,
                                        NPI: NPIValue,
                                        TIN: TINValue
                                    });
                                }
                            });
                            qualityConfig.QualityConfigRequestBodyObj = [];
                            angular.forEach(qualityConfig.SaveQualityConfigObj, function (value, index) {
                                var reqBodyObj =
                                  {
                                      "Id": value.Id,
                                      "PracticeDestinationMapQRDAMeasureId": value.MeasureId,
                                      "PracticeQRDAMeasureConfigId": value.PracticeQRDAMeasureConfigId,
                                      "ReportingTypeId": value.ReportingTypeId,
                                      "PerformanceYearId": value.PerformanceYearId,
                                      "IndividualReportingId": value.IndividualReportingId,
                                      "GroupReportingId": value.GroupReportingId,
                                      "NPI": value.NPI,
                                      "TIN": value.TIN,
                                      "PerformanceGoal": value.PerformanceGoal,
                                      "QualityGoalPoints": value.qualityGoalpoints,
                                      "QRDAPriorYearBonusPoints": value.QrdaYearBonusPoints,
                                      "Type": value.Type,
                                      "MonitoringTypeId": value.MonitoringTypeId
                                  }
                                qualityConfig.QualityConfigRequestBodyObj.push(reqBodyObj)
                            })
                            if (qualityConfig.QualityConfigRequestBodyObj.length) {
                                qualityConfigurationService.UpdateQualityMeasureData(qualityConfig.QualityConfigRequestBodyObj).then(function (res) {
                                    if (res.data == true) {
                                        toastr.success("Quality Measures Updated Successfully");
                                        qualityConfig.clear();
                                    }
                                })
                            } else {
                                toastr.error("Please Select Quality Measures");
                            }
                        }
                    } else {
                        toastr.error("Please select Report Entity");
                    }
                } else {
                    toastr.error("Please select Report Entity");
                }
            } else {
                toastr.error("Please select Reporting Type");
            }
        } else {
            toastr.error("Please select Performance Year");
        }
    }
    qualityConfig.clear = function () {
        qualityConfig.GetQualityConfigMasterData();
        qualityConfig.QrdaYearPoints = "";
        qualityConfig.performanceYear = 0;
        qualityConfig.reportingType = 0;
        qualityConfig.Listitems =  [];
        qualityConfig.reportingEntity = '';
        qualityConfig.SaveUpdateLabel = "Save";
        qualityConfig.copyReportingType = 0;
        qualityConfig.selectedCopyEntities = [];
        qualityConfig.reportingEntitiesList = [];
    }
    //fired when Copy - reporting type is selected
    qualityConfig.OnTypeCopyChange = function (value) {
        if (value == 2) {
            qualityConfig.getGroups();
        }
        else if (value == 1) {
            qualityConfig.GetClinicians();
        }
        else if (value == 0) {
            qualityConfig.selectedCopyEntities = [];
            qualityConfig.reportingEntitiesList = [];
        }
    }

    qualityConfig.CopyMeasures = function () {
        qualityConfig.selectedEntitiesArray = [];
        qualityConfig.selectedEntities = [];
        qualityConfig.selParticipants = [];
        qualityConfig.selIndependents = [];
        qualityConfig.copyGroupRepId = '';
        qualityConfig.copyIndvRepId = '';
        qualityConfig.copyPartcNPI = '';
        qualityConfig.copyPartcTIN = '';
        qualityConfig.copyEntityType = '';
        qualityConfig.copyConfigMeasuresDataObj = [];
        angular.forEach(qualityConfig.selectedCopyEntities, function (item) {
            qualityConfig.selectedEntitiesArray.push(item.id);
        });
        qualityConfig.selectedEntities = qualityConfig.selectedEntitiesArray.join(',');

        if (qualityConfig.getConfigMeasuresForEntities == null || qualityConfig.getConfigMeasuresForEntities.length == 0 || qualityConfig.getConfigMeasuresForEntities == '') {
            toastr.error("Please configure the measures.");
            return false;
        }

        angular.forEach(qualityConfig.selectedEntitiesArray, function (value, index) {
            angular.forEach(qualityConfig.getConfigMeasuresForEntities, function (data, index) {
                //Individual
                if (qualityConfig.copyReportingType == 1) {
                    if (value.split('_')[3] == 'Participant') {
                        qualityConfig.copyIndvRepId = 0;
                        qualityConfig.copyPartcNPI = value.split('_')[2];
                        qualityConfig.copyPartcTIN = value.split('_')[1];
                    }
                    else if (value.split('_')[3] == 'Independent') {
                        qualityConfig.copyIndvRepId = value.split('_')[0];
                        qualityConfig.copyPartcNPI = '';
                        qualityConfig.copyPartcTIN = '';
                    }
                    qualityConfig.copyGroupRepId = 0;
                    qualityConfig.copyEntityType = value.split('_')[3];
                }
                else { //group
                    qualityConfig.copyGroupRepId = value.split('_')[0];
                    qualityConfig.copyPartcNPI = '';
                    qualityConfig.copyPartcTIN = '';
                    qualityConfig.copyIndvRepId = 0;
                    qualityConfig.copyEntityType = "Group";
                }
                if (data.Id != 0 && data.MonitoringTypeId != 0) {
                    qualityConfig.copyConfigMeasuresDataObj.push({
                        Id: data.id,
                        PracticeDestinationMapQRDAMeasureId: data.practiceDestinationMapQRDAMeasureId,
                        PracticeQRDAMeasureConfigId: data.practiceQRDAMeasureConfigId,
                        MonitoringTypeId: data.monitoringTypeId,
                        PerformanceGoal: data.performanceGoal,
                        PerformanceYearId: data.performanceYearId,
                        ReportingTypeId: qualityConfig.copyReportingType,
                        QualityGoalPoints: data.qualityGoalPoints,
                        QRDAPriorYearBonusPoints: data.qrdaPriorYearBonusPoints,
                        Type: qualityConfig.copyEntityType,
                        IndividualReportingId: qualityConfig.copyIndvRepId,
                        GroupReportingId: qualityConfig.copyGroupRepId,
                        NPI: qualityConfig.copyPartcNPI,
                        TIN: qualityConfig.copyPartcTIN
                    })
                }
            });
        });

        if (qualityConfig.copyConfigMeasuresDataObj.length) {
            qualityConfigurationService.CopyQualityMeasureData(qualityConfig.copyConfigMeasuresDataObj).then(function (res) {
                if (res.data == true) {
                    toastr.success("Quality Measures are copied successfully.");
                    qualityConfig.clear();
                }
                else {
                    toastr.error("Unable to copy.");
                }
            })
        }
    }


    qualityConfig.ConfigurationOnchange = function (configItem) {
        if (configItem.MonitoringTypeId==1)
        {
            if (configItem.highPriority == true) {
                qualityConfig.highPriorityMeasureCount = qualityConfig.highPriorityMeasureCount + 1;
                qualityConfig.bonusMeasureScore = qualityConfig.bonusMeasureScore + 1;
            }
            if ($filter('lowercase')(configItem.measureType) == "outcome") {
                qualityConfig.outComeMeasureCount = qualityConfig.outComeMeasureCount + 1;
                qualityConfig.bonusMeasureScore = qualityConfig.bonusMeasureScore + 2;
            }
            if (qualityConfig.highPriorityMeasureCount > 0 || qualityConfig.outComeMeasureCount > 0)
                qualityConfig.mandatoryMeasure = "Yes";           
            if (qualityConfig.bonusMeasureScore > 6)
                qualityConfig.bonusMeasureScore = 6;
        }
        else
        {
            if (configItem.highPriority == true && qualityConfig.highPriorityMeasureCount >= 1) {
                qualityConfig.highPriorityMeasureCount = qualityConfig.highPriorityMeasureCount - 1;
                if (qualityConfig.bonusMeasureScore >= 1)
                    qualityConfig.bonusMeasureScore = qualityConfig.bonusMeasureScore - 1;
            }
            if ($filter('lowercase')(configItem.measureType) == "outcome" && qualityConfig.outComeMeasureCount >= 1) {
                qualityConfig.outComeMeasureCount = qualityConfig.outComeMeasureCount - 1;
                if (qualityConfig.bonusMeasureScore >= 2)
                    qualityConfig.bonusMeasureScore = qualityConfig.bonusMeasureScore - 2;
            }
            if (qualityConfig.highPriorityMeasureCount == 0 && qualityConfig.outComeMeasureCount == 0)
                qualityConfig.mandatoryMeasure = "No";            
        }          
    }

    qualityConfig.init()
};
