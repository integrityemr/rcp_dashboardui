﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('cpiaPrintActivitiesController', cpiaPrintActivitiesController);
cpiaPrintActivitiesController.$inject = ['$state', '$scope', '$stateParams', '$timeout', 'cpiaService', '$rootScope', '$filter', 'practiceLevelService'];
function cpiaPrintActivitiesController($state, $scope, $stateParams, $timeout, cpiaService, $rootScope, $filter, practiceLevelService) {
    var cpiaPrintCtrl = this;
    cpiaPrintCtrl.currentDate = Date.now();
    cpiaPrintCtrl.init = function () {
        $rootScope.print = true;
        cpiaPrintCtrl.printObject = cpiaService.printObject;
        cpiaPrintCtrl.printObject.fromDate = $filter('date')(cpiaPrintCtrl.printObject.fromDate, 'MM/dd/yyyy');
        cpiaPrintCtrl.printObject.toDate = $filter('date')(cpiaPrintCtrl.printObject.toDate, 'MM/dd/yyyy');
        cpiaPrintCtrl.userName = cpiaPrintCtrl.printObject.userName;
        $stateParams.cpiaType = cpiaPrintCtrl.printObject.cpiaType;
        $stateParams.individualReportingId = cpiaPrintCtrl.printObject.individualReportingId;
        $stateParams.groupReportingId = cpiaPrintCtrl.printObject.groupReportingId;
        var stateParams = $stateParams;
        cpiaService.setActivityObject(stateParams);
        cpiaPrintCtrl.printingDone = function () {
            $rootScope.print = false;
            $state.go("CPIAConfig");

        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printActivitiesGridButton').click();
            }, 100);
        })
    }
    cpiaPrintCtrl.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
};