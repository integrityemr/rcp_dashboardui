﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('QualityMeasureDashboardNavController', QualityMeasureDashboardNavController);
QualityMeasureDashboardNavController.$inject = ['$state', '$scope', '$filter', 'qualityConfigurationService', 'cpiaService', 'qualityDashboardService'];
function QualityMeasureDashboardNavController($state, $scope, $filter, qualityConfigurationService, cpiaService, qualityDashboardService) {
    var QualityMeasureDashboardNavCtrl = this;
    QualityMeasureDashboardNavCtrl.reportingTypeStatic = [];
    QualityMeasureDashboardNavCtrl.reportingType = 0;
    QualityMeasureDashboardNavCtrl.Listitems = [];

    QualityMeasureDashboardNavCtrl.reportingEntity = 0;
    QualityMeasureDashboardNavCtrl.init = function () {
        qualityConfigurationService.getreportType().then(function (result) {
            QualityMeasureDashboardNavCtrl.reportingTypeStatic.push({ "id": 0, "name": "Reporting Entity Type" });
            angular.forEach(result.data, function (value, index) {
                QualityMeasureDashboardNavCtrl.reportingTypeStatic.push({ id: value.id, name: value.name });
            });
        });
        QualityMeasureDashboardNavCtrl.Listitems.push({ "id": 0, "name": "Reporting Entity Name" });
    }

    QualityMeasureDashboardNavCtrl.OnTypeChange = function (reportingType) {

        if (reportingType == 2) {
            QualityMeasureDashboardNavCtrl.getGroups();
        }
        else {
            QualityMeasureDashboardNavCtrl.GetClinicians();
        }
    }

    QualityMeasureDashboardNavCtrl.getGroups = function () {
        QualityMeasureDashboardNavCtrl.Listitems = [];
        QualityMeasureDashboardNavCtrl.Listitems.push({ "id": 0, "name": "Reporting Entity Name" });
        cpiaService.GetGroupsQppExport().then(function (response) {
            QualityMeasureDashboardNavCtrl.groups = response.data;
            angular.forEach(QualityMeasureDashboardNavCtrl.groups, function (value, index) {
                QualityMeasureDashboardNavCtrl.Listitems.push({ id: value.id + '_' + value.tin + '_' + value.physicianNPI + '_' + value.auditGroupName, name: value.name });
            });
        }, function () {
            toastr.error("unable to load Groups");
        })
    }

    QualityMeasureDashboardNavCtrl.GetClinicians = function () {
        QualityMeasureDashboardNavCtrl.Listitems = [];
        QualityMeasureDashboardNavCtrl.Listitems.push({ "id": 0, "name": "Reporting Entity Name" });
        cpiaService.GetCliniciansQppExport().then(function (response) {
            QualityMeasureDashboardNavCtrl.clinicians = response.data;
            angular.forEach(QualityMeasureDashboardNavCtrl.clinicians, function (value, index) {
                QualityMeasureDashboardNavCtrl.Listitems.push({ id: value.id + '_' + value.physicianTIN + '_' + value.physicianNPI + '_' + value.physicianName, name: value.name });
            });

        }, function () {
            toastr.error("unable to load clinicians");
        })
    }

    QualityMeasureDashboardNavCtrl.SendReportingDetails = function () {
        if (QualityMeasureDashboardNavCtrl.reportingType == 0) {
            toastr.error("please select ReportingType");
        }
        else if (QualityMeasureDashboardNavCtrl.reportingEntity == 0) {
            toastr.error("please select Reporting Entity Name");
        }
        else {
            var rType = QualityMeasureDashboardNavCtrl.reportingType;
            var rName = QualityMeasureDashboardNavCtrl.reportingEntity;
            var RNameArry = rName.split('_');
            QualityMeasureDashboardNavCtrl.TIN = RNameArry[1];
            QualityMeasureDashboardNavCtrl.Name = RNameArry[3];
            if (rType == 2) {
                QualityMeasureDashboardNavCtrl.NPI = "";
                QualityMeasureDashboardNavCtrl.Type = "Group";

            }
            else {
                QualityMeasureDashboardNavCtrl.Type = "Individual"
                QualityMeasureDashboardNavCtrl.NPI = RNameArry[2];

            }
            var ReportingDetails = {
                reportingType: QualityMeasureDashboardNavCtrl.Type,
                TIN: QualityMeasureDashboardNavCtrl.TIN,
                NPI: QualityMeasureDashboardNavCtrl.NPI,
                ReportingEntityName: QualityMeasureDashboardNavCtrl.Name
            }

            //qualityDashboardService.setReportingDetails(ReportingDetails);
            localStorage.setItem("ReportingDetails", JSON.stringify(ReportingDetails));
            $state.go("qualityMeasuredashboard");
        }
        
    }

    QualityMeasureDashboardNavCtrl.init();
}