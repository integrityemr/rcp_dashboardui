﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('mipsDashBoardController', mipsDashBoardController);
mipsDashBoardController.$inject = ['$state', '$scope', '$filter', 'uiGridConstants'];
function mipsDashBoardController($state, $scope, $filter, uiGridConstants) {
    var dashBoard = this;
    dashBoard.model = {};



    dashBoard.init = function () {
        dashBoard.model.fromDate = '2/27/2017';
        dashBoard.model.toDate = '5/30/2017';
        dashBoard.model.measureId = 1;
        dashBoard.model.groupReortingId = 2;
        dashBoard.model.individualReportingId = 1;

    }
    dashBoard.goToMeasureReport = function () {
        // amcService.setDashBoardObject(dashBoard.model);
        $state.go("amcMeasureReport");
    }
    dashBoard.init();
}