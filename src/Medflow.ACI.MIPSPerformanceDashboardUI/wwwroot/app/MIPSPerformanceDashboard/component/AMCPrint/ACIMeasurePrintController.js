﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('aciMeasurePrintController', aciMeasurePrintController);
aciMeasurePrintController.$inject = ['$state', '$scope', '$timeout', '$filter', '$rootScope', 'uiGridConstants', 'enums', 'amcService', 'practiceLevelService'];
function aciMeasurePrintController($state, $scope, $timeout, $filter, $rootScope, uiGridConstants, enums, amcService, practiceLevelService) {
    var amcMeasurePrint = this;
    amcMeasurePrint.disableFields = true;
    amcMeasurePrint.enums = enums;
    amcMeasurePrint.groupFlag = false;
    amcMeasurePrint.init = function () {
        amcMeasurePrint.practiceDetailsPrintObj = {};
        amcMeasurePrint.currentDate = Date.now()
        amcMeasurePrint.active = false;
        amcMeasurePrint.practiceDetailsPrintObj = practiceLevelService.practiceLevelPrintObject;
        amcMeasurePrint.userObject = practiceLevelService.userObject;
        if (practiceLevelService.practiceDetailsPrintObj != null) {
            amcMeasurePrint.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
        }
        amcMeasurePrint.measureReportBaseScore = amcService.measureReportBaseScore;
        amcMeasurePrint.measureReportPerformancePoints = amcService.measureReportPerformancePoints;
        amcMeasurePrint.measureReportBonusPoints = amcService.measureReportBonusPoints;

        amcMeasurePrint.fromDate = $filter('date')(amcService.fromDate, 'MM/dd/yyyy');
        amcMeasurePrint.toDate = $filter('date')(amcService.toDate, 'MM/dd/yyyy');

        amcMeasurePrint.stageName = amcService.stageName;
        if (amcService.type === 'Independent') {
            amcMeasurePrint.groupFlag = true;
            amcMeasurePrint.clinicianName = amcService.selectedClinicianName;
            amcMeasurePrint.clinicianNPI = amcService.selectedClinicianNPI;
            amcMeasurePrint.clinicianTIN = amcService.selectedClinicianTIN;
        }
        else if (amcService.type === 'Group') {
            amcMeasurePrint.selectedGroupName = amcService.selectedGroupName;
            amcMeasurePrint.selectedGroupTIN = amcService.selectedGroupTIN;
        } else {
            amcMeasurePrint.IndividualFlag = true;
            amcMeasurePrint.clinicianName = amcService.selectedClinicianName;
            amcMeasurePrint.clinicianNPI = amcService.selectedClinicianNPI;
            amcMeasurePrint.clinicianTIN = amcService.selectedClinicianTIN;
            amcMeasurePrint.selectedGroupName = amcService.selectedGroupName;
            amcMeasurePrint.selectedGroupTIN = amcService.selectedGroupTIN;
        }
        amcMeasurePrint.totalMeasurePoints = amcService.totalMeasurePoints;
        amcMeasurePrint.aciScore = amcService.aciScore;

        $rootScope.print = true;
        amcMeasurePrint.printingDone = function () {
            $rootScope.print = false;
            $state.go("amcMeasureReport");
        }
        $scope.$watch('$viewContentLoaded', function () {
            setTimeout(function () {
                $('#printAMCMeasureGridButton').click();
            }, 100);
        })
    }
    amcMeasurePrint.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}