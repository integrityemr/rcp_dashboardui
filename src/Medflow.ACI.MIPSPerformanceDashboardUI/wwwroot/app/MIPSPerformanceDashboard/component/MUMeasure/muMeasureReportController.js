﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').controller('muMeasureReportController', muMeasureReportController);
muMeasureReportController.$inject = ['$state', '$scope', '$filter', '$q', '$window', 'muMeasureService', 'practiceInfoService', 'enums', 'practiceLevelService', 'clinicianLevelService'];
function muMeasureReportController($state, $scope, $filter, $q, $window, muMeasureService, practiceInfoService, enums, practiceLevelService, clinicianLevelService) {
    var muMeasure = this;
    muMeasure.setReportingObjTogetData = [];

    muMeasure.init = function () {
        if (localStorage.getItem('emergencyAccess') === "true") {
            muMeasure.loadInit();
            muMeasure.SaveAuditView("Viewed MU measures screen", "Emergency Access");
        }
        else {
            practiceInfoService.GetUserRole().then(function (res) {
                if (res) {
                    if (res.data.role === "ADMINISTRATOR") {
                        muMeasure.loadInit();
                        muMeasure.SaveAuditView("Viewed MU measures screen", "View");
                    }
                    else if (res.data.role === "ELIGIBLE CLINICIAN") {
                        muMeasure.loadECInit(res.data.userId);
                        muMeasure.SaveAuditView("Viewed MU measures screen", "View");
                    }
                    else {
                        muMeasure.loadECInit(res.data.userId);
                        muMeasure.SaveAuditView("Viewed MU measures screen", "View");
                    }
                }
            });
        }
    }

    muMeasure.loadInit = function () {
        muMeasure.active = false;
        muMeasure.fromDate = null;
        muMeasure.toDate = null;
        muMeasure.minFromDate = new Date('01-01-2017');
        muMeasure.maxFromDate = new Date('12-31-2018');
        muMeasure.minToDate = new Date('01-01-2017');
        muMeasure.maxToDate = new Date('12-31-2018');

        var date = new Date();
        var currentDate = new Date();
        muMeasure.yearEndDate = new Date(date.getFullYear(), 11, 31);
        muMeasure.yearStartDate = new Date(date.getFullYear(), 0, 1);
        muMeasure.getMeasureSet();
        muMeasure.getCliniciansAndGroups();
        muMeasure.productkey = localStorage.getItem('ProductKey');
    }

    muMeasure.loadECInit = function (UserId) {

        muMeasure.active = false;
        muMeasure.fromDate = null;
        muMeasure.toDate = null;
        //muMeasure.minFromDate = new Date('01-01-2017');
        //muMeasure.maxFromDate = new Date('12-31-2018');
        //muMeasure.minToDate = new Date('01-01-2017');
        //muMeasure.maxToDate = new Date('12-31-2018');

        var date = new Date();
        var currentDate = new Date();
        muMeasure.yearEndDate = new Date(date.getFullYear(), 11, 31);
        muMeasure.yearStartDate = new Date(date.getFullYear(), 0, 1);
        muMeasure.productkey = localStorage.getItem('ProductKey');


        clinicianLevelService.getClinicianMeasuresForIndividualDashBoard(UserId).then(function (response) {
            var objresdata = response.data;
            if (response.data.length > 0) {
                if (response.data[0].clinicianId !== 0 && response.data[0].clinicianId !== null && response.data[0].clinicianId !== undefined) {
                    muMeasure.getMeasureSet();
                    muMeasure.getCliniciansAndGroups();
                    var muMeasurephysicianNPI = response.data[0].npi;
                    var muMeasuremeasureCode = response.data[0].muStage;

                    muMeasure.MUfromDate = $filter('date')(response.data[0].fromDate, 'yyyy-MM-dd');
                    muMeasure.MUtoDate = $filter('date')(response.data[0].toDate, 'yyyy-MM-dd');
                    var one_day = 1000 * 60 * 60 * 24;
                    var datediff = Math.round(Math.abs((new Date(muMeasure.MUfromDate).getTime() - new Date(muMeasure.MUtoDate).getTime()) / one_day))
                    muMeasure.reportingDuration = datediff
                    if (datediff > 0)
                        muMeasure.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days

                    muMeasure.MUphysicianNPI = response.data[0].name + "-" + response.data[0].npi;
                    muMeasure.MUonlyphysicianNPI = response.data[0].npi;
                    muMeasure.physicianNPI = response.data[0].npi;
                    if (response.data[0].muStage > 0) {
                        muMeasureService.getMUMeasureReport(response.data[0].muStage, response.data[0].tin, response.data[0].npi, muMeasure.MUfromDate, muMeasure.MUtoDate).then(function (subresponse) {
                            muMeasure.muMeasureReport = subresponse.data;
                            muMeasure.MUphysicianNPI = muMeasurephysicianNPI;
                            //muMeasure.MUstageCode = muMeasuremeasureCode;
                            muMeasure.MUstageName = "";
                            muMeasure.measureSetList.map(function (item) {
                                if (item.stageCode === muMeasuremeasureCode) {
                                    muMeasure.MUstageName = item.stageName;
                                }
                            });
                            muMeasure.clinicianList.map(function (item) {
                                if (item.physicianNPI === muMeasurephysicianNPI) {
                                    muMeasure.MUphysicianNPI = item.physicianName + "-" + item.physicianNPI;
                                }
                            });
                            muMeasure.originalMeasureReport = angular.copy(muMeasure.muMeasureReport);
                            if (response.data) {
                                muMeasure.calculatePerfirmanceAndAssignToData(muMeasure.muMeasureReport);
                            }

                            muMeasure.muMeasureReport.map(function (item) {
                                item.map(function (inneritem) {
                                    muMeasure.LoadColorsOnchange(inneritem);
                                })
                            })

                            muMeasure.physicianNPI = response.data[0].npi;
                            muMeasure.physicianTIN = response.data[0].tin;
                            muMeasure.stageId = response.data[0].muStage;
                            muMeasure.measureCode = response.data[0].measureCode;
                            muMeasure.fromDate = new Date(muMeasure.MUfromDate);
                            muMeasure.toDate = new Date(muMeasure.MUtoDate);
                            muMeasure.MUfromDate = $filter('date')(muMeasure.MUfromDate, 'MM/dd/yyyy');
                            muMeasure.MUtoDate = $filter('date')(muMeasure.MUtoDate, 'MM/dd/yyyy');

                            muMeasure.MUstageName = null;
                            muMeasure.stageCode = response.data[0].muStage;
                        }, function (error) {
                            toastr.error('unable to load clinicians measures');
                        });
                    }
                }
            }
            else {
                muMeasure.disableMeasureSetDropDown = true;
                muMeasure.MUfromDate = null;
                muMeasure.MUtoDate = null;
                muMeasure.MUphysicianNPI = true;
            }
        });

    }

    //to get the clinician dropdown data
    muMeasure.getCliniciansAndGroups = function () {
        muMeasureService.getGroupsAndClinicians().then(function (response) {
            muMeasure.clinicianList = response.data;

            muMeasure.clinicianList.unshift({ physicianNPI: 0, physicianName: 'Select Clinician' });
            muMeasure.physicianNPI = muMeasure.clinicianList[0].physicianNPI;
            muMeasure.physicianTIN = muMeasure.clinicianList[0].physicianTIN;


            //muMeasure.groupList = muMeasure.resultGroupsAndClinicians.groupsList;
            //muMeasure.groupandClinicianList = muMeasure.resultGroupsAndClinicians.groupCliniciansList;
            muMeasure.originalClinicianList = angular.copy(muMeasure.clinicianList);
            //muMeasure.originalGroupsList = angular.copy(muMeasure.groupList);
        }, function (error) {
            toastr.error('unable to load clinicians data');
        });
    }
    //to get the measure set dropdwon data
    muMeasure.getMeasureSet = function () {
        muMeasureService.getMUStages().then(function (response) {
            muMeasure.measureSetList = response.data;
            if (muMeasure.measureSetList !== null) {
                muMeasure.measureSetList = $filter('filter')(muMeasure.measureSetList, { stageType: enums.stageType.MeaningFulUse });
                muMeasure.measureSetList.unshift({
                    stageCode: 0, stageId: 0, stageName: 'Select Stage', stageType: "select"
                });
                muMeasure.stageCode = muMeasure.measureSetList[0].stageCode;
                muMeasure.copiedMeasureSetList = angular.copy(muMeasure.measureSetList);
            }
        }, function (error) {
            toastr.error('unable to load measure set');
        });
    }

    muMeasure.getMUMeasureReport = function () {
        var NPI = muMeasure.MUonlyphysicianNPI;
        if (NPI !== undefined)
            muMeasure.physicianNPI = NPI;
        if (muMeasure.stageCode !== 0 && muMeasure.physicianNPI !== 0 && muMeasure.fromDate !== null && muMeasure.toDate !== null) {
            muMeasure.selectedStageAsTitle = $filter('filter')(muMeasure.measureSetList, { stageCode: muMeasure.stageCode })[0].stageName;
            muMeasure.clinicianList.map(function (item) {
                if (item.physicianNPI === muMeasure.physicianNPI) {
                    //muMeasure.physicianTIN = item.physicianTIN;
                    muMeasure.physicianName = item.physicianName;
                }
            })
            muMeasure.getMUMeasureList(muMeasure.stageCode, 0, muMeasure.physicianNPI, muMeasure.fromDate, muMeasure.toDate);
        }
        else {
            muMeasure.muMeasureReport = [];
        }
    }

    //to get the MU getMUMeasureExclude //not required
    muMeasure.getMUMeasureExclude = function (physicianNPI, muStageCode) {
        muMeasureService.getMUMeasureExclude(physicianNPI, muStageCode).then(function (response) {
            if (response.data) {
                muMeasure.muMeasureDataExclude = response.data;
                muMeasure.muMeasureReportExclude = angular.copy(muMeasure.muMeasureReport);
                toastr.success("Successfully Exclude Updated");
            }
        }, function (error) {
            toastr.error('unable to update exclude');
        });
    }


    muMeasure.checkYearToShowStage = function () {
        if (muMeasure.fromDate !== null && muMeasure.toDate !== null) {
            var currentYear = new Date().getFullYear();
            var nextYear = currentYear + 1;
            var fromDateYear = muMeasure.fromDate.getFullYear();
            var toDateYear = muMeasure.toDate.getFullYear();

            //if current Dates are greater than current year i.e., 2018 we have to show only MeaningFul Use Stage 3
            if (fromDateYear > currentYear && toDateYear > currentYear) {
                if (fromDateYear > nextYear && toDateYear > nextYear) {
                    muMeasure.disableMeasureSetDropDown = true;
                }
                else {
                    muMeasure.updatedMeasureSetList = [];
                    angular.forEach(muMeasure.measureSetList, function (value, index) {
                        if (value.stageCode === enums.stageCode.muStage3) {
                            muMeasure.updatedMeasureSetList.push(value);
                        }
                    })
                    muMeasure.measureSetList = angular.copy(muMeasure.updatedMeasureSetList);
                    muMeasure.measureSetList.unshift({
                        stageCode: 0, stageId: 0, stageName: 'Select Stage', stageType: "select"
                    });

                    muMeasure.disableMeasureSetDropDown = false;
                }
            }
            //else if (fromDateYear < currentYear && toDateYear < currentYear) {
            //    muMeasure.disableMeasureSetDropDown = true;
            //}
            else if ((fromDateYear === currentYear || fromDateYear === nextYear) && fromDateYear !== toDateYear) {
                var count = 0;
                muMeasure.disableMeasureSetDropDown = true;
                if (count === 0) {
                    toastr.error("Reporting period is not within the calendar year");
                    count = count + 1;
                }
            }
            else if (fromDateYear === currentYear && toDateYear === currentYear) {
                muMeasure.disableMeasureSetDropDown = false;
                muMeasure.measureSetList = muMeasure.copiedMeasureSetList;
            }
            else if (fromDateYear === nextYear && toDateYear === nextYear) {
                muMeasure.disableMeasureSetDropDown = false;
                muMeasure.measureSetList = muMeasure.copiedMeasureSetList;
                muMeasure.stageCode = 0;
            }
            else if (fromDateYear > nextYear && toDateYear > nextYear) {
                muMeasure.disableMeasureSetDropDown = true;
            }
            else {
                muMeasure.disableMeasureSetDropDown = false;
                muMeasure.measureSetList = muMeasure.copiedMeasureSetList;
            }
        }
    }

    muMeasure.validateDate = function () {
        var isFromDateValid = angular.isDate(muMeasure.fromDate);
        var isToDateValid = angular.isDate(muMeasure.toDate);
        if (isFromDateValid === false) {
            toastr.error("Invalid date");
        }
        if (isToDateValid === false) {
            toastr.error("Invalid date");
        }
    }

    muMeasure.LoadColorsOnchange = function (measure) {
        if (measure.classExclude === false && measure.isExclude === true) {
            measure.exclude = 'No';
            measure.classDanger = false;
            measure.classWarning = false;
            measure.classSuccess = false;
            //add the style of gray to the status bar
            measure.classExclude = true;
            measure.checked = true;
        }
        else if (measure.classExclude === true && measure.isExclude === true) {
            //when user unchecks the checkBox assign the actual object by filtering the finalObjective
            angular.forEach(muMeasure.OriginalObjectiveList, function (value, key) {
                value.map(function (item) {
                    if (item.objective === measure.objective && item.measureNo === measure.measureNo) {
                        measure.classDanger = item.classDanger;
                        measure.classSuccess = item.classSuccess;
                        measure.classWarning = item.classWarning;
                        measure.classExclude = item.classExclude;
                        measure.exclude = item.exclude;
                        measure.checked = false;
                    }

                });
            });
        }
        else if (measure.classExclude === true && measure.isExclude === false) {
            //when user unchecks the checkBox assign the actual object by filtering the finalObjective
            angular.forEach(muMeasure.OriginalObjectiveList, function (value, key) {
                value.map(function (item) {
                    if (item.objective === measure.objective && item.measureNo === measure.measureNo) {
                        measure.classDanger = item.classDanger;
                        measure.classSuccess = item.classSuccess;
                        measure.classWarning = item.classWarning;
                        measure.classExclude = item.classExclude;
                        measure.exclude = item.exclude;
                        measure.checked = false;
                    }

                });
            });
        }
    }

    muMeasure.changeStatusBarColourOnCheckBox = function (measure) {
        muMeasure.LoadColorsOnchange(measure);
        muMeasure.OnMUMeasureExclude(muMeasure.physicianNPI, measure.muMeasureId, muMeasure.stageCode, measure.isExclude, measure.measureName);
    }

    muMeasure.OnMUMeasureExclude = function (physicianNPI, muMeasureId, stageCode, excludeStatus, measureName) {
        muMeasure.AuditEditMessage = "";
        muMeasure.clinicianList.map(function (clinician) {
            if (muMeasure.physicianNPI === clinician.physicianNPI) {
                muMeasure.AuditEditMessage = clinician.auditPhysicianName;
            }
        });

        muMeasure.AuditEditMessage = "" + muMeasure.selectedStageAsTitle + "-" + measureName + "-for-" + muMeasure.AuditEditMessage + "";
        var data = {
            physicianNPI: physicianNPI,
            muMeasureId: muMeasureId,
            stageCode: stageCode,
            excludeStatus: excludeStatus,
            auditMessage: muMeasure.AuditEditMessage
        };
        muMeasureService.postMUMeasureExclude(data).then(function (response) {
            if (response.data === true) {
                muMeasure.AuditEditMessage = "";
                return true;
            }
        }, function (error) {
            toastr.error("unable to update exclude");
        });
    }


    muMeasure.openMeasureDescriptionPdf = function (measure) {
        //open measure.filePath in a new tab
        //get thefile path from db
        //measure.filePath = "D:\MeasurePdf\Patient Electronic access";
        var finalFilePath = measure.filePath.replace("\"", "");
        $window.open(finalFilePath);
    }

    //to set the from date as Year start date and to date as Year end date
    muMeasure.setFullCalender = function () {
        muMeasure.fromDate = angular.copy(muMeasure.yearStartDate);
        muMeasure.toDate = angular.copy(muMeasure.yearEndDate);
        var one_day = 1000 * 60 * 60 * 24;
        muMeasure.reportingDuration = Math.ceil((muMeasure.toDate.getTime() - muMeasure.fromDate.getTime()) / (one_day));
        muMeasure.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
        muMeasure.checkYearToShowStage();
        muMeasure.getMUMeasureReport();
    }

    //when toDate is changed validate the reporting days greater than or equal to 90
    muMeasure.onToDateChange = function () {
        if (muMeasure.toDate === undefined) {
            muMeasure.reportingDuration = "";
        }
        var one_day = 1000 * 60 * 60 * 24;
        muMeasure.reportingDuration = Math.ceil((muMeasure.toDate.getTime() - muMeasure.fromDate.getTime()) / (one_day));
        muMeasure.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
        if (muMeasure.reportingDuration < 89) {
            toastr.error("Reporting period must be greater than or equal to 90 days ");
        }
        else {
            muMeasure.validateDate();
            muMeasure.checkYearToShowStage();
        }
        muMeasure.getMUMeasureReport();
    }

    //on From date change calculate the toDate Automatically and populate
    muMeasure.onFromDateChange = function () {
        if (muMeasure.fromDate === undefined) {
            muMeasure.reportingDuration = "";
        }
        muMeasure.updateToDate(muMeasure.fromDate);
        muMeasure.checkYearToShowStage();
        muMeasure.getMUMeasureReport();
    }

    //calculate the toDate post 90 days of the from date
    muMeasure.updateToDate = function (fromDate) {
        if (fromDate === undefined || fromDate === null) { return false; }
        muMeasure.toDate = angular.copy(fromDate);
        var one_day = 1000 * 60 * 60 * 24;
        //adding 90 days to todate
        var reqTime = muMeasure.fromDate.getTime();
        muMeasure.toDate = new Date(reqTime + 89 * one_day);
        muMeasure.reportingDuration = Math.ceil((muMeasure.toDate.getTime() - muMeasure.fromDate.getTime()) / (one_day));
        muMeasure.reportingDuration += 1; // including dates, then 01/01/2017 to 31/03/2017 total 90 days
    };

    muMeasure.getmuMeasureData = function () {
        muMeasure.getMUMeasureList();
    }

    //to get the MU MeasureData
    muMeasure.getMUMeasureList = function (muStageCode, physicianTIN, physicianNPI, fromDate, toDate) {
        fromDate = $filter('date')(fromDate, 'yyyy-MM-dd');
        toDate = $filter('date')(toDate, 'yyyy-MM-dd');
        muMeasureService.getMUMeasureReport(muStageCode, physicianTIN, physicianNPI, fromDate, toDate).then(function (response) {
            muMeasure.muMeasureReport = response.data;
            muMeasure.originalMeasureReport = angular.copy(muMeasure.muMeasureReport);
            if (response.data) {
                muMeasure.calculatePerfirmanceAndAssignToData(muMeasure.muMeasureReport);
            }

            muMeasure.muMeasureReport.map(function (item) {
                item.map(function (inneritem) {
                    muMeasure.LoadColorsOnchange(inneritem);
                })
            })

            muMeasure.actionforAudit = "";
            //to fetch the clinician name using the NPI
            if (physicianTIN !== null && physicianNPI != null && muMeasure.clinicianList!=null) {
                angular.forEach(muMeasure.clinicianList, function (value, Key) {
                    if (value.physicianNPI === physicianNPI) {
                        muMeasure.actionforAudit = value.auditPhysicianName;
                    }
                });
            }
                var data = false;
                muMeasure.actionforAudit = "Query for Measure set - " + muMeasure.selectedStageAsTitle + "  Dates between - " + fromDate + " - " + toDate + " - for - " + muMeasure.actionforAudit + "";
                muMeasure.SaveAuditView(muMeasure.actionforAudit, "Query");


            muMeasure.physicianNPI = physicianNPI;
            muMeasure.physicianTIN = physicianTIN;
            muMeasure.stageId = muStageCode;
            //muMeasure.measureCode = response.data[0].measureCode;
            muMeasure.MUfromDate = $filter('date')(fromDate, 'MM/dd/yyyy');
            muMeasure.MUtoDate = $filter('date')(toDate, 'MM/dd/yyyy');

        }, function (error) {
            toastr.error('unable to load measure list');
        });
    }

    muMeasure.calculatePerfirmanceAndAssignToData = function (muMeasureReport) {
        //manipulating the data to get the status bar colours in Green Red and Yellow
        //hiding and checking the checkboxes 
        muMeasure.muMeasureData = [];
        muMeasure.finalObjectiveList = [];
        if (muMeasureReport !== null) {
            //create a list of objects with the count
            for (var i = 0; i < muMeasureReport.length; i++) {
                var objectiveList = [];
                objectiveList.push(muMeasureReport[i].muMeasureDistinctViewModel);
                objectiveList.map(function (value) {
                    value.map(function (item) {
                        item.checked = false;
                        if (item.isExclude === true) {
                            item.checked = true;
                        }
                        var threshold;
                        if (item.muThreshold !== null) {
                            if (item.muThreshold !== "") {
                                threshold = item.muThreshold.replace('>', '');
                            }
                        }
                        if (item.exclude === 'NA') {
                            item.showCheckBox = false;
                        }
                        else if (item.exclude === 'Yes') {
                            item.showCheckBox = true;
                        }
                        else {
                            item.showCheckBox = true;
                        }
                        if (item.performance !== null && item.performance !== "YES" && item.performance !== "NO") {
                            if (item.muThreshold.indexOf('>') >= 0)
                                threshold = parseInt(threshold) + 1;
                        }

                        if (item.performance === 'YES') {
                            item.classDanger = false;
                            item.classSuccess = true;
                            item.classWarning = false;
                            item.showCheckBox = true;
                            item.classExclude = false;
                            //item.checked = false;
                            muMeasure.muMeasureData.push(item);
                        }
                        else if (item.performance === 'NO') {
                            //item.performance = 'NO';
                            item.classDanger = true;
                            item.classSuccess = false;
                            item.classWarning = false;
                            item.classExclude = false;
                            muMeasure.muMeasureData.push(item);
                        }
                        else if (item.performance === null) {
                            if (threshold !== null) {
                                item.performance = "0";
                            }
                            item.classDanger = false;
                            item.classSuccess = true;
                            item.classWarning = false;
                            item.classExclude = false;
                            muMeasure.muMeasureData.push(item);
                        }
                        else if (parseInt(item.performance) >= parseInt(threshold)) {
                            item.classDanger = false;
                            item.classSuccess = true;
                            item.classWarning = false;
                            item.classExclude = false;
                            item.performance = parseInt(item.performance);
                            muMeasure.muMeasureData.push(item);
                        }
                        else if (parseInt(item.performance) < parseInt(threshold)) {
                            var calculation = 90 * parseInt(threshold) / 100;
                            muMeasure.calculation = parseInt(calculation);
                            muMeasure.performance = parseInt(item.performance);
                            item.performance = parseInt(item.performance);
                            if (muMeasure.calculation <= muMeasure.performance) {
                                item.performance = parseInt(item.performance);
                                item.classDanger = false;
                                item.classWarning = true;
                                item.classSuccess = false;
                                item.classExclude = false;
                                muMeasure.muMeasureData.push(item);
                            }
                            else {
                                item.performance = parseInt(item.performance);
                                item.classDanger = true;
                                item.classWarning = false;
                                item.classSuccess = false;
                                item.classExclude = false;
                                muMeasure.muMeasureData.push(item);
                            }
                        }
                        else {
                            item.classDanger = false;
                            item.classSuccess = true;
                            item.classWarning = false;
                            item.classExclude = false;
                            if (isNaN(item.performance) || item.performance === "") {
                                item.performance = "";
                            }
                            else {
                                item.performance = parseInt(item.performance);
                            }
                            muMeasure.muMeasureData.push(item);
                        }
                    });
                    muMeasure.finalObjectiveList.push(value);
                });

            }
        }
        muMeasure.OriginalObjectiveList = angular.copy(muMeasure.finalObjectiveList);

        muMeasure.muMeasureReport = angular.copy(muMeasure.OriginalObjectiveList);

    }
    //to clear all the fields
    muMeasure.clearFields = function () {
        muMeasure.muMeasureReport = [];
        muMeasure.originalMeasureReport = [];
        muMeasure.clinicianList = muMeasure.originalClinicianList;
        muMeasure.fromDate = null;
        muMeasure.toDate = null;
        muMeasure.reportingDuration = '';
        muMeasure.stageCode = 0;
        muMeasure.physicianNPI = 0;
        muMeasure.disableMeasureSetDropDown = false;
    }

    //to export the data into CSV or EXcel
    muMeasure.exportAmcData = function (isExport) {
        var entities = [];
        var auditfromDate = $filter('date')(new Date(muMeasure.fromDate), 'MM/dd/yyyy');
        var auditToDate = $filter('date')(new Date(muMeasure.toDate), 'MM/dd/yyyy');
        muMeasure.originalMeasureReport.map(function (values) {
            values.muMeasureDistinctViewModel.map(function (item) {
                if ((item.performance === "YES") || (item.performance === "NO")) {
                    item.performance = item.performance;
                }
                else {
                    item.performance = parseInt(item.performance);
                }
                if (item.numerator === null) {
                    item.numerator = '';
                }
                if (item.denominator === null) {
                    item.denominator = '';
                }
                if (item.muThreshold === null) {
                    item.muThreshold = '';
                }
                entities.push(item);
            })
        });
        var mystyle = {
            headers: true,
            columns: [
                { columnid: 'objective', title: "Objective" },
                { columnid: 'measureNo', title: "Measure No." },
                { columnid: 'measureName', title: "Measure Name" },
                { columnid: 'numerator', title: 'Numerator' },
                { columnid: 'denominator', title: 'Denominator' },
                { columnid: 'muThreshold', title: 'MU Threshold' },
                { columnid: 'performance', title: 'Performance' },
                { columnid: 'exclude', title: 'Exclude' }
            ],
        };
        var fileNameExcel = "muReport" + $filter('date')(new Date(), "MMddyyyy") + ".xls";
        var fileNameCsv = "muReport" + $filter('date')(new Date(), "MMddyyyy") + ".csv";

        //alasql('SELECT * INTO XLSXML("' + fileNameExcel + '",?) FROM ?', [mystyle, entities]);
        if (isExport === 'csv') {
            alasql("SELECT * INTO CSV('" + fileNameCsv + "',?) FROM ?", [mystyle, entities]);
        }
        else {
            alasql('SELECT * INTO XLSXML("' + fileNameExcel + '",?) FROM ?', [mystyle, entities]);
        }
        muMeasure.ExportAudit = "";
        muMeasure.clinicianList.map(function (clinician) {
            if (muMeasure.physicianTIN === muMeasure.physicianTIN) {
                muMeasure.ExportAudit = clinician.auditPhysicianName;
            }
        });
        if (muMeasure.ExportAudit !== null && muMeasure.selectedStageAsTitle !== null && auditfromDate !== null && auditToDate !== null) {
            muMeasure.ExportAudit = "Export for Measure set-" + muMeasure.selectedStageAsTitle + "Dates between-" + auditfromDate + "-" + auditToDate + "-for-" + muMeasure.physicianName + "";
            muMeasure.SaveAuditView(muMeasure.ExportAudit, "Export");
            muMeasure.ExportAudit = "";
        }
    }

    //to print the report
    muMeasure.print = function () {
        for (var i = 0; i < muMeasure.originalMeasureReport.length; i++) {
            for (var j = 0; j < muMeasure.originalMeasureReport[i].muMeasureDistinctViewModel.length; j++) {
                if (muMeasure.muMeasureReport[i][j].checked) {
                    muMeasure.originalMeasureReport[i].muMeasureDistinctViewModel[j].exclude = "Yes";
                }
                else {
                    if (muMeasure.originalMeasureReport[i].muMeasureDistinctViewModel[j].exclude !== "NA") {
                        muMeasure.originalMeasureReport[i].muMeasureDistinctViewModel[j].exclude = "";
                    }

                }
            }
        }
        muMeasureService.setMUPrintObj(muMeasure.originalMeasureReport, muMeasure.physicianName, muMeasure.physicianNPI, 0, muMeasure.fromDate, muMeasure.toDate, muMeasure.selectedStageAsTitle);
        var promises = [
            practiceLevelService.getLoggedInUserDetails(),
            practiceLevelService.getPracticeDetails()
        ];

        $q.all(promises).then(function (data) {
            muMeasure.loggedInUserObj = data[0].data.userName;
            muMeasure.practiceDetails = data[1].data;
            practiceLevelService.setLoggedInUser(muMeasure.loggedInUserObj);
            practiceLevelService.setPracticeDetails(muMeasure.practiceDetails);
            muMeasure.muMeasureObject = [];
            muMeasureService.muPrintObj.map(function (itemobj) {
                itemobj.muMeasureDistinctViewModel.map(function (item) {
                    var value = item.performance;
                    if (typeof value === "number" && isFinite(value) && Math.floor(value) === value) {
                        item.performance = parseInt(item.performance).toFixed(2);
                    }
                    muMeasure.muMeasureObject.push(item);
                });
            });
            muMeasure.clinicianName = muMeasureService.physicianName;
            muMeasure.tin = muMeasureService.physicianTIN;
            muMeasure.npi = muMeasureService.physicianNPI;
            muMeasure.fromDate = $filter('date')(new Date(muMeasureService.fromDate), 'MM/dd/yyyy');
            muMeasure.toDate = $filter('date')(new Date(muMeasureService.toDate), 'MM/dd/yyyy');
            muMeasure.muMeasureTitle = muMeasureService.muMeasureTitle;

            muMeasure.userObject = practiceLevelService.userObject;
            if (practiceLevelService.practiceDetailsPrintObj !== null) {
                muMeasure.practiceDetailsPrintObj = practiceLevelService.practiceDetailsPrintObj;
            }
            window.setTimeout(function () {
                var printContents = $("#muMeasurediv").html();
                var popupWin = window.open('', '_blank', 'width=1320,height=650');
                popupWin.document.open();
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
                popupWin.document.close();

            }, 1);
            muMeasure.PrintAudit = "";
            muMeasure.clinicianList.map(function (clinician) {
                if (muMeasure.physicianTIN === muMeasure.physicianTIN) {
                    muMeasure.PrintAudit = clinician.auditPhysicianName;
                }
            });
            muMeasure.PrintAudit = "Print for Measure set-" + muMeasure.selectedStageAsTitle + "Dates between-" + muMeasure.fromDate + "-" + muMeasure.toDate + "-for-" + muMeasure.physicianName + "";
            muMeasure.SaveAuditView(muMeasure.PrintAudit, "Print");
        }, function (error) {
            //toastr.error('error while getting logged in user details');
        });

    }

    muMeasure.SaveAuditView = function (action, actionType) {
        var value = {
            Action: action,
            ActionType: actionType
        };
        muMeasureService.auditForAMC(value).then(function (res) {
            if (res) {
                return true;
            }
        });
    }

    muMeasure.goToANRMeasureReport = function (measure) {
        var screenName = 'MUMeasureReport';
        localStorage.setItem("setScreenName", screenName);
        var tin = muMeasure.tin;
        measure.fromDate = muMeasure.MUfromDate;
        measure.toDate = muMeasure.MUtoDate;
        measure.tin = muMeasure.physicianTIN;
        measure.npi = muMeasure.physicianNPI;
        measure.type = "Physician";
        if (muMeasure.selectedStageAsTitle === null || muMeasure.selectedStageAsTitle === "" || muMeasure.selectedStageAsTitle === undefined) {
            measure.stageName = muMeasure.MUstageName;
            measure.physicianName = muMeasure.MUphysicianNPI;
        }
        else {
            measure.stageName = muMeasure.selectedStageAsTitle;
            measure.physicianName = muMeasure.physicianName;
        }
        //measure.groupName = muMeasure.groupName;
        measure.stageId = muMeasure.stageId;
        measure.measureCode = measure.measureCode;
        muMeasure.setReportingObjTogetData.push(measure); //setReportingObjTogetData - list
        localStorage.setItem("AciScoreObjectdata", JSON.stringify(muMeasure.setReportingObjTogetData));


        $state.go("anr");
    }

    muMeasure.init();
    $scope.$on('$locationChangeStart', function (event, next, current) {
        event.preventDefault();
    });
}