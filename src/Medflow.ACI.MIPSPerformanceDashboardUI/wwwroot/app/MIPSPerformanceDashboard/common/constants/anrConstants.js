﻿'use strict'
angular.module('anr.ACIMeasureconstants', [
])
        .constant("anrMeasureConstants", {
            getANRMeasurePatientsDataUrl: "GetANRMeasurePatientsData/",
            getVendorIsMRNValue: "GetVendorIsMRN"
        }).
        constant("categoryDescriptionEnum", {
            categoryDescription: "For the advancing care information performance category, the performance category score is the sum of a base score, performance score, and bonus score.A MIPS eligible clinician earns a base score by reporting the numerator (of at least one) and denominator or yes/no statement or null value as applicable, for each required measure."
        });

