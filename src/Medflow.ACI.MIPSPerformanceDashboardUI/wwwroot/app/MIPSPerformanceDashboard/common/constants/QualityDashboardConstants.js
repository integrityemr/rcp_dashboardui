﻿'use strict'
angular.module('amc.QualityDashboardConstants', [
])
        .constant("QualityDashboardConstants", {
            GetReportingMeasuresData: "GetReportingMeasuresDashboardDetails",
            GetMonitoringMeasuresData: "GetMonitoringMeasuresDashboardDetails",
            GetImprovementScoreData: "GetImprovementScore"
        });

