﻿'use strict'
angular.module('groupLevelDashBoard.constants', [
])
        .constant("groupLevelConstants", {
            getClinicniansOfGroupUrl: "GetSpecificPerformanceDashBoardDetailsForPractice/",
            getCPIADetailsForGroupUrl: "GetCPIADetailsForGroup/",
            getCliniciansForGroupUrl: "GetGroupLevelDashBoardDetails/"

        });