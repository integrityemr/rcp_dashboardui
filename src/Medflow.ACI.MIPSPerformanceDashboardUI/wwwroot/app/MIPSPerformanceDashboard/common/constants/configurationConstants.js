﻿'use strict'
angular.module('configuration.constants', [
])
    .constant("configurationConstants", {
        getPracticeDetailsUrl: "GetPracticeDetails/",
        getActivitiesByType: "GetCPIAActivitiesByType/",
        getActivitiesByNPIandTIN: "GetCPIAActivitiesByNPIAndTIN/",
        getGroupList: "GetGroupList/",
        getCliniciansList: "GetClinicians/",
        getCPIATypes: "GetCPIATypes/",
        getGroupDetailsForPrint: "GetGroupDetailsForPrint/",
        getReportingCount: "GetReportingCount/",
        getCliniciansForPrint: "GetCliniciansForPrint/",
        saveCPIAActivities: "SaveCPIAActivities/",
        updateCPIAActivities: "UpdateCPIAActivities/",
        getUserDetails: "GetUserDetails/",
        getUsername: "GetUsername/",
        getPracticeLogoPath: "GetPracticeLogoPath/",
        getPracticeDetails: "GetPracticeDetails/",
        copyCPIAActivities: "CopyCPIAActivities/",
        // Practice Information Service
        getAttributes: "AttributesList/",
        getClinicians: "ClinicianList/",
        getGroupBillingList: "GroupBillingList/",
        getIndividualBillingList: "IndividualBillingList/",
        saveGroupReporting: "CreateGroupReporting/",
        saveIndividualReporting: "CreateIndividualReporting/",
        createAuditLog:"CreateAuditLogPracticeInformation/",
        auditCPIAUrl: "AuditLog/",
        getUserRole: "GetUserRole/",
                
        //ACI Service 
        getMeasuresList: "MeasuresList/",
        getSRAHistoryList: "SRAHistoryList/",
        getACIConfigurationCountDetails: "ACIConfigurationCountDetails/",
        getSRADate: "SRADate/",
        ACIMeasureSetList: "ACIMeasureSetList/",
        ACIMeasureSetListByTINAndNPI: "ACIMeasureSetListByNPIandTIN/",

        createACIMeasureAndSRADate: "CreateACIMeasureAndSRADate/",
        updateACIMeasures: "EditACIMeasures/",
        getACICliniciansList: "getClinicians/",
        enabledPublicHealthPoints: "EnabledPublicHealthPoints/",
        enableQualityRegistryPoints: "EnableQualityRegistryPoints/",
        eligibleCPIAPoints: "EligibleCPIAPoints/",
        EligibleCPIAPointsforGroupsandIndividuals: "EligibleCPIAPointsforGroupsandIndividuals/",
        CreateActionLogFromAuditReportProject: "CreateActionLog/",
        getMeasureSetIdByCPIATypeUrl: "GetStageIdForACIConfig/",
        createAuditLogACIConfiguration: "CreateAuditLogACIConfiguration/",
        getBuildDetails: "GetBuildVersionDetails/", //for getting build details in help
        getCEHRT: "GetCEHRT/",
        getentitiesList: "GetQppReportingEntities/",
        GetCategoriesList: "GetQppReportingCategories/",
        getqppExportData: "GetQppExportData/",
        SaveQppAuditLogData: "QPPSaveAuditLogData/",
        GetQppAuditLogData: "GetQPPExportAuditLogData/",
        qppExport: "ExportQPPJson/",
        getConfigReportingStatusForQPPExport: "IsQPPReportingConfigured/",
        qppexportStatusSave: "SaveQPPExportStatus/",
        deleteSRAHistory: "DeleteSRAHistory/"
        //add Cofigstatus method here   "getReportingStatus"
    })
    .constant("configEnums", {
        cpiaType: {
            advanced: 0,
            individual: 1,
            group: 2,
            simple: 3
        }
    }).constant("regexPracticInfo", {
        regxTIN: /^\d{9}$/,
        regxAlphaNumeric: /^([a-zA-Z0-9 _-]+)$/
    }).constant("actionTypeEnum", {
        ActionType: {
            InsertActionType: "Insert",
            CreateScope: "create",
            DeleteActionType: "Delete",
            PrintActionType: "Print",
            SelectActionType: "Select",
            UpdateActionType: "Update"
        }
    }).constant("nextYearEnum", {
        nextYear: 2018
    }).constant("QPPperformanceYearEnums", {
        QppperformanceYearList: [
        { yearId: 1, performanceYear: 2018 },
         { yearId: 0, performanceYear: 2017 }
        ]
    });