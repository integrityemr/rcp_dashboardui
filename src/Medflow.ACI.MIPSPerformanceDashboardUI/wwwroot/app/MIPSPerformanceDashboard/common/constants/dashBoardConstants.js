﻿'use strict'
angular.module('dashBoard.constants', [
])
.constant("dashBoardConstants", {
    getPracticeLeveleDataUrl: "/"
}).constant("dashBoardEnums", {
    reportingPeriod: [
    { reportingPeriodId: 0, reportingPeriodName: 'Select' },
    { reportingPeriodId: 1, reportingPeriodName: 'Full Year' },
    { reportingPeriodId: 2, reportingPeriodName: '90 Days' },
    { reportingPeriodId: 3, reportingPeriodName: 'Date Range' }
    ]
})
.constant("muStageEnums", {
    measureStages: [
    { measureCode: 2, measureName: 'ACI alternate measure set (2017 only, Stage 2)' },
    { measureCode: 3, measureName: 'ACI measure set (Stage 3)' },
    { measureCode: 4, measureName: 'Meaningful Use Modified Stage2(2017)' },
    { measureCode: 5, measureName: 'Meaningful Use Stage3' },
    ]
})
.constant("performanceYearEnums", {
    performanceYearList: [
    { yearId: 1, performanceYear: 2018 },
    { yearId: 2, performanceYear: 2019 },
    ]
})
.constant("aciPerformanceYearEnums", {
    performanceYearList: [
    //{ yearId: 1, performanceYear: 2017 },
    { yearId: 2, performanceYear: 2018 },
    { yearId: 3, performanceYear: 2019 },
    ]
}).constant("cpiaPerformanceYearEnums", {
    performanceYearList: [
    //{ yearId: 1, performanceYear: 2017 },
   { name: '2018', value: '2018' },
   { name: '2019', value: '2019' }
    ]
});