﻿'use strict'
angular.module('ngRealTimeExportconstants', [])
.constant("RealTimeDataExportConstants", {
    PhysicianList: "PhysicianList/",
    PatientSummaryList: "PatientSummaryList/",
    Export: "Export/",
    Download: "Download/",
});