﻿'use strict'
angular.module('QppAudit.constants', [
])
    .constant("QppAuditConstants", {
        SaveQppAuditLogData: "QPPSaveAuditLogData/",
        GetQppAuditLogData: "GetQPPExportAuditLogData/"
    });