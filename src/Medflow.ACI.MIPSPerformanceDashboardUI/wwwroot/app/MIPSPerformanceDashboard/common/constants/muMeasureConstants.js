﻿'use strict'
angular.module('amc.constants', [
])
        .constant("muMeasureConstants", {
            getMUMeasureReportUrl: "GetMUMeasureCalculation/",
            getMUStageUrl: "GetMUStages",
            getGroupsCliniciansUrl: "GetGroupAndPhysicianDetails",
            getCliniciansUrl: "GetClinicians",
            postMUMeasureExcludeUrl: "PostMUMeasureExclude/",
            saveAuditLogAMC: "AuditLogAMC/",
            PostMUMeasureExclude: "PostMUMeasureExclude/"
        }).constant("enums", {
            stageType: {
                MeaningFulUse: 'MU',
                ACI: 'ACI'
            },
            stageCode: {
                aciStage2: 2,
                aciStage3: 3,
                muStage2: 4,
                muStage3: 5
            }
        });