﻿'use strict'
angular.module('practiceLevelDashBoard.constants', [
])
        .constant("practiceLevelConstants", {
            getLoggedInUserDetailsUrl: "GetUserDetails",
            getPracticeDetailsUrl: "GetPracticeDetails/",
            getPracticeLevelDashBoardDataUrl: "GetPerformanceDashBoardDetailsForPractice/",
            getIndividualAciAndCpiaScoreUrl: "GetACIandCPIAScoreForIndividual/",
            getGroupAciAndCpiaScoreUrl: "GetACIandCPIAScoreForGroup/",
            getActivityStartDatesOfGroupUrl: "GetGroupCPIAStartDates/",
            getActivityStartDatesOfIndividualUrl: "GetIndividualCPIAStartDates/",
            getCPIARegisgtsryStartDatesUrl: "GetGRIRegistryStartDates/",
            updateACIReportingPeriodUrl: "UpdateACIConfigReportingPeriod/",
            getGroupIndividualACICPIAScoresUrl: "GetACIandCPIAScoreForGroupIndividual/",
            getActivityStartDatesOfIndependentUrl: "GetIndependentCPIAStartDates/",

            //Audit related constants
            ViewActionType: "View",
            PrintActionType: "Print",
            ChangeActionType: "Change",
            ExportActionType: "Export",
            CreateActionLog: "CreateActionLog/",
            MIPSPerfDash: "MIPS Performance Dashboard",
            MIPSPerfDashPracLevel: "MIPS Performance Dashboard - Practice Level",
            MIPSPerfDashGroupLevel: "MIPS Performance Dashboard - Group Level",
            MIPSPerfDashIndividualLevel: "MIPS Performance Dashboard - Individual Level",
            MIPSPerfDashImpActivities: "MIPS Performance Dashboard - Improvement Activities",
            MIPSACIMeasureReport: "MIPS ACI-Measure Report",
            ANRPatientList: "Patient List"
        });