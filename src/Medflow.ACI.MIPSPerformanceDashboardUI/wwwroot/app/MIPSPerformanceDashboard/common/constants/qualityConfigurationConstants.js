﻿'use strict'
angular.module('amc.QualityConfigurationConstants', [
])
        .constant("qualityConfigurationConstants", {
            GetPerformanceYear: "GetQualityConfigPerformanceYear",
            GetReportType: "GetQualityReportingType",
            GetMonitoringType: "GetQualityMonitoringType",
            GetQualityMeasures: "GetQualityConfigMasterData",
            SaveQualityMeasures: "SaveQualityConfigurations",
            UpdateQualityMeasures: "UpdateQualityConfigurations",
            GetSaveQualityMeasuresData: "GetQualityConfigData",
            GetQualityMeasureDecline: "GetQualityMeasureDecline",
            CopyQualityMeasures: "CopyQualityMeasuresConfig"
        });