﻿'use strict'
angular.module('griuserconfiguration.constants', [
])
    .constant("griuserconfigurationConstants", {
        UpdateRegistryDetails: "UpdateRegistryDetails/",
        GetRegistryList: "GetRegistryList",
        IRISLegacy: "814",
        IRISGRITechnology: "815"
    });

