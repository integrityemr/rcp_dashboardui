'use strict'
angular.module('amc.ACIMeasureconstants', [
])
        .constant("amcMeasureConstants", {
            getAMCMeasureReportForIndividualClinicianUrl: "GetACIMeasureCalculationForIndividualClinician/",
            getAMCMeasureReportForGroupUrl: "GetACIMeasureCalculationForGrouprReporting/",
            getAMCMeasureReportForGroupIndividualClinicianUrl: "GetACIMeasureCalculationForGroupIndividualClinician/"

        }).
        constant("categoryDescriptionEnum", {
            categoryDescription: "For the advancing care information performance category, the performance category score is the sum of a base score, performance score, and bonus score.A MIPS eligible clinician earns a base score by reporting the numerator (of at least one) and denominator or yes/no statement or null value as applicable, for each required measure."
        });


'use strict'
angular.module('clinicianLevelDashBoard.constants', [
])
        .constant("clinicianLevelConstants", {
            getGroupForClinicianUrl: "GetSpecificPerformanceDashBoardDetailsForPractice/",
            getCPIADetailsForIndividualUrl: "GetCPIADetailsForIndividual/",
            getCPIADetailsForGroupIndividualUrl: "GetCPIADetailsForGroupIndividual/",
            getGroupsForClinicianUrl: "GetClinicianLevelDashBoardDetails/",
            getClinicianMeasuresForIndividualDashBoardUrl: "GetPerformanceDashBoardDetailsForIndividual/"
        });
'use strict'
angular.module('configuration.constants', [
])
    .constant("configurationConstants", {
        getPracticeDetailsUrl: "GetPracticeDetails/",
        getActivitiesByType: "GetCPIAActivitiesByType/",
        getActivitiesByNPIandTIN: "GetCPIAActivitiesByNPIAndTIN/",
        getGroupList: "GetGroupList/",
        getCliniciansList: "GetClinicians/",
        getCPIATypes: "GetCPIATypes/",
        getGroupDetailsForPrint: "GetGroupDetailsForPrint/",
        getReportingCount: "GetReportingCount/",
        getCliniciansForPrint: "GetCliniciansForPrint/",
        saveCPIAActivities: "SaveCPIAActivities/",
        updateCPIAActivities: "UpdateCPIAActivities/",
        getUserDetails: "GetUserDetails/",
        getUsername: "GetUsername/",
        getPracticeLogoPath: "GetPracticeLogoPath/",
        getPracticeDetails: "GetPracticeDetails/",
        copyCPIAActivities: "CopyCPIAActivities/",
        // Practice Information Service
        getAttributes: "AttributesList/",
        getClinicians: "ClinicianList/",
        getGroupBillingList: "GroupBillingList/",
        getIndividualBillingList: "IndividualBillingList/",
        saveGroupReporting: "CreateGroupReporting/",
        saveIndividualReporting: "CreateIndividualReporting/",
        createAuditLog: "CreateAuditLogPracticeInformation/",
        auditCPIAUrl: "AuditLog/",
        getUserRole: "GetUserRole/",

        //ACI Service 
        getMeasuresList: "MeasuresList/",
        getSRAHistoryList: "SRAHistoryList/",
        getACIConfigurationCountDetails: "ACIConfigurationCountDetails/",
        getSRADate: "SRADate/",
        ACIMeasureSetList: "ACIMeasureSetList/",
        ACIMeasureSetListByTINAndNPI: "ACIMeasureSetListByNPIandTIN/",

        createACIMeasureAndSRADate: "CreateACIMeasureAndSRADate/",
        updateACIMeasures: "EditACIMeasures/",
        getACICliniciansList: "getClinicians/",
        enabledPublicHealthPoints: "EnabledPublicHealthPoints/",
        enableQualityRegistryPoints: "EnableQualityRegistryPoints/",
        eligibleCPIAPoints: "EligibleCPIAPoints/",
        EligibleCPIAPointsforGroupsandIndividuals: "EligibleCPIAPointsforGroupsandIndividuals/",
        CreateActionLogFromAuditReportProject: "CreateActionLog/",
        getMeasureSetIdByCPIATypeUrl: "GetStageIdForACIConfig/",
        createAuditLogACIConfiguration: "CreateAuditLogACIConfiguration/",
        getBuildDetails: "GetBuildVersionDetails/", //for getting build details in help
        getCEHRT: "GetCEHRT/",
        getentitiesList: "GetQppReportingEntities/",
        GetCategoriesList: "GetQppReportingCategories/",
        getqppExportData: "GetQppExportData/",
        SaveQppAuditLogData: "QPPSaveAuditLogData/",
        GetQppAuditLogData: "GetQPPExportAuditLogData/",
        qppExport: "ExportQPPJson/",
        getConfigReportingStatusForQPPExport: "IsQPPReportingConfigured/",
        qppexportStatusSave: "SaveQPPExportStatus/",
        deleteSRAHistory: "DeleteSRAHistory/"
        //add Cofigstatus method here   "getReportingStatus"
        //new added
    })
    .constant("configEnums", {
        cpiaType: {
            advanced: 0,
            individual: 1,
            group: 2,
            simple: 3
        }
    }).constant("yearEnums", {
        yearType: {
            2018: 2018,
            2019: 2019
        }
    }).constant("regexPracticInfo", {
        regxTIN: /^\d{9}$/,
        regxAlphaNumeric: /^([a-zA-Z0-9 _-]+)$/
    }).constant("actionTypeEnum", {
        ActionType: {
            InsertActionType: "Insert",
            CreateScope: "create",
            DeleteActionType: "Delete",
            PrintActionType: "Print",
            SelectActionType: "Select",
            UpdateActionType: "Update"
        }
    }).constant("nextYearEnum", {
        nextYear: 2018
    }).constant("QPPperformanceYearEnums", {
        QppperformanceYearList: [
        { yearId: 1, performanceYear: 2018 },
         { yearId: 0, performanceYear: 2017 }
        ]
    });
'use strict'
angular.module('dashBoard.constants', [
])
        .constant("dashBoardConstants", {
            getPracticeLeveleDataUrl: "/"
        }).constant("dashBoardEnums", {
            reportingPeriod: [
                { reportingPeriodId: 0, reportingPeriodName: 'Select' },
                { reportingPeriodId: 1, reportingPeriodName: 'Full Year' },
                { reportingPeriodId: 2, reportingPeriodName: '90 Days' },
                { reportingPeriodId: 3, reportingPeriodName: 'Date Range' }
            ]
        })
        .constant("muStageEnums", {
            measureStages: [
             { measureCode: 2, measureName: 'ACI alternate measure set (2017 only, Stage 2)' },
             { measureCode: 3, measureName: 'ACI measure set (Stage 3)' },
             { measureCode: 4, measureName: 'Meaningful Use Modified Stage2(2017)' },
             { measureCode: 5, measureName: 'Meaningful Use Stage3' },
            ]
        })
        .constant("performanceYearEnums", {
            performanceYearList: [
            { yearId: 1, performanceYear: 2017 },
            { yearId: 2, performanceYear: 2018 },
            { yearId: 3, performanceYear: 2019 },
            ]
        });

'use strict'
angular.module('griuserconfiguration.constants', [
])
    .constant("griuserconfigurationConstants", {
        UpdateRegistryDetails: "UpdateRegistryDetails/",
        GetRegistryList: "GetRegistryList",
        IRISLegacy: "814",
        IRISGRITechnology: "815"
    });


'use strict'
angular.module('groupLevelDashBoard.constants', [
])
        .constant("groupLevelConstants", {
            getClinicniansOfGroupUrl: "GetSpecificPerformanceDashBoardDetailsForPractice/",
            getCPIADetailsForGroupUrl: "GetCPIADetailsForGroup/",
            getCliniciansForGroupUrl: "GetGroupLevelDashBoardDetails/"

        });
'use strict'
angular.module('amc.constants', [
])
        .constant("muMeasureConstants", {
            getMUMeasureReportUrl: "GetMUMeasureCalculation/",
            getMUStageUrl: "GetMUStages",
            getGroupsCliniciansUrl: "GetGroupAndPhysicianDetails",
            getCliniciansUrl: "GetClinicians",
            postMUMeasureExcludeUrl: "PostMUMeasureExclude/",
            saveAuditLogAMC: "AuditLogAMC/",
            PostMUMeasureExclude: "PostMUMeasureExclude/"
        }).constant("enums", {
            stageType: {
                MeaningFulUse: 'MU',
                ACI: 'ACI'
            },
            stageCode: {
                aciStage2: 2,
                aciStage3: 3,
                muStage2: 4,
                muStage3: 5
            }
        });
'use strict'
angular.module('practiceLevelDashBoard.constants', [
])
        .constant("practiceLevelConstants", {
            getLoggedInUserDetailsUrl: "GetUserDetails",
            getPracticeDetailsUrl: "GetPracticeDetails/",
            getPracticeLevelDashBoardDataUrl: "GetPerformanceDashBoardDetailsForPractice/",
            getIndividualAciAndCpiaScoreUrl: "GetACIandCPIAScoreForIndividual/",
            getGroupAciAndCpiaScoreUrl: "GetACIandCPIAScoreForGroup/",
            getActivityStartDatesOfGroupUrl: "GetGroupCPIAStartDates/",
            getActivityStartDatesOfIndividualUrl: "GetIndividualCPIAStartDates/",
            getCPIARegisgtsryStartDatesUrl: "GetGRIRegistryStartDates/",
            updateACIReportingPeriodUrl: "UpdateACIConfigReportingPeriod/",
            getGroupIndividualACICPIAScoresUrl: "GetACIandCPIAScoreForGroupIndividual/",
            getActivityStartDatesOfIndependentUrl: "GetIndependentCPIAStartDates/",

            //Audit related constants
            ViewActionType: "View",
            PrintActionType: "Print",
            ChangeActionType: "Change",
            ExportActionType: "Export",
            CreateActionLog: "CreateActionLog/",
            MIPSPerfDash: "MIPS Performance Dashboard",
            MIPSPerfDashPracLevel: "MIPS Performance Dashboard - Practice Level",
            MIPSPerfDashGroupLevel: "MIPS Performance Dashboard - Group Level",
            MIPSPerfDashIndividualLevel: "MIPS Performance Dashboard - Individual Level",
            MIPSPerfDashImpActivities: "MIPS Performance Dashboard - Improvement Activities",
            MIPSACIMeasureReport: "MIPS ACI-Measure Report",
            ANRPatientList: "Patient List"
        });
'use strict'
angular.module('anr.ACIMeasureconstants', [
])
        .constant("anrMeasureConstants", {
            getANRMeasurePatientsDataUrl: "GetANRMeasurePatientsData/",
            getVendorIsMRNValue: "GetVendorIsMRN"
        }).
        constant("categoryDescriptionEnum", {
            categoryDescription: "For the advancing care information performance category, the performance category score is the sum of a base score, performance score, and bonus score.A MIPS eligible clinician earns a base score by reporting the numerator (of at least one) and denominator or yes/no statement or null value as applicable, for each required measure."
        });


'use strict'
angular.module('ngRealTimeExportconstants', [])
.constant("RealTimeDataExportConstants", {
    PhysicianList: "PhysicianList/",
    PatientSummaryList: "PatientSummaryList/",
    Export: "Export/",
    Download: "Download/",
});
'use strict'
angular.module('ngScheduledExportconstants', [])
.constant("ScheduledExportconstants", {
    GetConfiguredScheduler: "GetConfiguredScheduler/",
    SaveSchedulerConfig: "SaveSchedulerConfig/",
    UpdateSchedulerConfig: "UpdateSchedulerConfig/",
    GetFileAndTimeTyp: "GetFileAndTimeTyp/",
    DeleteSchedulerConfig: "DeleteSchedulerConfig/"
});