﻿'use strict'
angular.module('clinicianLevelDashBoard.constants', [
])
        .constant("clinicianLevelConstants", {
            getGroupForClinicianUrl: "GetSpecificPerformanceDashBoardDetailsForPractice/",
            getCPIADetailsForIndividualUrl: "GetCPIADetailsForIndividual/",
            getCPIADetailsForGroupIndividualUrl: "GetCPIADetailsForGroupIndividual/",
            getGroupsForClinicianUrl: "GetClinicianLevelDashBoardDetails/",
            getClinicianMeasuresForIndividualDashBoardUrl: "GetPerformanceDashBoardDetailsForIndividual/"
        });