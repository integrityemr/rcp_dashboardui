﻿'use strict'
angular.module('aci.mipsPerformanceDashboard').factory('httpFactory', httpFactory);

httpFactory.$inject = ['$http', '$q','util','constants'];

function httpFactory($http, $q,$util,constants) {
    var factory = {
        get: get,
        post: post,
        put: put,
        getUser:getUser,
        handleError: handleError
    }
    var AuditClientTime = constants.AuditClientTimevalue;
    var deferred = $q.defer();
    return factory;

    function get(url, params) {
        var req = {
            method: 'GET',
            headers: { AuditClientTime: $util.browserTime() },
            url: url
        }
        return $http(req).then(function (response) {
            return response;
        }, factory.handleError);
    }

    function getUser(url) {
        var req = {
            method: 'GET',
            headers: { AuditClientTime: $util.browserTime() },
            url: url
        }
        return $http(req).then(function (response) {
            return response;
        }, factory.handleError);
    }

    function post(url, params) {
        var req = {
            method: 'POST',
            data: params,
            headers: { AuditClientTime: $util.browserTime() },
            url: url
        }
        return $http(req).then(function (response) {
            return response;
        }, factory.handleError);
    }

    function put(url, params) {
        var req = {
            method: 'PUT',
            data: params,
            headers: { AuditClientTime: $util.browserTime() },
            url: url
        }
        return $http(req).then(function (response) {
           return response;
        }, factory.handleError);
    }

    function handleError(response) {
        if (response.status == 401) {            
            if (window.location.href.includes("MIPSPerformanceUI/#")) {
                sessionStorage.clear();
                location.href = '#/DashBoard/logout';
                toastr.error("Unauthorized Token");                
            }
        }
        if (!angular.isObject(response) || !response) { // if we want to show error 
            return ($q.reject('An unknown error occurred.'));
        }
        deferred.resolve(false);
        return ($q.reject(response));// Otherwise, use expected error message.
    };

};